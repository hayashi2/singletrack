# -*- coding: utf-8 -*-

import matplotlib
matplotlib.use("Agg")
from matplotlib.pyplot import *
import numpy as np
v0 = 220.0 # km
n0 = 1.0   # n/cc

iniV = -320.
iniN = 10.
uvx_sw_lim =-336. #-(700.**2 * 6./10.)**(1./2)  #-700.
un_sw_lim = 18   #10.0                           # 6.
duvx_sw  =(uvx_sw_lim - iniV)/3./v0
dun_sw  = (un_sw_lim - iniN)/3./n0

V = np.zeros(100)
N = np.zeros(100)
V[0] = iniV
N[0] = iniN

for i in range(1,100):
    if(i<30 or V[i-1] <= uvx_sw_lim):
        V[i] = V[i-1]
    else:
        V[i] = V[i-1] + duvx_sw*v0
for i in range(1,100):
    if(i<30 or N[i-1] >= un_sw_lim):
        N[i] = N[i-1]
    else:
        N[i] = N[i-1] + dun_sw*n0
    
Pdyn = (2e-6)*N*(V**2)


fig = figure()
ax0 = fig.add_subplot(311)
ax1 = fig.add_subplot(312)
ax2 = fig.add_subplot(313)

ax0.plot(V)
ax1.plot(N)
ax2.plot(Pdyn)
ax0.set_ylabel("Vx [km/s]")
ax1.set_ylabel("N [/cm^3]")
ax2.set_ylabel("Pdyn [nPa]")
ax0.set_ylim(-800,-300)
ax1.set_ylim(3,13)
ax2.set_ylim(1,7)
ax0.grid()
ax1.grid()
ax2.grid()
ax0.set_title("case1")
savefig("SW_1.png")

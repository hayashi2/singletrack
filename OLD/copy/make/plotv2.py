# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 13:49:29 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 100
ppath = "/work/m_hayashi/RB050/"
fname = "ptl"
iniene = 110
tit ="RB050"
irank = "000"
number = 16

def main():
   x,y,ene=[],[],[]
   t = 0
   n=0
   while(t <= 210):
      enenum = readfile( ppath + fname + irank +".dat",t)[0]
      xnum = readfile( ppath + fname + irank +".dat",t)[1]
      ynum = readfile( ppath+ fname + irank +".dat",t)[2]
     # scatter(xnum[0,50],ynum[0,50],c="k",cmap=cm.jet,s=50,alpha=0.9,lw=0,vmin=-1.8,vmax=1.8)
      ene=np.append(ene,enenum[0,number])
      x=np.append(x,xnum[0,number])
      y=np.append(y,ynum[0,number])
      t+=1
      
   x1 = np.insert(x,0,0)
   y1 = np.insert(y,0,0)
   r = np.sqrt((x[1:200]/6370)**2 +(y[1:200]/6370)**2)
   vd = (np.sqrt((x[1:200]-x1[1:200])**2+(y[1:200]-y1[1:200])**2))/6
   time = np.arange(6,2400,6)
   figure(1)
   title(tit+"  vd")
   ylabel("[km/s]")
   xlabel("time[sec]")
   plot(time[1:190],vd[1:190])
   figure(2)
   title(tit+" delta E")
   ylabel("delta E[MeV]")
  # ylim(-0.5,3.0)
   xlabel("time[sec]")
   plot(time[1:190],ene[1:190]-iniene)
   show()

def readfile(filename,t):
   ene=[]      
   head = ("head","<i")
   tail = ("tail","<i")
   rtime = ("rtime","<f8")
   nnp = ("np","<i4")
   maxptl =("maxptl","<i4")
   xxp = ("xp","<"+str(ptl)+"f8")
   yyp = ("yp","<"+str(ptl)+"f8")
   zzp = ("zp","<"+str(ptl)+"f8")
   llp = ("lp","<"+str(ptl)+"f8")
   xxeq = ("xeq","<"+str(ptl)+"f8")
   yyeq = ("yeq","<"+str(ptl)+"f8")
   zzeq = ("zeq","<"+str(ptl)+"f8")
   aaeq = ("aeq","<"+str(ptl)+"f8")
   bbeq = ("beq","<"+str(ptl)+"f8")
   p_ppara = ("ppara","<"+str(ptl)+"f8")
   p_pperp = ("pperp","<"+str(ptl)+"f8")
   mmu = ("mu","<"+str(ptl)+"f8")
   bbp = ("bp","<"+str(ptl)+"f8")
   pprebp = ("prebp","<"+str(ptl)+"f8")
   ddb = ("db","<"+str(ptl)+"f8")
   ppredb = ("predb","<"+str(ptl)+"f8")
   nnump = ("nump","<"+str(ptl)+"i4")
   iierr = ("ierr","<"+str(ptl)+"i4")

   dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                  aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                  iierr,tail])
               
   fd = open(filename,"r")
   chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
   xp = chunk[t]["xp"].reshape((1,ptl),order="F")
   yp = chunk[t]["yp"].reshape((1,ptl),order="F")
   zp = chunk[t]["zp"].reshape((1,ptl),order="F")
   lp = chunk[t]["lp"].reshape((1,ptl),order="F")
   xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
   yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
   zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
   aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
   beq = chunk[t]["beq"].reshape((1,ptl),order="F")
   ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
   pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
   mu = chunk[t]["mu"].reshape((1,ptl),order="F")
   bp = chunk[t]["bp"].reshape((1,ptl),order="F")
   prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
   db = chunk[t]["db"].reshape((1,ptl),order="F")
   predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
   nump = chunk[t]["nump"].reshape((1,ptl),order="F")
   ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   
   fd.close()     
   g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
   ene = (m * c**2 * (g - 1) / e)/1000000
   return ene,xp/1000,yp/1000
    
main()

# -*- coding: utf-8 -*-
import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
import matplotlib.ticker as tic
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

# BOX SIZE
gx = 500
gy = 300
gz = 300
dx = 1./4
dy = 1./4
dz = 1./4
#Earth position
rx = 120
ry = 150
rz = 150

e0 = 2.2
b0 = 10.0

xgsm = 0
ygsm = 7.
rgsm = np.sqrt(xgsm**2 + ygsm**2)
phi = np.arctan2(ygsm,xgsm)
r = np.array([rgsm*np.cos(phi),rgsm*np.sin(phi),0])

px = int(round(-xgsm/dx + rx))
py = int(round(-ygsm/dy + ry))
tmax = 100 #100*12
deltt = 11
RUN = "run5"
path = "/work/m_hayashi/GM/"+RUN+"/MHD/"
ename = "MHD_E"
bname = "MHD_B"
#savefile = "/work/m_hayashi/"+RUN+"_Ephi.npz"

def main():
    
    
    nx = -(np.arange(0,gx)-rx)
    ny = -(np.arange(0,gy)-ry)
    NY,NX = np.meshgrid(ny,nx)
    print NX,NY
    cosphi = NX.astype(float)/np.sqrt(NX**2 + NY**2)
    sinphi = NY.astype(float)/np.sqrt(NX**2 + NY**2)
    for time in xrange(0,tmax):
        #### read E file ####
        filename = path + ename + str(time).zfill(3) + ".dat"
        print("Reading...... "+filename)
        head = ("head","<i")
        tail = ("tail","<i")
        b=("b","<"+str(gx*gy*gz*3)+"f4")
         
        dt = np.dtype([head,b,tail])
        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
        #Convert Global-MHD coodinate ->GSM
        bf = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*e0
        ex = -bf[:,:,rz,0]
        ey = -bf[:,:,rz,1]        
        Ephi = ey*cosphi - ex*sinphi
        clf()
        fig = figure(figsize=(12,5))
        ax1 = fig.add_subplot(111)
        xlim = np.arange(12.,-12.,-1./4)
        
        ax1.plot(xlim,Ephi[int(rx-12/dx):int(rx+12/dx),py],"k",lw=1.5,label=r"$E_{\phi}$")
        ax1.set_xlabel("X_GSM [Re]",fontsize=22)
    #ax1.set_ylabel("B  [nT]",fontsize=22)
        ax1.set_ylabel(" E"+" [mV/m]",fontsize=22)
    #ax1.tick_params("y",colors="r")
        ax1.set_title("t = "+str(time*11)+" sec  y = 7 Re",fontsize=22)
        ax1.grid()
        ax1.xaxis.set_minor_locator(tic.AutoMinorLocator())
        ax1.yaxis.set_minor_locator(tic.AutoMinorLocator())
        xticks = np.linspace(-12,12,13)
        ax1.set_xticks(xticks)
        ax1.tick_params(axis="both",labelsize = 18)
    #ax.set_xlim(0,1200)
        #ax1.set_xticklabels(np.linspace(0,tmax*deltt,9).astype(str))

    #h1, l1 = ax1.get_legend_handles_labels()
    #h2, l2 = ax2.get_legend_handles_labels()
    #ax2.legend(h1+h2, l1+l2,loc="upper right",fontsize=22)
        ax1.set_xlim(12,-12)
        ax1.set_ylim(-6,6)
    #ax2.set_ylim(-5,5)
    
        fig.tight_layout()
        fig.subplots_adjust(hspace=0.1)
        fig.savefig("/work/m_hayashi/figure/"+RUN+"/length/length_"+RUN+"_"+str(time).zfill(3)+".png",dpi=200)
        close(fig)
main()

# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 10:30:51 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 100
ppath = "/work/m_hayashi/RB023/"
fname = "ptl"
def main():
    maxe,mine=[],[]
    num=0
    while(num <= 127):
        filename= ppath + fname +str(num).zfill(3) +".dat"
        a,b = readfile(filename)
        maxe = np.append(maxe,a)
        mine = np.append(mine,b)
        num+=1        
        print 'finish %i'%num
    print max(maxe),min(mine)
        
def readfile(filename):
    
    ene=[]
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")

    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
  
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=414)
    
    xp = chunk["xp"].reshape((414,ptl),order="F")
    yp = chunk["yp"].reshape((414,ptl),order="F")
    zp = chunk["zp"].reshape((414,ptl),order="F")
    lp = chunk["lp"].reshape((414,ptl),order="F")
    xeq = chunk["xeq"].reshape((414,ptl),order="F")
    yeq = chunk["yeq"].reshape((414,ptl),order="F")
    zeq = chunk["zeq"].reshape((414,ptl),order="F")
    aeq = chunk["aeq"].reshape((414,ptl),order="F")
    beq = chunk["beq"].reshape((414,ptl),order="F")
    ppara = chunk["ppara"].reshape((414,ptl),order="F")
    pperp = chunk["pperp"].reshape((414,ptl),order="F")
    mu = chunk["mu"].reshape((414,ptl),order="F")
    bp = chunk["bp"].reshape((414,ptl),order="F")
    prebp = chunk["prebp"].reshape((414,ptl),order="F")
    db = chunk["db"].reshape((414,ptl),order="F")
    predb = chunk["predb"].reshape((414,ptl),order="F")
    nump = chunk["nump"].reshape((414,ptl),order="F")
    ierr = chunk["ierr"].reshape((414,ptl),order="F")
    print pperp[413,:]
    close(filename)     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)       
    ene = np.append(ene,  (m * c**2 * (g - 1) / e)/1000000)
    test = np.sort(ene)
    #print test[-1::-1]
    
    print np.max(ene)
    return np.max(ene),np.min(ene)
    
main()

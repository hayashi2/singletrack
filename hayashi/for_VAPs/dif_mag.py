# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("Agg")
from pylab import *
import numpy as np
from spacepy import pycdf
import matplotlib.ticker as tic
import matplotlib.colors as col
import matplotlib.cm as cm
import sys
import subprocess
import os
from datetime import datetime,date,timedelta
from matplotlib import gridspec
la_fontsize = 20
ti_fontsize = 18
def main():
    args = sys.argv
    print datetime.today()

    
    ###### Input check ######
    
    if (len(args)!=3):
        print "#########WARNING######### Incorrect input"
        print "Arguments  1st: start date, 2nd: end date"
        sys.exit()
        
    if(len(args[1])!= 12 or len(args[2])!=12):
        print "#########WARNING######### Incorrect input"
        print "start_date or end_date is incorrect"
        sys.exit()

    
    ###### Data acquisition ######
    start_date = args[1]
    end_date = args[2]

    s_date = datetime.strptime(start_date,"%Y%m%d%H%M")
    e_date = datetime.strptime(end_date,"%Y%m%d%H%M")

    
    for single_date in daterange(s_date, e_date):        
        year = str(single_date.year).zfill(4)
        month = str(single_date.month).zfill(2)
        day = str(single_date.day).zfill(2)
        
        mfilename = "rbspb_rel03_ect-mageis-L3_"+year+month+day+"_v7.1.0.cdf"
        #pfilename = "erg_orb_l2_"+year+month+day+"_v01.cdf"
        Mdata = ReadMagEIS(year,mfilename,"B")
        print Mdata
        FEDU = Mdata["FEDU"]
        Epoch = Mdata["Epoch"][:]
        Alpha = Mdata["FEDU_Alpha"][:].astype(int)
        E_label = Mdata["FEDU_ENERGY_LABL"][0,:]
        L = Mdata["L"][:]
        MLT = Mdata["MLT"][:]
        MLAT = Mdata["MLAT"][:]
                
        #position = ReadPosition(year,pfilename)
        
        break #one loop
    FLUX = np.zeros((int(24*60*60/10.789000),np.shape(FEDU)[1],np.shape(FEDU)[2]))
    print Alpha,E_label
    for i in xrange(np.shape(FEDU[:,:,:])[1]):
        FLUX[:,i,:],Ep = Recreate(FEDU[:,i,:],Epoch,s_date) 
    L,MLT,MLAT = Reposition(L,MLT,MLAT,Epoch,s_date)
    
    ###### Plot range ######
    delta_m = int(((e_date - s_date).total_seconds())/60.)
    s_hour = s_date.hour
    s_min = s_date.minute
    
    ep_st = int((s_hour*60.*60.+s_min*60.)/10.789000)
    ep_ed = ep_st + int(delta_m*60./10.789000)

    print Ep[ep_st]
    ###### Figure1 Setting 1 ######
    fig1 = figure(1,figsize=(14,15))
    #gs = gridspec.GridSpec(4,1,height_ratios=[12,3,3,3])
    ax1 = fig1.add_subplot(511)
    ax2 = fig1.add_subplot(512)
    ax3 = fig1.add_subplot(513)
    ax4 = fig1.add_subplot(514)
    ax5 = fig1.add_subplot(515)

    ### Draw and Plot ###
    im1 = ax1.imshow(FLUX[ep_st:ep_ed,0:10,3].T*1e3,origin="lower",interpolation="none",\
                         aspect="auto",norm=col.LogNorm())
    im2 = ax2.imshow(FLUX[ep_st:ep_ed,0:10,4].T*1e3,origin="lower",interpolation="none",\
                         aspect="auto",norm=col.LogNorm())
    im3 = ax3.imshow(FLUX[ep_st:ep_ed,0:10,7].T*1e3,origin="lower",interpolation="none",\
                         aspect="auto",norm=col.LogNorm())
    im4 = ax4.imshow(FLUX[ep_st:ep_ed,0:10,10].T*1e3,origin="lower",interpolation="none",\
                         aspect="auto",norm=col.LogNorm())
    im5 = ax5.imshow(FLUX[ep_st:ep_ed,0:10,13].T*1e3,origin="lower",interpolation="none",\
                         aspect="auto",norm=col.LogNorm())
    
    box1 = ax1.get_position()
    box2 = ax2.get_position()
    box3 = ax3.get_position()
    box4 = ax4.get_position()
    box5 = ax5.get_position()
    pad, width=0.02,0.01
    cax1 = fig1.add_axes([box1.xmax+pad, box1.ymin,width,box1.height])
    cax2 = fig1.add_axes([box2.xmax+pad, box2.ymin,width,box2.height])
    cax3 = fig1.add_axes([box3.xmax+pad, box3.ymin,width,box3.height])
    cax4 = fig1.add_axes([box4.xmax+pad, box4.ymin,width,box4.height])
    cax5 = fig1.add_axes([box5.xmax+pad, box5.ymin,width,box5.height])
    CB1 = colorbar(im1,cax=cax1)
    CB2 = colorbar(im2,cax=cax2)
    CB3 = colorbar(im3,cax=cax3)
    CB4 = colorbar(im4,cax=cax4)
    CB5 = colorbar(im5,cax=cax5)
    for CB in (CB1,CB2,CB3,CB4,CB5):
        CB.set_label(r"$cm^{-2}s^{-1}str^{-1}MeV^{-1}$",fontsize = la_fontsize-5)
        #CB.set_axis_label_font(size = ti_fontsize)

  ###### Figure1 setting 2 ######
    for ax in (ax1,ax2,ax3,ax4,ax5):
        
        axxticks = np.arange(0,np.shape(FLUX[ep_st:ep_ed,0,:])[0])
        ax.set_xticks(axxticks)
        ax.set_yticks(np.arange(Alpha[0:10].size))
        ax.set_yticklabels(Alpha[0:10].astype(int))
        #ax5.tick_params(labelsize = ti_fontsize)
        #ax5.yaxis.set_label_coords(-0.07,0.5)

    for ax in (ax1,ax2,ax3,ax4):
        setp(ax.get_xticklabels(),visible=False)

    ax1.set_title("RBSP-B/MagEIS-B")
    ax1.set_ylabel(str(E_label[3])[:-21]+"\n"+r"$\alpha[deg]$",fontsize = la_fontsize)
    ax2.set_ylabel(str(E_label[4])[:-21]+"\n"+r"$\alpha[deg]$",fontsize = la_fontsize)
    ax3.set_ylabel(str(E_label[7])[:-21]+"\n"+r"$\alpha[deg]$",fontsize = la_fontsize)
    ax4.set_ylabel(str(E_label[10])[:-21]+"\n"+r"$\alpha[deg]$",fontsize = la_fontsize)
    ax5.set_ylabel(str(E_label[13])[:-21]+"\n"+r"$\alpha[deg]$",fontsize = la_fontsize)
    
    left, width = -0.04, .5
    bottom, height = 0, .5
    right = left + width
    top = bottom + height

    ax5.text(left, bottom, "hh:mm\nL\nMLT\nMLAT",horizontalalignment='right',\
                 verticalalignment='top', transform=ax5.transAxes,fontsize=ti_fontsize)
       
    xticks = np.arange(0,delta_m+10,10)
    ticks_L, ticks_MLT, ticks_MLAT = Position_ticks(L[ep_st:ep_ed], MLT[ep_st:ep_ed], MLAT[ep_st:ep_ed], 10)
    x = [((s_date + timedelta(minutes=i)).time()).strftime("%H:%M") \
             +"\n"+ticks_L[count] +"\n"+ticks_MLT[count] +"\n"+ticks_MLAT[count]\
             for count,i in enumerate(xrange(0,delta_m+10,10))]
    xticks = np.linspace(0,np.shape(FLUX[ep_st:ep_ed,0,:])[0]-1,np.size(xticks))
    for ax in (ax1,ax2,ax3,ax4,ax5):
        ax.set_xticks(xticks)
    ax5.set_xticklabels(x,fontsize=ti_fontsize)


    
    fig1.subplots_adjust(hspace=0.1,right=0.9,bottom=0.1)
    fig1.savefig("MagEIS_B_diff"+s_date.strftime("%Y%m%d%H%M")+"-"+e_date.strftime("%Y%m%d%H%M")+".png")
    close(fig1)


def ReadPEPT(year,filename,NUM):
    URL = "https://rbsp-ect.lanl.gov/data_pub/rbspb/rept/level3/pitchangle/"+year+"/"+filename
    filepath = "/work/m_hayashi/VAPs_data/REPT_"+NUM+"/"+filename
    if(os.path.exists(filepath)==True):
         pass
    else:
        print "Download file..."
        cmd = "wget "+ URL+" -O "+filepath
        subprocess.call(cmd,shell=True)
    data = pycdf.CDF(filepath)
    return data

def ReadMagEIS(year,filename,NUM):

    URL = "https://rbsp-ect.lanl.gov/data_pub/rbspb/mageis/level3/pitchangle/"+year+\
"/"+filename
    filepath = "/work/m_hayashi/VAPs_data/MagEIS_"+NUM+"/"+filename
    if(os.path.exists(filepath)==True):
         pass
    else:
        print "Download file..."
        cmd = "wget "+ URL+" -O "+filepath
        subprocess.call(cmd,shell=True)
    data = pycdf.CDF(filepath)
    return data



def ReadPosition(year,filename):
    URL = "https://ergsc.isee.nagoya-u.ac.jp/data/ergsc/satellite/erg/orb/def/"+year+"/"+filename
    filepath = "/work/m_hayashi/ERG_data/def/"+filename
    if(os.path.exists(filepath)==True):
         pass
    else:
        print "Download file..."
        cmd = "wget"+ " --http-user=erg_project --http-passwd=geospace "+ URL+" -O "+filepath
        subprocess.call(cmd,shell=True)
    data = pycdf.CDF(filepath)
    return data



def Recreate(DATA,Epoch,s_date):
    NEW_DATA = np.zeros((int(24*60*60/10.789000),np.shape(DATA)[1]))
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(DATA[:,:])[0]):
        if (np.round((Epoch[0] -basetime).total_seconds()) < 0):
            DATA = DATA[1:,:]
            #last_Ep = Epoch[0]
            Epoch = Epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(DATA[:,:])[0]):
        #gr = int(np.round((Epoch[i] -basetime).total_seconds()))/10
        gr += int(np.round((Epoch[i] - last).total_seconds()/10.789000))
        last = Epoch[i]
        if(gr>=24*60*60/10.789000):
            print Epoch[i]
            break
        
        NEW_DATA[gr,:] = DATA[i,:]
    return NEW_DATA,Epoch[:]



def Reposition(Lm,MLT,MLAT,epoch,s_date):
    NEW_L = np.zeros(24*60*60/10.789000)
    NEW_MLT = np.zeros(24*60*60/10.789000)
    NEW_MLAT = np.zeros(24*60*60/10.789000)
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(epoch[:])[0]-1):
        if (np.round((epoch[0] -basetime).total_seconds()) < 0):
            Lm = Lm[1:]
            MLT = MLT[1:]
            MLAT = MLAT[1:]
            #last_Ep = Epoch[0]
            epoch = epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(epoch[:])[0]-1):
        gr += int(np.round((epoch[i] -last).total_seconds()/10.789000))
        last = epoch[i]
        if(gr>=24*60*60/10.789000):
            print epoch[i]
            break
        NEW_L[gr] = Lm[i]
        NEW_MLT[gr] = MLT[i]
        NEW_MLAT[gr] = MLAT[i]
    return NEW_L,NEW_MLT,NEW_MLAT



def Position_ticks(L,MLT,MLAT,num):
    L_ticks, MLT_ticks,MLAT_ticks = [], [], []
    t = 0
    i = 0
    while(i <= np.size(L) ):
        #print t
        if(i == np.size(L)):
            L_ticks.append(str(round(L[i-1],1)))
            MLT_ticks.append(str(round(MLT[i-1],1)))
            MLAT_ticks.append(str(round(MLAT[i-1],1)))
        else:
            L_ticks.append(str(round(L[i],1)))
            MLT_ticks.append(str(round(MLT[i],1)))
            MLAT_ticks.append(str(round(MLAT[i],1)))
        t += num
        i = int(t*60./10.789000)

    return L_ticks, MLT_ticks,MLAT_ticks


def daterange(s_date,e_date):
    return (s_date + timedelta(days=i) for i in range((e_date-s_date).days+1))

main()

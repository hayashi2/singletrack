# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *

path = "/work/m_hayashi/MHDdata/"
name = "Vsw600_20Re.npz"

k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1
"""
savd...
 np.savez("/work/m_hayashi/MHDdata/Vsw400_6Re(noon).npz)",time=rtime, bx=bx, by=by, bz=bz, ex=ex, ey=ey, ez=ez, vx=vx,vy=vy, vz=vz, N=N, P=P)
"""
def main():
    data=np.load(path+name)
    
    rtime = data["time"]
    bx = -data["bx"]
    by = -data["by"]
    bz = data["bz"]
    ex = -data["ex"]
    ey = -data["ey"]
    ez = data["ez"] 
    vx = -data["vx"]
    vy = -data["vy"]
    vz = data["vz"]
    N  = data["N"]
    P  = data["P"]
    

    T  = (P*1e-9)/(k*(N*1e6))
    B  = np.sqrt(bx**2 + by**2 + bz**2)
    V  = np.sqrt(vx**2 + vy**2 + vz**2 )
    Ma = (V*(np.sqrt(N)))/(20*B)
    Va = 20.3*B/np.sqrt(N)
    Vth = 3.0*k*T/1.67e-23
    Vs  = 0.12*(T+1.28*10e5)**(0.5)
    Vms = np.sqrt(Vs**2+Va**2)
    Mms = V/Vms
    Pdyn = (2e-6)*N*(V**2)
   # Pt = P + (B**2)/2
#    print "maxEy: ",np.max(ey)
#    print "minEy: ",np.min(ey)
#    print "maxVsw: ",np.max(V)
#    print "maxMms: ",np.max(Mms)
#    print "maxPdyn: ",np.max(Pdyn)
     
    #figure(figsize=(10,20))

    fig,axes = subplots(4,1,sharex=True, sharey=False)
    fig.subplots_adjust(hspace=0)
    
    axes[0].set_title("(20, 0, 0)$_{GSM}$",fontsize=20)
    axes[0].plot(rtime,vx,linewidth=1.0,color="b",label="Vx [km/s]")
    #axes[0].set_ylabel("Vx[km/s]")
    axes[0].tick_params(labelbottom="off")
    axes[0].set_xlim(0,1200)
    axes[0].set_ylim(np.min(vx)*1.1,0)
    axes[0].locator_params(axis="y",tight=True,nbins=4)
    axes[0].minorticks_on()
    axes[0].grid()
    axes[0].legend(loc="upper right",fontsize=20)
    
    axes[1].plot(rtime,N,linewidth=1.0,color="k",label="N [/cm$^3$]")
    axes[1].tick_params(labelbottom="off")
    axes[1].set_xlim(0,1200)
    axes[1].set_ylim(0,)
    #yscale('log')
    #axes[1].set_ylabel(" N(/cm^3)")
    axes[1].locator_params(axis="y",tight=True,nbins=10)
    axes[1].minorticks_on()
    axes[1].grid()
    axes[1].legend(loc="upper right",fontsize=20)

    axes[2].plot(rtime,Pdyn,linewidth=1.0,color="b",label="P$_{dyn}$ [nPa]")
    axes[2].set_xlim(0,1200)
    axes[2].tick_params(labelbottom="off")
    #axes[2].set_ylabel("Pdyn(npa)")
    axes[2].locator_params(axis="y",tight=True,nbins=5)
    axes[2].minorticks_on()
    axes[2].grid()
    axes[2].legend(loc="upper right",fontsize=20)
    
    axes[3].plot(rtime,bz,linewidth=1.0,color="k",label="Bz [nT]")
    axes[3].set_xlim(0,1200)
    #axes[3].set_ylabel("Bz(nT)")
    axes[3].set_ylim(np.min(bz)*1.1,np.max(bz)*1.1)
    axes[3].locator_params(axis="y",tight=True,nbins=5)
    axes[3].set_xlabel("time[sec]",fontsize=15)
    axes[3].minorticks_on()
    axes[3].grid()
    axes[3].legend(loc="upper right",fontsize=20)
    show()
    #savefig( "GSM_Vsw800_20Re.jpeg")

main()

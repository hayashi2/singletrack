import numpy as np
from pylab import *
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

path = "/work/saito/GMHDRB/inputdata/"
name = "MHDE_RB"
v1,v2,v3 = [],[],[]
t = [] 
time = 0
while(time <= 2376):

    filename = path + name + str(time).zfill(6) + ".dat"

    head = ("head","<i")
    tail = ("tail","<i")
    vx = ("vx","<2097152f4")
         
    dt = np.dtype([head,vx,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["vx"].reshape((128,128,128),order="F")
    bt=bf.transpose(1,0,2)

    v1 = np.append(v1,bt[64,109])
    v2 = np.append(v2,bt[64,88])
    v3 = np.append(v3,bt[64,40])
    t = np.append(t,time)
    time += 12

subplot(3,1,1)
plot(t,e1)
xlabel("time[sec]")
ylabel("Ephi[mV/m]")
title("(15,0)")

subplot(3,1,2)
plot(t,e2)
xlabel("time[sec]")
ylabel("Ephi[mV/m]")
title("(8,0)")

subplot(3,1,3)
plot(t,e3) 
xlabel("time[sec]")
ylabel("Ephi[mV/m]")
title("(-8,0)")

show()
# savefig("EphiL"+str(time/24)+".jpeg")
 #   print "saved ",time

# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("Agg")
from pylab import *
import numpy as np
from spacepy import pycdf
import matplotlib.ticker as tic
import matplotlib.colors as col
import matplotlib.cm as cm
import sys
import subprocess
import os
from datetime import datetime,date,timedelta
from matplotlib import gridspec
from scipy import signal
from scipy.signal import find_peaks_cwt

def main():
    args = sys.argv
    print datetime.today()

    
    ###### Input check ######
    
    if (len(args)!=3):
        print "#########WARNING######### Incorrect input"
        print "Arguments  1st: start date, 2nd: end date"
        sys.exit()
        
    if(len(args[1])!= 12 or len(args[2])!=12):
        print "#########WARNING######### Incorrect input"
        print "start_date or end_date is incorrect"
        sys.exit()

    
    ###### Data acquisition ######
    start_date = args[1]
    end_date = args[2]

    s_date = datetime.strptime(start_date,"%Y%m%d%H%M")
    e_date = datetime.strptime(end_date,"%Y%m%d%H%M")

    
    for single_date in daterange(s_date, e_date):        
        year = str(single_date.year).zfill(4)
        month = str(single_date.month).zfill(2)
        day = str(single_date.day).zfill(2)
        
        mfilename = "rbspb_rel03_ect-mageis-L3_"+year+month+day+"_v7.1.0.cdf"
        rfilename = "rbspb_rel03_ect-rept-sci-L3_"+year+month+day+"_v5.1.0.cdf"
        #pfilename = "erg_orb_l2_"+year+month+day+"_v01.cdf"
        
        Mdata = ReadMagEIS(year,mfilename,"B")
        Rdata = ReadREPT(year,rfilename,"B")
        #print Mdata
        #position = ReadPosition(year,pfilename)
        FLUX = Mdata["FEDU"]
        Epoch = Mdata["Epoch"][:]
        Alpha = Mdata["FEDU_Alpha"][:]
        E_label = Mdata["FEDU_ENERGY_LABL"][:,:]
        L = Mdata["L"][:]
        MLT = Mdata["MLT"][:]
        MLAT = Mdata["MLAT"][:]
        
        R_FLUX = Rdata["FEDU"]
        R_Epoch = Rdata["Epoch"][:]
        R_Alpha = Rdata["FEDU_Alpha"][:]
        R_Label = Rdata["FEDU_Energy"][:]

        break #one loop
    print FLUX.attrs
    print Alpha,R_Alpha

    FLUX = Recreate(FLUX[:,5,:]*1e3,Epoch,s_date) #FLUX of a =90
    #FLUX = Recreate(FLUX[:,2,:]*1e3,Epoch,s_date) # a =40
    L,MLT,MLAT = Reposition(L,MLT,MLAT,Epoch,s_date)
    R_FLUX = Recreate(R_FLUX[:,8,:],R_Epoch,s_date) #a = 90
    #R_FLUX = Recreate(R_FLUX[:,3,:],R_Epoch,s_date) #a =37

    ###### Plot range ######
    delta_m = int(((e_date - s_date).total_seconds())/60.)
    s_hour = s_date.hour
    s_min = s_date.minute
    
    ep_st = int((s_hour*60.*60.+s_min*60.)/10.789000)
    ep_ed = ep_st + int(delta_m*60./10.789000)

    ###### Figure1 Setting 1 ######
    fig1 = figure(1,figsize=(16,8))
    #gs = gridspec.GridSpec(4,1,height_ratios=[12,3,3,3])
    ax1 = fig1.add_subplot(111)
    
    ax1xticks = np.linspace(0,delta_m,np.shape(FLUX[ep_st:ep_ed,:])[0])
    ### Draw and Plot ###
    yt = []
    El = []
    ini_f = []
    for i in xrange(np.shape(FLUX)[1]-8-7):
        c = cm.jet(i/np.float(np.shape(FLUX)[1]-8-7),1)
        E = E_label[0,i+7]
        El.append(float(E[:-25])/1e3)
        #ax1.plot(ax1xticks,FLUX[ep_st:ep_ed,i+3],label=E[:-21],color=c)
        yhat = savitzky_golay(FLUX[ep_st:ep_ed,i+7],21,3)
        ini_f.append(yhat[0])
        ax1.plot(ax1xticks, yhat, label=E[:-21] , color=c)
        maxId = signal.argrelmax(yhat)
        ax1.plot(ax1xticks[maxId], yhat[maxId], 'rs')
        for j in maxId[0]:
            if(0 < j < 30):
            #if(0<j<40):
                yt.append(yhat[j])
                break

    R_yt = []
    R_ini_f = []
    for i in xrange(np.shape(R_FLUX)[1]-6):
        c = cm.jet(i/np.float(np.shape(R_FLUX)[1]-6),1)
        yhat = savitzky_golay(R_FLUX[ep_st:ep_ed,i],21,3)
        R_ini_f.append(yhat[0])
        maxId = signal.argrelmax(yhat)
        for j in maxId[0]:
            if(0 < j < 30):
                R_yt.append(yhat[j])
                break

  ###### Figure1 setting 2 ######
#    for ax in (ax1,ax2,ax3):
#        setp(ax.get_xticklabels(),visible=False)
            
    """        
    ax1.set_title( "rbspb_rel03_ect-mageis-L3 FEDU - "+\
                       "Unidirectional Differential Electron Flux"+\
                       "\n FEDU_Alpha = "+str(np.round(Alpha[5],decimals=3))+" deg",fontsize=15)
    ax1.set_ylabel("/$cm^2$ s str MeV",fontsize=15)
    ax1.grid()
    ax1.legend(bbox_to_anchor=(1.03, 1), loc=2, borderaxespad=0.,fontsize=15)
    ax1.set_yscale("log")
    ax1.xaxis.set_minor_locator(tic.AutoMinorLocator())
    #ax1.set_yticklabels(np.logspace(0,5,6),fontsize=13)
    ax1.set_xlim(0,delta_m)
    left, width = -0.04, .5
    bottom, height = 0, .5
    right = left + width
    top = bottom + height

    ax1.text(left, bottom, "hh:mm\nL\nMLT\nMLAT",horizontalalignment='right',\
                 verticalalignment='top', transform=ax1.transAxes,fontsize=15)

    xticks = np.arange(0,delta_m+10,10)
    ticks_L, ticks_MLT, ticks_MLAT = Position_ticks(L[ep_st:ep_ed], MLT[ep_st:ep_ed], MLAT[ep_st:ep_ed], 10)
    x = [((s_date + timedelta(minutes=i)).time()).strftime("%H:%M") \
             +"\n"+ticks_L[count] +"\n"+ticks_MLT[count] +"\n"+ticks_MLAT[count]\
             for count,i in enumerate(xrange(0,delta_m+10,10))]

    ax1.set_xticks(xticks)
    ax1.set_xticklabels(x,fontsize=15)
    ax1.yaxis.set_label_coords(-0.07,0.5)

    fig1.subplots_adjust(hspace=0.1,right=0.8,bottom=0.2)
    fig1.savefig("MagEIS_B_sm"+s_date.strftime("%Y%m%d%H%M")+"-"+e_date.strftime("%Y%m%d%H%M")+".png")
    close(fig1)
    ###########################################################
    """

    fig2 = figure(2)
    ax1 = fig2.add_subplot(111)
    
    
    ax1.plot(El,ini_f,"s--",color="b",label="MagEIS-B pre-shock")
    ax1.plot(El,yt,"s--",color ="r",label="MagEIS-B shock injected")

    ax1.plot(R_Label[0:-6-1],R_ini_f,"o-",color="b",label="REPT-B pre-shock")
    ax1.plot(R_Label[0:-6],R_yt,"o-",color ="r",label="shock injected")
    
    

    pre = ini_f + R_ini_f
    after = yt + R_yt[1:-1]
    Elabel = El +R_Label[1:-6-1].tolist()
    ax1.set_title("Unidirectional Differential Electron Flux"+\
                      "\n FEDU_Alpha = "+str(np.round(Alpha[5],decimals=3))+" deg",fontsize=13)
    ax1.set_ylabel("$cm^{-2} s^{-1} str^{-1} MeV^{-1}$",fontsize=20)
    ax1.set_yscale("log")
    ax1.set_xlabel("Energy [MeV]",fontsize=20)
    ax1.grid()
    ax1.legend(loc ="upper right")
    ax1.set_xlim(0,6)
    ax1.set_ylim(1e1,1e7)
    ax1.xaxis.set_minor_locator(tic.AutoMinorLocator())
    fig2.savefig("e_spec_all.png")
    close(fig2)
    ###########################################################
    
    ##############################################################
    fig3 = figure()
    ax3 = fig3.add_subplot(111)
    ax3.plot(Elabel, \
                 np.log10(np.asarray(after)/np.asarray(pre)),"--",color="k")
    ax3.set_title("Unidirectional Differential Electron Flux"+\
                       "\n FEDU_Alpha = "+str(np.round(Alpha[5],decimals=3))+" deg",fontsize=13)
    print Elabel,np.log10(np.asarray(after)/np.asarray(pre))
    ax3.set_xlabel("Enery [MeV]",fontsize=15)
    ax3.set_ylabel("log$_{10}$(f/f$_0$)",fontsize=15)
    ax3.xaxis.set_minor_locator(tic.AutoMinorLocator())
    ax3.grid()
    ax3.set_xlim(0.0,4.5)
    ax3.set_ylim(0.,2.0)
    ax3.tick_params(axis="both",labelsize = 13)
    fig3.savefig("VAP_deltaf.png")




def ReadREPT(year,filename,NUM):
    filepath = "/work/m_hayashi/VAPs_data/REPT_"+NUM+"/"+filename
    data = pycdf.CDF(filepath)
    return data

def ReadMagEIS(year,filename,NUM):
    filepath = "/work/m_hayashi/VAPs_data/MagEIS_"+NUM+"/"+filename
    data = pycdf.CDF(filepath)
    return data

def Recreate(DATA,Epoch,s_date):
    NEW_DATA = np.zeros((int(24*60*60/10.789000),np.shape(DATA)[1]))
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(DATA[:,:])[0]):
        if (np.round((Epoch[0] -basetime).total_seconds()) < 0):
            DATA = DATA[1:,:]
            #last_Ep = Epoch[0]
            Epoch = Epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(DATA[:,:])[0]):
        #gr = int(np.round((Epoch[i] -basetime).total_seconds()))/10
        gr += int(np.round((Epoch[i] - last).total_seconds()/10.789000))
        last = Epoch[i]
        if(gr>=24*60*60/10.789000):
            print Epoch[i]
            break
        
        NEW_DATA[gr,:] = DATA[i,:]
    return NEW_DATA




def Reposition(Lm,MLT,MLAT,epoch,s_date):
    NEW_L = np.zeros(24*60*60/10.789000)
    NEW_MLT = np.zeros(24*60*60/10.789000)
    NEW_MLAT = np.zeros(24*60*60/10.789000)
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(epoch[:])[0]-1):
        if (np.round((epoch[0] -basetime).total_seconds()) < 0):
            Lm = Lm[1:]
            MLT = MLT[1:]
            MLAT = MLAT[1:]
            #last_Ep = Epoch[0]
            epoch = epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(epoch[:])[0]-1):
        gr += int(np.round((epoch[i] -last).total_seconds()/10.789000))
        last = epoch[i]
        if(gr>=24*60*60/10.789000):
            print epoch[i]
            break
        NEW_L[gr] = Lm[i]
        NEW_MLT[gr] = MLT[i]
        NEW_MLAT[gr] = MLAT[i]
    return NEW_L,NEW_MLT,NEW_MLAT


def Position_ticks(L,MLT,MLAT,num):
    L_ticks, MLT_ticks,MLAT_ticks = [], [], []
    t = 0
    i = 0
    while(i <= np.size(L) ):
        #print t
        if(i == np.size(L)):
            L_ticks.append(str(round(L[i-1],1)))
            MLT_ticks.append(str(round(MLT[i-1],1)))
            MLAT_ticks.append(str(round(MLAT[i-1],1)))
        else:
            L_ticks.append(str(round(L[i],1)))
            MLT_ticks.append(str(round(MLT[i],1)))
            MLAT_ticks.append(str(round(MLAT[i],1)))
        t += num
        i = int(t*60./10.789000)

    return L_ticks, MLT_ticks,MLAT_ticks


def savitzky_golay(y, window_size, order, deriv=0, rate=1):

    import numpy as np
    from math import factorial

    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
       # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')


def daterange(s_date,e_date):
    return (s_date + timedelta(days=i) for i in range((e_date-s_date).days+1))

main()

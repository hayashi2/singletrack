# -*- coding: utf-8 -*-
import numpy as np

m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 

enemin = 5
enest = enemin-1
files ="/work/m_hayashi/RBF2/ptl"
ptl = 1000
savepath ="/work/m_hayashi/weightdata/xy_5M"
dA = 0.2*0.2
da = float(1.0/360.0)*np.pi
de = 0.2e6
dt = 6

def main():
    count = np.zeros((150,150,180,80)) #x,y,a,ene,
    #flux1 = np.zeros((150,150,180,80))
    #flux = np.zeros((150,150))         # integral of a and ene
    weight = np.empty((128,ptl))

    # file loop 
    for filenum in range(0,128):
        filename = files + str(filenum).zfill(3) +".dat"

    #read file
        xp1, yp1, a1, mu1, ene1, frag1 =  readfile(filename,0) 
        x1,y1 = xp1/6370000,yp1/6370000
        L1 = np.sqrt((x1)**2 + (y1)**2)

    #grid number
        xg, yg, ag, eg = griding(x1,y1,a1,ene1)
   
    #ptl loop
        for m in range(0,ptl):
    #count per bins
                count[xg[0,m],yg[0,m],ag[0,m],eg[0,m]] +=1 

    #weighting
    for filenum2 in range(0,128):
        filename2 = files + str(filenum2).zfill(3) +".dat"
        
        xp2, yp2, a2, mu2, ene2, frag2 =  readfile(filename2,0) 
        x2,y2 = xp2/6370000,yp2/6370000
        L2 = np.sqrt((x2)**2 + (y2)**2)
        #grid number
        xg2, yg2, ag2, eg2 = griding(x2,y2,a2,ene2)

        for m in range(0,ptl):
            if(count[xg2[0,m],yg2[0,m],ag2[0,m],eg2[0,m]]):
                weight[filenum2,m] = L2[0,m]*np.abs(np.sin(a2[0,m]))*ene2[0,m]/count[xg2[0,m],yg2[0,m],ag2[0,m],eg2[0,m]]

    np.savez(savepath, weight = weight) 
    
def readfile(filename,t):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
    xp = chunk[t]["xp"].reshape((1,ptl),order="F")
    yp = chunk[t]["yp"].reshape((1,ptl),order="F")
    zp = chunk[t]["zp"].reshape((1,ptl),order="F")
    lp = chunk[t]["lp"].reshape((1,ptl),order="F")
    xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
    yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
    zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
    aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
    beq = chunk[t]["beq"].reshape((1,ptl),order="F")
    ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
    pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
    mu = chunk[t]["mu"].reshape((1,ptl),order="F")
    bp = chunk[t]["bp"].reshape((1,ptl),order="F")
    prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
    db = chunk[t]["db"].reshape((1,ptl),order="F")
    predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
    nump = chunk[t]["nump"].reshape((1,ptl),order="F")
    ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   
    fd.close()     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)

    return xeq ,yeq,aeq,mu,ene,ierr

def griding(x,y,a,ene):
    xg = (5*x).astype(int) + 75
    yg = (5*y).astype(int) + 75
    ag = (((180/np.pi)*a)).astype(int)
    eg = (5*(ene/1e6 - enest)).astype(int)
    
    return xg,yg,ag,eg

main()

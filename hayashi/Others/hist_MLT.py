# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
import matplotlib.patches as mpatches
import matplotlib.colors as col

ptl = 10000
f_num = 144
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
re = 6370e3
e0 = 2.2
B0 = 32000. #nT

RUN = "run5"
pdt = 5
savedir = "/0"
#sttime = 220/pdt -1 #run3 0
#ftime = 220/pdt -1 #run3 0
sttime = 260/pdt -1 #run5 0
ftime = 260/pdt -1 #run5 0
#sttime = 265/pdt -1 #run3 1
#ftime = 265/pdt -1 #run3 1
#sttime = 310/pdt -1 #run5 1
#ftime = 310/pdt -1 #run5 1

def main():        
    
    for t in np.arange(ftime,120):
        print t
        clf()
        fig = figure(figsize=(7,5))
        ax0 = fig.add_subplot(111)    
        
        
        ene, mlt = [],[] 
        for f in xrange(f_num):
            filename = "/work/m_hayashi/RBdata/RB_GM"+RUN+"_000/ptl"+str(f).zfill(3)+".dat"
            print "reading......" + filename
            p_mlt,aeq,beq,p_ene,ierr = readptl(filename)
            aeq = np.degrees(aeq)
            Lg = (B0/beq)**(1./3)
        
            for i in xrange(ptl):
                if(85 <= aeq[0,i] < 95 and 6.5<= Lg[0,i] < 7 and ierr[t,i] == 0):
                    ene.append(p_ene[t,i])
                    mlt.append(p_mlt[t,i])

            #ax0.set_xlim(11,-11)
            #ax0.set_ylim(11,-11)
        im0 = ax0.hist2d(mlt,ene,bins = [np.linspace(0.,24.,49),np.linspace(0.,12.,49)],norm=col.LogNorm(vmin=1e0,vmax=1e2))
        CB0 = colorbar(im0[3],ax=ax0)
        
        #CB0.set_label(r"$E_{\phi}$ [mV/m]")
        
        ax0.set_title("time = "+str((t+1)*pdt)+" sec "+r"$\alpha_{eq}$ = 90 deg")
        ax0.grid()
        ax0.set_xlabel("MLT")
        ax0.set_ylabel("W [MeV]")
        
        fig.tight_layout()
        fig.savefig("/work/m_hayashi/figure/hist_MLT/"+RUN + savedir +"/hist_MLT"+str(t).zfill(3)\
                        +".png",dpi = 200)
        close(fig)
    """
    fig2 = figure(2)
    ax1 = fig2.add_subplot(111)
    time = np.linspace(300,450,np.shape(x_2[0:25,index_2])[0])
    ax1.plot(time,ene_2[0:25,index_2]-ene_2[0,index_2],"r",label="2 MeV")
    ax1.plot(time,ene_8[0:25,index_8]-ene_8[0,index_8],"b",label="500 keV")
    ax1.set_xlabel("time [sec]")
    ax1.legend(loc="upper left",fontsize=18)
    ax1.set_ylabel(r"$\delta$"+"W [MeV]")
    ax1.set_xlim(300,450)
    ax1.grid()
    savefig("12MLT_500k_2M.png")
    """
    
def readptl(filename):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt)
    #time = chunk["rtime"]
    #xp = chunk["xp"]
    #yp = chunk["yp"]
    #zp = chunk["zp"]
    #lp = chunk["lp"]
    xeq = chunk["xeq"]
    yeq = chunk["yeq"]
    #zeq = chunk["zeq"]
    aeq = chunk["aeq"]
    beq = chunk["beq"]
    ppara = chunk["ppara"]
    pperp = chunk["pperp"]
    #mu = chunk["mu"]
    #bp = chunk["bp"]
    #prebp = chunk["prebp"]
    #db = chunk["db"]
    #predb = chunk["predb"] 
    #nump = chunk["nump"]
    ierr = chunk["ierr"]
    fd.close()
    
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)       
    ene = (m * c**2 * (g - 1) / e)/1e6
    mlt = np.degrees(np.arctan2(-yeq,-xeq)+np.pi)/15
    return mlt,aeq,beq*1e9,ene,ierr  # =>nT

def readE(filepath,t):
    filename = filepath + str(t).zfill(3)+".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<"+str(gx*gy*gz*3)+"f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    E = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*e0
    #Convert Global-MHD coodinate ->GSM
    return -E[:,:,rz,0],-E[:,:,rz,1] 

main()

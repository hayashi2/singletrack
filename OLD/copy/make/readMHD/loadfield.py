# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *

path = "/work/m_hayashi/MHDdata/"
name = "Vsw600_20Re.npz"

k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1
"""
savd...
 np.savez("/work/m_hayashi/MHDdata/Vsw400_6Re(noon).npz)",time=rtime, bx=bx, by=by, bz=bz, ex=ex, ey=ey, ez=ez, vx=vx,vy=vy, vz=vz, N=N, P=P)
"""
def main():
    data=np.load(path+name)
    
    rtime = data["time"]
    bx = -data["bx"]
    by = -data["by"]
    bz = data["bz"]
    ex = -data["ex"]
    ey = -data["ey"]
    ez = data["ez"] 
    vx = -data["vx"]
    vy = -data["vy"]
    vz = data["vz"]
    N  = data["N"]
    P  = data["P"]
    

    T  = (P*1e-9)/(k*(N*1e6))
    B  = np.sqrt(bx**2 + by**2 + bz**2)
    V  = np.sqrt(vx**2 + vy**2 + vz**2 )
    Ma = (V*(np.sqrt(N)))/(20*B)
    Va = 20.3*B/np.sqrt(N)
    Vth = 3.0*k*T/1.67e-23
    Vs  = 0.12*(T+1.28*10e5)**(0.5)
    Vms = np.sqrt(Vs**2+Va**2)
    Mms = V/Vms
    Pdyn = (2e-6)*N*(V**2)
   # Pt = P + (B**2)/2
#    print "maxEy: ",np.max(ey)
#    print "minEy: ",np.min(ey)
#    print "maxVsw: ",np.max(V)
#    print "maxMms: ",np.max(Mms)
#    print "maxPdyn: ",np.max(Pdyn)
     
    figure(figsize=(10,20))
    subplots_adjust(hspace=0.7)
    subplot(12,1,1)
    title("Vsw800_20Re")
    plot(rtime,vx)
    ylabel("Vx[km/s]")
    ylim(np.min(vx)*1.1,np.max(vx)*1.1)
    locator_params(axis="y",tight=True,nbins=4)
    
    subplot (12,1,2)
    plot(rtime,bx)
    ylabel("Bx[nT]")
    locator_params(axis="y",tight=True,nbins=5)
    
    subplot(12,1,3)
    plot(rtime,by)
    ylabel("By[nT]")
    locator_params(axis="y",tight=True,nbins=5)

    subplot(12,1,4)
    plot(rtime,bz)
    ylabel("Bz[nT]")
    locator_params(axis="y",tight=True,nbins=4)

    subplot(12,1,5)
    plot(rtime,ex)
    ylabel("Ex[mV/m]")
    locator_params(axis="y",tight=True,nbins=4)
    
    subplot(12,1,6)
    plot(rtime,ey)
    ylabel("Ey[mV/m]")
    locator_params(axis="y",tight=True,nbins=5)
    
    subplot(12,1,7)
    plot(rtime,ez)
    ylabel("Ez[mV/m]")
    locator_params(axis="y",tight=True,nbins=4)
   
    subplot(12,1,8)
    plot(rtime,N)
    yscale('log')
    ylabel("log N(/cm^3)")

    subplot(12,1,9)
    plot(rtime,T)
    yscale('log')
    ylabel("log T(K)")

    subplot(12,1,10)
    plot(rtime,Pdyn)
    ylabel("Pdyn(npa)")
    locator_params(axis="y",tight=True,nbins=5)

    subplot(12,1,11)
    plot(rtime,Ma)
    ylabel("Ma")
    locator_params(axis="y",tight=True,nbins=3)
    
    subplot(12,1,12)
    plot(rtime,Mms)
    ylabel("Mms")
    ylim(np.min(Mms)*1.1,np.max(Mms)*1.1)
    locator_params(axis="y",tight=True,nbins=5)
    xlabel("time[sec]")
    
    show()
    #savefig( "GSM_Vsw800_20Re.jpeg")
    print Ma
    print B
    print V/Vms
    print N

main()

# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *
from scipy.signal import hilbert,chirp
from matplotlib.ticker import AutoMinorLocator

path = "/work/m_hayashi/MHDdata/"
name = "Vsw600_-47.npz"

k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1

"""
savd...
 np.savez("/work/m_hayashi/MHDdata/Vsw400_6Re(noon).npz)",time=rtime, bx=bx, by=by, bz=bz, ex=ex, ey=ey, ez=ez, vx=vx,vy=vy, vz=vz, N=N, P=P)
"""
def main():
    data=np.load(path+name)
    
    rtime = data["time"]
    bx = -data["bx"]
    by = -data["by"]
    bz = data["bz"]
    ex = -data["ex"]
    ey = -data["ey"]
    ez = data["ez"] 
    vx = -data["vx"]
    vy = -data["vy"]
    vz = data["vz"]
    N  = data["N"]
    P  = data["P"]
    """
    analytic_ey = hilbert(ey)
    amplitude_envelope = np.abs(analytic_ey)
    ins_phase = np.unwrap(np.angle(analytic_ey))
    ins_fre =np.diff(ins_phase)/(2.0*np.pi)*209
    """
    analytic_bz = hilbert(bz)
    amplitude_envelope = np.abs(analytic_bz)
    ins_phase = np.unwrap(np.angle(analytic_bz))
    ins_phase1 = np.angle(analytic_bz)
    ins_fre =np.diff(ins_phase)/(2.0*np.pi)*209
    
    #figure
    fig = figure(figsize=(12,13))
    suptitle("(4,7,0)Re",fontsize=15)
    subplots_adjust(hspace=0.3
)
    ax0 = fig.add_subplot(311)
    ax0.plot(rtime,bz,label="Bz")
    ax0.plot(rtime,amplitude_envelope,label="envelope")
    ax0.set_xlabel("time(sec)")
    ax0.set_ylabel("[nT]",fontsize=15)
    ax0.legend(loc=4)
    grid()
    ax0.xaxis.set_minor_locator(AutoMinorLocator())
    ax0.yaxis.set_minor_locator(AutoMinorLocator())

    ax1 = fig.add_subplot(312)
    ax1.plot(rtime[1:],ins_fre,label="frequency")
    ax1.set_xlabel("time(sec)")
    ax1.set_ylabel("[Hz]",fontsize=15)
    ax1.legend(loc=4)
    grid()
    ax1.xaxis.set_minor_locator(AutoMinorLocator())
    ax1.yaxis.set_minor_locator(AutoMinorLocator())

    ax2 = fig.add_subplot(313)
    ax2.plot(rtime,ins_phase1,label="phase")
    ax2.set_xlabel("time(sec)")
    ax2.legend(loc=4)
    ax2.xaxis.set_minor_locator(AutoMinorLocator())
    ax2.yaxis.set_minor_locator(AutoMinorLocator())
    grid()
    
    show()
        

main()

# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *
import matplotlib.patches as mpatches

patha = "/work/ymatumot/simulation/GM-RB"
pathb = "/run_Vsw600/"
bname = "MHD_B"
ename = "MHD_E"
dname = "MHD_"
outpath = "/home/m_hayashi/output/verc/"
k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1
"""
-30 <= x <= 69 (Re)
-40 <= y <= 40 (Re)
""" 
#time = 22

def main():
    for time in range(1,101):
        vx = readDATA(0,time)*v0
        vy = readDATA(1,time)*v0
        vy1 = np.empty([241,297])
        for n in range(0,241):
            vy1[n,:]= -vy[240-n,:]
            
        print np.shape(vx),np.shape(vy1)
        figure(figsize=(10,10))
        clf()
        
        #quiver(vx[88:154:3,57:123:3],-vy1[88:154:3,57:123:3],angles="xy",scale_units="xy",scale=40,width=0.002)
        quiver(vx[0:296:6,0:240:6],vy1[0:296:6,0:240:6],angles="xy",scale_units="xy",scale=40,width=0.002)
        title("V time="+str(time*12)+"s")
       # xlim(-11,11)
        #ylim(-11,11)
        setp(gca().get_xticklabels(), fontsize=20, visible=True)               
        setp(gca().get_yticklabels(), fontsize=20, visible=True)
        ylabel("y[Re]",fontsize=25)
        xlabel("x[Re]",fontsize=25)
    #Convert Global-MHD coordinate ->GSM                         
#        xticks([1,6,11,16,21],["10","5","0","-5","-10"])
#        yticks([1,6,11,16,21],["10","5","0","-5","-10"])
       

        savefig(outpath+"verc"+str(time).zfill(3)+".jpeg")
    
def readDATA(vn,time):
    
    if vn == 0:
        filename = patha + pathb + dname + "Vx" + str(time).zfill(3) + ".dat"
    elif vn == 1:
        filename = patha + pathb + dname + "Vy" + str(time).zfill(3) + ".dat"
    elif vn == 2:
        filename = patha + pathb + dname + "Vz" + str(time).zfill(3) + ".dat"
    elif vn == 3:
        filename = patha + pathb + dname + "N" + str(time).zfill(3) + ".dat"
    elif vn == 4:
        filename = patha + pathb + dname + "P" + str(time).zfill(3) + ".dat"

    head = ("head","<i")
    tail = ("tail","<i")
    v = ("v","<17250057f4")
    
    dt = np.dtype([head,v,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    vf = chunk[0]["v"].reshape((297,241,241),order="F")
    vt = vf.transpose(1,0,2)
    print str(vn) + " read... " + str(time) 
    return vt[:,:,120]
    
def nx(x):
    return int(x*3 + 90)

def ny(y):
    return int(y*3 + 121)

main()

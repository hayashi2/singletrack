module non_advection

  implicit none

  private

  public :: non_advection__init, non_advection__ssprk3, non_advection__rrk2,  &
             non_advection__df

  integer, save              :: neq, nxs, nxe, nys, nye, nzs, nze
  integer, save              :: nxs1, nxe1, nys1, nye1, nzs1, nze1
  integer, save              :: nxeh, nyeh, nzeh, nxeh1, nyeh1, nzeh1
  real(8), save              :: gam, dre0, dre1, dre2, drb0, drb1, ccr, dccr
  real(8), save              :: delt, rin, drs, px0, py0, pz0, pxm, pym, pzm
  real(8), save              :: ddelt, theta
  real(8), save, allocatable :: s(:,:,:,:,:), f0(:,:,:,:), u0(:,:,:,:), df(:,:,:,:)


contains


  subroutine non_advection__init(neqin,nxsin,nxein,nysin,nyein,nzsin,nzein,    &
                                 nxs1in,nxe1in,nys1in,nye1in,nzs1in,nze1in,    &
                                 nxehin,nyehin,nzehin,nxeh1in,nyeh1in,nzeh1in, &
                                 gamin,dre0in,dre1in,dre2in,drb0in,drb1in,     &
                                 ccrin,dccrin,                                 &
                                 deltin,rinin,drsin,                           &
                                 px0in,py0in,pz0in,pxmin,pymin,pzmin)
    integer, intent(in) :: neqin, nxsin, nxein, nysin, nyein, nzsin, nzein
    integer, intent(in) :: nxs1in, nxe1in, nys1in, nye1in, nzs1in, nze1in
    integer, intent(in) :: nxehin, nyehin, nzehin, nxeh1in, nyeh1in, nzeh1in
    real(8), intent(in) :: gamin, dre0in, dre1in, dre2in, drb0in, drb1in, ccrin, dccrin
    real(8), intent(in) :: deltin, rinin, drsin
    real(8), intent(in) :: px0in, py0in, pz0in, pxmin, pymin, pzmin

    neq   = neqin
    nxs   = nxsin
    nxe   = nxein
    nys   = nysin
    nye   = nyein
    nzs   = nzsin
    nze   = nzein
    nxs1  = nxs1in
    nxe1  = nxe1in
    nys1  = nys1in
    nye1  = nye1in
    nzs1  = nzs1in
    nze1  = nze1in
    nxeh  = nxehin
    nyeh  = nyehin
    nzeh  = nzehin
    nxeh1 = nxeh1in
    nyeh1 = nyeh1in
    nzeh1 = nzeh1in
    gam   = gamin
    dre0  = dre0in
    dre1  = dre1in
    dre2  = dre2in
    drb0  = drb0in
    drb1  = drb1in
    ccr   = ccrin
    dccr  = dccrin
    delt  = deltin
    rin   = rinin
    drs   = drsin
    px0   = px0in
    py0   = py0in
    pz0   = pz0in
    pxm   = pxmin
    pym   = pymin
    pzm   = pzmin
    ddelt = 1.D0/delt
    theta = 0.505D0  !implicit factor
    allocate(s(4,neq,nxs:nxe,nys:nye,nzs:nze))
    allocate(f0(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1))
    allocate(u0(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1))
    allocate(df(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1))

  end subroutine non_advection__init


  subroutine non_advection__ssprk3(gf,           &
                                   nerr,nstat,   &
                                   f,Db,wrk,bf,flg)

    use boundary, only : boundary__f

    real(8), intent(inout) :: gf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    integer, intent(inout) :: nerr, nstat(:)
    real(8), intent(in)    :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: Db(nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)   :: wrk(3,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)   :: bf(3,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: flg(neq,nxs:nxe,nys:nye,nzs:nze)
    integer                :: istep, ieq, i, j, k
    real(8), parameter     :: fac=1.D0/12.D0
    real(8)                :: c1, c2

!$OMP PARALLEL DO PRIVATE(ieq,i,j,k)
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       u0(3,i,j,k) = 0.5*(+gf(3,i,j,k)+gf(4,i,j,k))
       u0(4,i,j,k) = u0(3,i,j,k)
       u0(5,i,j,k) = 0.5*(+gf(5,i,j,k)+gf(6,i,j,k)) 
       u0(6,i,j,k) = u0(5,i,j,k)
       u0(7,i,j,k) = 0.5*(+gf(7,i,j,k)+gf(8,i,j,k))
       u0(8,i,j,k) = u0(7,i,j,k)
       do ieq=1,neq
          f0(ieq,i,j,k) = gf(ieq,i,j,k)
          gf(ieq,i,j,k) = f(ieq,i,j,k)
       enddo
    enddo
    enddo
    enddo
!$OMP END PARALLEL DO

    !Runge-Kutta substeps (1-4)
    do istep=1,3

       call calc_source(istep,nerr,nstat,gf,Db,wrk,bf)

       c1 = fac*(-7.D0*istep*istep+30.D0*istep-23.D0)
       c2 = fac*(+7.D0*istep*istep-30.D0*istep+35.D0)

!$OMP PARALLEL DO PRIVATE(ieq,i,j,k)
       do k=nzs,nze
       do j=nys,nye
       do i=nxs,nxe
       do ieq=1,neq
          gf(ieq,i,j,k) = c1*f(ieq,i,j,k)+c2*(gf(ieq,i,j,k)+s(istep,ieq,i,j,k)*delt*flg(ieq,i,j,k))
       enddo
       enddo
       enddo
       enddo
!$OMP END PARALLEL DO

       call boundary__f(gf,neq,nerr,nstat)

    enddo

  end subroutine non_advection__ssprk3


  subroutine non_advection__rrk2(gf,                          &
                                 ncomw,nerr,nstat,mnpr,opsum, &
                                 f,Db,wrk,bf,flg)

    use boundary, only : boundary__f

    real(8), intent(inout) :: gf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    integer, intent(inout) :: nerr, nstat(:)
    integer, intent(in)    :: ncomw, mnpr, opsum
    real(8), intent(in)    :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: Db(nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)   :: wrk(3,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)   :: bf(3,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: flg(neq,nxs:nxe,nys:nye,nzs:nze)
    integer :: istep, ieq, i, j, k
    real(8) :: ds, g11, g33, g13, g11_g, g33_g, g13_g, ig33_g

!$OMP PARALLEL DO PRIVATE(ieq,i,j,k)
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       u0(3,i,j,k) = 0.5*(+gf(3,i,j,k)+gf(4,i,j,k))
       u0(4,i,j,k) = u0(3,i,j,k)
       u0(5,i,j,k) = 0.5*(+gf(5,i,j,k)+gf(6,i,j,k)) 
       u0(6,i,j,k) = u0(5,i,j,k)
       u0(7,i,j,k) = 0.5*(+gf(7,i,j,k)+gf(8,i,j,k))
       u0(8,i,j,k) = u0(7,i,j,k)
       do ieq=1,neq
          f0(ieq,i,j,k) = gf(ieq,i,j,k)
          gf(ieq,i,j,k) = f(ieq,i,j,k)
       enddo
    enddo
    enddo
    enddo
!$OMP END PARALLEL DO

    !Rational Runge-Kutta substeps (1-2)
    do istep=1,2

       call calc_source(istep,nerr,nstat,gf,Db,wrk,bf)

       select case(istep)

       case(1)
          ds = 0.5*delt
!$OMP PARALLEL DO PRIVATE(ieq,i,j,k)
          do k=nzs,nze
          do j=nys,nye
          do i=nxs,nxe
          do ieq=1,neq
             gf(ieq,i,j,k) = f(ieq,i,j,k)+ds*s(istep,ieq,i,j,k)*flg(ieq,i,j,k)
          enddo
          enddo
          enddo
          enddo
!$OMP END PARALLEL DO

          call boundary__f(gf,neq,nerr,nstat)

       end select
    enddo

!$OMP PARALLEL DO PRIVATE(ieq,i,j,k)
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe
       do ieq=1,neq
          s(3,ieq,i,j,k) = 2.*s(1,ieq,i,j,k)-s(2,ieq,i,j,k)
       enddo
    enddo
    enddo
    enddo
!$OMP END PARALLEL DO

    g11 = 0.D0
    g33 = 0.D0
    g13 = 0.D0
!$OMP PARALLEL DO PRIVATE(ieq,i,j,k) &
!$OMP REDUCTION(+:g11,g33,g13)
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe
       do ieq=1,neq
          g11 = g11+s(1,ieq,i,j,k)*s(1,ieq,i,j,k)*flg(ieq,i,j,k)**2
          g33 = g33+s(3,ieq,i,j,k)*s(3,ieq,i,j,k)*flg(ieq,i,j,k)**2
          g13 = g13+s(1,ieq,i,j,k)*s(3,ieq,i,j,k)*flg(ieq,i,j,k)**2
       enddo
    enddo
    enddo
    enddo
!$OMP END PARALLEL DO

    call MPI_ALLREDUCE(g11,g11_g,1,mnpr,opsum,ncomw,nerr)
    call MPI_ALLREDUCE(g33,g33_g,1,mnpr,opsum,ncomw,nerr)
    call MPI_ALLREDUCE(g13,g13_g,1,mnpr,opsum,ncomw,nerr)

    ig33_g = 1.D0/g33_g
    !update f
!$OMP PARALLEL DO PRIVATE(ieq,i,j,k)
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe
       do ieq=1,neq
          gf(ieq,i,j,k) = f(ieq,i,j,k)+(+2.*g13_g*ig33_g*s(1,ieq,i,j,k) &
                                        -g11_g*ig33_g*s(3,ieq,i,j,k))*flg(ieq,i,j,k)*delt
       enddo
    enddo
    enddo
    enddo
!$OMP END PARALLEL DO

  end subroutine non_advection__rrk2


  subroutine calc_source(istep,nerr,nstat,f,Db,wrk,bf)

    use boundary, only : boundary__bf, boundary__divva
    use functions
    integer, intent(in)    :: istep
    integer, intent(inout) :: nerr, nstat(:)
    real(8), intent(inout) :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: Db(nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)   :: wrk(3,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)   :: bf(3,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), parameter :: cv0=7.5D-1, eps=2.D-1
    real(8), parameter :: rout=1.0, drout=1.0D0/1.0D0, rout2=7.0
    integer            :: i, j, k, im, ip, jm, jp, km, kp
    real(8)            :: r0, rm, x0, xm, y0, ym, z0, zm, dr05, drm5
    real(8)            :: vax, vay, vaz, cs2, cx, cy, cz, cabs
    real(8)            :: dn, dn0, dsn0, dn1, dn2, dn3
    real(8)            :: vf, cv, dre, drb, divb, divv
    real(8)            :: tmp1, tmp2
    real(8)            :: divva(nxs1:nxe1,nys1:nye1,nzs1:nze1)

!$OMP PARALLEL PRIVATE(i,j,k)

!$OMP DO
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       !Cs to N
       f(1,i,j,k) = 1.D0/(f(1,i,j,k)*f(1,i,j,k))*f(2,i,j,k)
       wrk(2,i,j,k) = dlog(f(1,i,j,k))

       !z+- -> v, va
       f(3,i,j,k) = 0.5*(+f(3,i,j,k)+f(4,i,j,k))
       f(4,i,j,k) = +f(3,i,j,k)-f(4,i,j,k)
       f(5,i,j,k) = 0.5*(+f(5,i,j,k)+f(6,i,j,k)) 
       f(6,i,j,k) = +f(5,i,j,k)-f(6,i,j,k)
       f(7,i,j,k) = 0.5*(+f(7,i,j,k)+f(8,i,j,k))
       f(8,i,j,k) = +f(7,i,j,k)-f(8,i,j,k)
    enddo
    enddo
    enddo
!$OMP END DO

    !div(V) and shock capturing viscosity
!$OMP DO PRIVATE(im,jm,km,x0,xm,y0,ym,z0,zm,r0,rm,dr05,drm5, &
!$OMP            dn0,vf,cv,divv,vax,vay,vaz,cs2)
    do k=nzs,nze1
       km = k-1
    do j=nys,nye1
       jm = j-1
    do i=nxs,nxe1
       im = i-1

       !div(V)
       divv = -f(7,i,j,km)
       vaz = 0.5*f(8,i,j,km)
       divv = divv-f(5,i,jm,k)
       vay = 0.5*f(6,i,jm,k)
       divv = divv-f(3,im,j,k)
       vax = 0.5*f(4,im,j,k)

       cs2 = +0.5*gam/f(1,i,j,k)*f(2,i,j,k) 
       divv = divv+f(3,i,j,k)
       vax = vax+0.5*f(4,i,j,k)
       divv = divv+f(5,i,j,k)
       vay = vay+0.5*f(6,i,j,k)
       divv = divv+f(7,i,j,k)
       vaz = vaz+0.5*f(8,i,j,k)

       !at (i,j,k)
       x0 = (i-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)
       xm = (i-pxm)*drs
       ym = (j-pym)*drs
       zm = (k-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0 = 1./dsqrt(Db(i,j,k))
       vax = vax+bdx(x0,z0,xm,zm,dr05,drm5)*dn0
       vay = vay+bdy(y0,z0,ym,zm,dr05,drm5)*dn0
       vaz = vaz+bdz(z0,r0,zm,rm,dr05,drm5)*dn0

       vf = dsqrt(+cs2+vax*vax+vay*vay+vaz*vaz)

       cv = 0.5*cv0*(1.0-tanh(sngl((divv/vf-(-eps))/0.05)))

       wrk(1,i,j,k) = cv*divv*f(1,i,j,k)*(-vf+0.5*(gam+1.)*divv)
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

    !div(Va)
!$OMP DO PRIVATE(im,jm,km,ip,jp,kp,x0,xm,y0,ym,z0,zm,r0,rm,dr05,drm5,dn0)
    do k=nzs,nze
       km = k-1
       kp = k+1
    do j=nys,nye
       jm = j-1
       jp = j+1
    do i=nxs,nxe
       im = i-1
       ip = i+1

       !at (i,j,k)
       x0 = (i-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)
       xm = (i-pxm)*drs
       ym = (j-pym)*drs
       zm = (k-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0 = 1./dsqrt(Db(i,j,k))

       !divva at (i,j,k)
       divva(i,j,k) = -0.25*(+f(4,im,j,k)*(-wrk(2,im,j,k)+wrk(2,i,j,k)) &
                             +f(4,i,j,k)*(-wrk(2,i,j,k)+wrk(2,ip,j,k))  &
                             +f(6,i,jm,k)*(-wrk(2,i,jm,k)+wrk(2,i,j,k)) & 
                             +f(6,i,j,k)*(-wrk(2,i,j,k)+wrk(2,i,jp,k))  &
                             +f(8,i,j,km)*(-wrk(2,i,j,km)+wrk(2,i,j,k)) &
                             +f(8,i,j,k)*(-wrk(2,i,j,k)+wrk(2,i,j,kp))  &
                             +bdx(x0,z0,xm,zm,dr05,drm5)*dn0            &
                             *(-wrk(2,im,j,k)+wrk(2,ip,j,k)             &
                               +Db(im,j,k)-Db(ip,j,k))                  &
                             +bdy(y0,z0,ym,zm,dr05,drm5)*dn0            &
                             *(-wrk(2,i,jm,k)+wrk(2,i,jp,k)             &
                               +Db(i,jm,k)-Db(i,jp,k))                  &
                             +bdz(z0,r0,zm,rm,dr05,drm5)*dn0            &
                             *(-wrk(2,i,j,km)+wrk(2,i,j,kp)             &
                               +Db(i,j,km)-Db(i,j,kp))                  &
                            )
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP DO PRIVATE(ip,jp,kp,x0,xm,y0,ym,z0,zm,r0,rm,dr05,drm5, &
!$OMP            dn0,dn1,dn2,dn3)
    do k=nzs,nze
       kp = k+1
    do j=nys,nye
       jp = j+1
    do i=nxs,nxe
       ip = i+1

       dn1 = dsqrt(0.5*(+f(1,i,j,k)+f(1,ip,j,k)))
       dn2 = dsqrt(0.5*(2.*dn1*dn1-f(1,ip,j,k)+f(1,i,jp,k)))
       dn3 = dsqrt(0.5*(2.*dn2*dn2-f(1,i,jp,k)+f(1,i,j,kp)))

       !dBx at i+1/2,j,k
       x0 = (i+0.5-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i+0.5-pxm)*drs
       ym = (j-pym)*drs
       zm = (k-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0 = dsqrt(0.5*(Db(i,j,k)+Db(ip,j,k)))
       bf(1,i,j,k) = f(4,i,j,k)*dn1 &
                    +(dn1/dn0-1.)*bdx(x0,z0,xm,zm,dr05,drm5)

       !dBy at i,j+1/2,k
       x0 = (i-px0)*drs
       y0 = (j+0.5-py0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i-pxm)*drs
       ym = (j+0.5-pym)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0 = dsqrt(0.5*(Db(i,j,k)+Db(i,jp,k)))
       bf(2,i,j,k) = f(6,i,j,k)*dn2 &
                    +(dn2/dn0-1.)*bdy(y0,z0,ym,zm,dr05,drm5)

       !dBz at i,j,k+1/2
       y0 = (j-py0)*drs
       z0 = (k+0.5-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       ym = (j-pym)*drs
       zm = (k+0.5-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0 = dsqrt(0.5*(Db(i,j,k)+Db(i,j,kp)))
       bf(3,i,j,k) = f(8,i,j,k)*dn3 &
                    +(dn3/dn0-1.)*bdz(z0,r0,zm,rm,dr05,drm5)
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!!$!$OMP DO
!!$    do k=nzs1,nze1
!!$    do j=nys1,nye1
!!$    do i=nxs1,nxe1
!!$       !N0 -> ln(N0)
!!$       Db(i,j,k) = dlog(Db(i,j,k))
!!$    enddo
!!$    enddo
!!$    enddo
!!$!$OMP END DO NOWAIT

!$OMP END PARALLEL

    call boundary__bf(bf,3,nerr,nstat)
    call boundary__divva(divva,nerr,nstat)

!$OMP PARALLEL PRIVATE(i,j,k)

    !non-advection terms
!$OMP DO PRIVATE(im,ip,jm,jp,km,kp,                                          &
!$OMP            x0,xm,y0,ym,z0,zm,r0,rm,dr05,drm5,vax,vay,vaz,dn,dsn0,divv, &
!$OMP            dre,drb,divb,cx,cy,cz,cabs,tmp1,tmp2)
    do k=nzs,nze
       km = k-1
       kp = k+1
    do j=nys,nye
       jm = j-1
       jp = j+1
    do i=nxs,nxe
       im = i-1
       ip = i+1

       !current density at (i,j,k)
       cx = +0.5*0.5*(-bf(3,i,jm,km)+bf(3,i,jp,km)-bf(3,i,jm,k)+bf(3,i,jp,k)) &
            -0.5*0.5*(-bf(2,i,jm,km)+bf(2,i,jm,kp)-bf(2,i,j,km)-bf(2,i,j,kp))    
       cy = +0.5*0.5*(-bf(1,im,j,km)+bf(1,im,j,kp)-bf(1,i,j,km)+bf(1,i,j,kp)) &
            -0.5*0.5*(-bf(3,im,j,km)+bf(3,ip,j,km)-bf(3,im,j,k)+bf(3,ip,j,k))
       cz = +0.5*0.5*(-bf(2,im,jm,k)+bf(2,ip,jm,k)-bf(2,im,j,k)+bf(2,ip,j,k)) &
            -0.5*0.5*(-bf(1,im,jm,k)+bf(1,im,jp,k)-bf(1,i,jm,k)+bf(1,i,jp,k))
       cabs = sqrt(cx*cx+cy*cy+cz*cz)
       !end of

       !resistivity
       x0 = (i-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)

!       drb = +0.5*(drb0+drb0*tanh(sngl((r0-rout2)*drout)))
       drb = +0.5*(1.+tanh(sngl((r0-rout2)*drout))) &
                 *(+drb0+(drb1-drb0)*(0.5*(1.+tanh((cabs-ccr)*dccr)))**2)

       !div(V)
       divv = -f(7,i,j,km)
       divv = divv-f(5,i,jm,k)
       divv = divv-f(3,im,j,k)
       divv = divv+f(3,i,j,k)
       divv = divv+f(5,i,j,k)
       divv = divv+f(7,i,j,k)


       !*** at (i,j,k) for Cs ***!
       s(istep,1,i,j,k) = -0.5*(gam-1.)*dsqrt(1./f(1,i,j,k)*f(2,i,j,k))*divv &
                          -dsqrt(1./(f(1,i,j,k)*f(2,i,j,k)))*(gam-1.)        &
                          *(+divv*wrk(1,i,j,k)-drb*cabs*cabs)


       !*** at (i,j,k) for P ***!
       s(istep,2,i,j,k) = -gam*divv*f(2,i,j,k)    &
                          -2.*(gam-1.)*(+divv*wrk(1,i,j,k)-drb*cabs*cabs)


       !*** at (i+1/2,j,k) for Zx+- ***!
       !dipole field at i+1/2,j,k
       x0 = (i+0.5-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i+0.5-pxm)*drs
       ym = (j-pym)*drs
       zm = (k-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dsn0 = dsqrt(1./n0(r0))
       vax   = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       vay   = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       vaz   = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       !end of dipole 

       dn  = 2./(+f(1,i,j,k)+f(1,ip,j,k))

       !current density at (i+1/2,j,k)
       cx = +0.25*0.5*(-bf(3,i,jm,km)+bf(3,i,jp,km)     &
                       -bf(3,i,jm,k)+bf(3,i,jp,k)         &
                       -bf(3,ip,jm,km)+bf(3,ip,jp,km) &
                       -bf(3,ip,jm,k)+bf(3,ip,jp,k))    &
            -0.25*0.5*(-bf(2,i,jm,km)+bf(2,i,jm,kp)     &
                       -bf(2,i,j,km)-bf(2,i,j,kp)         &
                       -bf(2,ip,jm,km)-bf(2,ip,jm,kp) &
                       -bf(2,ip,j,km)-bf(2,ip,j,kp))        
       cy = +0.5*(-bf(1,i,j,km)+bf(1,i,j,kp))                             &
            -0.5*(-bf(3,i,j,km)+bf(3,ip,j,km)-bf(3,i,j,k)+bf(3,ip,j,k))
       cz = +0.5*(-bf(2,i,jm,k)+bf(2,ip,jm,k)-bf(2,i,j,k)+bf(2,ip,j,k)) &
            -0.5*(-bf(1,i,jm,k)+bf(1,i,jp,k))
       cabs = sqrt(cx*cx+cy*cy+cz*cz)
       !end of
       
       !damping and anomalous resistivity
       dre = +0.5*((dre1+dre0)-(dre1-dre0)*tanh(sngl((r0-(rin+rout))*drout)))
!       drb = +0.5*((drb1+drb0)-(drb1-drb0)*tanh(sngl((r0-(rin+rout))*drout)))
       drb = +drb0+(drb1-drb0)*(0.5*(1.+tanh((cabs-ccr)*dccr)))**2

       tmp1 =                                                    &
            -0.5*dn*(-f(2,i,j,k)+f(2,ip,j,k))                    &
            -0.125*(                                             &
            -(+f(6,i,jm,k)+f(6,i,j,k))**2                        &
            +(+f(6,ip,jm,k)+f(6,ip,j,k))**2                      &
            -(+f(8,i,j,km)+f(8,i,j,k))**2                        & 
            +(+f(8,ip,j,km)+f(8,ip,j,k))**2 )                    &
            -vay*0.5*(                                           &
            -(+f(6,i,jm,k)+f(6,i,j,k))                           &
            +(+f(6,ip,jm,k)+f(6,ip,j,k)))                        &
            -vaz*0.5*(                                           &
            -(+f(8,i,j,km)+f(8,i,j,k))                           &
            +(+f(8,ip,j,km)+f(8,ip,j,k)))                        &
            -0.5*(+(vax+f(4,i,j,k))**2                           &
                  +(+vay+0.25*(+f(6,i,jm,k)+f(6,ip,jm,k)         &
                               +f(6,i,j,k)+f(6,ip,j,k)))**2      &
                  +(+vaz+0.25*(+f(8,i,j,km)+f(8,ip,j,km)         &
                               +f(8,i,j,k)+f(8,ip,j,k)))**2 )    &
!                *dn*(-f(1,i,j,k)+f(1,ip,j,k))                    &
                *(-wrk(2,i,j,k)+wrk(2,ip,j,k))                    &
!!$            +0.5*(+vay*vay+vaz*vaz)*(-Db(i,j,k)+Db(ip,j,k))      &
            -(+vax+f(4,i,j,k))*0.5*(+divva(i,j,k)+divva(ip,j,k))      &
!!$            -0.5*vax*                                            &
!!$            (+0.25*(+vay*(-Db(i,jm,k)+Db(i,jp,k)                 &
!!$                          -Db(ip,jm,k)+Db(ip,jp,k))              &
!!$                    +vaz*(-Db(i,j,km)+Db(i,j,kp)                 & 
!!$                          -Db(ip,j,km)+Db(ip,j,kp)))             &
!!$            )                                                    &
            +dn*dre*                                             &
            (+f(3,i,j,km)+f(3,i,jm,k)                            &
             +f(3,im,j,k)-6.*f(3,i,j,k)+f(3,ip,j,k)              &
             +f(3,i,jp,k)+f(3,i,j,kp))                           &
            -dn*(-wrk(1,i,j,k)+wrk(1,ip,j,k))                     
             
       tmp2 =                                                     &
            +0.5*(+vax+f(4,i,j,k))*0.5*(-f(3,im,j,k)+f(3,ip,j,k)) &
            -0.5*(+vax+f(4,i,j,k))*0.5*(-f(5,i,jm,k)-f(5,ip,jm,k) &
                                        +f(5,i,j,k)+f(5,ip,j,k)   &
                                        -f(7,i,j,km)-f(7,ip,j,km) &
                                        +f(7,i,j,k)+f(7,ip,j,k))  &
            +dsqrt(dn)*drb*                                       &
            (+bf(1,i,j,km)+bf(1,i,jm,k)                           &
             +bf(1,im,j,k)-6.*bf(1,i,j,k)+bf(1,ip,j,k)            &
             +bf(1,i,jp,k)+bf(1,i,j,kp))                            

       s(istep,3,i,j,k) = tmp1+tmp2
       s(istep,4,i,j,k) = tmp1-tmp2


       !*** at (i,j+1/2,k) for Zy+- ***!
       !dipole field at i,j+1/2,k
       x0 = (i-px0)*drs
       y0 = (j+0.5-py0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i-pxm)*drs
       ym = (j+0.5-pym)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dsn0 = dsqrt(1./n0(r0))
       vax   = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       vay   = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       vaz   = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       !end dipole 

       dn  = 2./(+f(1,i,j,k)+f(1,i,jp,k))

       !current density at (i,j+1/2,k)
       cx = 0.5*(-bf(3,i,j,km)+bf(3,i,jp,km)-bf(3,i,j,k)+bf(3,i,jp,k)) &
           -0.5*(-bf(2,i,j,km)+bf(2,i,j,kp))
       cy = 0.25*0.5*(-bf(1,im,j,km)+bf(1,im,j,kp)     &
                      -bf(1,i,j,km)+bf(1,i,j,kp)         &
                      -bf(1,im,jp,km)+bf(1,im,jp,kp) &
                      -bf(1,i,jp,km)+bf(1,i,jp,kp) )   &
           -0.25*0.5*(-bf(3,im,j,km)+bf(3,ip,j,km)     &
                      -bf(3,im,j,k)+bf(3,ip,j,k)         &
                      -bf(3,im,jp,km)+bf(3,ip,jp,km) &
                      -bf(3,im,jp,k)+bf(3,ip,jp,k) )
       cz = 0.5*(-bf(2,im,j,k)+bf(2,ip,j,k)) &
           -0.5*(-bf(1,im,j,k)+bf(1,im,jp,k)-bf(1,i,j,k)+bf(1,i,jp,k)) 
       cabs = sqrt(cx*cx+cy*cy+cz*cz)
       !end of

       !damping and anomalous resistivity
       dre = +0.5*((dre1+dre0)-(dre1-dre0)*tanh(sngl((r0-(rin+rout))*drout)))
!       drb = +0.5*((drb1+drb0)-(drb1-drb0)*tanh(sngl((r0-(rin+rout))*drout)))
       drb = +drb0+(drb1-drb0)*(0.5*(1.+tanh((cabs-ccr)*dccr)))**2
       !end of

       tmp1 =                                                    &
            -0.5*dn*(-f(2,i,j,k)+f(2,i,jp,k))                    &
            -0.125*(                                             &
            -(+f(4,im,j,k)+f(4,i,j,k))**2                        &
            +(+f(4,im,jp,k)+f(4,i,jp,k))**2                      &
            -(+f(8,i,j,km)+f(8,i,j,k))**2                        &
            +(+f(8,i,jp,km)+f(8,i,jp,k))**2 )                    &
            -vax*0.5*(                                           &
            -(+f(4,im,j,k)+f(4,i,j,k))                           &
            +(+f(4,im,jp,k)+f(4,i,jp,k)) )                       &
            -vaz*0.5*(                                           &
            -(+f(8,i,j,km)+f(8,i,j,k))                           &
            +(+f(8,i,jp,km)+f(8,i,jp,k)) )                       &
            -0.5*(+(+vax+0.25*(+f(4,im,j,k)+f(4,i,j,k)           &
                               +f(4,im,jp,k)+f(4,i,jp,k)))**2    &
                  +(+vay+f(6,i,j,k))**2                          &
                  +(+vaz+0.25*(+f(8,i,j,km)+f(8,i,jp,km)         &
                               +f(8,i,j,k)+f(8,i,jp,k)))**2 )    &
!                *dn*(-f(1,i,j,k)+f(1,i,jp,k))                    &
                *(-wrk(2,i,j,k)+wrk(2,i,jp,k))                    &
!!$            +0.5*(+vax*vax+vaz*vaz)*(-Db(i,j,k)+Db(i,jp,k))      &
            -(+vay+f(6,i,j,k))*0.5*(+divva(i,j,k)+divva(i,jp,k))      &
!!$            -0.5*vay*                                            &
!!$            (+0.25*(+vax*(-Db(im,j,k)+Db(ip,j,k)                 &
!!$                          -Db(im,jp,k)+Db(ip,jp,k))              &
!!$                    +vaz*(-Db(i,j,km)+Db(i,j,kp)                 &
!!$                          -Db(i,jp,km)+Db(i,jp,kp)))             &
!!$            )                                                    &
            +dn*dre*                                             &
            (+f(5,i,j,km)+f(5,i,jm,k)                            & 
             +f(5,im,j,k)-6.*f(5,i,j,k)+f(5,ip,j,k)              &
             +f(5,i,jp,k)+f(5,i,j,kp))                           &
            -dn*(-wrk(1,i,j,k)+wrk(1,i,jp,k))                         

       tmp2 =                                                     &
            -0.5*(+vay+f(6,i,j,k))*0.5*(-f(3,im,j,k)+f(3,i,j,k)   &
                                        -f(3,im,jp,k)+f(3,i,jp,k) &
                                        -f(7,i,j,km)-f(7,i,jp,km) &
                                        +f(7,i,j,k)+f(7,i,jp,k))  &
            +0.5*(+vay+f(6,i,j,k))*0.5*(-f(5,i,jm,k)+f(5,i,jp,k)) &
            +dsqrt(dn)*drb*                                      &
            (+bf(2,i,j,km)+bf(2,i,jm,k)                           &
             +bf(2,im,j,k)-6.*bf(2,i,j,k)+bf(2,ip,j,k)            &
             +bf(2,i,jp,k)+bf(2,i,j,kp))                            

       s(istep,5,i,j,k) = tmp1+tmp2
       s(istep,6,i,j,k) = tmp1-tmp2


       !*** at (i,j,k+1/2) for Zz+- ***!
       !dipole field at i,j,k+1/2
       y0 = (j-py0)*drs
       z0 = (k+0.5-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       ym = (j-pym)*drs
       zm = (k+0.5-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)
       
       dsn0 = dsqrt(1./n0(r0))
       vax   = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       vay   = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       vaz   = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       !end of dipole 

       dn  = 2./(+f(1,i,j,k)+f(1,i,j,kp))

       !current density at (i,j,k+1/2)
       cx = 0.5*(-bf(3,i,jm,k)+bf(3,i,jp,k))                             &
           -0.5*(-bf(2,i,jm,k)+bf(2,i,jm,kp)-bf(2,i,j,k)+bf(2,i,j,kp))
       cy = 0.5*(-bf(1,im,j,k)+bf(1,im,j,kp)-bf(1,i,j,k)+bf(1,i,j,kp)) &
           -0.5*(-bf(3,im,j,k)+bf(3,ip,j,k))
       cz = 0.25*0.5*(-bf(2,im,jm,k)+bf(2,ip,jm,k)     &
                      -bf(2,im,j,k)+bf(2,ip,j,k)         &
                      -bf(2,im,jm,kp)+bf(2,ip,jm,kp) &
                      -bf(2,im,j,kp)+bf(2,ip,j,kp) )   &
           -0.25*0.5*(-bf(1,im,jm,k)+bf(1,im,jp,k)     &
                      -bf(1,i,jm,k)+bf(1,i,jp,k)         &
                      -bf(1,im,jm,kp)+bf(1,im,jp,kp) &
                      -bf(1,i,jm,kp)+bf(1,i,jp,kp) )
       cabs = sqrt(cx*cx+cy*cy+cz*cz)
       !end of
          
       !damping and anomalous resistivity
       dre = +0.5*((dre1+dre0)-(dre1-dre0)*tanh(sngl((r0-(rin+rout))*drout)))
!       drb = +0.5*((drb1+drb0)-(drb1-drb0)*tanh(sngl((r0-(rin+rout))*drout)))
       drb = +drb0+(drb1-drb0)*(0.5*(1.+tanh((cabs-ccr)*dccr)))**2
       !end of

       tmp1 =                                                      &
            -0.5*dn*(-f(2,i,j,k)+f(2,i,j,kp))                      &
            -0.125*(                                               &
            -(+f(4,im,j,k)+f(4,i,j,k))**2                          &
            +(+f(4,im,j,kp)+f(4,i,j,kp))**2                        &
            -(+f(6,i,jm,k)+f(6,i,j,k))**2                          &
            +(+f(6,i,jm,kp)+f(6,i,j,kp))**2 )                      &
            -vax*0.5*(                                             &
            -(+f(4,im,j,k)+f(4,i,j,k))                             &
            +(+f(4,im,j,kp)+f(4,i,j,kp)) )                         &
            -vay*0.5*(                                             &
            -(+f(6,i,jm,k)+f(6,i,j,k))                             &
            +(+f(6,i,jm,kp)+f(6,i,j,kp)) )                         &
            -0.5*(+(+vax+0.25*(+f(4,im,j,k)+f(4,i,j,k)             &
                               +f(4,im,j,kp)+f(4,i,j,kp)))**2      &
                  +(+vay+0.25*(+f(6,i,jm,k)+f(6,i,j,k)             &
                               +f(6,i,jm,kp)+f(6,i,j,kp)))**2      &
                  +(+vaz+f(8,i,j,k))**2 )                          &
!                *dn*(-f(1,i,j,k)+f(1,i,j,kp))                      &
                *(-wrk(2,i,j,k)+wrk(2,i,j,kp))                      &
!!$            +0.5*(+vax*vax+vay*vay)*(-Db(i,j,k)+Db(i,j,kp))        &
            -(+vaz+f(8,i,j,k))*0.5*(+divva(i,j,k)+divva(i,j,kp))      &
!!$            -0.5*vaz*                                              &
!!$            (+0.25*(+vax*(-Db(im,j,k)+Db(ip,j,k)                   &
!!$                          -Db(im,j,kp)+Db(ip,j,kp))                &
!!$                    +vay*(-Db(i,jm,k)+Db(i,jp,k)                   &
!!$                          -Db(i,jm,kp)+Db(i,jp,kp)) )              &
!!$            )                                                      &
            +dn*dre*                                               &
            (+f(7,i,j,km)+f(7,i,jm,k)                              &
             +f(7,im,j,k)-6.*f(7,i,j,k)+f(7,ip,j,k)                &
             +f(7,i,jp,k)+f(7,i,j,kp))                             &
            -dn*(-wrk(1,i,j,k)+wrk(1,i,j,kp))                         
       
       tmp2 =                                                      &
            -0.5*(+vaz+f(8,i,j,k))*0.5*(-f(3,im,j,k)+f(3,i,j,k)    &
                                        -f(3,im,j,kp)+f(3,i,j,kp)  &
                                        -f(5,i,jm,k)+f(5,i,j,k)    &
                                        -f(5,i,jm,kp)+f(5,i,j,kp)) &
            +0.5*(+vaz+f(8,i,j,k))*0.5*(-f(7,i,j,km)+f(7,i,j,kp))  &
            +dsqrt(dn)*drb*                                        &
            (+bf(3,i,j,km)+bf(3,i,jm,k)                            &
             +bf(3,im,j,k)-6.*bf(3,i,j,k)+bf(3,ip,j,k)             &
             +bf(3,i,jp,k)+bf(3,i,j,kp))                             

       s(istep,7,i,j,k) = tmp1+tmp2
       s(istep,8,i,j,k) = tmp1-tmp2
    enddo
    enddo
    enddo
!$OMP END DO

!!$    !ln(N0) -> N0
!!$!$OMP DO
!!$    do k=nzs1,nze1
!!$    do j=nys1,nye1
!!$    do i=nxs1,nxe1
!!$       Db(i,j,k) = dexp(Db(i,j,k))
!!$    enddo
!!$    enddo
!!$    enddo
!!$!$OMP END DO NOWAIT

!$OMP DO
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       f(1,i,j,k) = dsqrt(f(2,i,j,k)/f(1,i,j,k))

       f(3,i,j,k) = f(3,i,j,k)+f(4,i,j,k)
       f(4,i,j,k) = f(3,i,j,k)-2.*f(4,i,j,k)
       f(5,i,j,k) = f(5,i,j,k)+f(6,i,j,k)
       f(6,i,j,k) = f(5,i,j,k)-2.*f(6,i,j,k)
       f(7,i,j,k) = f(7,i,j,k)+f(8,i,j,k)
       f(8,i,j,k) = f(7,i,j,k)-2.*f(8,i,j,k)
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP END PARALLEL

  end subroutine calc_source


  subroutine non_advection__df(gdxf,gdyf,gdzf,       &
                               gf,f,dxf,dyf,dzf,flg)

    use functions
    real(8), intent(inout) :: gdxf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: gdyf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: gdzf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: gf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: dxf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: dyf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: dzf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: flg(neq,nxs:nxe,nys:nye,nzs:nze)
    integer                :: i, j, k, l, ieq, im, ip, jm, jp, km, kp
    real(8), parameter     :: fac=1.D0/6.D0
    real(8)                :: r0, rm, x0, xm, y0, ym, z0, zm, dr05, drm5
    real(8)                :: vax, vay, vaz, dn0, dsn0, dxn, dyn, dzn
    real(8)                :: dxvax, dxvay, dxvaz, dyvax, dyvay, dyvaz, dzvax, dzvay, dzvaz 
    real(8)                :: jcb(9), sol(3), src_x, src_y, src_z, ux_x, uy_y, uz_z

!$OMP PARALLEL 

!$OMP WORKSHARE
    df = gf-f
!$OMP END WORKSHARE

!$OMP DO PRIVATE(ieq,i,j,k,im,ip,jm,jp,km,kp,       &
!$OMP            r0,rm,x0,xm,y0,ym,z0,zm,dr05,drm5, &
!$OMP            vax,vay,vaz,dn0,dsn0,dxn,dyn,dzn,  &
!$OMP            dxvay,dxvaz,dyvax,dyvaz,dzvax,dzvay, &
!$OMP            jcb, sol)
    do k=nzs,nze
       km = k-1
       kp = k+1
    do j=nys,nye
       jm = j-1
       jp = j+1
    do i=nxs,nxe
       im = i-1
       ip = i+1

       ieq=1

       jcb(1) = 0.0D0
       jcb(2) = -0.125*(-f0(5,im,jm,k)-f0(6,im,jm,k) &
                        +f0(5,ip,jm,k)+f0(6,ip,jm,k) &
                        -f0(5,im,j,k)-f0(6,im,j,k)   &
                        +f0(5,ip,j,k)+f0(6,ip,j,k))
       jcb(3) = -0.125*(-f0(7,im,j,km)-f0(8,im,j,km) &
                        +f0(7,ip,j,km)+f0(8,ip,j,km) &
                        -f0(7,im,j,k)-f0(8,im,j,k)   &
                        +f0(7,ip,j,k)+f0(8,ip,j,k))
       jcb(4) = -0.125*(-f0(3,im,jm,k)-f0(4,im,jm,k) &
                        +f0(3,im,jp,k)+f0(4,im,jp,k) &
                        -f0(3,i,jm,k)-f0(4,i,jm,k)   &
                        +f0(3,i,jp,k)+f0(4,i,jp,k))
       jcb(5) = 0.0D0
       jcb(6) = -0.125*(-f0(7,i,jm,km)-f0(8,i,jm,km) &
                        +f0(7,i,jp,km)+f0(8,i,jp,km) &
                        -f0(7,i,jm,k)-f0(8,i,jm,k)   &
                        +f0(7,i,jp,k)+f0(8,i,jp,k))
       jcb(7) = -0.125*(-f0(3,im,j,km)-f0(4,im,j,km) &
                        +f0(3,im,j,kp)+f0(4,im,j,kp) &
                        -f0(3,i,j,km)-f0(4,i,j,km)   &
                        +f0(3,i,j,kp)+f0(4,i,j,kp))
       jcb(8) = -0.125*(-f0(5,i,jm,km)-f0(6,i,jm,km) &
                        +f0(5,i,jm,kp)+f0(6,i,jm,kp) &
                        -f0(5,i,j,km)-f0(6,i,j,km)   &
                        +f0(5,i,j,kp)+f0(6,i,j,kp))
       jcb(9) = 0.0D0

       call invmat(sol,jcb,delt*flg(ieq,i,j,k),dxf(ieq,i,j,k),dyf(ieq,i,j,k),dzf(ieq,i,j,k))

       gdxf(ieq,i,j,k) = sol(1)
       gdyf(ieq,i,j,k) = sol(2)
       gdzf(ieq,i,j,k) = sol(3)


       ieq=2

       jcb(1) = 0.0D0
       jcb(2) = -0.125*(-f0(5,im,jm,k)-f0(6,im,jm,k) &
                        +f0(5,ip,jm,k)+f0(6,ip,jm,k) &
                        -f0(5,im,j,k)-f0(6,im,j,k)   &
                        +f0(5,ip,j,k)+f0(6,ip,j,k))
       jcb(3) = -0.125*(-f0(7,im,j,km)-f0(8,im,j,km) &
                        +f0(7,ip,j,km)+f0(8,ip,j,km) &
                        -f0(7,im,j,k)-f0(8,im,j,k)   &
                        +f0(7,ip,j,k)+f0(8,ip,j,k))
       jcb(4) = -0.125*(-f0(3,im,jm,k)-f0(4,im,jm,k) &
                        +f0(3,im,jp,k)+f0(4,im,jp,k) &
                        -f0(3,i,jm,k)-f0(4,i,jm,k)   &
                        +f0(3,i,jp,k)+f0(4,i,jp,k))
       jcb(5) = 0.0D0
       jcb(6) = -0.125*(-f0(7,i,jm,km)-f0(8,i,jm,km) &
                        +f0(7,i,jp,km)+f0(8,i,jp,km) &
                        -f0(7,i,jm,k)-f0(8,i,jm,k)   &
                        +f0(7,i,jp,k)+f0(8,i,jp,k))
       jcb(7) = -0.125*(-f0(3,im,j,km)-f0(4,im,j,km) &
                        +f0(3,im,j,kp)+f0(4,im,j,kp) &
                        -f0(3,i,j,km)-f0(4,i,j,km)   &
                        +f0(3,i,j,kp)+f0(4,i,j,kp))
       jcb(8) = -0.125*(-f0(5,i,jm,km)-f0(6,i,jm,km) &
                        +f0(5,i,jm,kp)+f0(6,i,jm,kp) &
                        -f0(5,i,j,km)-f0(6,i,j,km)   &
                        +f0(5,i,j,kp)+f0(6,i,j,kp))
       jcb(9) = 0.0D0

       call invmat(sol,jcb,delt*flg(ieq,i,j,k),dxf(ieq,i,j,k),dyf(ieq,i,j,k),dzf(ieq,i,j,k))

       gdxf(ieq,i,j,k) = sol(1)
       gdyf(ieq,i,j,k) = sol(2)
       gdzf(ieq,i,j,k) = sol(3)


       ieq=3

       !dipole field at i+1/2,j,k
       x0 = (i+0.5-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i+0.5-pxm)*drs
       ym = (j-pym)*drs
       zm = (k-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       dxn = dn0*dxn0(x0,r0)
       dyn = dn0*dyn0(y0,r0)
       dzn = dn0*dzn0(z0,r0)
       
       vay   = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       dxvay = dxbdy(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dxn
       dzvay = dzbdy(y0,z0,r0,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dzn

       vaz   = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       dxvaz = dxbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dxn
       dyvaz = dybdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dyn
       !end of dipole 

       jcb(1) = 0.0D0
       jcb(2) = -(+0.5*(-f0(6,i,jm,k)+f0(6,ip,jm,k)-f0(6,i,j,k)+f0(6,ip,j,k))  &
                  -dxvay)
       jcb(3) = -(+0.5*(-f0(8,i,j,km)+f0(8,ip,j,km)-f0(8,i,j,k)+f0(8,ip,j,k)) &
                  -dxvaz)
       jcb(4) = -0.25*(-f0(3,i,jm,k)-f0(4,i,jm,k)+f0(3,i,jp,k)+f0(4,i,jp,k))
       jcb(5) = 0.0D0
       jcb(6) = -(+0.125*(-f0(8,i,jm,km)-f0(8,ip,jm,km) +f0(8,i,jp,km)+f0(8,ip,jp,km) &
                          -f0(8,i,jm,k)-f0(8,ip,jm,k)+f0(8,i,jp,k)+f0(8,ip,jp,k))     &
                  -dyvaz)
       jcb(7) = -0.25*(-f0(3,i,j,km)-f0(4,i,j,km)+f0(3,i,j,kp)+f0(4,i,j,kp))
       jcb(8) = -(+0.125*(-f0(6,i,jm,km)-f0(6,ip,jm,km)+f0(6,i,jm,kp)+f0(6,ip,jm,kp) &
                          -f0(6,i,j,km)-f0(6,ip,j,km)+f0(6,i,j,kp)+f0(6,ip,j,kp))    &
                  -dzvay)
       jcb(9) = 0.0D0

       call invmat(sol,jcb,delt*flg(ieq,i,j,k),dxf(ieq,i,j,k),dyf(ieq,i,j,k),dzf(ieq,i,j,k))

       gdxf(ieq,i,j,k) = sol(1)
       gdyf(ieq,i,j,k) = sol(2)
       gdzf(ieq,i,j,k) = sol(3)


       ieq=4

       jcb(1) = 0.0D0
       jcb(2) = -(+0.5*(-f0(5,i,jm,k)+f0(5,ip,jm,k)-f0(5,i,j,k)+f0(5,ip,j,k)) &
                  +dxvay)
       jcb(3) = -(+0.5*(-f0(7,i,j,km)+f0(7,ip,j,km)-f0(7,i,j,k)+f0(7,ip,j,k)) &
                  +dxvaz)
       jcb(4) = -0.25*(-f0(3,i,jm,k)-f0(4,i,jm,k)+f0(3,i,jp,k)+f0(4,i,jp,k))
       jcb(5) = 0.0D0
       jcb(6) = -(+0.125*(-f0(7,i,jm,km)-f0(7,ip,jm,km) +f0(7,i,jp,km)+f0(7,ip,jp,km)  &
                          -f0(7,i,jm,k)-f0(7,ip,jm,k)+f0(7,i,jp,k)+f0(7,ip,jp,k))      &
                  +dyvaz)
       jcb(7) = -0.25*(-f0(3,i,j,km)-f0(4,i,j,km)+f0(3,i,j,kp)+f0(4,i,j,kp))
       jcb(8) = -(+0.125*(-f0(5,i,jm,km)-f0(5,ip,jm,km)+f0(5,i,jm,kp)+f0(5,ip,jm,kp) &
                          -f0(5,i,j,km)-f0(5,ip,j,km)+f0(5,i,j,kp)+f0(5,ip,j,kp))    &
                  +dzvay)
       jcb(9) = 0.0D0

       call invmat(sol,jcb,delt*flg(ieq,i,j,k),dxf(ieq,i,j,k),dyf(ieq,i,j,k),dzf(ieq,i,j,k))

       gdxf(ieq,i,j,k) = sol(1)
       gdyf(ieq,i,j,k) = sol(2)
       gdzf(ieq,i,j,k) = sol(3)


       ieq=5

       !dipole field at i,j+1/2,k
       x0 = (i-px0)*drs
       y0 = (j+0.5-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i-pxm)*drs
       ym = (j+0.5-pym)*drs
       zm = (k-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)
                  
       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       dxn = dn0*dxn0(x0,r0)
       dyn = dn0*dyn0(y0,r0)
       dzn = dn0*dzn0(z0,r0)

       vax   = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       dyvax = dybdx(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dyn
       dzvax = dzbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dzn

       vaz   = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       dxvaz = dxbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dxn
       dyvaz = dybdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dyn
       !end of dipole

       jcb(1) = 0.0D0
       jcb(2) = -0.25*(-f0(5,im,j,k)-f0(6,im,j,k)+f0(5,ip,j,k)+f0(6,ip,j,k))
       jcb(3) = -(+0.125*(-f0(8,im,j,km)+f0(8,ip,j,km)-f0(8,im,jp,km)+f0(8,ip,jp,km) &
                          -f0(8,im,j,k)+f0(8,ip,j,k)-f0(8,im,jp,k)+f0(8,ip,jp,k))    &
                  -dxvaz)
       jcb(4) = -(+0.5*(-f0(4,im,j,k)+f0(4,im,jp,k)-f0(4,i,j,k)+f0(4,i,jp,k)) &
                  -dyvax)
       jcb(5) = 0.0D0
       jcb(6) = -(+0.5*(-f0(8,i,j,km)+f0(8,i,jp,km)-f0(8,i,j,k)+f0(8,i,jp,k)) &
                  -dyvaz)
       jcb(7) = -(+0.125*(-f0(4,im,j,km)+f0(4,im,j,kp)-f0(4,im,jp,km)+f0(4,im,jp,kp) &
                          -f0(4,i,j,km)+f0(4,i,j,kp)-f0(4,i,jp,km)+f0(4,i,jp,kp))    &
                  -dzvax)
       jcb(8) = -0.25*(-f0(5,i,j,km)-f0(6,i,j,km)+f0(5,i,j,kp)+f0(6,i,j,kp))
       jcb(9) = 0.0D0

       call invmat(sol,jcb,delt*flg(ieq,i,j,k),dxf(ieq,i,j,k),dyf(ieq,i,j,k),dzf(ieq,i,j,k))

       gdxf(ieq,i,j,k) = sol(1)
       gdyf(ieq,i,j,k) = sol(2)
       gdzf(ieq,i,j,k) = sol(3)


       ieq=6

       jcb(1) = 0.0D0
       jcb(2) = -0.25*(-f0(5,im,j,k)-f0(6,im,j,k)+f0(5,ip,j,k)+f0(6,ip,j,k))
       jcb(3) = -(+0.125*(-f0(7,im,j,km)+f0(7,ip,j,km) -f0(7,im,jp,km)+f0(7,ip,jp,km) &
                          -f0(7,im,j,k)+f0(7,ip,j,k)-f0(7,im,jp,k)+f0(7,ip,jp,k))     &
                  +dxvaz)
       jcb(4) = -(+0.5*(-f0(3,im,j,k)+f0(3,im,jp,k)-f0(3,i,j,k)+f0(3,i,jp,k)) &
                  +dyvax)
       jcb(5) = 0.0D0
       jcb(6) = -(+0.5*(-f0(7,i,j,km)+f0(7,i,jp,km)-f0(7,i,j,k)+f0(7,i,jp,k)) &
                  +dyvaz)
       jcb(7) = -(+0.125*(-f0(3,im,j,km)+f0(3,im,j,kp) -f0(3,im,jp,km)+f0(3,im,jp,kp) &
                          -f0(3,i,j,km)+f0(3,i,j,kp)-f0(3,i,jp,km)+f0(3,i,jp,kp))     &
                  +dzvax)
       jcb(8) = -0.25*(-f0(5,i,j,km)-f0(6,i,j,km)+f0(5,i,j,kp)+f0(6,i,j,kp))
       jcb(9) = 0.0D0

       call invmat(sol,jcb,delt*flg(ieq,i,j,k),dxf(ieq,i,j,k),dyf(ieq,i,j,k),dzf(ieq,i,j,k))

       gdxf(ieq,i,j,k) = sol(1)
       gdyf(ieq,i,j,k) = sol(2)
       gdzf(ieq,i,j,k) = sol(3)


       ieq=7

       !dipole field at i,j,k+1/2
       x0 = (i-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k+0.5-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i-pxm)*drs
       ym = (j-pym)*drs
       zm = (k+0.5-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       dxn = dn0*dxn0(x0,r0)
       dyn = dn0*dyn0(y0,r0)
       dzn = dn0*dzn0(z0,r0)

       vax   = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       dyvax = dybdx(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dyn
       dzvax = dzbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dzn

       vay   = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       dxvay = dxbdy(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dxn
       dzvay = dzbdy(y0,z0,r0,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dzn
       !end of dipole 

       jcb(1) = 0.0D0
       jcb(2) = -(+0.125*(-f0(6,im,jm,k)+f0(6,ip,jm,k)-f0(6,im,jm,kp)+f0(6,ip,jm,kp) &
                          -f0(6,im,j,k)+f0(6,ip,j,k)-f0(6,im,j,kp)+f0(6,ip,j,kp))    &
                  -dxvay)
       jcb(3) = -0.25*(-f0(7,im,j,k)-f0(8,im,j,k)+f0(7,ip,j,k)+f0(8,ip,j,k))
       jcb(4) = -(+0.125*(-f0(4,im,jm,k)+f0(4,im,jp,k)-f0(4,im,jm,kp)+f0(4,im,jp,kp) &
                          -f0(4,i,jm,k)+f0(4,i,jp,k)-f0(4,i,jm,kp)+f0(4,i,jp,kp))    &
                  -dyvax)
       jcb(5) = 0.0D0
       jcb(6) = -0.25*(-f0(7,i,jm,k)-f0(8,i,jm,k)+f0(7,i,jp,k)+f0(8,i,jp,k))
       jcb(7) = -(+0.5*(-f0(4,im,j,k)+f0(4,im,j,kp)-f0(4,i,j,k)+f0(4,i,j,kp)) &
                  -dzvax)
       jcb(8) = -(+0.5*(-f0(6,i,jm,k)+f0(6,i,jm,kp)-f0(6,i,j,k)+f0(6,i,j,kp)) &
                 -dzvay)
       jcb(9) = 0.0D0

       call invmat(sol,jcb,delt*flg(ieq,i,j,k),dxf(ieq,i,j,k),dyf(ieq,i,j,k),dzf(ieq,i,j,k))

       gdxf(ieq,i,j,k) = sol(1)
       gdyf(ieq,i,j,k) = sol(2)
       gdzf(ieq,i,j,k) = sol(3)


       ieq=8

       jcb(1) = 0.0D0
       jcb(2) = -(+0.125*(-f0(5,im,jm,k)+f0(5,ip,jm,k) -f0(5,im,jm,kp)+f0(5,ip,jm,kp) &
                          -f0(5,im,j,k)+f0(5,ip,j,k)-f0(5,im,j,kp)+f0(5,ip,j,kp))     &
                  +dxvay)
       jcb(3) = -0.25*(-f0(7,im,j,k)-f0(8,im,j,k)+f0(7,ip,j,k)+f0(8,ip,j,k))
       jcb(4) = -(+0.125*(-f0(3,im,jm,k)+f0(3,im,jp,k) -f0(3,im,jm,kp)+f0(3,im,jp,kp) &
                          -f0(3,i,jm,k)+f0(3,i,jp,k)-f0(3,i,jm,kp)+f0(3,i,jp,kp))     &
                  +dyvax)
       jcb(5) = 0.0D0
       jcb(6) = -0.25*(-f0(7,i,jm,k)-f0(8,i,jm,k)+f0(7,i,jp,k)+f0(8,i,jp,k))
       jcb(7) = -(+0.5*(-f0(3,im,j,k)+f0(3,im,j,kp)-f0(3,i,j,k)+f0(3,i,j,kp)) &
                  +dzvax)
       jcb(8) = -(+0.5*(-f0(5,i,jm,k)+f0(5,i,jm,kp)-f0(5,i,j,k)+f0(5,i,j,kp)) &
                  +dzvay)
       jcb(9) = 0.0D0

       call invmat(sol,jcb,delt*flg(ieq,i,j,k),dxf(ieq,i,j,k),dyf(ieq,i,j,k),dzf(ieq,i,j,k))

       gdxf(ieq,i,j,k) = sol(1)
       gdyf(ieq,i,j,k) = sol(2)
       gdzf(ieq,i,j,k) = sol(3)
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP DO PRIVATE(ieq,i,j,k,im,ip,jm,jp,km,kp,                           &
!$OMP            r0,rm,x0,xm,y0,ym,z0,zm,dr05,drm5,                     &
!$OMP            vax,vay,vaz,dn0,dsn0,dxn,dyn,dzn,                      &
!$OMP            dxvax,dxvay,dxvaz,dyvax,dyvay,dyvaz,dzvax,dzvay,dzvaz, &
!$OMP            src_x,src_y,src_z,ux_x,uy_y,uz_z)
    do k=nzs,nze
       km = k-1
       kp = k+1
    do j=nys,nye
       jm = j-1
       jp = j+1
    do i=nxs,nxe
       im = i-1
       ip = i+1

       ieq=1
       ux_x = 0.5*(-f0(3,im,j,k)-f0(4,im,j,k) &
                   +f0(3,i,j,k)+f0(4,i,j,k))
       uy_y = 0.5*(-f0(5,i,jm,k)-f0(6,i,jm,k) &
                   +f0(5,i,j,k)+f0(6,i,j,k))
       uz_z = 0.5*(-f0(7,i,j,km)-f0(8,i,j,km) &
                   +f0(7,i,j,k)+f0(8,i,j,k)) 

       src_x = +0.5*(-df(ieq,im,j,k)+df(ieq,ip,j,k))*ddelt
       src_y = +0.5*(-df(ieq,i,jm,k)+df(ieq,i,jp,k))*ddelt
       src_z = +0.5*(-df(ieq,i,j,km)+df(ieq,i,j,kp))*ddelt

       gdxf(ieq,i,j,k) = +gdxf(ieq,i,j,k)*dexp(-ux_x*delt*flg(ieq,i,j,k)) &
                         +src_x*delt*flg(ieq,i,j,k)*(+1.-0.5*ux_x*delt*flg(ieq,i,j,k) &
                                                     +fac*(ux_x*delt*flg(ieq,i,j,k))**2)
       gdyf(ieq,i,j,k) = +gdyf(ieq,i,j,k)*dexp(-uy_y*delt*flg(ieq,i,j,k)) &
                         +src_y*delt*flg(ieq,i,j,k)*(+1.-0.5*uy_y*delt*flg(ieq,i,j,k) &
                                                     +fac*(uy_y*delt*flg(ieq,i,j,k))**2)
       gdzf(ieq,i,j,k) = +gdzf(ieq,i,j,k)*dexp(-uz_z*delt*flg(ieq,i,j,k)) &
                         +src_z*delt*flg(ieq,i,j,k)*(+1.-0.5*uz_z*delt*flg(ieq,i,j,k) &
                                                     +fac*(uz_z*delt*flg(ieq,i,j,k))**2)

       ieq=2
       src_x = +0.5*(-df(ieq,im,j,k)+df(ieq,ip,j,k))*ddelt
       src_y = +0.5*(-df(ieq,i,jm,k)+df(ieq,i,jp,k))*ddelt
       src_z = +0.5*(-df(ieq,i,j,km)+df(ieq,i,j,kp))*ddelt

       gdxf(ieq,i,j,k) = +gdxf(ieq,i,j,k)*dexp(-ux_x*delt*flg(ieq,i,j,k)) &
                         +src_x*delt*flg(ieq,i,j,k)*(+1.-0.5*ux_x*delt*flg(ieq,i,j,k) &
                                                     +fac*(ux_x*delt*flg(ieq,i,j,k))**2)
       gdyf(ieq,i,j,k) = +gdyf(ieq,i,j,k)*dexp(-uy_y*delt*flg(ieq,i,j,k)) &
                         +src_y*delt*flg(ieq,i,j,k)*(+1.-0.5*uy_y*delt*flg(ieq,i,j,k) &
                                                     +fac*(uy_y*delt*flg(ieq,i,j,k))**2)
       gdzf(ieq,i,j,k) = +gdzf(ieq,i,j,k)*dexp(-uz_z*delt*flg(ieq,i,j,k)) &
                         +src_z*delt*flg(ieq,i,j,k)*(+1.-0.5*uz_z*delt*flg(ieq,i,j,k) &
                                                     +fac*(uz_z*delt*flg(ieq,i,j,k))**2)

       ieq=3
       !dipole field at i+1/2,j,k
       x0 = (i+0.5-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i+0.5-pxm)*drs
       ym = (j-pym)*drs
       zm = (k-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       dxn = dn0*dxn0(x0,r0)
       dyn = dn0*dyn0(y0,r0)
       dzn = dn0*dzn0(z0,r0)
       
       vax   = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       dxvax = dxbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dxn
       dyvax = dybdx(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dyn
       dzvax = dzbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dzn

       vay   = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       dyvay = dybdy(y0,z0,r0,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dyn

       vaz   = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       dzvaz = dzbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dzn
       !end of dipole 

       ux_x = 0.25*(-f0(3,im,j,k)-f0(4,im,j,k)  &
                    +f0(3,ip,j,k)+f0(4,ip,j,k))
       uy_y = (+0.5*(-f0(6,i,jm,k)-f0(6,ip,jm,k) &
                     +f0(6,i,j,k)+f0(6,ip,j,k))  &
               -dyvay)
       uz_z = (+0.5*(-f0(8,i,j,km)-f0(8,ip,j,km) &
                     +f0(8,i,j,k)+f0(8,ip,j,k))  &
               -dzvaz)

       src_x = +0.5*(-df(ieq,im,j,k)+df(ieq,ip,j,k))*ddelt                          &
               -0.5*(-u0(3,im,j,k)+u0(3,ip,j,k))*(+dxvax)                           &
               -0.5*(-u0(6,i,jm,k)+u0(6,ip,jm,k)-u0(6,i,j,k)+u0(6,ip,j,k))*(+dyvax) &
               -0.5*(-u0(8,i,j,km)+u0(8,ip,j,km)-u0(8,i,j,k)+u0(8,ip,j,k))*(+dzvax)
       src_y = +0.5*(-df(ieq,i,jm,k)+df(ieq,i,jp,k))*ddelt                          &
               -0.5*(-u0(3,i,jm,k)+u0(3,i,jp,k))*(+dxvax)                          &
               -0.5*(-u0(6,i,jm,k)-u0(6,ip,jm,k)+u0(6,i,j,k)+u0(6,ip,j,k))*(+dyvax) &
               -0.125*(-u0(8,i,jm,km)-u0(8,ip,jm,km)+u0(8,i,jp,km)+u0(8,ip,jp,km)   &
                       -u0(8,i,jm,k)-u0(8,ip,jm,k)+u0(8,i,jp,k)+u0(8,ip,jp,k))*(+dzvax)
       src_z = +0.5*(-df(ieq,i,j,km)+df(ieq,i,j,kp))*ddelt                              &
               -0.5*(-u0(3,i,j,km)+u0(3,i,j,kp))*(+dxvax)                              &
               -0.125*(-u0(6,i,jm,km)-u0(6,ip,jm,km)+u0(6,i,jm,kp)+u0(6,ip,jm,kp)       &
                       -u0(6,i,j,km)-u0(6,ip,j,km)+u0(6,i,j,kp)+u0(6,ip,j,kp))*(+dyvax) &
               -0.5*(-u0(8,i,j,km)-u0(8,ip,j,km)+u0(8,i,j,k)+u0(8,ip,j,k))*(+dzvax)

       gdxf(ieq,i,j,k) = +gdxf(ieq,i,j,k)*dexp(-ux_x*delt*flg(ieq,i,j,k)) &
                         +src_x*delt*flg(ieq,i,j,k)*(+1.-0.5*ux_x*delt*flg(ieq,i,j,k) &
                                                     +fac*(ux_x*delt*flg(ieq,i,j,k))**2)
       gdyf(ieq,i,j,k) = +gdyf(ieq,i,j,k)*dexp(-uy_y*delt*flg(ieq,i,j,k)) &
                         +src_y*delt*flg(ieq,i,j,k)*(+1.-0.5*uy_y*delt*flg(ieq,i,j,k) &
                                                     +fac*(uy_y*delt*flg(ieq,i,j,k))**2)
       gdzf(ieq,i,j,k) = +gdzf(ieq,i,j,k)*dexp(-uz_z*delt*flg(ieq,i,j,k)) &
                         +src_z*delt*flg(ieq,i,j,k)*(+1.-0.5*uz_z*delt*flg(ieq,i,j,k) &
                                                     +fac*(uz_z*delt*flg(ieq,i,j,k))**2)

       ieq=4
       uy_y = (+0.5*(-f0(5,i,jm,k)-f0(5,ip,jm,k) &
                     +f0(5,i,j,k)+f0(5,ip,j,k))  &
               +dyvay)
       uz_z = (+0.5*(-f0(7,i,j,km)-f0(7,ip,j,km) &
                     +f0(7,i,j,k)+f0(7,ip,j,k))  &
               +dzvaz) 

       src_x = +0.5*(-df(ieq,im,j,k)+df(ieq,ip,j,k))*ddelt                           &
               -0.5*(-u0(3,im,j,k)+u0(3,ip,j,k))*(-dxvax) &
               -0.5*(-u0(5,i,jm,k)+u0(5,ip,jm,k)-u0(5,i,j,k)+u0(5,ip,j,k))*(-dyvax)  &
               -0.5*(-u0(7,i,j,km)+u0(7,ip,j,km)-u0(7,i,j,k)+u0(7,ip,j,k))*(-dzvax)
       src_y = +0.5*(-df(ieq,i,jm,k)+df(ieq,i,jp,k))*ddelt                           &
               -0.5*(-u0(3,i,jm,k)+u0(3,i,jp,k))*(-dxvax) &
               -0.5*(-u0(5,i,jm,k)-u0(5,ip,jm,k)+u0(5,i,j,k)+u0(5,ip,j,k))*(-dyvax)  &
               -0.125*(-u0(7,i,jm,km)-u0(7,ip,jm,km)+u0(7,i,jp,km)+u0(7,ip,jp,km)    &
                       -u0(7,i,jm,k)-u0(7,ip,jm,k)+u0(7,i,jp,k)+u0(7,ip,jp,k))*(-dzvax)
       src_z = +0.5*(-df(ieq,i,j,km)+df(ieq,i,j,kp))*ddelt                              &
               -0.5*(-u0(3,i,j,km)+u0(3,i,j,kp))*(-dxvax)    &
               -0.125*(-u0(5,i,jm,km)-u0(5,ip,jm,km)+u0(5,i,jm,kp)+u0(5,ip,jm,kp)       &
                       -u0(5,i,j,km)-u0(5,ip,j,km)+u0(5,i,j,kp)+u0(5,ip,j,kp))*(-dyvax) &
               -0.5*(-u0(7,i,j,km)-u0(7,ip,j,km)+u0(7,i,j,k)+u0(7,ip,j,k))*(-dzvax)

       gdxf(ieq,i,j,k) = +gdxf(ieq,i,j,k)*dexp(-ux_x*delt*flg(ieq,i,j,k)) &
                         +src_x*delt*flg(ieq,i,j,k)*(+1.-0.5*ux_x*delt*flg(ieq,i,j,k) &
                                                     +fac*(ux_x*delt*flg(ieq,i,j,k))**2)
       gdyf(ieq,i,j,k) = +gdyf(ieq,i,j,k)*dexp(-uy_y*delt*flg(ieq,i,j,k)) &
                         +src_y*delt*flg(ieq,i,j,k)*(+1.-0.5*uy_y*delt*flg(ieq,i,j,k) &
                                                     +fac*(uy_y*delt*flg(ieq,i,j,k))**2)
       gdzf(ieq,i,j,k) = +gdzf(ieq,i,j,k)*dexp(-uz_z*delt*flg(ieq,i,j,k)) &
                         +src_z*delt*flg(ieq,i,j,k)*(+1.-0.5*uz_z*delt*flg(ieq,i,j,k) &
                                                     +fac*(uz_z*delt*flg(ieq,i,j,k))**2)

       ieq=5
       !dipole field at i,j+1/2,k
       x0 = (i-px0)*drs
       y0 = (j+0.5-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i-pxm)*drs
       ym = (j+0.5-pym)*drs
       zm = (k-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)
                  
       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       dxn = dn0*dxn0(x0,r0)
       dyn = dn0*dyn0(y0,r0)
       dzn = dn0*dzn0(z0,r0)

       vax   = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       dxvax = dxbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dxn

       vay   = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       dxvay = dxbdy(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dxn
       dyvay = dybdy(y0,z0,r0,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dyn
       dzvay = dzbdy(y0,z0,r0,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dzn

       vaz   = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       dzvaz = dzbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dzn
       !end of dipole

       ux_x = (+0.5*(-f0(4,im,j,k)+f0(4,i,j,k)    &
                     -f0(4,im,jp,k)+f0(4,i,jp,k)) &
               -dxvax)
       uy_y = 0.25*(-f0(5,i,jm,k)-f0(6,i,jm,k)    &
                    +f0(5,i,jp,k)+f0(6,i,jp,k)) 
       uz_z = (+0.5*(-f0(8,i,j,km)-f0(8,i,jp,km)  &
                     +f0(8,i,j,k)+f0(8,i,jp,k))   &
               -dzvaz) 

       src_x = +0.5*(-df(ieq,im,j,k)+df(ieq,ip,j,k))*ddelt                       &
               -0.5*(-u0(4,im,j,k)+u0(4,i,j,k)                                   &
                     -u0(4,im,jp,k)+u0(4,i,jp,k))*(+dxvay)                       &
               -0.5*(-u0(5,im,j,k)+u0(5,ip,j,k))*(+dyvay)                       &
               -0.125*(-u0(8,im,j,km)+u0(8,ip,j,km)-u0(8,im,jp,km)+u0(8,ip,jp,km)&
                       -u0(8,im,j,k)+u0(8,ip,j,k)-u0(8,im,jp,k)+u0(8,ip,jp,k))*(+dzvay)
       src_y = +0.5*(-df(ieq,i,jm,k)+df(ieq,i,jp,k))*ddelt                           &
               -0.5*(-u0(4,im,j,k)+u0(4,im,jp,k)-u0(4,i,j,k)+u0(4,i,jp,k))*(+dxvay)  &
               -0.5*(-u0(5,i,jm,k)+u0(5,i,jp,k))*(+dyvay) &
               -0.5*(-u0(8,i,j,km)+u0(8,i,jp,km)-u0(8,i,j,k)+u0(8,i,jp,k))*(+dzvay)
       src_z = +0.5*(-df(ieq,i,j,km)+df(ieq,i,j,kp))*ddelt         &
               -0.125*(-u0(4,im,j,km)+u0(4,im,j,kp)-u0(4,im,jp,km)+u0(4,im,jp,kp)       &
                       -u0(4,i,j,km)+u0(4,i,j,kp)-u0(4,i,jp,km)+u0(4,i,jp,kp))*(+dxvay) &
               -0.5*(-u0(5,i,j,km)+u0(5,i,j,kp))*(+dyvay)    &
               -0.5*(-u0(8,i,j,km)-u0(8,i,jp,km)+u0(8,i,j,k)+u0(8,i,jp,k))*(+dzvay)

       gdxf(ieq,i,j,k) = +gdxf(ieq,i,j,k)*dexp(-ux_x*delt*flg(ieq,i,j,k)) &
                         +src_x*delt*flg(ieq,i,j,k)*(+1.-0.5*ux_x*delt*flg(ieq,i,j,k) &
                                                     +fac*(ux_x*delt*flg(ieq,i,j,k))**2)
       gdyf(ieq,i,j,k) = +gdyf(ieq,i,j,k)*dexp(-uy_y*delt*flg(ieq,i,j,k)) &
                         +src_y*delt*flg(ieq,i,j,k)*(+1.-0.5*uy_y*delt*flg(ieq,i,j,k) &
                                                     +fac*(uy_y*delt*flg(ieq,i,j,k))**2)
       gdzf(ieq,i,j,k) = +gdzf(ieq,i,j,k)*dexp(-uz_z*delt*flg(ieq,i,j,k)) &
                         +src_z*delt*flg(ieq,i,j,k)*(+1.-0.5*uz_z*delt*flg(ieq,i,j,k) &
                                                     +fac*(uz_z*delt*flg(ieq,i,j,k))**2)

       ieq=6
       ux_x = (+0.5*(-f0(3,im,j,k)+f0(3,i,j,k)    &
                     -f0(3,im,jp,k)+f0(3,i,jp,k)) &
               +dxvax)
       uz_z = (+0.5*(-f0(7,i,j,km)-f0(7,i,jp,km)  &
                     +f0(7,i,j,k)+f0(7,i,jp,k))   &
               +dzvaz) 

       src_x = +0.5*(-df(ieq,im,j,k)+df(ieq,ip,j,k))*ddelt                             &
               -0.5*(-u0(3,im,j,k)+u0(3,i,j,k)-u0(3,im,jp,k)+u0(3,i,jp,k))*(-dxvay)    &
               -0.5*(-u0(5,im,j,k)+u0(5,ip,j,k))*(-dyvay)   & 
               -0.125*(-u0(7,im,j,km)+u0(7,ip,j,km)-u0(7,im,jp,km)+u0(7,ip,jp,km)      &
                       -u0(7,im,j,k)+u0(7,ip,j,k)-u0(7,im,jp,k)+u0(7,ip,jp,k))*(-dzvay)
       src_y = +0.5*(-df(ieq,i,jm,k)+df(ieq,i,jp,k))*ddelt                           &
               -0.5*(-u0(3,im,j,k)+u0(3,im,jp,k)-u0(3,i,j,k)+u0(3,i,jp,k))*(-dxvay)  &
               -0.5*(-u0(5,i,jm,k)+u0(5,i,jp,k))*(-dyvay) &
               -0.5*(-u0(7,i,j,km)+u0(7,i,jp,km)-u0(7,i,j,k)+u0(7,i,jp,k))*(-dzvay)
       src_z = +0.5*(-df(ieq,i,j,km)+df(ieq,i,j,kp))*ddelt         &
               -0.125*(-u0(3,im,j,km)+u0(3,im,j,kp)-u0(3,im,jp,km)+u0(3,im,jp,kp)       &
                       -u0(3,i,j,km)+u0(3,i,j,kp)-u0(3,i,jp,km)+u0(3,i,jp,kp))*(-dxvay) &
               -0.5*(-u0(5,i,j,km)+u0(5,i,j,kp))*(-dyvay)    &
               -0.5*(-u0(7,i,j,km)-u0(7,i,jp,km)+u0(7,i,j,k)+u0(7,i,jp,k))*(-dzvay)

       gdxf(ieq,i,j,k) = +gdxf(ieq,i,j,k)*dexp(-ux_x*delt*flg(ieq,i,j,k)) &
                         +src_x*delt*flg(ieq,i,j,k)*(+1.-0.5*ux_x*delt*flg(ieq,i,j,k) &
                                                     +fac*(ux_x*delt*flg(ieq,i,j,k))**2)
       gdyf(ieq,i,j,k) = +gdyf(ieq,i,j,k)*dexp(-uy_y*delt*flg(ieq,i,j,k)) &
                         +src_y*delt*flg(ieq,i,j,k)*(+1.-0.5*uy_y*delt*flg(ieq,i,j,k) &
                                                     +fac*(uy_y*delt*flg(ieq,i,j,k))**2)
       gdzf(ieq,i,j,k) = +gdzf(ieq,i,j,k)*dexp(-uz_z*delt*flg(ieq,i,j,k)) &
                         +src_z*delt*flg(ieq,i,j,k)*(+1.-0.5*uz_z*delt*flg(ieq,i,j,k) &
                                                     +fac*(uz_z*delt*flg(ieq,i,j,k))**2)

       ieq=7
       !dipole field at i,j,k+1/2
       x0 = (i-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k+0.5-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i-pxm)*drs
       ym = (j-pym)*drs
       zm = (k+0.5-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       dxn = dn0*dxn0(x0,r0)
       dyn = dn0*dyn0(y0,r0)
       dzn = dn0*dzn0(z0,r0)

       vax   = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       dxvax = dxbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dxn

       vay   = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       dyvay = dybdy(y0,z0,r0,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dyn

       vaz   = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       dxvaz = dxbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dxn
       dyvaz = dybdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dyn
       dzvaz = dzbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dzn
       !end of dipole 

       ux_x = (+0.5*(-f0(4,im,j,k)+f0(4,i,j,k)    &
                     -f0(4,im,j,kp)+f0(4,i,j,kp)) &
               -dxvax)
       uy_y = (+0.5*(-f0(6,i,jm,k)+f0(6,i,j,k)    &
                     -f0(6,i,jm,kp)+f0(6,i,j,kp)) &
               -dyvay) 
       uz_z = 0.25*(-f0(7,i,j,km)-f0(8,i,j,km)   &
                    +f0(7,i,j,kp)+f0(8,i,j,kp))  

       src_x = +0.5*(-df(ieq,im,j,k)+df(ieq,ip,j,k))*ddelt                              &
               -0.5*(-u0(4,im,j,k)+u0(4,i,j,k)-u0(4,im,j,kp)+u0(4,i,j,kp))*(+dxvaz)     &
               -0.125*(-u0(6,im,jm,k)+u0(6,ip,jm,k)-u0(6,im,jm,kp)+u0(6,ip,jm,kp)       &
                       -u0(6,im,j,k)+u0(6,ip,j,k)-u0(6,im,j,kp)+u0(6,ip,j,kp))*(+dyvaz) &
               -0.5*(-u0(7,im,j,k)+u0(7,ip,j,k))*(+dzvaz)
       src_y = +0.5*(-df(ieq,i,jm,k)+df(ieq,i,jp,k))*ddelt         &
               -0.125*(-u0(4,im,jm,k)+u0(4,im,jp,k)-u0(4,im,jm,kp)+u0(4,im,jp,kp)       &
                       -u0(4,i,jm,k)+u0(4,i,jp,k)-u0(4,i,jm,kp)+u0(4,i,jp,kp))*(+dxvaz) &
               -0.5*(-u0(6,i,jm,k)+u0(6,i,j,k)-u0(6,i,jm,kp)+u0(6,i,j,kp))*(+dyvaz)     &
               -0.5*(-u0(7,i,jm,k)+u0(7,i,jp,k))*(+dzvaz)    
       src_z = +0.5*(-df(ieq,i,j,km)+df(ieq,i,j,kp))*ddelt                           &
               -0.5*(-u0(4,im,j,k)+u0(4,im,j,kp)-u0(4,i,j,k)+u0(4,i,j,kp))*(+dxvaz)  &
               -0.5*(-u0(6,i,jm,k)+u0(6,i,jm,kp)-u0(6,i,j,k)+u0(6,i,j,kp))*(+dyvaz)  &
               -0.5*(-u0(7,i,j,km)+u0(7,i,j,kp))*(+dzvaz) 

       gdxf(ieq,i,j,k) = +gdxf(ieq,i,j,k)*dexp(-ux_x*delt*flg(ieq,i,j,k)) &
                         +src_x*delt*flg(ieq,i,j,k)*(+1.-0.5*ux_x*delt*flg(ieq,i,j,k) &
                                                     +fac*(ux_x*delt*flg(ieq,i,j,k))**2)
       gdyf(ieq,i,j,k) = +gdyf(ieq,i,j,k)*dexp(-uy_y*delt*flg(ieq,i,j,k)) &
                         +src_y*delt*flg(ieq,i,j,k)*(+1.-0.5*uy_y*delt*flg(ieq,i,j,k) &
                                                     +fac*(uy_y*delt*flg(ieq,i,j,k))**2)
       gdzf(ieq,i,j,k) = +gdzf(ieq,i,j,k)*dexp(-uz_z*delt*flg(ieq,i,j,k)) &
                         +src_z*delt*flg(ieq,i,j,k)*(+1.-0.5*uz_z*delt*flg(ieq,i,j,k) &
                                                     +fac*(uz_z*delt*flg(ieq,i,j,k))**2)

       ieq=8
       ux_x = (+0.5*(-f0(3,im,j,k)+f0(3,i,j,k)    &
                     -f0(3,im,j,kp)+f0(3,i,j,kp)) &
               +dxvax)
       uy_y = (+0.5*(-f0(5,i,jm,k)+f0(5,i,j,k)    &
                     -f0(5,i,jm,kp)+f0(5,i,j,kp)) &
               +dyvay) 

       src_x = +0.5*(-df(ieq,im,j,k)+df(ieq,ip,j,k))*ddelt                              &
               -0.5*(-u0(3,im,j,k)+u0(3,i,j,k)-u0(3,im,j,kp)+u0(3,i,j,kp))*(-dxvaz)     &
               -0.125*(-u0(5,im,jm,k)+u0(5,ip,jm,k)-u0(5,im,jm,kp)+u0(5,ip,jm,kp)       &
                       -u0(5,im,j,k)+u0(5,ip,j,k)-u0(5,im,j,kp)+u0(5,ip,j,kp))*(-dyvaz) &
               -0.5*(-u0(7,im,j,k)+u0(7,ip,j,k))*(-dzvaz)    
       src_y = +0.5*(-df(ieq,i,jm,k)+df(ieq,i,jp,k))*ddelt                              &
               -0.125*(-u0(3,im,jm,k)+u0(3,im,jp,k)-u0(3,im,jm,kp)+u0(3,im,jp,kp)       &
                       -u0(3,i,jm,k)+u0(3,i,jp,k)-u0(3,i,jm,kp)+u0(3,i,jp,kp))*(-dxvaz) &
               -0.5*(-u0(5,i,jm,k)+u0(5,i,j,k)-u0(5,i,jm,kp)+u0(5,i,j,kp))*(-dyvaz)     &
               -0.5*(-u0(7,i,jm,k)+u0(7,i,jp,k))*(-dzvaz)    
       src_z = +0.5*(-df(ieq,i,j,km)+df(ieq,i,j,kp))*ddelt                           &
               -0.5*(-u0(3,im,j,k)+u0(3,im,j,kp)-u0(3,i,j,k)+u0(3,i,j,kp))*(-dxvaz)  &
               -0.5*(-u0(5,i,jm,k)+u0(5,i,jm,kp)-u0(5,i,j,k)+u0(5,i,j,kp))*(-dyvaz)  &
               -0.5*(-u0(7,i,j,km)+u0(7,i,j,kp))*(-dzvaz) 

       gdxf(ieq,i,j,k) = +gdxf(ieq,i,j,k)*dexp(-ux_x*delt*flg(ieq,i,j,k)) &
                         +src_x*delt*flg(ieq,i,j,k)*(+1.-0.5*ux_x*delt*flg(ieq,i,j,k) &
                                                     +fac*(ux_x*delt*flg(ieq,i,j,k))**2)
       gdyf(ieq,i,j,k) = +gdyf(ieq,i,j,k)*dexp(-uy_y*delt*flg(ieq,i,j,k)) &
                         +src_y*delt*flg(ieq,i,j,k)*(+1.-0.5*uy_y*delt*flg(ieq,i,j,k) &
                                                     +fac*(uy_y*delt*flg(ieq,i,j,k))**2)
       gdzf(ieq,i,j,k) = +gdzf(ieq,i,j,k)*dexp(-uz_z*delt*flg(ieq,i,j,k)) &
                         +src_z*delt*flg(ieq,i,j,k)*(+1.-0.5*uz_z*delt*flg(ieq,i,j,k) &
                                                     +fac*(uz_z*delt*flg(ieq,i,j,k))**2)
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP DO PRIVATE(ieq,i,j,k,im,ip,jm,jp,km,kp)
    do k=nzs,nze
       km = k-1
       kp = k+1
    do j=nys,nye
       jm = j-1
       jp = j+1
    do i=nxs,nxe
       im = i-1
       ip = i+1
    do ieq=1,neq
       gdxf(ieq,i,j,k) = gdxf(ieq,i,j,k)+dre2*                  &
            (+dxf(ieq,i,j,km)+dxf(ieq,i,jm,k)                   &
             +dxf(ieq,im,j,k)-6.*dxf(ieq,i,j,k)+dxf(ieq,ip,j,k) &
             +dxf(ieq,i,jp,k)+dxf(ieq,i,j,kp)                   &
            )*flg(ieq,i,j,k)*delt

       gdyf(ieq,i,j,k) = gdyf(ieq,i,j,k)+dre2*                  &
            (+dyf(ieq,i,j,km)+dyf(ieq,i,jm,k)                   &
             +dyf(ieq,im,j,k)-6.*dyf(ieq,i,j,k)+dyf(ieq,ip,j,k) &
             +dyf(ieq,i,jp,k)+dyf(ieq,i,j,kp)                   &
            )*flg(ieq,i,j,k)*delt

       gdzf(ieq,i,j,k) = gdzf(ieq,i,j,k)+dre2*                  &
            (+dzf(ieq,i,j,km)+dzf(ieq,i,jm,k)                   &
             +dzf(ieq,im,j,k)-6.*dzf(ieq,i,j,k)+dzf(ieq,ip,j,k) &
             +dzf(ieq,i,jp,k)+dzf(ieq,i,j,kp)                   &
            )*flg(ieq,i,j,k)*delt
    enddo
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT
       
!$OMP END PARALLEL

  end subroutine non_advection__df


  subroutine invmat(sol,jcb,dt,dxf,dyf,dzf)

    real(8), intent(out) :: sol(3)
    real(8), intent(in)  :: jcb(9), dt, dxf, dyf, dzf
    real(8)              :: detmat, imat(9), mat(9), mat0(9)

    mat0(1) = 1.+dt*(1.-theta)*jcb(1)
    mat0(2) =   +dt*(1.-theta)*jcb(2)
    mat0(3) =   +dt*(1.-theta)*jcb(3)
    mat0(4) =   +dt*(1.-theta)*jcb(4)
    mat0(5) = 1.+dt*(1.-theta)*jcb(5)
    mat0(6) =   +dt*(1.-theta)*jcb(6)
    mat0(7) =   +dt*(1.-theta)*jcb(7)
    mat0(8) =   +dt*(1.-theta)*jcb(8)
    mat0(9) = 1.+dt*(1.-theta)*jcb(9)
    
    mat(1) = 1.-dt*theta*jcb(1)
    mat(2) =   -dt*theta*jcb(2)
    mat(3) =   -dt*theta*jcb(3)
    mat(4) =   -dt*theta*jcb(4)
    mat(5) = 1.-dt*theta*jcb(5)
    mat(6) =   -dt*theta*jcb(6)
    mat(7) =   -dt*theta*jcb(7)
    mat(8) =   -dt*theta*jcb(8)
    mat(9) = 1.-dt*theta*jcb(9)

    detmat = mat(1)*mat(5)*mat(9)+mat(2)*mat(6)*mat(7)+mat(3)*mat(4)*mat(8) &
            -mat(1)*mat(6)*mat(8)-mat(2)*mat(4)*mat(9)-mat(3)*mat(5)*mat(7)

    imat(1) = mat(5)*mat(9)-mat(6)*mat(8)
    imat(2) = mat(3)*mat(8)-mat(2)*mat(9)
    imat(3) = mat(2)*mat(6)-mat(3)*mat(5)
    imat(4) = mat(6)*mat(7)-mat(4)*mat(9)
    imat(5) = mat(1)*mat(9)-mat(3)*mat(7)
    imat(6) = mat(3)*mat(4)-mat(1)*mat(6)
    imat(7) = mat(4)*mat(8)-mat(5)*mat(7)
    imat(8) = mat(2)*mat(7)-mat(1)*mat(8)
    imat(9) = mat(1)*mat(5)-mat(2)*mat(4)

    imat(1:9) = imat(1:9)/detmat

    sol(1) = +(+imat(1)*mat0(1)+imat(2)*mat0(4)+imat(3)*mat0(7))*dxf &
             +(+imat(1)*mat0(2)+imat(2)*mat0(5)+imat(3)*mat0(8))*dyf &
             +(+imat(1)*mat0(3)+imat(2)*mat0(6)+imat(3)*mat0(9))*dzf
    sol(2) = +(+imat(4)*mat0(1)+imat(5)*mat0(4)+imat(6)*mat0(7))*dxf &
             +(+imat(4)*mat0(2)+imat(5)*mat0(5)+imat(6)*mat0(8))*dyf &
             +(+imat(4)*mat0(3)+imat(5)*mat0(6)+imat(6)*mat0(9))*dzf
    sol(3) = +(+imat(7)*mat0(1)+imat(8)*mat0(4)+imat(9)*mat0(7))*dxf &
             +(+imat(7)*mat0(2)+imat(8)*mat0(5)+imat(9)*mat0(8))*dyf &
             +(+imat(7)*mat0(3)+imat(8)*mat0(6)+imat(9)*mat0(9))*dzf

  end subroutine invmat


end module non_advection

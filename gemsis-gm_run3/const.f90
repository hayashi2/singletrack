module const

  implicit none

!---- NUMERICAL CONSTANTS ----!
  integer, parameter :: nx   = 500
  integer, parameter :: ny   = 300
  integer, parameter :: nz   = 300
  integer, parameter :: nxgs = 2
  integer, parameter :: nxge = nxgs+nx-1
  integer, parameter :: nygs = 2
  integer, parameter :: nyge = nygs+ny-1
  integer, parameter :: nzgs = 2
  integer, parameter :: nzge = nzgs+nz-1
  integer, parameter :: neq  = 8 !# OF MHD EQS.

  !# OF PROCS.
  integer, parameter :: nproc   = 60                      !NUMBER OF PROCESSORS
  integer, parameter :: nproc_i = 10                       !NUM OF PROCS IN X
  integer, parameter :: nproc_j = 6                       !NUM OF PROCS IN Y
  integer, parameter :: nproc_k = nproc/(nproc_i*nproc_j) !NUM OF PROCS IN Z

  !OUTPUT FILES AND DIRECTORY
  character(len=128) :: dir_prt = '/work/m_hayashi/GM/run3/'    !DIR FOR INIT PARAMS
  character(len=128) :: dir_in  = '/work/ymatumot/GM/base2/'    !DIR OF RESTART DATA FOR INPUT
  character(len=128) :: dir_out = '/work/m_hayashi/GM/run3/'    !DIR OF RESTART DATA FOR OUTPUT
  character(len=128) :: file9   = 'init_param.dat' !INIT PARAMS
  character(len=128) :: file10  = 'interm_tmp'     !RESTART DATA NAME
  character(len=128) :: file11  = 'interm_tmp'     !RESTART DATA NAME

  !TIME COUNTERS
  logical, parameter :: isinit = .false. !TRUE:INIT OF RUN, FALSE:FROM RESTART DATA
  integer, parameter :: itmax  = 400000  !# OF MAXIMUM ITERATIONS
  integer, parameter :: intvl1 = 500 !INTERVAL FOR DATA OUTPUT
  integer, parameter :: intvl2 = 10000  !INTERVAL FOR RESTART DATA OUTPUT
  real(8), parameter :: cfl    = 0.3D0  !CFL NUMBER
!  real(8), parameter :: etlim  = 5.*60. !MAXIMUM ELAPSE TIME IN SEC.
  real(8), parameter :: etlim  = 5.*24.*60.*60. !MAXIMUM ELAPSE TIME IN SEC.


!---- PHYSICAL PARAMETERS ----!
  real(8), parameter :: delx   = 1.0D0     !UNIT IN SPACIAL LENGTH
  real(8), parameter :: gam    = 5.D0/3.D0 !SPECIFIC HEAT RATIO  
  real(8), parameter :: ub0    = 1.0D0     !CORRESPONDS TO 10nT AT R=14.58(RE) IN THE EQUATOR
  real(8), parameter :: un0    = 1.0D0     !/CC AT R=14.58(RE) IN THE EQUATOR
  real(8), parameter :: up0    = 1.0D0     !NORMALIZED BY B0^2/(8PI) (=BETA VALUE)
  real(8), parameter :: va0    = 1.0D0     !220 KM/S AT R=14.58(RE) WITH N=N0, B=B0

  !EARTH POSITION & DIPOLE FIELDS
  real(8), parameter :: rs     = 4.0D0*delx  !EARTH RADIUS
  real(8), parameter :: x0     = nxgs+30.0D0*rs+0.01D0
  real(8), parameter :: y0     = nygs+(ny-1.0D0)*0.5D0 !FULL CALC.
  real(8), parameter :: z0     = nzgs+(nz-1.0D0)*0.5D0 !FULL CALC.
!  real(8), parameter :: y0     = nyge  !QUADRANT CALC.
!  real(8), parameter :: z0     = nzgs  !QUADRANT CALC.
  real(8)            :: px0 = x0, py0 = y0, pz0 = z0
  real(8)            :: rin    = 4.0D0       !INNER BOUNDARY (RE)
  real(8)            :: Bm     = 3.108D3     !DIPOLE MOMENT IN THE SIMULATION UNIT
  real(8)            :: drs    = 1.D0/rs
  !MIRROR DIPOLE 
  real(8)            :: pxm    = 2.*nxgs-x0 
  real(8)            :: pym    = y0   
  real(8)            :: pzm    = z0
  !PLASMA SPHERE-LIKE
  real(8)            :: unmax  = 1.0D0*un0 !AT INNER BOUNDARY
  real(8)            :: unmin  = 1.0D0*un0 !AT OUTER BOUNDARY

  !SOLAR WIND PARAMETERS AT I=NXGS-1 AT T=0 (NORMALIZATIONS: V0=220KM/S, N0=1/cc, B0=10nT)
  real(8)            :: uvx_sw  = va0*800.0D0/220.D0, uvy_sw = 0.0D0, uvz_sw = 0.0D0
  real(8)            :: ubx_sw  = 0.0D0, uby_sw = 0.0D0, ubz_sw = 0.3D0
  real(8)            :: un_sw   = un0*1.0D0
  real(8)            :: beta_sw = 5.0D0

  !SOLAR WIND PARAMETER VARIATIONS PER DT_SW
  real(8), parameter :: dt_sw  = 11.0D0            !INTERVAL FOR CHANGING SW CONDITIONS (SEC.)
  real(8)            :: uvx_sw_lim = 650.0/220.0D0 !DESIRED VALUE OF SOLAR WIND Vx AT T>0
  real(8)            :: un_sw_lim = 3.0            !DESIRED VALUE OF SOLAR WIND N AT T>0
  real(8)            :: duvx_sw = ( 650.0D0 - 400.0D0)/4.0/220.0D0
  real(8)            :: dun_sw = (3.0D0 - 3.0D0)/4.0D0

  character(len=128) :: filesw  = '/work/m_hayashi/20170716_0500_0700_sw.dat'  !SOLAR WIND DATA FILE NAME !Now, no data

  !REYNOLDS NUMBER IN THE SIMULATION DOMAIN
  real(8), parameter :: re0  = 5.0D0
  real(8), parameter :: rb0  = 5.0D0
  real(8), parameter :: dre0 = 1.0D0/re0
  real(8), parameter :: drb0 = 1.0D0/rb0
  !REYNOLDS NUMBER IN THE VICINITY OF THE INNER BOUNDARY
  real(8), parameter :: re1  = 1.0D0
  real(8), parameter :: rb1  = 1.0D0
  real(8), parameter :: dre1 = 1.0D0/re1
  real(8), parameter :: drb1 = 1.0D0/rb1
  !REYNOLDS NUMBER FOR DF
  real(8), parameter :: re2  = 5.0D0
  real(8), parameter :: dre2 = 1.D0/re2

  !CRITICAL CURRENT DENSITY FOR ANOMALOUS RESISTIVITY
  real(8), parameter :: ccr  = 0.5D0, dccr=1.0D0/(0.1*ccr) 

end module const

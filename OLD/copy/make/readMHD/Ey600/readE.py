# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 13:30:36 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches

epath ="/work/ymatumot/simulation/GM-RB/run_Vsw600/"
ename = "MHD_E"
num = 0
while(num <=208):
    filename = epath + ename + str(num).zfill(3) +".dat"
    
    head = ("head","<i")
    tail = ("tail","<i")
    b = ("b","<51750171f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*2.2
    list=bf.transpose(1,0,2,3)
    
    figure(figsize=(12,10))
    title("Vsw600  time="+str(num*12)+"s")
    xlabel("x(GSM)[Re]",fontsize=20)
    ylabel("y(GSM)[Re]",fontsize=20)
    #Convert Global-MHD coordinate ->GSM
   
    imshow(-list[:,:,120,1],extent=[-30,69,-40,40],origin='lower',vmax=12,vmin=-12)
    #imshow(-list[88:154,57:123,120,1],extent=[-11,11,-11,11],origin="lower",vmax=12,vmin=-12)
    ax=colorbar()    
    ax.set_label('Ey [mV/m]',fontsize=20)
    #ax.set_clim(-4,2.2)

    wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='k')
    wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='w')    
    fig = gcf()
    fig.gca().add_artist(wedge1)
    fig.gca().add_artist(wedge2)
   # xlim(20,-60)
    #ylim(40,-40)
    
    xticks([-20,0,20,40,60],["20","0","-20","-40","-60"])
    yticks([-40,-30,-20,-10,0,10,20,30,40],["40","30","20","10","0","-10","-20","-30","-40"])
    """
    xticks([-10,0,10],["10","0","-10"])
    yticks([-10,0,10],["10","0","-10"])
    """
    savefig('Ey%04i.jpeg'%(num))
    print 'saved %d'%(num*12)
    num+=1  
    

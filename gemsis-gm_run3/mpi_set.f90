module mpi_set

  implicit none
  private
  public :: mpi_set__init, para_range, mpi_wtime

  include 'mpif.h'

  integer, public, parameter   :: mnpr  = MPI_DOUBLE_PRECISION
  integer, public, parameter   :: mproc_null = MPI_PROC_NULL
  integer, public, parameter   :: opsum = MPI_SUM, opmax = MPI_MAX
  integer, public              :: nerr, ncomw, nstat(MPI_STATUS_SIZE)
  integer, public              :: nrank, nrank_i, nrank_j, nrank_k
  integer, public              :: iup, idown, jup, jdown, kup, kdown
  integer, public              :: nxs, nxe, nxeh, nxs1, nxe1, nxeh1
  integer, public              :: nys, nye, nyeh, nys1, nye1, nyeh1
  integer, public              :: nzs, nze, nzeh, nzs1, nze1, nzeh1


contains


  subroutine mpi_set__init(nxgs,nxge,nygs,nyge,nzgs,nzge,      &
                           nproc,nproc_i,nproc_j,nproc_k)

    integer, intent(in) :: nxgs, nxge, nygs, nyge, nzgs, nzge
    integer, intent(in) :: nproc, nproc_i, nproc_j, nproc_k
    integer             :: i, j, k, irank, nsize 
    integer             :: isize, jsize, ksize
    integer             :: ptable(-1:nproc_i,-1:nproc_j,-1:nproc_k)


    !*********** Initialization for MPI  ***************!
    call MPI_INIT(nerr)
    ncomw = MPI_COMM_WORLD
    call MPI_COMM_SIZE(ncomw, nsize, nerr)
    call MPI_COMM_RANK(ncomw, nrank, nerr)

    if(nsize /= nproc) then
       stop 'error in proc no.'
       call MPI_ABORT(ncomw, 9, nerr)
       call MPI_FINALIZE(nerr)
    endif
    if(nsize /= (nproc_i*nproc_j*nproc_k)) then
       stop 'error in proc no.'
       call MPI_ABORT(ncomw, 9, nerr)
       call MPI_FINALIZE(nerr)
    endif

    !rank table
    do k=-1,nproc_k
    do j=-1,nproc_j
    do i=-1,nproc_i
       ptable(i,j,k) = MPI_PROC_NULL
    enddo
    enddo
    enddo
    irank = 0
    do i=0,nproc_i-1
    do j=0,nproc_j-1
    do k=0,nproc_k-1
       ptable(i,j,k) = irank
       if(nrank == irank)then
          nrank_i = i
          nrank_j = j
          nrank_k = k
       endif
       irank = irank+1
    enddo
    enddo
    enddo

    call para_range(nxs,nxe,nxgs,nxge,nproc_i,nrank_i)
    call para_range(nys,nye,nygs,nyge,nproc_j,nrank_j)
    call para_range(nzs,nze,nzgs,nzge,nproc_k,nrank_k)

    !Nori-shiro
    nxs1  = nxs-1
    nxe1  = nxe+1
    nys1  = nys-1
    nye1  = nye+1
    nzs1  = nzs-1
    nze1  = nze+1
    !Nori-shiro for Staggered grid
    nxeh  = nxe-1
    nxeh1 = nxeh+1
    nyeh  = nye-1
    nyeh1 = nyeh+1
    nzeh  = nze-1
    nzeh1 = nzeh+1

    !For MPI_SENDRECV
    isize  = nxe-nxs+1
    jsize  = nye-nys+1
    ksize  = nze-nzs+1
    iup   = ptable(nrank_i+1,nrank_j,nrank_k)
    idown = ptable(nrank_i-1,nrank_j,nrank_k)
    jup   = ptable(nrank_i,nrank_j+1,nrank_k)
    jdown = ptable(nrank_i,nrank_j-1,nrank_k)
    kup   = ptable(nrank_i,nrank_j,nrank_k+1)
    kdown = ptable(nrank_i,nrank_j,nrank_k-1)

  end subroutine mpi_set__init


  subroutine para_range(ns,ne,n1,n2,isize,irank)

    integer, intent(in)  :: n1, n2, isize, irank
    integer, intent(out) :: ns, ne
    integer              :: iwork1, iwork2

    !start and end of loop counter
    iwork1 = (n2-n1+1)/isize
    iwork2 = mod(n2-n1+1,isize)
    ns = irank*iwork1+n1+min(irank,iwork2)
    ne = ns+iwork1-1
    if(iwork2 > irank) ne = ne+1

  end subroutine para_range


end module mpi_set

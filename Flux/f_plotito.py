# -*- coding: utf-8 -*-
import matplotlib 
matplotlib.use("Agg")
from pylab import *
import numpy as np
import matplotlib.colors as col
import  matplotlib.cm as cm
from matplotlib.colors import LogNorm
import matplotlib.ticker as tic
from matplotlib.ticker import MaxNLocator 
import time

m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 4000

#files ="/work/m_hayashi/RBF6/ptl"
iniene = 0
#enest = iniene-1
#dx = 0.5
#dy = 0.5
dL = 0.5
dphi = float(1.0/180.0)*np.pi*15.
#dA = dx*dy
da = float(1.0/180.0)*np.pi
de = 0.2
dt = 12 
#grx = 2*int(15/dx)
#gry = 2*int(15/dx)
grL = int((10.-5.5)/dL)
grML = 2*int(np.pi/dphi)
gra = int(np.pi/da)
gre = int(3./de)
grt = 200
arx = 15
ary = 30

#xgm,ygm = 66,120 #12MLT
#xgm,ygm = 90,96 #18MLT
#xgm,ygm = 114,120 #0MLT
xgm, ygm = 103,107
def main():
    start =time.time()
    data = np.load("/work/c0099ito/Fluxdata/Data_RB000_exp.npz")
    flux1= data["flux"]

    #flux1 = np.sum(flux1[:,:,:,:,:],axis=1) # integral for mlt
    #flux1 = flux1[:,18,:,:,:] #21MLT
    print flux1.shape
    flux1 = np.sum(flux1[:,:,85:95,:,:],axis=2)  # integral for a
    flux1 = np.sum(flux1[:,:,10:,:],axis=2) #1-3
    
    flux1 = np.roll(flux1,12,axis=1)
    for t in xrange(1,grt):
        data = flux1[3:,6:19,t]
        clf()
        fig2 = figure(2,figsize=(8,4))
        ax1 = fig2.add_subplot(1,1,1)
        fig2.subplots_adjust(bottom =0.2)
        ay1 = ax1.imshow(data,\
                             origin="lower",interpolation="none",aspect="auto"\
                             ,norm=LogNorm(vmin=1,vmax=100))

        ax1.set_ylabel("L")
        ax1.set_xlabel("MLT")
        ax1.set_title("time = "+str(t*12) +" sec")
        #setp(ax1.get_xticklabels(),visible=False) #軸をけすやつ
        ax1ticks = np.linspace(-0.5,np.shape(data)[1]-0.5,13)
        ax1.set_xticks(ax1ticks)
        ax1.set_xticklabels(np.linspace(6,18,13,dtype=int))
        ax1ticks = np.linspace(-0.5,np.shape(data)[0]-0.5,7)
        ax1.set_yticks(ax1ticks)
        ax1.set_yticklabels(np.linspace(7,10,7,dtype=float))

        box1 = ax1.get_position()
        pad, width = 0.01,0.02
        cax1 = fig2.add_axes([box1.xmax+pad, box1.ymin, width, box1.height])
        CB1 = colorbar(ay1,cax=cax1)
        CB1.update_ticks()
        CB1.set_label("flux [$/Re^{2}/str/keV$] ")
        fig2.savefig("/work/m_hayashi/figure/ITO/LMLTroll"+str(t)+".png")
        close(fig2)
    elapsed_time = time.time()-start
    print "elapsed_time:{0}".format(elapsed_time) +" [sec]"
    


main()

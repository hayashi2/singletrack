# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *
from matplotlib.ticker import AutoMinorLocator
import matplotlib.patches as mpatches
import matplotlib.ticker as tick
path = "/work/m_hayashi/MHDdata/"
name = "Vsw600_E.npz"
outputpath="/home/m_hayashi/output/Ephi/"
k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1

phase=np.empty([241,297,201])

def main():
    data=np.load(path+name)
    ex = data["ex"]
    ey = data["ey"]
    ez = data["ez"] 
    ephi = data["ephi"]

    ML=np.empty((120,360))
    for t in range(0,201):
        
        for phi in range(0,360):
            for l in range(0,120):
                x = l*np.cos(np.radians(phi-180))
                y = l*np.sin(np.radians(phi-180))

                x = -x
                x = (3*x/10 + 90).astype(int)
                y = (3*y/10 + 120).astype(int)
            
                ML[l,phi] = ephi[y,x,t] 
            
           # print x ,y
        clf()
        imshow(ML[30:120,:],aspect="auto",origin="lower",extent=[0,24,3,12],vmin=-10,vmax=10)
        title("time=" +str(12*t)+"sec")
        xlabel("MLT")
        ylabel("R [Re]")
        CB=colorbar(shrink=1)
        CB.set_label('Ephi [mV/m]',fontsize=12)
        savefig(outputpath+"Ephi"+str(t).zfill(3) +".jpeg")
main()

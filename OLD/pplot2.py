# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 14:06:49 2015

@author: m_hayashi
"""
"""
実行すると30秒ごとの粒子の位置が図示された画像ファイルが出力されます。約80ファイル作られます。
エネルギーによって色が変化します。
"""
import numpy as np
from pylab import *
import matplotlib.patches as mpatches
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 

def main():
    n=5
    while(n<=479):
        num=0
        setfig(n)
    
        while(num <= 127):
            filename= "/work/m_hayashi/RB000/ptl%03i.dat"%num
            energy,x,y=readfile(filename,n)
            scatter (x/6380000,y/6380000,c=energy,cmap=cm.jet,s=5,alpha=0.9,lw=0,vmin=1.4,vmax=3.6)
            draw()        
            num+=1        
        ax=colorbar(ticks=[1.5,2.0,2.5,3.0,3.5])    
        ax.set_label('energy [MeV]',fontsize=15)
        ax.set_clim(0.5,3.5)
    #show()
        savefig('position%04d.jpeg' %(n*5+5))
        print 'saved %d'%(n*5+5)
        n+=6

def setfig(n):
    figure(figsize=(12,10))  
    wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
    wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
    fig = gcf()
    fig.gca().add_artist(wedge1)
    fig.gca().add_artist(wedge2)    
    
    plot((-10,10), (0,0 ), color='gray',linestyle='--')
    plot((0,0),(-10,10),'gray',linestyle='--')
    
    xlim(11,-11)
    ylim(-11,11)
    setp(gca().get_xticklabels(), fontsize=20, visible=True)
    setp(gca().get_yticklabels(), fontsize=20, visible=True)
    ylabel("y[Re]",fontsize=25)
    xlabel("x[Re]",fontsize=25)
    title("time="+str(n*5+5)+"s")
    
def readfile(filename,n):
    
    ene=[]
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,
                   ("xp","<128f8"),("yp","<128f8"),("zp","<128f8"),
                   ("lp","<128f8"),("xeq","<128f8"),("yeq","<128f8"),
                   ("zeq","<128f8"),("aeq","<128f8"),("beq","<128f8"),
                   ("ppara","<128f8"),("pperp","<128f8"),("mu","<128f8"),
                   ("bp","<128f8"),("prebp","<128f8"),("db","<128f8"),
                   ("predb","<128f8"),("nump","<128i4"),("ierr","<128i4"),tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=n+1)
    
    xp = chunk[n]["xp"].reshape((1,128),order="F")
    yp = chunk[n]["yp"].reshape((1,128),order="F")
    zp = chunk[n]["zp"].reshape((1,128),order="F")
    lp = chunk[n]["lp"].reshape((1,128),order="F")
    xeq = chunk[n]["xeq"].reshape((1,128),order="F")
    yeq = chunk[n]["yeq"].reshape((1,128),order="F")
    zeq = chunk[n]["zeq"].reshape((1,128),order="F")
    aeq = chunk[n]["aeq"].reshape((1,128),order="F")
    beq = chunk[n]["beq"].reshape((1,128),order="F")
    ppara = chunk[n]["ppara"].reshape((1,128),order="F")
    pperp = chunk[n]["pperp"].reshape((1,128),order="F")
    mu = chunk[n]["mu"].reshape((1,128),order="F")
    bp = chunk[n]["bp"].reshape((1,128),order="F")
    prebp = chunk[n]["prebp"].reshape((1,128),order="F")
    db = chunk[n]["db"].reshape((1,128),order="F")
    predb = chunk[n]["predb"].reshape((1,128),order="F") 
    nump = chunk[n]["nump"].reshape((1,128),order="F")
    ierr = chunk[n]["ierr"].reshape((1,128),order="F")
    
    close(filename)     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
       
    ene = np.append(ene,  (m * c**2 * (g - 1) / e)/1000000)        
    return ene,xp,yp
    
main()

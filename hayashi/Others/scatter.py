# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
import matplotlib.patches as mpatches

ptl = 10000
f_num = 144
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
re = 6370e3
gx = 500
gy = 300
gz = 300
rx = 120
ry = 150
rz = 150
dx = 1./4
deltat = 11
pdt = 5.
e0 = 2.2
e_min =5.0
e_max = 5.1
RUN = "run3"
#Efile = "/work/m_hayashi/"+RUN+"_Ephi.npz" 
def main():        
    Efile = "/work/m_hayashi/GM/"+RUN+"/MHD/MHD_E"
    nx = -(np.arange(0,gx)-rx)
    ny = -(np.arange(0,gy)-ry)
    NY,NX = np.meshgrid(ny,nx)
   
    cosphi = NX.astype(float)/np.sqrt(NX**2 + NY**2)
    sinphi = NY.astype(float)/np.sqrt(NX**2 + NY**2)
    del NX,NY,nx,ny
    #ephi = np.load(Efile)
    #ephi = ephi["Ephi"]

    for t in np.arange(220.,550.,deltat):
    #for t in np.arange(516,600,deltat):

        clf()
        fig = figure(figsize=(7,5))
        ax0 = fig.add_subplot(111)    
        Etime =int(t)/deltat
        print Etime
        Ex,Ey = readE(Efile,Etime)
        Ephi = Ey*cosphi - Ex*sinphi
        
        ptime = round(t/pdt) -1
        print ptime
        x_pos,y_pos,c_ene = [],[],[]
        for f in xrange(f_num):
            print f
            filename = "/work/m_hayashi/RBdata/RB_GMrun3_000/ptl"+str(f).zfill(3)+".dat"
            time,x,y,aeq,ene,ierr = readptl(filename)
            aeq = np.degrees(aeq)
            for i in xrange(ptl):
                if(85 <= aeq[0,i] < 95 and  e_min <= ene[0,i] <e_max and ierr[ptime,i]==0):
                    x_pos.append(x[ptime,i])
                    y_pos.append(y[ptime,i])
                    c_ene.append(ene[ptime,i]-ene[0,i])

        ax0.set_xlim(11,-11)
        ax0.set_ylim(11,-11)
        im0 = ax0.imshow(Ephi[rx-11/dx:rx+11/dx,ry-11/dx:ry+11/dx].T,origin = "lower",\
                             aspect="auto",extent = [11,-11,11,-11],\
                             vmin=-10,vmax=10,cmap = "RdGy_r",interpolation="spline36")
        im1 = ax0.scatter(x_pos,y_pos,c=c_ene,s=5,vmin = -0.2,vmax =1.2,linewidth=0)
        CB0 = colorbar(im0)
        CB1 = colorbar(im1)
        CB0.set_label(r"$E_{\phi}$ [mV/m]")
        CB1.set_label(r"$W - W_{init}$[MeV]")
        ax0.set_title("time = "+str(int(t))+" sec   "+"$W_{init}$ = "+str(e_min)+" - "+str(e_max)+" MeV")
        ax0.grid()
        ax0.set_xlabel("X [Re]")
        ax0.set_ylabel("Y [Re]")
        wedge1 = mpatches.Wedge((0,0),1,-90,90,fc="w")
        wedge2 = mpatches.Wedge((0,0),1,90,-90,fc="k")
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)
        fig.tight_layout()
        fig.savefig("/work/m_hayashi/figure/scatter/run3/8/sc"+str(Etime).zfill(3)\
                        +".png",dpi = 200)
        close(fig)
    """
    fig2 = figure(2)
    ax1 = fig2.add_subplot(111)
    time = np.linspace(300,450,np.shape(x_2[0:25,index_2])[0])
    ax1.plot(time,ene_2[0:25,index_2]-ene_2[0,index_2],"r",label="2 MeV")
    ax1.plot(time,ene_8[0:25,index_8]-ene_8[0,index_8],"b",label="500 keV")
    ax1.set_xlabel("time [sec]")
    ax1.legend(loc="upper left",fontsize=18)
    ax1.set_ylabel(r"$\delta$"+"W [MeV]")
    ax1.set_xlim(300,450)
    ax1.grid()
    savefig("12MLT_500k_2M.png")
    """
    
def readptl(filename):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt)
    time = chunk["rtime"]
    xp = chunk["xp"]
    yp = chunk["yp"]
    #zp = chunk["zp"]
    #lp = chunk["lp"]
    #xeq = chunk["xeq"]
    #yeq = chunk["yeq"]
    #zeq = chunk["zeq"]
    aeq = chunk["aeq"]
    #beq = chunk["beq"]
    ppara = chunk["ppara"]
    pperp = chunk["pperp"]
    #mu = chunk["mu"]
    #bp = chunk["bp"]
    #prebp = chunk["prebp"]
    #db = chunk["db"]
    #predb = chunk["predb"] 
    #nump = chunk["nump"]
    ierr = chunk["ierr"]
    fd.close()
    
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)       
    ene = (m * c**2 * (g - 1) / e)/1e6
    return time,-xp/re,-yp/re,aeq,ene,ierr

def readE(filepath,t):
    filename = filepath + str(t).zfill(3)+".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<"+str(gx*gy*gz*3)+"f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    E = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*e0
    #Convert Global-MHD coodinate ->GSM
    return -E[:,:,rz,0],-E[:,:,rz,1] 

main()

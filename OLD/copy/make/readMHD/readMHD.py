# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 13:30:36 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
name=20
path="/work/ymatumot/simulation/GM-RB/run_Vsw400/MHD_"  #filename MHD_B000.dat~MHD_B208.dat

filename=path+"B%03i.dat"%name

head = ("head","<i")
tail = ("tail","<i")
b=("b","<51750171f4")

dt = np.dtype([head,b,tail])
fd = open(filename,"r")
chunk = np.fromfile(fd, dtype=dt,count=1)
fd.close()
#print chunk[0]['b']
    
bf = chunk[0]["b"].reshape((297,241,241,3),order="F")
list=bf.transpose(1,0,2,3)

figure(1)
figure(figsize=(10,10))
imshow(list[:,:,120,2],origin="lower")   #Bz
title("time="+str(name*12)+"s")
ax=colorbar()    
ax.set_label('Bz [mV/m]',fontsize=15)
#ax.set_clim(0,48)
#xlabel("x(Re)")
#ylabel("y(Re)")
show()
"""
figure(2)
figure(figsize=(10,10))
imshow(list[88:154,57:123,120,2],extent=[-11,11,-11,11],origin='lower')   #Bz
#imshow(list[88:154,57:123,120,1],extent=[-11,11,-11,11],origin='lower')  #Ey
#gca().invert_xaxis()
colorbar()
xlabel("x(Re)")
ylabel("y(Re)")
"""


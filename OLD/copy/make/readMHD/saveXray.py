# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *
patha = "/work/ymatumot/simulation/GM-RB"
pathb = "/run_Vsw600/"
dname = "MHD_"
k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1
alpha = 6e-16

nx = np.arange(90,-207,-1)*1.0
ny = np.arange(-120,121,1)*1.0
nz = np.arange(-120,121,1)*1.0



r = np.empty([297,241,241])

zm = 0
while(zm <= 240):
    ym = 0
    while(ym <= 240):
        xm = 0
        while(xm <= 296):
            r[xm,ym,zm] = sqrt((nx[xm]/3)**2 + (ny[ym]/3)**2 + (nz[zm]/3)**2)
            xm += 1
        ym += 1
    zm += 1

nH = 25*((10/r)**3)
print r[:,:,120]
#imshow(r[90,:,:])
#show()

#Convert Global-MHD coordinate ->GSM
def main():
    rtime = 0
    while(rtime<=208):
        vx = -readDATA(0,rtime)*v0
        vy = -readDATA(1,rtime)*v0
        vz = readDATA(2,rtime)*v0
        N  = readDATA(3,rtime)
        Pt  = readDATA(4,rtime)*p0
    
        T  = (Pt*1e-9)/(k*(N*1e6))
        u  = (np.sqrt(vx**2 + vy**2 + vz**2 ))*1e5
        vth = np.sqrt(3.0*k*T/1.67e-23)
        g = np.sqrt(u**2 + vth**2)
   
        P = alpha*(nH)*N[:,:,:]*g[:,:,:]
        np.savez("/work/m_hayashi/Xray/Vsw600/MHDXray_"+str(rtime).zfill(3)+".npz", Xray=P) 
        print "saved ",rtime*12
        rtime += 1

    
def readDATA(vn,time):
        
    if vn == 0:
        filename = patha + pathb + dname + "Vx" + str(time).zfill(3) + ".dat"
    elif vn == 1:
        filename = patha + pathb + dname + "Vy" + str(time).zfill(3) + ".dat"
    elif vn == 2:
        filename = patha + pathb + dname + "Vz" + str(time).zfill(3) + ".dat"
    elif vn == 3:
        filename = patha + pathb + dname + "N" + str(time).zfill(3) + ".dat"
    elif vn == 4:
        filename = patha + pathb + dname + "P" + str(time).zfill(3) + ".dat"

    head = ("head","<i")
    tail = ("tail","<i")
    d = ("d","<17250057f4")
        
    dt = np.dtype([head,d,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    data = chunk[0]["d"].reshape((297,241,241),order="F")
    print str(vn) + " read... " + str(time) 
    
    return data


main()

# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 13:49:29 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 100
g=np.empty([1,5])
dele=np.empty([1,5])
def main():
   
   """""""""
   30MeV
   """""""""
   ppath = "/work/m_hayashi/RB045/"
   fname = "ptl"
   iniene = 30
   irank = "022"
   number = 72
   g[0,0]=iniene*(1e6)/(m*(c**2))
   t = 0
   while(t <= 210):
      enenum = readfile( ppath + fname + irank +".dat",t)[0]
      if(t==49):
         dele[0,0]=enenum[0,number]-iniene
      t+=1
   """""""""
   40MeV
   """""""""
   ppath = "/work/m_hayashi/RB046/"
   fname = "ptl"
   iniene = 40
   irank = "002"
   number = 7
   g[0,1]=iniene*(1e6)/(m*(c**2))
   t = 0
   while(t <= 210):
      enenum = readfile( ppath + fname + irank +".dat",t)[0]
      if(t==49):
         dele[0,1]=enenum[0,number]-iniene
      t+=1
   
   """""""""
   50MeV
   """""""""
   ppath = "/work/m_hayashi/RB044/"
   fname = "ptl"
   iniene = 50
   irank = "032"
   number = 89
   g[0,2]=iniene*(1e6)/(m*(c**2))
   t = 0
   while(t <= 210):
      enenum = readfile( ppath + fname + irank +".dat",t)[0]
      if(t==49):
         dele[0,2]=enenum[0,number]-iniene
      t+=1

   """""""""
   70MeV
   """""""""
   ppath = "/work/m_hayashi/RB048/"
   fname = "ptl"
   iniene = 70
   irank = "002"
   number = 7
   g[0,3]=iniene*(1e6)/(m*(c**2))
   t = 0
   while(t <= 210):
      enenum = readfile( ppath + fname + irank +".dat",t)[0]
      if(t==49):
         dele[0,3]=enenum[0,number]-iniene
      t+=1
   """""""""
   90MeV
   """""""""
   ppath = "/work/m_hayashi/RB049/"
   fname = "ptl"
   iniene = 90
   irank = "002"
   number = 6
   g[0,4]=iniene*(1e6)/(m*(c**2))
   t = 0
   while(t <= 210):
      enenum = readfile( ppath + fname + irank +".dat",t)[0]
      if(t==49):
         dele[0,4]=enenum[0,number]-iniene
      t+=1
   """""""""
   """""""""
   g1 = g*e
   bb = 1-1/(g1**2)
   t=g1*bb
   
   A=np.array([t[0,:],np.ones(len(t[0,:]))])
   A=A.T
   a,b=np.linalg.lstsq(A,dele[0,:])[0]
   ax = gca()
   plot(t[0,:],dele[0,:],"bo",markersize=15)
   plot(t[0,:],a*t[0,:],"b--")
   grid()
   ylabel("delW[MeV]")
   xlabel("gbb")
   
   #xticks(fontsize=15)
   #yticks(fontsize=15)
   ax.yaxis.set_major_locator(MultipleLocator(5))
   ax.yaxis.set_minor_locator(MultipleLocator(1))
   tick_params(axis="y",which="minor",length=5)
   tick_params(axis="y",which="major",length=10)
   show()
   print g*bb,g,bb
def readfile(filename,t):
   ene=[]      
   head = ("head","<i")
   tail = ("tail","<i")
   rtime = ("rtime","<f8")
   nnp = ("np","<i4")
   maxptl =("maxptl","<i4")
   xxp = ("xp","<"+str(ptl)+"f8")
   yyp = ("yp","<"+str(ptl)+"f8")
   zzp = ("zp","<"+str(ptl)+"f8")
   llp = ("lp","<"+str(ptl)+"f8")
   xxeq = ("xeq","<"+str(ptl)+"f8")
   yyeq = ("yeq","<"+str(ptl)+"f8")
   zzeq = ("zeq","<"+str(ptl)+"f8")
   aaeq = ("aeq","<"+str(ptl)+"f8")
   bbeq = ("beq","<"+str(ptl)+"f8")
   p_ppara = ("ppara","<"+str(ptl)+"f8")
   p_pperp = ("pperp","<"+str(ptl)+"f8")
   mmu = ("mu","<"+str(ptl)+"f8")
   bbp = ("bp","<"+str(ptl)+"f8")
   pprebp = ("prebp","<"+str(ptl)+"f8")
   ddb = ("db","<"+str(ptl)+"f8")
   ppredb = ("predb","<"+str(ptl)+"f8")
   nnump = ("nump","<"+str(ptl)+"i4")
   iierr = ("ierr","<"+str(ptl)+"i4")

   dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                  aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                  iierr,tail])
               
   fd = open(filename,"r")
   chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
   xp = chunk[t]["xp"].reshape((1,ptl),order="F")
   yp = chunk[t]["yp"].reshape((1,ptl),order="F")
   zp = chunk[t]["zp"].reshape((1,ptl),order="F")
   lp = chunk[t]["lp"].reshape((1,ptl),order="F")
   xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
   yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
   zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
   aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
   beq = chunk[t]["beq"].reshape((1,ptl),order="F")
   ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
   pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
   mu = chunk[t]["mu"].reshape((1,ptl),order="F")
   bp = chunk[t]["bp"].reshape((1,ptl),order="F")
   prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
   db = chunk[t]["db"].reshape((1,ptl),order="F")
   predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
   nump = chunk[t]["nump"].reshape((1,ptl),order="F")
   ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   
   fd.close()     
   g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
   print g
   ene = (m * c**2 * (g - 1) / e)/1000000
   return ene,xp/1000,yp/1000
    
main()

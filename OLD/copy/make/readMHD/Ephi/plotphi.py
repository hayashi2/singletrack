import numpy as np
from pylab import *
import matplotlib.patches as mpatches
"""
-30 <= x <= 69 (Re)
-40 <= y <= 40 (Re)
""" 
x1 = -4
y1 = -7
x2 = -7
y2 = -4

def main():
    t,e1,e2 = readEphi()
    index1 = np.where(e1 == np.min(e1))
    index2 = np.where(e2 == np.min(e2))
    print t[index1[0]],t[index2[0]]
    subplot(2,1,1)
    plot(t,e1)
    title("("+str(x1)+","+str(y1)+")")
    xlabel("time[sec]")
    ylabel("Ephi[mV/m]")
    
    subplot(2,1,2)
    plot(t,e2)
    title("("+str(x2)+","+str(y2)+")")
    xlabel("time[sec]")
    ylabel("Ephi[mV/m]")
    show()
    

def readEphi():
    time = 0
    t,e1,e2 = [],[],[]
    nx = np.arange(297,0,-1)-207
    ny = (np.ones(297))*(-120)
    na = np.empty([241,297])
    nb = np.empty([241,297])
    n=0
    while(n<=240):
        na[n] = nx
        nb[n] = ny + n
        n+=1
    
    cosphi=-na/np.sqrt((na)**2+(nb)**2)
    sinphi=nb/np.sqrt((na)**2+(nb)**2)

    while(time <= 208 ):
    
        path = "/work/ymatumot/simulation/GM-RB/run_Vsw600/"
        ename = "MHD_E"

        filename = path + ename + str(time).zfill(3) + ".dat"

        head = ("head","<i")
        tail = ("tail","<i")
        b=("b","<51750171f4")
         
        dt = np.dtype([head,b,tail])
        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
    
        bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*2.2
        bt=bf.transpose(1,0,2,3)
        ey = bt[:,:,120,1]
        ex = bt[:,:,120,0]

        ephi = ey*cosphi - ex*sinphi
        e1 = np.append(e1,ephi[py(y1),px(x1)])
        e2 = np.append(e2,ephi[py(y2),px(x2)])
        t = np.append(t,time*12)
        print "read.."+str(time)
        time += 1
    return t,e1,e2

def px(x):
    return int(x*3 + 90)

def py(y):
    return int(y*3 + 121)

main()

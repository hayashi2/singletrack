# -*- coding: utf-8 -*-

import numpy as np
import yt
import matplotlib
matplotlib.use("Agg")

import matplotlib.pyplot as plt

# BOX SIZE
gx = 500
gy = 300
gz = 300
#Earth position
rx = 120
ry = 150
rz = 150

dx = 1./4
dy = 1./4
dz = 1./4
dt = 11
v0 = 220 #(km/s)
path = "/work/m_hayashi/GM/run3/MHD/"
vname = "MHD_V"


def main():
    for t in xrange(0,100):
        filename = path + vname + str(t).zfill(3) + ".dat"
        Vx,Vy = readV(filename)
        
        Vx = np.transpose(Vx[rx-20/dx:rx+20/dx,ry-15/dy:ry+15/dy,rz-15/dz:rz+15/dz],(0,1,2))
        Vy = np.transpose(Vy[rx-20/dx:rx+20/dx,ry-15/dy:ry+15/dy,rz-15/dz:rz+15/dz],(0,1,2))
    
        Vtol = np.sqrt(Vx**2 +Vy**2)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        #ax =fig.add_subplot(111)
        

        data = dict(Vtol = (Vtol, "km/s"),Vy = (Vy, "km/s"),Vx = (Vx, "km/s"))

    #bbox = np.array([[-30, 69], [-40, 40],[-40, 40]])
        bbox = np.array([[-21, 21], [-18, 18],[-18, 18]])
        ds = yt.load_uniform_grid(data,Vtol.shape, bbox=bbox, nprocs=1)
        slc = yt.SlicePlot(ds, "z", "Vtol")
    #slc.set_log("Bz",True, linthresh=1.e1) #symlog scale
        slc.set_cmap("Vtol", "Blue-Red")
        slc.set_zlim(field="Vtol",zmin=1e-1,zmax=1e3)
        slc.annotate_line_integral_convolution("Vx","Vy",lim=(0.5,0.7),alpha=1.,const_alpha=True) #LIC
        slc.annotate_title("time= " +str(t*dt) +"  [sec]")

    # save to "png" or "eps"
        slc.save("/work/m_hayashi/figure/LIC/v_"+str(t).zfill(3)+".png")
        

def readV(filename):
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<"+str(gx*gy*gz*3)+"f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*v0

    return bf[:,:,:,0],bf[:,:,:,1]

main()

# -*- coding: utf-8 -*-

import numpy as np
import sys
import os
import glob
import commands
import matplotlib as mpl
mpl.use("Agg")
from matplotlib.pylab import *
from scipy.io import FortranFile
import struct

def main():
    nx = 500
    ny = 300
    nz = 300
    nproc = 60
    nproc_i = 10
    nproc_j = 6    

    start =350.
    di = '/work/m_hayashi/GM/run5/MHD/'
    #di = "/work/m_hayashi/GM/base2/"
    bd = dipole(nx,ny,nz)

    for c,i in enumerate(np.arange(start,451.,0.5)):
    #for count,i in enumerate(np.arange(1,360.5)):
        count = c + int((start -350.)/0.5)
        if(count == 0):
            pat = "/work/ymatumot/GM/base2/"+str(int(i*1e3)).zfill(6)+"_*_rank=*.dat"
        else:
            pat = "/work/m_hayashi/GM/run5/"+str(int(i*1e3)).zfill(6)+"_*_rank=*.dat"
        
        print "Start reading \""+pat+" \""
        data = gm_read(nx, ny, nz, nproc, nproc_i, nproc_j, pat)
        
        B = data[:,:,:,0:3] + bd
        V = data[:,:,:,6:9]
        E = np.empty(B.shape)
        E[:,:,:,0] = -(V[:,:,:,1]*B[:,:,:,2]-V[:,:,:,2]*B[:,:,:,1])
        E[:,:,:,1] = -(V[:,:,:,2]*B[:,:,:,0]-V[:,:,:,0]*B[:,:,:,2])
        E[:,:,:,2] = -(V[:,:,:,0]*B[:,:,:,1]-V[:,:,:,1]*B[:,:,:,0])
        N = data[:,:,:,4]
        P = data[:,:,:,5]

        print B[:,:,:,0]
        print "Saving \""+di+"MHD_*"+str(int(count)).zfill(3)+".dat\""
        f = open(di+'MHD_B'+str(int(count)).zfill(3)+'.dat',"wb")
        #f = open('/work/m_hayashi/GM/MHD_Bsample.dat',"wb")
        recl = nx*ny*nz*3*np.dtype("float32").itemsize
        f.write(struct.pack("i",recl))
        f.write(struct.pack(str(nx*ny*nz*3)+"f4",*B.flatten("F")))
        f.write(struct.pack("i",recl))
    
        f.close()
        
        f = open(di+'MHD_E'+str(int(count)).zfill(3)+'.dat', 'wb')
        recl = nx*ny*nz*3*np.dtype("f4").itemsize
        f.write(struct.pack("i",recl))
        f.write(struct.pack(str(nx*ny*nz*3)+"f4",*E.flatten("F")))
        f.write(struct.pack("i",recl))
        f.close()
        
        f = open(di+'MHD_V'+str(int(count)).zfill(3)+'.dat', 'wb')
        recl = nx*ny*nz*3*np.dtype("f4").itemsize
        f.write(struct.pack("i",recl))
        f.write(struct.pack(str(nx*ny*nz*3)+"f4",*V.flatten("F")))
        f.write(struct.pack("i",recl))
        f.close()
        
        f = open(di+'MHD_N'+str(int(count)).zfill(3)+'.dat', 'wb')
        recl = nx*ny*nz*np.dtype("f4").itemsize
        f.write(struct.pack("i",recl))
        f.write(struct.pack(str(nx*ny*nz)+"f4",*N.flatten("F")))
        f.write(struct.pack("i",recl))
        f.close()

        f = open(di+'MHD_P'+str(int(count)).zfill(3)+'.dat', 'wb')
        recl = nx*ny*nz*3*np.dtype("f4").itemsize
        f.write(struct.pack("i",recl))
        f.write(struct.pack(str(nx*ny*nz)+"f4",*P.flatten("F")))
        f.write(struct.pack("i",recl))
        f.close()
        
        """
        f = FortranFile(di+'MHD_B'+str(int(i)).zfill(3)+'.dat', 'w')
        f.write_record(B)
        f.close()
        f = FortranFile(di+'MHD_E'+str(int(i)).zfill(3)+'.dat', 'w')
        f.write_record(E)
        f.close()
        f = FortranFile(di+'MHD_V'+str(int(i)).zfill(3)+'.dat', 'w')
        f.write_record(V)
        f.close()        
        f = FortranFile(di+'MHD_N'+str(int(i)).zfill(3)+'.dat', 'w')
        f.write_record(N)
        f.close()
        f = FortranFile(di+'MHD_P'+str(int(i)).zfill(3)+'.dat', 'w')
        f.write_record(P)
        f.close()
        """
        del data,B,E,N,P

        
        


def dipole(nx,ny,nz):
    print "Creating Dipole B........."
    nxgs = 0
    nygs = 0
    nzgs = 0
    
    bd = np.zeros((nx,ny,nz,3))
    d0 = np.zeros((nx,ny,nz))

    delx = 1.0
    rs =4.0*delx #Earth radius
    drs = 1./rs
    rin = 4.0
    px0 = nxgs + 30.*rs+0.01
    py0 = nygs + (ny-1.)/2.
    pz0 = nzgs + (nz-1.)/2.
    #py0 = ny-1
    #pz0 = 0.
    pxn = -px0 #mirror dipole
    pyn = py0
    pzn = pz0
    #r0 = 4.0 #inner boundary(Re)
    Bm = 3.108e3
    un0 = 1.0
    un0_min = 1.0*un0

    for k in xrange(0,nz):
        for j in xrange(0,ny):
            for i in xrange(0,nx):
                x0 = (i-px0)*drs
                xn = (i-pxn)*drs
                y0 = (j-py0)*drs
                yn = (j-pyn)*drs
                z0 = (k-pz0)*drs
                zn = (k-pzn)*drs

                r1 = np.sqrt(x0**2 + y0**2 + z0**2)
                r2 = np.sqrt(xn**2 + yn**2 + zn**2)

                dr1 = r1**(-5)
                dr2 = r2**(-5)
                
                if (r1 <= rin ):
                    bd[i,j,k,0] = 0.0
                    bd[i,j,k,1] = 0.0
                    bd[i,j,k,2] = 0.0


                else:
                    bd[i,j,k,0] = -Bm*(dr1*3.*x0*z0+dr2*3.*xn*zn)
                    bd[i,j,k,1] = -Bm*(dr1*3.*y0*z0+dr2*3.*yn*zn)
                    bd[i,j,k,2] = -Bm*(dr1*(+3.*z0**2 - r1**2)+ dr2*(3.*zn**2 -r2**2))
                """    
                bd[i,j,k,0] = -Bm*(dr1*3.*x0*z0+dr2*3.*xn*zn)
                bd[i,j,k,1] = -Bm*(dr1*3.*y0*z0+dr2*3.*yn*zn)
                bd[i,j,k,2] = -Bm*(dr1*(+3.*z0**2 - r1**2)+ dr2*(3.*zn**2 -r2**2))
                """
                d0[i,j,k] = un0 -(un0-un0_min)*np.tanh((r1-rin)/2.)
    
    return bd 
    

#start and end of loop counter
def para_range(n1,n2,isize,irank):
    iwork1 = (n2-n1+1)/isize
    iwork2 = (n2-n1+1)%(isize)
    nst =  irank*iwork1+n1+min([irank,iwork2])
    ned = nst+iwork1-1
    if(iwork2 > irank):
        ned +=1
    return nst,ned

#PROCEDURE FOR READING MPI-PARALLELIZED SIMULATION DATA
def gm_read(nx, ny, nz, nproc, nproc_i, nproc_j, pat):
    
    nproc_k = nproc/(nproc_i*nproc_j)
    
    #CHECK PROC NUM
    if(nproc != (nproc_i*nproc_j*nproc_k)):
        print "Number of Proc. mismatch ", nproc, nproc_i, nproc_j, nproc_k
        sys.exit()

    #CREATE FILE LIST
    #f_list=[]
    #f_list = glob.glob(pat)
    f_list = commands.getoutput("ls "+pat)
    f_list = f_list.split("\n")
    f_len = len(f_list)
    print f_list

    #CHECK FILE NUM CONSISTENCY WITH PROC NUM
    if(f_len%nproc != 0):
        print "parameter mismatch ", nproc, f_len
        sys.exit()

    nlist = f_len/nproc

    data = np.zeros((nx,ny,nz,nlist),dtype=float)

    for irank in xrange(0,nproc):
        print "rank= ",str(irank)
        rank = 0
        for i in xrange(0,nproc_i):
            for j in xrange(0,nproc_j):
                for k in xrange(nproc_k):
                    if(irank == rank):
                        irank_i = i
                        irank_j = j
                        irank_k = k
                    rank += 1
        nxs, nxe = para_range(0, nx-1, nproc_i, irank_i)
        nys, nye = para_range(0, ny-1, nproc_j, irank_j)
        nzs, nze = para_range(0, nz-1, nproc_k, irank_k)

        for ilist in xrange(0,nlist):
            tmp = np.zeros((nxe-nxs+1, nye-nys+1, nze-nzs+1),dtype=float)
            print ilist, " Reading......    ", f_list[irank+nproc*ilist]
            tmp = readfile(tmp, f_list[irank+nproc*ilist])
            data[nxs:nxe+1,nys:nye+1,nzs:nze+1,ilist] = tmp

    return data

def readfile(tmp, filename):
    
    head = ("head","<i")
    tail = ("tail","<i")
    unit = ("unit","<"+str(tmp.size)+"f4")

    dt = np.dtype([head,unit,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()

    unit = chunk[0]["unit"].reshape(tmp.shape,order="F")
    return unit

def readdipole(nx,ny,nz):
    bdpath = "/work/ymatumot/GM/base1/bd.dat"
    head = ("head","<i")
    tail = ("tail","<i")
    unit = ("unit","<"+str(nx*ny*nz*3)+"f4")

    dt = np.dtype([head,unit,tail])
    fd = open(bdpath,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()

    unit = chunk[0]["unit"].reshape((nx,ny,nz,3),order="F")
    return unit
main()

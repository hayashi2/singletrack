import numpy as np
from pylab import *
import matplotlib.patches as mpatches
"""
-30 <= x <= 69 (Re)
-40 <= y <= 40 (Re)
"""
e0 = 2.2 
x1 = 9
y1 = 7
x2 = 8
y2 = 7
x3 = 7
y3 = 7
x4 = 6
y4 = 7
def main():
    t,e1,e2,e3,e4= readEphi()
    index1 = np.where(e1 == np.min(e1))
    index2 = np.where(e2 == np.min(e2))
    index3 = np.where(e3 == np.min(e3))
    index4 = np.where(e4 == np.min(e4))

    print t[index1[0]],t[index2[0]],t[index3[0]],t[index4[0]]

   # subplot(2,1,1)
    
    plot(t[15:50],e1[15:50],c="k",lw=2,label="("+str(x1)+","+str(y1)+")")
    xlabel("time[sec]",fontsize=20)
    ylabel("Ephi[mV/m]",fontsize=20)
    """
    subplot(4,1,2)
    plot(t[0:100],e2[0:100])
    title("("+str(x2)+","+str(y2)+")")
    xlabel("time[sec]")
    ylabel("Ephi[mV/m]")
    
    subplot(4,1,3)
    plot(t[0:100],e3[0:100])
    title("("+str(x3)+","+str(y3)+")")
    xlabel("time[sec]")
    ylabel("Ephi[mV/m]")
    """
   # subplot(2,1,2)
    plot(t[15:50],e4[15:50],c="r",lw=2,label="("+str(x4)+","+str(y4)+")")
    #title("("+str(x4)+","+str(y4)+")")
    #xlabel("time[sec]")
    #ylabel("Ephi[mV/m]")
    xticks(fontsize=15)
    yticks(fontsize=15)
    ax=gca()
    ax.yaxis.set_major_locator(MultipleLocator(5))
    ax.yaxis.set_minor_locator(MultipleLocator(1))
    tick_params(axis="y",which="minor",length=5)
    tick_params(axis="y",which="major",length=10)
    legend(loc="lower right")
    show()
    
    

def readEphi():
    time = 0
    t,e1,e2,e3,e4 = [],[],[],[],[] 
    nx = np.arange(90,-207,-1)
    ny = (np.ones(297))*120
    na = np.empty([241,297])
    nb = np.empty([241,297])
    n=0
    while(n<=240):
        na[n] = nx
        nb[n] = ny - n
        n+=1
    
        cosphi=na/np.sqrt((na)**2+(nb)**2)
        sinphi=nb/np.sqrt((na)**2+(nb)**2)

    while(time <= 208 ):
    
        path = "/work/ymatumot/simulation/GM-RB/run_Vsw600/"
        ename = "MHD_E"

        filename = path + ename + str(time).zfill(3) + ".dat"

        head = ("head","<i")
        tail = ("tail","<i")
        b=("b","<51750171f4")
         
        dt = np.dtype([head,b,tail])
        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
    
        bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*2.2
        bt=bf.transpose(1,0,2,3)
        ey = -bt[:,:,120,1]
        ex = -bt[:,:,120,0]

        ephi = ey*cosphi - ex*sinphi
        e1 = np.append(e1,ephi[py(y1),px(x1)])
        e2 = np.append(e2,ephi[py(y2),px(x2)])
        e3 = np.append(e3,ephi[py(y3),px(x3)])
        e4 = np.append(e4,ephi[py(y4),px(x4)])
        t = np.append(t,time*12)
        print "read.."+str(time)
        time += 1
    return t,e1,e2,e3,e4

def px(x):
    return int(-x*3 + 90)

def py(y):
    return int(-y*3 + 121)

main()

# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
from matplotlib.colors import LogNorm,SymLogNorm
import matplotlib.ticker as tic

RUN ="run_Vsw600"
path ="/work/m_hayashi/GM/"+RUN+"/"
vname = "MHD_Vx"
nname = "MHD_N"
pname = "MHD_P"
bname = "MHD_B"

#BOX SIZE
nx = 297
ny = 241
nz = 241
#Earth position
rx = 90
ry = 121
rz = 121

dx = dy = dz = 1./3
deltat=12

#Standard Value
b0 = 10
e0 = 2.2
v0 = 220.0
n0 = 1.0
p0 = 0.04
k = 1.38e-23
mu0 = 4*np.pi*1e-7

filenum = 100


def main():
    Vx = np.zeros(filenum)
    N = np.zeros(filenum)
    for num in xrange(0,filenum):
        print "Reading... "+path+vname+str(num).zfill(3)+".dat"
        v = readV(num)
        print "Reading... "+path+nname+str(num).zfill(3)+".dat"
        n = readN(num)
        Vx[num] = v[1]
        N[num] = 4#n[1]
        Pdyn = (2e-6)*N*(Vx**2) #nPa
        
    fig = figure()
    ax1 = fig.add_subplot(311)
    ax2 = fig.add_subplot(312)
    ax3 = fig.add_subplot(313)

    time = np.arange(0,filenum*deltat,deltat)
    ax1.plot(time,-Vx) #25-5Re
    ax2.plot(time,N)
    ax3.plot(time,Pdyn)

    ax1.set_title(RUN)
    ax1.set_ylabel("Vx \n[km/s]",fontsize=14)
    ax2.set_ylabel("N \n[/cm^3]",fontsize=14)
    ax3.set_ylabel("$P_{dyn}$ \n[nPa]",fontsize=14)
    ax3.set_xlabel("time [sec]",fontsize=14)
    ax1.set_ylim(350,650)
    ax2.set_ylim(2.5,5.)
    ax3.set_ylim(0.5,4)
    for ax in (ax1,ax2,ax3):
        ax.grid()
        ax.xaxis.set_minor_locator(tic.AutoMinorLocator())
        ax.yaxis.set_minor_locator(tic.AutoMinorLocator())
        ax.yaxis.set_label_coords(-0.08,0.5)
        ax.set_xlim(0,filenum*deltat)
    for ax in (ax1,ax2):
        setp(ax.get_xticklabels(),visible=False)

    fig.subplots_adjust(hspace=0.2)
    fig.savefig(RUN+".png")
    close(fig)

def readV(num):

    filename = path + vname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    v=("v","<"+str(nx*ny*nz)+"f4")  ##new run ==>nx*ny*nz*3
    
    dt = np.dtype([head,v,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    vf = chunk["v"].reshape((nx,ny,nz),order="F")*v0 ##new run ==>reshape((nx,ny,nz,3)
     
    #convert GSM
    vx = -vf[:,:,:] ##new run ==>-vf[:,:,:,0]
    return vx[0:rx,ry,rz]

def readN(num):  
    filename = path + nname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    n=("n","<"+str(nx*ny*nz)+"f4")
    
    dt = np.dtype([head,n,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    nf = chunk["n"].reshape((nx,ny,nz),order="F")*n0
    
    return nf[0:rx,ry,rz] 

def readP(num):  
    filename = path + pname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    p=("p","<"+str(nx*ny*nz)+"f4")
    
    dt = np.dtype([head,p,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    pf = chunk["p"].reshape((nx,ny,nz),order="F")*p0
    
    return pf[0:rx,ry,rz]

def readB(num):

    filename = path + bname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b = ("b","<"+str(nx*ny*nz*3)+"f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    bf = chunk["b"].reshape((nx,ny,nz,3),order="F")*b0
    
    #convert GSM
    b = np.sqrt(bf[0:rx,ry,rz,0]**2 + bf[0:rx,ry,rz,1]**2 + bf[0:rx,ry,rz,2]**2)
    return b

main()

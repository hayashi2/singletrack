# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *

patha = "/work/ymatumot/simulation/GM-RB"
pathb = "/run_Vsw800/"
bname = "MHD_B"
ename = "MHD_E"
dname = "MHD_"
k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1
"""
-30 <= x <= 69 (Re)
-40 <= y <= 40 (Re)
""" 
x1 = -20
y1 = 0

def main():
    rtime,bx,by,bz = readB()
    ex,ey,ez =  readE()
    vx = readDATA(0)*v0
    vy = readDATA(1)*v0
    vz = readDATA(2)*v0
    N  = readDATA(3)
    P  = readDATA(4)*p0
    
    T  = (P*1e-9)/(k*(N*1e6))
    B  = np.sqrt(bx**2 + by**2 + bz**2)
    V  = np.sqrt(vx**2 + vy**2 + vz**2 )
    Ma = (V*(N**(0.5)))/(20*B)
    Va = 20.3*B/np.sqrt(N)
    Vth = 3.0*k*T/1.67e-23
    Vs  = 0.12*(T+1.28*10e5)**(0.5)
    Vms = np.sqrt(Vs**2+Va**2)
    Mms = V/Vms
    Pdyn = (2e-6)*N*(V**2)
    Pt = P + (B**2)/2
    print P,Pt

    figure(figsize=(10,20))
    subplots_adjust(hspace=0.7)
    subplot(12,1,1)
    title(pathb + " ("+str(x1)+","+str(y1)+")")
    plot(rtime,vx)
    ylabel("Vx[km/s]")
    ylim(0,np.max(vx)*1.2)
    locator_params(axis="y",tight=True,nbins=4)
    
    subplot (12,1,2)
    plot(rtime,bx)
    ylabel("Bx[nT]")
    locator_params(axis="y",tight=True,nbins=5)
    
    subplot(12,1,3)
    plot(rtime,by)
    ylabel("By[nT]")
    locator_params(axis="y",tight=True,nbins=5)

    subplot(12,1,4)
    plot(rtime,bz)
    ylabel("Bz[nT]")
    locator_params(axis="y",tight=True,nbins=4)

    subplot(12,1,5)
    plot(rtime,ex)
    ylabel("Ex[mV/m]")
    locator_params(axis="y",tight=True,nbins=4)
    
    subplot(12,1,6)
    plot(rtime,ey)
    ylabel("Ey[mV/m]")
    locator_params(axis="y",tight=True,nbins=4)
    
    subplot(12,1,7)
    plot(rtime,ez)
    ylabel("Ez[mV/m]")
    locator_params(axis="y",tight=True,nbins=4)
   
    subplot(12,1,8)
    plot(rtime,N)
    yscale('log')
    ylabel("log N(/cm^3)")

    subplot(12,1,9)
    plot(rtime,T)
    yscale('log')
    ylabel("log T(K)")

    subplot(12,1,10)
    plot(rtime,Pdyn)
    ylabel("Pdyn(npa)")
    locator_params(axis="y",tight=True,nbins=5)

    subplot(12,1,11)
    plot(rtime,Ma)
    ylabel("Ma")
    locator_params(axis="y",tight=True,nbins=3)
    
    subplot(12,1,12)
    plot(rtime,Mms)
    ylabel("Mms")
    ylim(0,np.max(Mms)*1.2)
    locator_params(axis="y",tight=True,nbins=5)
    xlabel("time[sec]")

    savefig( "Vsw800 plot("+str(x1)+","+str(y1)+").jpeg")
    
    
def readB():
    b1,b2,b3 = [],[],[]
    t = []
    time = 0
    while(time <= 208):
        
        bfilename = patha + pathb + bname + str(time).zfill(3) + ".dat"
        
        head = ("head","<i")
        tail = ("tail","<i")
        b = ("b","<51750171f4")
         
        dt = np.dtype([head,b,tail])
        fd = open(bfilename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
    
        bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*b0
        bt = bf.transpose(1,0,2,3)

        b1 = np.append(b1,bt[ny(y1),nx(x1),120,0])
        b2 = np.append(b2,bt[ny(y1),nx(x1),120,1])
        b3 = np.append(b3,bt[ny(y1),nx(x1),120,2])
        t = np.append(t,time*12)
        print "read... B" +str(time)
        time += 1
        
    return t,b1,b2,b3

def readE():
    e1,e2,e3 = [],[],[]
    time = 0
    while(time <= 208):
        
        efilename = patha + pathb + ename + str(time).zfill(3) + ".dat"
        
        head = ("head","<i")
        tail = ("tail","<i")
        e = ("e","<51750171f4")
         
        dt = np.dtype([head,e,tail])
        fd = open(efilename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
    
        ef = chunk[0]["e"].reshape((297,241,241,3),order="F")*e0
        et = ef.transpose(1,0,2,3)

        e1 = np.append(e1,et[ny(y1),nx(x1),120,0])
        e2 = np.append(e2,et[ny(y1),nx(x1),120,1])
        e3 = np.append(e3,et[ny(y1),nx(x1),120,2])
        print "read... E"+str(time)
        time += 1
        
    return e1,e2,e3



    
def readDATA(vn):
     v1 = []
     
     time = 0
     while(time <= 208):
        
          if vn == 0:
              filename = patha + pathb + dname + "Vx" + str(time).zfill(3) + ".dat"
          elif vn == 1:
              filename = patha + pathb + dname + "Vy" + str(time).zfill(3) + ".dat"
          elif vn == 2:
              filename = patha + pathb + dname + "Vz" + str(time).zfill(3) + ".dat"
          elif vn == 3:
              filename = patha + pathb + dname + "N" + str(time).zfill(3) + ".dat"
          elif vn == 4:
              filename = patha + pathb + dname + "P" + str(time).zfill(3) + ".dat"

          head = ("head","<i")
          tail = ("tail","<i")
          v = ("v","<17250057f4")
         
          dt = np.dtype([head,v,tail])
          fd = open(filename,"r")
          chunk = np.fromfile(fd, dtype=dt,count=1)
          fd.close()
    
          vf = chunk[0]["v"].reshape((297,241,241),order="F")
          vt = vf.transpose(1,0,2)
          v1 = np.append(v1,vt[ny(y1),nx(x1),120])
          print str(vn) + " read... " + str(time) 
          time += 1
     return v1

def nx(x):
    return int(x*3 + 90)

def ny(y):
    return int(y*3 + 121)

main()

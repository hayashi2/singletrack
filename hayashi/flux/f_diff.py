# -*- coding: utf-8 -*-
import matplotlib 
matplotlib.use("Agg")
from pylab import *
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches
import scipy.optimize
import matplotlib.colors as col
import  matplotlib.cm as cm
import matplotlib.ticker as tic
from matplotlib.ticker import MaxNLocator


m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 10000

iniene = 1
enest = iniene-1
#dx = 0.5
#dy = 0.5
#dA = dx*dy

dL = 0.5
dphi = float(1.0/180.0)*np.pi*15.
da = float(1.0/180.0)*np.pi
de = 0.25
dt = 6 
#grx = 2*int(15/dx)
#gry = 2*int(15/dx)
grL = int((10.-5.5)/dL)
grML = 2*int(np.pi/dphi)
gra = int(np.pi/da)
gre = int(10./de)+1
grt =180

la_fontsize=16
ti_fontsize=15

def main():
    
    data = np.load("/work/m_hayashi/Fluxdata/Data_RB005_VAPs0716.npz")
    flux1= data["flux"]
    #flux1 = np.sum(flux1[:,:,:,:,:],axis=1)/24 # integral for mlt
    flux1 = flux1[:,21,:,:,:] #MLT=16
    flux1 =flux1[3,:,:,:] #L=7    
    print "load"
    """
    alp = np.empty((gra,gre,grt))
    for i in range(0,gre):
        for j in range(0,grt):
            alp[:,i,j] = 2*np.pi*np.sin(np.radians(np.arange(0,gra)))
    flux1 = np.sum(alp[80:100,:,:]*flux1[80:100,:,:],axis=0) #integral for a
    """
    
    fig = figure()
    fig.subplots_adjust(hspace=0.2,right=0.85)
    ax1 = fig.add_subplot(311)
    ax2 = fig.add_subplot(312)
    ax3 = fig.add_subplot(313)

    im1 = ax1.imshow(np.sum(flux1[60:120,2:4,:],axis=1)/2,origin="lower",interpolation="none",\
                         aspect="auto",norm=col.LogNorm())
    im2 = ax2.imshow(np.sum(flux1[60:120,12:14,:],axis=1)/2,origin="lower",interpolation="none",\
                         aspect="auto",norm=col.LogNorm())
    im3 = ax3.imshow(np.sum(flux1[60:120,24:26,:],axis=1),origin="lower",interpolation="none",\
                         aspect="auto",norm=col.LogNorm())

    box1 = ax1.get_position()
    box2 = ax2.get_position()
    box3 = ax3.get_position()
    pad, width=0.02,0.01
    cax1 = fig.add_axes([box1.xmax+pad, box1.ymin,width,box1.height])
    cax2 = fig.add_axes([box2.xmax+pad, box2.ymin,width,box2.height])
    cax3 = fig.add_axes([box3.xmax+pad, box3.ymin,width,box3.height])
    CB1 = colorbar(im1,cax=cax1)
    CB2 = colorbar(im2,cax=cax2)
    CB3 = colorbar(im3,cax=cax3)
    CB1.set_label("500 keV  flux")
    CB2.set_label("3 MeV  flux")
    CB3.set_label("6 MeV  flux")
    ax1.set_ylabel(r"$\alpha_{eq}$")
    ax2.set_ylabel(r"$\alpha_{eq}$")
    ax3.set_ylabel(r"$\alpha_{eq}$")
    ax3.set_xlabel("time [sec]")
    ax1.set_title("MLT = 21")
    for ax in (ax1,ax2):
        setp(ax.get_xticklabels(),visible=False)

    
    for ax in (ax1,ax2,ax3):
        ax.set_xticks(np.linspace(0,np.shape(flux1)[2],5))
        ax.set_xticklabels(np.linspace(0,1200,5))
        ax.set_yticks(np.linspace(0,np.shape(flux1[60:120,:,:])[0],7))
        ax.set_yticklabels(np.linspace(60,120,7).astype(str))
        
    fig.savefig("diff_RB005_VAPsmodel_MLT21.png")




     
def readfile(filename):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt)
   
    xp = chunk["xp"]
    yp = chunk["yp"]
    zp = chunk["zp"]
    lp = chunk["lp"]
    xeq = chunk["xeq"]
    yeq = chunk["yeq"]
    zeq = chunk["zeq"]
    aeq = chunk["aeq"]
    beq = chunk["beq"]
    ppara = chunk["ppara"]
    pperp = chunk["pperp"]
    mu = chunk["mu"]
    bp = chunk["bp"]
    prebp = chunk["prebp"]
    db = chunk["db"]
    predb = chunk["predb"] 
    nump = chunk["nump"]
    ierr = chunk["ierr"]
  
    fd.close()     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)
    return xeq[0:180,:] ,yeq[0:180,:],aeq[0:180,:],ene[0:180,:],ierr[0:180,:]


def weighting():
    count = np.empty((grx,gry,gra,gre)) #x,y,a,ene,
    weight = np.empty((128,ptl))

    # file loop 
    for filenum in range(0,128):
        filename = files + str(filenum).zfill(3) +".dat"

    #read file
        xp1, yp1, a1,ene1, frag1 =  readfile(filename) 
        x1,y1 = xp1/6370000,yp1/6370000
        L1 = np.sqrt((x1)**2 + (y1)**2)

    #grid number
        xg, yg, ag, eg = griding(x1,y1,a1,ene1)
   
    #ptl loop
        for m in range(0,ptl):
    #count per bins
                count[xg[0,m],yg[0,m],ag[0,m],eg[0,m]] +=1 

    #weighting
    for filenum2 in range(0,128):
        print "weighting..."
        filename2 = files + str(filenum2).zfill(3) +".dat"
        
        xp2, yp2, a2, ene2, frag2 =  readfile(filename2) 
        x2,y2 = xp2/6370000,yp2/6370000
        L2 = np.sqrt((x2)**2 + (y2)**2)
        #grid number
        xg2, yg2, ag2, eg2 = griding(x2,y2,a2,ene2)

        for m in range(0,ptl):
            if(count[xg2[0,m],yg2[0,m],ag2[0,m],eg2[0,m]]):
                weight[filenum2,m] = np.exp(-ene2[0,m]/1e6)
               #weight[filenum2,m] =1./count[xg2[0,m],yg2[0,m],ag2[0,m],eg2[0,m]]

                
    count = np.sum(count[:,:,:,:],axis=2)
    count = np.sum(count[:,:,:],axis=2)
    
    imshow(count[:,:])
    show()
    return weight

def griding(x,y,a,ene):
    xg = np.around(x/dx).astype(int) + grx/2
    yg = np.around(y/dy).astype(int) + gry/2
    ag = np.around(a/da).astype(int)
    eg = np.around((ene/1e6 -enest)/de).astype(int)
    
    return xg,yg,ag,eg

def fit_exp_linear(parameter,x,y):
    a = parameter[0]
    b = parameter[1]
    residual = y -(a* np.exp(b*x))
    return residual

def exp_string(a,b):
    return "$y = %0.4f e^{ %0.4f x}$" % (a,b)


main()

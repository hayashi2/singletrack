# -*- coding: utf-8 -*-
from pylab import *
import numpy as np
import scipy.optimize
import time

m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 

ptl = 10000

iniene = 1
enest = iniene-1
#dx = 0.5       #Re
#dy = 0.5       #Re
dL = 0.5 
dphi = float(1.0/180.0)*np.pi*15. #MLT
#dA = dx*dy
da = float(1.0/180.0)*np.pi       #rad
de = 0.5        #MeV
dt = 6          #sec

#grx = 2*int(15/dx)
#gry = 2*int(15/dx)
grL = int((10.-5.5)/dL)
grML = 2*int(np.pi/dphi)
gra = int(np.pi/da)
gre = int(20/de)
grt =180
arx = 15
ary = 30

files ="/work/m_hayashi/RBF7/ptl"


def main():
    start =time.time()
    #w = weighting()
    w = np.ones((128,ptl))  #particle weight
    count = np.zeros((grL,grML,gra,gre,grt)) #L,phi,a,ene,t
    flux1 = np.zeros((grL,grML,gra,gre,grt))
    
    """""""""""""""""""""""""""""""""""""""""""""
    read ptl file
    """"""""""""""""""""""""""""""""""""""""""""" 
    for filenum in xrange(0,128):
        print "counting...", filenum
        filename = files + str(filenum).zfill(3) +".dat"

        xp,yp,aeq,beq,ene,ierr,zeq=  readfile(filename)
        x, y,zeq= xp/6370000,yp/6370000,zeq/6370000
    
        """""""""""""""""""""""""""""""""""""""""""""
        data ===> drid
        """""""""""""""""""""""""""""""""""""""""""""
        mltg, ag, eg ,Lg= griding(x,y,aeq,ene,beq*1e9)   

        """""""""""""""""""""""""""""""""""""""""""""
        count per bins
        """""""""""""""""""""""""""""""""""""""""""""
        for t in xrange(0,grt):
            for m in xrange(0,ptl):
                if(ierr[t,m]==0 and np.abs(zeq[t,m])< 1.) :
                    count[Lg[t,m],mltg[t,m],ag[t,m],eg[t,m],t] += w[filenum,m]/(float(Lg[t,m])/2 +5.5)
                
    del mltg, ag, eg ,Lg

    """""""""""""""""""""""""""""""""""""""""""""
    calculate fluxes
    """""""""""""""""""""""""""""""""""""""""""""
    for an in xrange(1,gra-1):
        ap = (an)*np.pi/gra
        flux1[:,:,an,:,:] = count[:,:,an,:,:]/(dL*dphi*da*de*dt*2*np.pi*(sin(ap))) 

    #np.savez("/work/m_hayashi/output/fluxdata/fluxdata9.npz",flux=flux1)
    print flux1

    elapsed_time = time.time()-start
    print "elapsed_time:{0}".format(elapsed_time) +" [sec]"
    



def readfile(filename):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt)
   
    #xp = chunk["xp"]
    #yp = chunk["yp"]
    #zp = chunk["zp"]
    #lp = chunk["lp"]
    xeq = chunk["xeq"]
    yeq = chunk["yeq"]
    zeq = chunk["zeq"]
    aeq = chunk["aeq"]
    beq = chunk["beq"]
    ppara = chunk["ppara"]
    pperp = chunk["pperp"]
    #mu = chunk["mu"]
    #bp = chunk["bp"]
    #prebp = chunk["prebp"]
    #db = chunk["db"]
    #predb = chunk["predb"] 
    #nump = chunk["nump"]
    ierr = chunk["ierr"]
      
    fd.close()     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)
    return xeq[0:grt,:] ,yeq[0:grt,:],aeq[0:grt,:],beq[0:grt,:],ene[0:grt,:],ierr[0:grt,:],zeq[0:grt]


def griding(x,y,a,ene,beq):
    Lg = np.empty((grt,ptl))
    
    mltg = np.around((np.degrees(np.arctan2(y,x)+np.pi))/15).astype(int)-1
    ag = np.around(a/da).astype(int)
    eg = np.around((ene/1e6 -enest)/de).astype(int)
    for m in xrange(0,ptl):
        for t in xrange(0,grt):
            
            if(beq[t,m] >161.):
                Lg[t,m] = 0   #L=5.5
            
            elif(161.>= beq[t,m]>122.4):
                Lg[t,m] = 1   #L=6.0
            
            elif(122.4>= beq[t,m]>94.):
                Lg[t,m] = 2   #L=6.5
                
            elif(94.>= beq[t,m]>72.6):
                Lg[t,m] = 3   #L=7.0

            elif(72.6>= beq[t,m]>56.):
                Lg[t,m] = 4   #L=7.5
                
            elif(56.>= beq[t,m]>43.4):
                Lg[t,m] = 5   #L=8.0

            elif(43.4>= beq[t,m]>33.2):
                Lg[t,m] = 6   #L=8.5

            elif(33.2>= beq[t,m]>25.3):
                Lg[t,m] = 7   #L=9.0

            elif(25.3>= beq[t,m]):
                Lg[t,m] = 8   #L=9.5
            
    return mltg,ag,eg,Lg

main()

# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 13:49:29 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 100
ppath = "/work/m_hayashi/RB"
fname = "ptl"
iniene = 1
outpath ="/home/m_hayashi/output/demo/"
def main():
   t = 1
   while(t <= 414):
      clf()
      energy1,x1,y1=[],[],[]
      energy2,x2,y2=[],[],[]
      energy3,x3,y3=[],[],[]
      setfig(t)
      
      num=0
      
      while(num <= 127):
         filename= ppath +"019/"+ fname +str(num).zfill(3) +".dat"
         energy1 = np.append(energy1,readfile(filename,t)[0])
         x1 = np.append(x1,readfile(filename,t)[1])
         y1 = np.append(y1,readfile(filename,t)[2])
         filename= ppath +"020/" +fname +str(num).zfill(3) +".dat"
         energy2 = np.append(energy2,readfile(filename,t)[0])
         x2 = np.append(x2,readfile(filename,t)[1])
         y2 = np.append(y2,readfile(filename,t)[2])

         filename= ppath +"021/" +fname +str(num).zfill(3) +".dat"
         energy3 = np.append(energy3,readfile(filename,t)[0])
         x3 = np.append(x3,readfile(filename,t)[1])
         y3 = np.append(y3,readfile(filename,t)[2])         
         num+=1
      
      scatter(x1,y1,c=(energy1-iniene),cmap=cm.jet,s=5,alpha=0.9,lw=0,vmin=-1.8,vmax=1.8 )
      scatter(x2,y2,c=(energy2-iniene),cmap=cm.jet,s=5,alpha=0.9,lw=0,vmin=-1.8,vmax=1.8 )
      scatter(x3,y3,c=(energy3-iniene),cmap=cm.jet,s=5,alpha=0.9,lw=0,vmin=-1.8,vmax=1.8 )
      ax=colorbar()    
      ax.set_label('delta Energy [MeV]',fontsize=15)
     # ax.set_clim(1.40,2.20)
      ax.set_clim(-1.8,1.8)
      grid()
      savefig(outpath+'positionXZ%03d.jpeg' %((t-1)/4))
      print 'saved %d'%(t*6+6)
      t+=4
      
def setfig(n):
   figure(figsize=(12,10))  
   wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='k')
   wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='w')    
   fig = gcf()
   fig.gca().add_artist(wedge1)
   fig.gca().add_artist(wedge2)    
   
   plot((-10,10), (0,0 ), color='gray',linestyle='--')
   plot((0,0),(-10,10),'gray',linestyle='--')
   
   xlim(-11,11)
   ylim(-11,11)
   setp(gca().get_xticklabels(), fontsize=20, visible=True)
   setp(gca().get_yticklabels(), fontsize=20, visible=True)
   ylabel("y[Re]",fontsize=25)
   xlabel("x[Re]",fontsize=25)
    #Convert Global-MHD coordinate ->GSM
   xticks([-10,-5,0,5,10],["10","5","0","-5","-10"])
   yticks([-10,-5,0,5,10],["10","5","0","-5","-10"])
   title("time="+str(n*6+6)+"s")
   draw()

def readfile(filename,t):
   ene=[]      
   head = ("head","<i")
   tail = ("tail","<i")
   rtime = ("rtime","<f8")
   nnp = ("np","<i4")
   maxptl =("maxptl","<i4")
   xxp = ("xp","<"+str(ptl)+"f8")
   yyp = ("yp","<"+str(ptl)+"f8")
   zzp = ("zp","<"+str(ptl)+"f8")
   llp = ("lp","<"+str(ptl)+"f8")
   xxeq = ("xeq","<"+str(ptl)+"f8")
   yyeq = ("yeq","<"+str(ptl)+"f8")
   zzeq = ("zeq","<"+str(ptl)+"f8")
   aaeq = ("aeq","<"+str(ptl)+"f8")
   bbeq = ("beq","<"+str(ptl)+"f8")
   p_ppara = ("ppara","<"+str(ptl)+"f8")
   p_pperp = ("pperp","<"+str(ptl)+"f8")
   mmu = ("mu","<"+str(ptl)+"f8")
   bbp = ("bp","<"+str(ptl)+"f8")
   pprebp = ("prebp","<"+str(ptl)+"f8")
   ddb = ("db","<"+str(ptl)+"f8")
   ppredb = ("predb","<"+str(ptl)+"f8")
   nnump = ("nump","<"+str(ptl)+"i4")
   iierr = ("ierr","<"+str(ptl)+"i4")

   dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                  aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                  iierr,tail])
               
   fd = open(filename,"r")
   chunk = np.fromfile(fd, dtype=dt,count=t+1)
   time = chunk[t]["rtime"]
   xp = chunk[t]["xp"].reshape((1,ptl),order="F")
   yp = chunk[t]["yp"].reshape((1,ptl),order="F")
   zp = chunk[t]["zp"].reshape((1,ptl),order="F")
   lp = chunk[t]["lp"].reshape((1,ptl),order="F")
   xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
   yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
   zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
   aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
   beq = chunk[t]["beq"].reshape((1,ptl),order="F")
   ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
   pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
   mu = chunk[t]["mu"].reshape((1,ptl),order="F")
   bp = chunk[t]["bp"].reshape((1,ptl),order="F")
   prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
   db = chunk[t]["db"].reshape((1,ptl),order="F")
   predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
   nump = chunk[t]["nump"].reshape((1,ptl),order="F")
   ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   fd.close()     
   g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
   ene = (m * c**2 * (g - 1) / e)/1000000
   return ene,xp/6380000,yp/6380000
    
main()

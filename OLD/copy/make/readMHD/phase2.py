# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *
from scipy.signal import hilbert,chirp
from matplotlib.ticker import AutoMinorLocator
import matplotlib.patches as mpatches
import matplotlib.ticker as tick
path = "/work/m_hayashi/MHDdata/"
name = "Vsw600_E.npz"
outputpath="/home/m_hayashi/output/phase/"
k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1
ML = np.empty([160,360,101])
phaseML=np.empty([160,360,101])
mML = np.empty([160,360,101])
mxy = np.empty([220,220,101])

def main():
    data=np.load(path+name)
    ex = data["ex"]
    ey = data["ey"]
    ez = data["ez"] 
    ephi = data["ephi"]

    for t in range(0,101):
        
        for phi in range(0,360):
            for l in range(0,160):
                x = l*np.cos(np.radians(phi-180))
                y = l*np.sin(np.radians(phi-180))

                x = -x
                x = (3*x/10 + 90).astype(int)
                y = (3*y/10 + 120).astype(int)
                
                ML[l,phi,t] = ephi[y,x,t] 
                
    for phi in range(0,360):
        for l in range(0,160):
            analytic_E = hilbert(ML[l,phi,:])
            amplitude_envelope = np.abs(analytic_E)
            #ins_phase = np.unwrap(np.angle(analytic_E))
            ins_phase = np.angle(analytic_E)
            ins_fre =np.diff(ins_phase)/(2.0*np.pi)*201
            
            phaseML[l,phi,:] = ins_phase
            
    print "end hirbert"

    
    for l in range(0,160):
        mML[l,0:358,:] =(phaseML[l,2:360,:]-phaseML[l,0:358,:])*(180./float(3*2*np.pi))
   # mML[:,359,:] = mML[:,358,:]

    print "end grad"

    for t in range(0,101):
        for l in range(0,160):
            for phi in range(1,359):
               if(np.abs(mML[l,phi,t]) <= 0.01 ):
                   mML[l,phi,t] = (mML[l,phi+1,t]+mML[l,phi-1,t])/2
               else:
                   pass

    for y in range(0,220):
        for x in range(0,220):
            Y = -y+110
            X = -x + 110
            L = (np.sqrt((float(X))**2 +(float(Y))**2)).astype(int)
            PHI = np.degrees(np.arctan2(X,Y)).astype(int) +179
            mxy[y,x,:] = mML[L,PHI,:]            
    print "end reshape"  

    mxy[:,0:219,:] = (mxy[:,1:220,:] + mxy[:,0:219,:])/2

    """
    for t in range(0,201):
        for y in range(0,220):
            for x in range(1,219):
               if(np.abs(mxy[y,x,t]) <= 0.01 ):
                   mxy[y,x,t] = (mxy[y,x+1,t]+mxy[y,x-1,t])/2
               else:
                   pass
    """

    for t in range(0,101):
        print str(t)
        clf()
        figure(figsize=(10,8))
        imshow(mxy[:,-1::-1,t].T ,aspect="auto",origin="lower",extent=[-11,11,11,-11],vmin=-4,vmax=4)
        
        title("time=" +str(12*t)+"sec",fontsize=22)
        xlabel("x [Re]",fontsize=22)
        ylabel("y [Re]",fontsize=22)
        xticks(fontsize=20)
        yticks(fontsize=20)
        CB=colorbar(shrink=1)
        CB.set_label('m',fontsize=22)
        CB.set_ticks(np.arange(-4,5.))
        CB.ax.set_yticklabels([str(i) for i in arange(-4.,5.)],fontsize=20)
        wedge0 = mpatches.Wedge((0,0), 5, -180, 180,fc='w',linewidth = 0)
        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='k')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='w')    
        fig = gcf()
        fig.gca().add_artist(wedge0)
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)
    
        savefig(outputpath+"mnum"+str(t).zfill(3) +".jpeg")
main()

# -*- coding: utf-8 -*-
"""
Created on Fri Dec 18 11:32:35 2015

@author: m_hayashi
"""

import os

path = "/home/m_hayashi/output/RB"
rmdir = "hist"
dirnum = 1
while(dirnum<=30):
    dirname = path + str(dirnum).zfill(3) + "/" + rmdir
    os.removedirs(dirname) #empty dir
    dirnum += 1

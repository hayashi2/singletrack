import numpy as np
from pylab import *
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

time = 0
nx = np.arange(90,-207,-1)
ny = (np.ones(297))*(120)
na = np.empty([241,297])
nb = np.empty([241,297])
n=0
while(n<=240):
    na[n] = nx
    nb[n] = ny - n
    n+=1
    
cosphi=na/np.sqrt((na)**2+(nb)**2)
sinphi=nb/np.sqrt((na)**2+(nb)**2)

print cosphi
print sinphi

while(time <= 208 ):
    
    path = "/work/ymatumot/simulation/GM-RB/run_Vsw400/"
    ename = "MHD_E"

    filename = path + ename + str(time).zfill(3) + ".dat"

    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<51750171f4")
         
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*2.2
    bt=bf.transpose(1,0,2,3)
    ey = -bt[:,:,120,1]
    ex = -bt[:,:,120,0]

    ephi = ey*cosphi - ex*sinphi

    figure(figsize=(12,10))
    #Convert Global-MHD coodinate ->GSM
    xlim(30,-69)
    ylim(40,-40)
    imshow(ephi,extent=[30,-69,40,-40],origin='lower', vmax=2.64,vmin=-4.84)
    xlabel("x(GSM)[Re]")
    ylabel("y(GSM)[Re]")
    title("time="+str(time*12)+"s")

    CB=colorbar()   
    CB.set_label('Ephi [mV/m]',fontsize=15)
    CB.set_clim(-4.84,2.64)  
    
    wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
    wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
    fig = gcf()
    fig.gca().add_artist(wedge1)
    fig.gca().add_artist(wedge2)

    savefig("EphiL"+str(time/2)+".jpeg")
    print "saved ",time*12
    time += 2


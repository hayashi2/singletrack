# -*- coding: utf-8 -*-
import numpy as np
import matplotlib as mpl
mpl.use("Agg")
from pylab import *

m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8
re = 6370000 # m => Re

filenum = 1
mptl = 1
dt = 1
tmax = 500

path = "/work/m_hayashi/RBdata/RB_T/"

def main():
    for i in xrange(0,filenum):
        filename = path +"ptl"+str(i).zfill(3)+ ".dat"
        xeq,yeq,ene = readptl(filename)
        print (xeq[0]/re,yeq[0]/re)
        print (xeq[-1]/re,yeq[-1]/re)
        print("reading... "+filename)
        fig = figure()
        ax = fig.add_subplot(111)
        ax.scatter(xeq[0]/re,yeq[0]/re)
        fig.savefig("test.png")

def readptl(filename):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(mptl)+"f8")
    yyp = ("yp","<"+str(mptl)+"f8")
    zzp = ("zp","<"+str(mptl)+"f8")
    llp = ("lp","<"+str(mptl)+"f8")
    xxeq = ("xeq","<"+str(mptl)+"f8")
    yyeq = ("yeq","<"+str(mptl)+"f8")
    zzeq = ("zeq","<"+str(mptl)+"f8")
    aaeq = ("aeq","<"+str(mptl)+"f8")
    bbeq = ("beq","<"+str(mptl)+"f8")
    p_ppara = ("ppara","<"+str(mptl)+"f8")
    p_pperp = ("pperp","<"+str(mptl)+"f8")
    mmu = ("mu","<"+str(mptl)+"f8")
    bbp = ("bp","<"+str(mptl)+"f8")
    pprebp = ("prebp","<"+str(mptl)+"f8")
    ddb = ("db","<"+str(mptl)+"f8")
    ppredb = ("predb","<"+str(mptl)+"f8")
    nnump = ("nump","<"+str(mptl)+"i4")
    iierr = ("ierr","<"+str(mptl)+"i4")
    
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu,bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt)
    
    time = chunk["rtime"]
    xp = chunk["xp"]
    yp = chunk["yp"]
    zp = chunk["zp"]
    lp = chunk["lp"]
    xeq = chunk["xeq"]
    yeq = chunk["yeq"]
    zeq = chunk["zeq"]
    aeq = chunk["aeq"]
    beq = chunk["beq"]
    ppara = chunk["ppara"]
    pperp = chunk["pperp"]
    mu = chunk["mu"]
    bp = chunk["bp"]
    prebp = chunk["prebp"]
    db = chunk["db"]
    predb = chunk["predb"] 
    nump = chunk["nump"]
    ierr = chunk["ierr"]
    
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)       
    ene = (m * c**2 * (g - 1) / e)/1e6   #eV => MeV

    return xeq,yeq,ene 


main()


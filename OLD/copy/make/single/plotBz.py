import numpy as np
from pylab import *
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

path = "/work/saito/GMHDRB/inputdata/"
name = "MHDB_RB"
b1,b2,b3 = [],[],[]
t = [] 
time = 0
while(time <= 2376):

    filename = path + name + str(time).zfill(6) + ".dat"

    head = ("head","<i")
    tail = ("tail","<i")
    bz = ("bz","<6291456f4")
         
    dt = np.dtype([head,bz,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["bz"].reshape((128,128,128,3),order="F")
    bt = bf.transpose(1,0,2,3)

    b1 = np.append(b1,bt[64,109,64,2]*(1e9))
    b2 = np.append(b2,bt[64,88,64,2]*(1e9))
    b3 = np.append(b3,bt[64,40,64,2]*(1e9))
    t = np.append(t,time)
    time += 12

subplots_adjust(hspace = 1.0)
subplot(3,1,1)
plot(t,b1)
xlabel("time[sec]")
ylabel("Bz[nT]")
title("(15,0)")

subplot(3,1,2)
plot(t,b2)
xlabel("time[sec]")
ylabel("Bz[nT]")
title("(8,0)")

subplot(3,1,3)
plot(t,b3) 
xlabel("time[sec]")
ylabel("Bz[nT]")
title("(-8,0)")

show()
# savefig("EphiL"+str(time/24)+".jpeg")
 #   print "saved ",time

# -*- coding: utf-8 -*-
from pylab import *
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches

m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 1000
files ="/work/m_hayashi/RBF4/ptl"
t1=0
iniene = 2
enest = iniene -1
outputtitle= "2MeV   12MLT"
dA = 0.5*0.5
da = float(1.0/360.0)*np.pi
de = 0.2e6
dt = 6
dL = 0.1
dphi =np.pi/60 
#dWg =(1+(enest-enest))*5 

def main():
    w = weighting()

    count = np.zeros((250,180,80,200,4)) #L,a,ene,t
    flux1 = np.zeros((250,180,80,200,4))
    flux = np.zeros((250,200,4))         # integral of a and ene
    
    for t2 in range(0,200):
        print t2
        clf()
    # file loop 
        for filenum in range(0,128):
            filename = files + str(filenum).zfill(3) +".dat"

    #read file
            xp1,yp1,a1,mu1,ene1,ierr1 =  readfile(filename,t1) 
            x1,y1 = xp1/6370000,yp1/6370000
            L1 = np.sqrt((x1)**2 + (y1)**2)

   
            xp2,yp2,a2,mu2,ene2,ierr2 =  readfile(filename,t2)
            x2,y2 = xp2/6370000,yp2/6370000
            L2 = np.sqrt((x2)**2 + (y2)**2)
    	    dg2= np.arctan2(y2,x2)*180/np.pi +180
            mlt= dg2*1/3 #degree to mlt

       #grid number
            xg, yg, ag, eg = griding(x2,y2,a2,ene2)
            Lg = (10*L2).astype(int)
	    MLTg= mlt.astype(int)
            energy = ene2/1e6
    #ptl loop
            for m in range(0,ptl):

                #count per bins
                if(ierr2[0,m]==0 and Lg[0,m]<=250 and(MLTg[0,m] <= 1 or MLTg[0,m] >= 117)): #12MLT
                    if( energy[0,m] < (iniene+0.6)):  #0.6MeV
                         count[Lg[0,m],ag[0,m],eg[0,m],t2,0] = count[Lg[0,m],ag[0,m],eg[0,m],t2,0] + w[filenum,m]/L2[0,m]

                    if((iniene+0.6) < energy[0,m] < (iniene+1.0)): #1.0MeV
                        count[Lg[0,m],ag[0,m],eg[0,m],t2,1] = count[Lg[0,m],ag[0,m],eg[0,m],t2,1] + w[filenum,m]/L2[0,m]

                    if((iniene+1.0) < energy[0,m] < (iniene+1.4)): #1.4MeV
                         count[Lg[0,m],ag[0,m],eg[0,m],t2,2] = count[Lg[0,m],ag[0,m],eg[0,m],t2,2] + w[filenum,m]/L2[0,m]
      
                    if((iniene+1.4) < energy[0,m] < (iniene+1.6)): #1.6MeV
                        count[Lg[0,m],ag[0,m],eg[0,m],t2,3] = count[Lg[0,m],ag[0,m],eg[0,m],t2,3] + w[filenum,m]/L2[0,m]
    
    # end file loop

    #calicurate flix
        for an in range(0,180):
            ap = (an -0.5)*np.pi/180
            flux1[:,an,:,t2,:] += count[:,an,:,t2,:]/(dL*dphi*da*de*dt*np.abs((np.cos(ap)))*2*np.pi*(sin(ap)))               
    #integral of a and ene                       
        for i in range(0,180):
            for j in range(0,80):
                flux[:,t2,:] +=  flux1[:,i,j,t2,:]
    figure(1)
    #show images
    suptitle(outputtitle)
    ax1=subplot(221)
    imshow(flux[50:100,:,0],extent=[0,1200,5,10],origin="lower",aspect="auto",vmin=0,vmax=50)
    title(" dW < 0.6MeV",fontsize=10)
    ylabel("R [Re]",fontsize=13) 

    ax2= subplot(223,sharex=ax1)
    imshow(flux[50:100,:,1],extent=[0,1200,5,10],origin="lower",aspect="auto",vmin=0,vmax=50)
    title("0.6 < dW < 1.0MeV",fontsize=10)
    xlabel("time[sec]",fontsize=13)
    ylabel("R [Re]",fontsize=13)
  

    ax3=subplot(222,sharey=ax1)
    imshow(flux[50:100,:,2],extent=[0,1200,5,10],origin="lower",aspect="auto",vmin=0,vmax=50)
    title("1.0 < dW < 1.4MeV",fontsize=10)
    #xlabel("time[sec]",fontsize=13)
    
    ax4=subplot(224,sharex=ax3,sharey=ax2)
    imshow(flux[50:100,:,3],extent=[0,1200,5,10],origin="lower",aspect="auto",vmin=0,vmax=50)
    title("1.4 < dW < 1.6MeV",fontsize=10)
    xlabel("time[sec]",fontsize=13)
 

    figure(2)
    imshow(np.ones((2,2)),vmax=50,vmin=0)
    CB=colorbar()   
    CB.set_label('flux [/Re$^2$ sec str MeV]',fontsize=15)
    CB.ax.tick_params(labelsize=13)    
    show()
    
def readfile(filename,t):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
    xp = chunk[t]["xp"].reshape((1,ptl),order="F")
    yp = chunk[t]["yp"].reshape((1,ptl),order="F")
    zp = chunk[t]["zp"].reshape((1,ptl),order="F")
    lp = chunk[t]["lp"].reshape((1,ptl),order="F")
    xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
    yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
    zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
    aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
    beq = chunk[t]["beq"].reshape((1,ptl),order="F")
    ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
    pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
    mu = chunk[t]["mu"].reshape((1,ptl),order="F")
    bp = chunk[t]["bp"].reshape((1,ptl),order="F")
    prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
    db = chunk[t]["db"].reshape((1,ptl),order="F")
    predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
    nump = chunk[t]["nump"].reshape((1,ptl),order="F")
    ierr = chunk[t]["ierr"].reshape((1,ptl),order="F") 
   
    fd.close()     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)

    return xeq ,yeq,aeq,mu,ene,ierr

def weighting():
    count = np.zeros((150,150,180,80)) #x,y,a,ene,
    weight = np.empty((128,ptl))

    # file loop 
    for filenum in range(0,128):
        filename = files + str(filenum).zfill(3) +".dat"

    #read file
        xp1, yp1, a1, mu1, ene1, frag1 =  readfile(filename,0) 
        x1,y1 = xp1/6370000,yp1/6370000
        L1 = np.sqrt((x1)**2 + (y1)**2)

    #grid number
        xg, yg, ag, eg = griding(x1,y1,a1,ene1)
   
    #ptl loop
        for m in range(0,ptl):
    #count per bins
                count[xg[0,m],yg[0,m],ag[0,m],eg[0,m]] +=1 

    
    for filenum2 in range(0,128):
        filename2 = files + str(filenum2).zfill(3) +".dat"
        
        xp2, yp2, a2, mu2, ene2, frag2 =  readfile(filename2,0) 
        x2,y2 = xp2/6370000,yp2/6370000
        L2 = np.sqrt((x2)**2 + (y2)**2)
        #grid number
        xg2, yg2, ag2, eg2 = griding(x2,y2,a2,ene2)

        for m in range(0,ptl):
            if(count[xg2[0,m],yg2[0,m],ag2[0,m],eg2[0,m]]):
                 weight[filenum2,m] = L2[0,m]*np.abs(np.sin(a2[0,m]))*5/count[xg2[0,m],yg2[0,m],ag2[0,m],eg2[0,m]]
    return weight

def griding(x,y,a,ene):
    xg = np.around(5*x).astype(int) + 75
    yg = np.around(5*y).astype(int) + 75
    ag = np.around(((180/np.pi)*a)).astype(int)
    eg = np.around(5*(ene/1e6 - enest)).astype(int)
    
    return xg,yg,ag,eg



main()

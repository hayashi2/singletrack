# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
from matplotlib.colors import LogNorm,SymLogNorm
import matplotlib.ticker as tic

RUN ="run4"
path ="/work/m_hayashi/GM/"+RUN+"/MHD/"
vname = "MHD_V"
nname = "MHD_N"
pname = "MHD_P"
bname = "MHD_B"

#BOX SIZE
nx = 500
ny = 300
nz = 300

#Standard Value
b0 = 10
e0 = 2.2
v0 = 220.0
n0 = 1.0
p0 = 0.04
k = 1.38e-23
mu0 = 4*np.pi*1e-7

# File Number and Earth Position  
filenum = 121

rx = 120
ry = 150
rz = 150


def main():
    for num in xrange(1,filenum+1):
        print "Reading... "+path+vname+str(num).zfill(3)+".dat"
        Vx = readV(num)
        print "Reading... "+path+nname+str(num).zfill(3)+".dat"
        N = readN(num)
        print "Reading... "+path+pname+str(num).zfill(3)+".dat"
        P =readP(num)
        print "Reading... "+path+bname+str(num).zfill(3)+".dat"
        B = readB(num)
        
        T = (P*1e-9)/k/(N*1e6)
        Pb = ((B*1e-9)**2)/2/mu0*1e9 #nPa
        Pdyn = (2e-6)*N*(Vx**2) #nPa
        clf()
        fig = figure()
        ax1 = fig.add_subplot(611)
        ax2 = fig.add_subplot(612)
        ax3 = fig.add_subplot(613)
        ax4 = fig.add_subplot(614)
        ax5 = fig.add_subplot(615)
        ax6 = fig.add_subplot(616)

        ax1.plot(np.arange(100./4, 5. ,-0.25),Vx[21:101]) #25-5Re
        ax2.plot(np.arange(100./4, 5. ,-0.25),N[21:101])
        ax3.plot(np.arange(100./4, 5. ,-0.25),Pdyn[21:101])
        ax4.plot(np.arange(100./4, 5. ,-0.25),P[21:101])
        ax5.plot(np.arange(100./4, 5. ,-0.25),Pb[21:101])
        ax6.plot(np.arange(100./4, 5. ,-0.25),T[21:101])

        ax1.set_title(RUN + "   time = " +str(num*11)+" sec")
        ax1.set_ylabel("Vx \n[km/s]",fontsize=14)
        ax2.set_ylabel("N \n[/cm^3]",fontsize=14)
        ax3.set_ylabel("$P_{dyn}$ \n[nPa]",fontsize=14)
        ax4.set_ylabel("P \n[nPa]",fontsize=14)
        ax5.set_ylabel("$P_B$ \n[nPa]",fontsize=14)
        ax6.set_ylabel("T \n[K]",fontsize=14)
        ax6.set_xlabel("X [Re]",fontsize=14)
        ax5.set_yscale("log")
        ax6.set_yscale("log")
        ax1.set_ylim(-500,200)
        ax2.set_ylim(3,30)
        ax3.set_ylim(1,8)
        ax4.set_ylim(0,2)
        ax5.set_ylim(1e-3,1e2)
        ax6.set_ylim(8e4,1e7)
        ax1.set_yticks(np.linspace(-400,200,4))
        ax2.set_yticks(np.linspace(4,28,5))
        ax3.set_yticks(np.linspace(1,7,4))
        ax4.set_yticks(np.linspace(0.,2.,5))
        ax5.set_yticks([1e-2,1e0,1e2])
        ax6.set_yticks([1e5,1e6,1e7])
        for ax in (ax1,ax2,ax3,ax4):
            ax.grid()
            ax.set_xlim(100./4, 5.)
            ax.xaxis.set_minor_locator(tic.AutoMinorLocator())
            ax.yaxis.set_minor_locator(tic.AutoMinorLocator())
            ax.yaxis.set_label_coords(-0.08,0.5)
        for ax in (ax5,ax6):
            ax.grid()
            ax.set_xlim(100./4, 5.)
            ax.xaxis.set_minor_locator(tic.AutoMinorLocator())
            ax.yaxis.set_label_coords(-0.08,0.5)
        for ax in (ax1,ax2,ax3,ax4,ax5):
            setp(ax.get_xticklabels(),visible=False)

        fig.subplots_adjust(hspace=0.2)
        fig.savefig("/work/m_hayashi/figure/"+RUN+"/Para/Para_"+RUN+"_"+str(num)+".png")
        close(fig)

def readV(num):

    filename = path + vname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    v=("v","<"+str(nx*ny*nz*3)+"f4")
    
    dt = np.dtype([head,v,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    vf = chunk["v"].reshape((nx,ny,nz,3),order="F")*v0
    
    #convert GSM
    vx = -vf[:,:,:,0]
    return vx[0:rx,ry,rz]

def readN(num):  
    filename = path + nname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    n=("n","<"+str(nx*ny*nz)+"f4")
    
    dt = np.dtype([head,n,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    nf = chunk["n"].reshape((nx,ny,nz),order="F")*n0
    
    return nf[0:rx,ry,rz] 

def readP(num):  
    filename = path + pname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    p=("p","<"+str(nx*ny*nz)+"f4")
    
    dt = np.dtype([head,p,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    pf = chunk["p"].reshape((nx,ny,nz),order="F")*p0
    
    return pf[0:rx,ry,rz]

def readB(num):

    filename = path + bname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b = ("b","<"+str(nx*ny*nz*3)+"f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    bf = chunk["b"].reshape((nx,ny,nz,3),order="F")*b0
    
    #convert GSM
    b = np.sqrt(bf[0:rx,ry,rz,0]**2 + bf[0:rx,ry,rz,1]**2 + bf[0:rx,ry,rz,2]**2)
    return b

main()

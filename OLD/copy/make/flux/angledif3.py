# -*- coding: utf-8 -*-
from pylab import *
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches
import matplotlib.cm as cm

m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 1000
files ="/work/m_hayashi/RBF4/ptl"
t1=0
iniene = 2
enest = iniene -1
dA = 0.5*0.5
da = float(1.0/360.0)*np.pi
de = 0.4e6
dt = 6
dL = 0.1
BL6=161
BL7=94
BL8=56
BL7f=76
def main():
    count = np.zeros((180,100,7)) #a,t
   
    for t2 in range(0,100):
        clf()
        print t2
    # file loop 
        for filenum in range(0,128):
            filename = files + str(filenum).zfill(3) +".dat"

    #read file   
            xp,yp,a,mu,ene,bp =  readfile(filename,t2)
            x,y = xp/6370000,yp/6370000
            L = np.sqrt((x)**2 + (y)**2)
    	    dg2= np.arctan2(y,x)*180/np.pi +180
            mlt= dg2*1/3 #gedree to mlt

    #grid number
            xg = (2*x).astype(int) + 30
            yg = (2*y).astype(int) + 30
            ag = ((180/np.pi)*a).astype(int)
            eg = (5*(ene/1e6-enest)).astype(int)
            Lg = (10*L).astype(int)
	    MLTg= mlt.astype(int)
    #ptl loop
            for m in range(0,ptl):

                #count per bins
                if((bp[0,m] <= BL7 and bp[0,m] >= BL7f) and (MLTg[0,m] <= 1 or MLTg[0,m] >= 117)):
                    if(ene[0,m] <=iniene+0.2):
                        count[ag[0,m],t2,0] += 1
                   
                    for i in np.arange(0.2,1.4,0.2):
                        if((iniene+i) < ene[0,m] <= (iniene+i+0.2)):
                            j=(5*i).astype(int)
                            count[ag[0,m],t2,j] += 1
    
    # end file loop
    
    for t in range(0,100):
        clf()
        figure(figsize=(8,6))
        for k in range(0,7):
            x = np.arange(20,160)
            y = count[20:160,t,k]
            title("2MeV 12MLT  "+ str(t*6)+"sec")
            plot(x,y,color=cm.jet(float(k)/6),linewidth=2,label=str(iniene+float(2*k)/10)+"< w <"+str(iniene+float(k)/10 +0.2))
        
        legend(loc="upper right",fontsize=10)
        xlabel("pitch angle [deg]")
        ylabel("count")
        ylim(0,400)
        savefig("/home/m_hayashi/output/flux/aeq/aeq_2dis"+str(t).zfill(3)+".jpeg")

def readfile(filename,time):
    t=2*time
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
    xp = chunk[t]["xp"].reshape((1,ptl),order="F")
    yp = chunk[t]["yp"].reshape((1,ptl),order="F")
    zp = chunk[t]["zp"].reshape((1,ptl),order="F")
    lp = chunk[t]["lp"].reshape((1,ptl),order="F")
    xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
    yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
    zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
    aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
    beq = chunk[t]["beq"].reshape((1,ptl),order="F")
    ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
    pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
    mu = chunk[t]["mu"].reshape((1,ptl),order="F")
    bp = chunk[t]["bp"].reshape((1,ptl),order="F")
    prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
    db = chunk[t]["db"].reshape((1,ptl),order="F")
    predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
    nump = chunk[t]["nump"].reshape((1,ptl),order="F")
    ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   
    fd.close()     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)
    return xeq ,yeq,aeq,mu,ene/1e6,beq*1e9

main()

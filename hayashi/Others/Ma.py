# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
from matplotlib.colors import LogNorm,SymLogNorm
import matplotlib.ticker as tic

RUN ="run1"
path ="/work/m_hayashi/GM/"+RUN+"/MHD/"
vname = "MHD_V"
nname = "MHD_N"
pname = "MHD_P"
bname = "MHD_B"

#BOX SIZE
nx = 500
ny = 300
nz = 300

#Standard Value
b0 = 10
e0 = 2.2
v0 = 220.0
n0 = 1.0
p0 = 0.04
k = 1.38e-23
mu0 = 4*np.pi*1e-7

# File Number and Earth Position  
filenum = 100

rx = 122
ry = 151
rz = 151


def main():
    for num in (1,100):
        print "Reading... "+path+vname+str(num).zfill(3)+".dat"
        Vx = readV(num)
        print "Reading... "+path+nname+str(num).zfill(3)+".dat"
        N = readN(num)
        print "Reading... "+path+pname+str(num).zfill(3)+".dat"
        P =readP(num)
        print "Reading... "+path+bname+str(num).zfill(3)+".dat"
        B = readB(num)
        
        T = (P*1e-9)/k/(N*1e6)
        Pb = ((B*1e-9)**2)/2/mu0*1e9 #nPa
        Pdyn = (2e-6)*N*(Vx**2) #nPa

        Ma = np.abs(Vx)*np.sqrt(N)/20./B
        Va = 20.3*B/np.sqrt(N)
        Vs = 0.12*np.sqrt(T+1.28*1e5)
        Vms = np.sqrt(Vs**2 + Va**2)
        Mms = np.abs(Vx)/Vms

        print "Ma = \n" ,Ma[4],-Vx[4]/Va[4]
        print "Mms = \n",Mms[4]

def readV(num):

    filename = path + vname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    v=("v","<"+str(nx*ny*nz*3)+"f4")
    
    dt = np.dtype([head,v,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    vf = chunk["v"].reshape((nx,ny,nz,3),order="F")*v0
    
    #convert GSM
    vx = -vf[:,:,:,0]
    return vx[0:rx,ry,rz]

def readN(num):  
    filename = path + nname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    n=("n","<"+str(nx*ny*nz)+"f4")
    
    dt = np.dtype([head,n,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    nf = chunk["n"].reshape((nx,ny,nz),order="F")*n0
    
    return nf[0:rx,ry,rz] 

def readP(num):  
    filename = path + pname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    p=("p","<"+str(nx*ny*nz)+"f4")
    
    dt = np.dtype([head,p,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    pf = chunk["p"].reshape((nx,ny,nz),order="F")*p0
    
    return pf[0:rx,ry,rz]

def readB(num):

    filename = path + bname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b = ("b","<"+str(nx*ny*nz*3)+"f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"rb")
    chunk = np.fromfile(fd, dtype=dt)
    fd.close()
    
    bf = chunk["b"].reshape((nx,ny,nz,3),order="F")*b0
    
    #convert GSM
    b = np.sqrt(bf[0:rx,ry,rz,0]**2 + bf[0:rx,ry,rz,1]**2 + bf[0:rx,ry,rz,2]**2)
    return b

main()

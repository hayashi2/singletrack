# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 13:30:36 2015

@author: m_hayashi
"""

import numpy as np
import matplotlib
path = "/work/m_hayashi/GM/run_Vsw600/"  
filename = "MHD_B"
gx = 297
gy = 241
gz = 241
BOXSIZE = gx*gy*gz*3

head = ("head","<i")
tail = ("tail","<i")
b=("b","<"+str(BOXSIZE)+"f4")

dt = np.dtype([head,b,tail])
fd = open(path + filename,"r")
chunk = np.fromfile(fd, dtype=dt,count=1)
fd.close()
    
bf = chunk[0]["b"].reshape((297,241,241,3),order="F")


# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LogNorm

patha = "/work/ymatumot/simulation/GM-RB"
pathb = "/run_Vsw600/"
dname = "MHD_"
k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1
alpha = 6e-16

nx = np.arange(90,-207,-1)
ny = (np.ones(297))*(-120)
na = np.empty([241,297])
nb = np.empty([241,297])
n=0
while(n<=240):
    na[n] = nx
    nb[n] = ny + n
    n+=1

r = np.sqrt((na/3)**2 +(nb/3)**2)
nH = 25*((10/r)**3)


#Convert Global-MHD coordinate ->GSM
def main():
    rtime = 0
    while(rtime<=208):
        vx = -readDATA(0,rtime)*v0
        vy = -readDATA(1,rtime)*v0
        vz = readDATA(2,rtime)*v0
        N  = readDATA(3,rtime)
        Pt  = readDATA(4,rtime)*p0
    
        T  = (Pt*1e-9)/(k*(N*1e6))
        u  = (np.sqrt(vx**2 + vy**2 + vz**2 ))*1e5
        vth = np.sqrt(3.0*k*T/1.67e-23)
        g = np.sqrt(u**2 + vth**2)
   
        P = alpha*(nH)*N[:,:,120]*g[:,:,120]

        figure(figsize=(12,10))
    #Convert Global-MHD coodinate ->GSM
        xlim(30,-69)
        ylim(40,-40)
        imshow(P,extent=[30,-69,40,-40],origin='lower',norm=LogNorm(vmin=1e-8,vmax=1e-5))
        xlabel("x(GSM)[Re]")
        ylabel("y(GSM)[Re]")
        title("P_xy  time="+str(rtime*12)+"s")

        CB=colorbar()   
        CB.set_label('P[eV/cm^3 sec]',fontsize=15)
        #CB.set_clim(0,0.8e-5)  
        #CB.formatter.set_powerlimits((0,1))

        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
        fig = gcf()
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)

        savefig("/home/m_hayashi/make/readMHD/Pxy/Pxy"+str(rtime/2)+".jpeg")
        print "saved ",rtime*12
        rtime += 2

    
def readDATA(vn,time):
        
    if vn == 0:
        filename = patha + pathb + dname + "Vx" + str(time).zfill(3) + ".dat"
    elif vn == 1:
        filename = patha + pathb + dname + "Vy" + str(time).zfill(3) + ".dat"
    elif vn == 2:
        filename = patha + pathb + dname + "Vz" + str(time).zfill(3) + ".dat"
    elif vn == 3:
        filename = patha + pathb + dname + "N" + str(time).zfill(3) + ".dat"
    elif vn == 4:
        filename = patha + pathb + dname + "P" + str(time).zfill(3) + ".dat"

    head = ("head","<i")
    tail = ("tail","<i")
    d = ("d","<17250057f4")
        
    dt = np.dtype([head,d,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    df = chunk[0]["d"].reshape((297,241,241),order="F")
    data = df.transpose(1,0,2)
    print str(vn) + " read... " + str(time) 
    
    return data


main()

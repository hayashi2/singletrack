import numpy as np
from pylab import *

ptl = 100
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
filepath =  "/work/m_hayashi/RB021/ptl"

def main():
    for num in range(0,128):
        filename = filepath + str(num).zfill(3)+".dat"
        beq= readptl(filename,0)*1e9
        print beq
        
def readptl(filename,time):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt, count = time+1)
    
    n=time
    xp = chunk[n]["xp"].reshape((1,ptl),order="F")
    yp = chunk[n]["yp"].reshape((1,ptl),order="F")
    zp = chunk[n]["zp"].reshape((1,ptl),order="F")
    lp = chunk[n]["lp"].reshape((1,ptl),order="F")
    xeq = chunk[n]["xeq"].reshape((1,ptl),order="F")
    yeq = chunk[n]["yeq"].reshape((1,ptl),order="F")
    zeq = chunk[n]["zeq"].reshape((1,ptl),order="F")
    aeq = chunk[n]["aeq"].reshape((1,ptl),order="F")
    beq = chunk[n]["beq"].reshape((1,ptl),order="F")
    ppara = chunk[n]["ppara"].reshape((1,ptl),order="F")
    pperp = chunk[n]["pperp"].reshape((1,ptl),order="F")
    mu = chunk[n]["mu"].reshape((1,ptl),order="F")
    bp = chunk[n]["bp"].reshape((1,ptl),order="F")
    prebp = chunk[n]["prebp"].reshape((1,ptl),order="F")
    db = chunk[n]["db"].reshape((1,ptl),order="F")
    predb = chunk[n]["predb"].reshape((1,ptl),order="F") 
    nump = chunk[n]["nump"].reshape((1,ptl),order="F")
    ierr = chunk[n]["ierr"].reshape((1,ptl),order="F")

    
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)       
    ene = (m * c**2 * (g - 1) / e)/1000000
    
    return beq
main()

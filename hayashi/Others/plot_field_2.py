# -*- coding: utf-8 -*-
import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
import matplotlib.ticker as tic
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

# BOX SIZE
gx = 500
gy = 300
gz = 300
dx = 1./4
dy = 1./4
dz = 1./4
#Earth position
rx = 120
ry = 150
rz = 150

e0 = 2.2
b0 = 10.0

#xgsm = 0
#ygsm = 7.
xgsm =  3.50
ygsm = 6.06
rgsm = np.sqrt(xgsm**2 + ygsm**2)
phi = np.arctan2(ygsm,xgsm)
r = np.array([rgsm*np.cos(phi),rgsm*np.sin(phi),0])

px = int(round(-xgsm/dx + rx))
py = int(round(-ygsm/dy + ry))
tmax = 100 #100*12
deltt = 11
RUN = "run3"
path = "/work/m_hayashi/GM/"+RUN+"/MHD/"
ename = "MHD_E"
bname = "MHD_B"
#savefile = "/work/m_hayashi/"+RUN+"_Ephi.npz"

def main():
    
    
    nx = -(np.arange(0,gx)-rx)
    ny = -(np.arange(0,gy)-ry)
    NY,NX = np.meshgrid(ny,nx)
    print NX,NY
    cosphi = NX.astype(float)/np.sqrt(NX**2 + NY**2)
    sinphi = NY.astype(float)/np.sqrt(NX**2 + NY**2)
    ephi = []
    bx,by,bz = np.zeros((tmax)),np.zeros((tmax)),np.zeros((tmax))
    Bx,By,Bz = np.zeros((tmax)),np.zeros((tmax)),np.zeros((tmax))
    for time in xrange(0,tmax):
        #### read E file ####
        filename = path + ename + str(time).zfill(3) + ".dat"
        print("Reading...... "+filename)
        head = ("head","<i")
        tail = ("tail","<i")
        b=("b","<"+str(gx*gy*gz*3)+"f4")
         
        dt = np.dtype([head,b,tail])
        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
        #Convert Global-MHD coodinate ->GSM
        bf = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*e0
        ex = -bf[:,:,rz,0]
        ey = -bf[:,:,rz,1]        
        Ephi = ey*cosphi - ex*sinphi
        ephi.append(Ephi[px,py])
        
        #### read B file ####
        filename = path + bname + str(time).zfill(3) + ".dat"
        print("Reading...... "+filename)
        head = ("head","<i")
        tail = ("tail","<i")
        b=("b","<"+str(gx*gy*gz*3)+"f4")
         
        dt = np.dtype([head,b,tail])
        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
        #Convert Global-MHD coodinate ->GSM
        bf = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*b0
        bx[time] = -bf[px,py,rz,0]
        by[time] = -bf[px,py,rz,1]
        bz[time] = bf[px,py,rz,2]
        #Br = bx*cosphi + by*sinphi
        #br[time] = Br[px,py]
        
    for t in xrange(10,tmax-10):
        B = np.array([bx[t],by[t],bz[t]])
        b_ave = np.array([np.average(bx[t-10:t+10]),np.average(by[t-10:t+10]),np.average(bz[t-10:t+10])])
        b_ave_ave = np.sqrt(b_ave[0]**2 + b_ave[1]**2 + b_ave[2]**2)
        ez = b_ave/b_ave_ave
        ey = np.cross(b_ave,r)
        ey = ey/np.linalg.norm(ey)
        ex = np.cross(ey,ez)
        Bx[t] = np.dot(B,ex)
        By[t] = np.dot(B,ey)
        Bz[t] = np.linalg.norm(B) - b_ave_ave
        
    fig = figure(figsize=(12,4))
    ax1 = fig.add_subplot(111)
    
    time = np.arange(0,(tmax)*deltt,11)
    ax1.plot(time,Bx,"k",lw=1.5,label=r"$B_{r}$")
    ax1.plot(time,Bz,"k",lw=1.5,label=r"$B_{\parallel}$")
    ax2 = ax1.twinx()

    ax2.plot(time,ephi,"r",lw=1.5,label=r"$E_{\phi}$")
    ax2.set_xlabel("time [sec]",fontsize=22)
    ax1.set_ylabel("B  [nT]",fontsize=22)
    ax2.set_ylabel(r" $E_{\phi}$"+" [mV/m]",fontsize=22,color="r")
    ax1.tick_params("both",colors="k",labelsize=18)
    ax2.tick_params("y",colors="r",labelsize=18)
    
    ax1.set_title("L = 7  16MLT MLAT = 0")

    ax1.grid()
    ax1.xaxis.set_minor_locator(tic.AutoMinorLocator())
    ax1.yaxis.set_minor_locator(tic.AutoMinorLocator())
    xticks = np.linspace(0,tmax*deltt,9)
    #ax1.set_xticks(xticks)
    #ax.set_xlim(0,1200)
    #ax1.set_xticklabels(np.linspace(0,tmax*deltt,9).astype(str))

    h1, l1 = ax1.get_legend_handles_labels()
    h2, l2 = ax2.get_legend_handles_labels()
    ax2.legend(h1+h2, l1+l2,loc="upper right",fontsize=22)
    #ax1.set_xlim(0,(tmax)*deltt)
    ax1.set_xlim(0,1000)
    ax2.set_ylim(-6,6)
    ax1.set_ylim(-5,5)
    
    fig.tight_layout()
    fig.subplots_adjust(hspace=0.1)
    fig.savefig("field_"+RUN+"_16MLT.png",dpi=200)
    
main()

# -*- coding: utf-8 -*-
import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
import matplotlib.ticker as tick
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

# BOX SIZE
gx = 500
gy = 300
gz = 300
#Earth position
rx = 120
ry = 150
rz = 150

e0 =2.2

tmax = 100 #100*12
RUN = "run5"
path = "/work/m_hayashi/GM/"+RUN+"/MHD/"
ename = "MHD_E"
savefile = "/work/m_hayashi/"+RUN+"_Ephi.npz"

def main():
    Ephi = np.zeros((gx,gy,tmax))
    
    nx = -(np.arange(0,gx)-rx)
    ny = -(np.arange(0,gy)-ry)
    NY,NX = np.meshgrid(ny,nx)
    print NX,NY
    cosphi = NX.astype(float)/np.sqrt(NX**2 + NY**2)
    sinphi = NY.astype(float)/np.sqrt(NX**2 + NY**2)


    for time in xrange(0,tmax):
        # read E file 
        filename = path + ename + str(time).zfill(3) + ".dat"
        print("Reading...... "+filename)
        head = ("head","<i")
        tail = ("tail","<i")
        b=("b","<"+str(gx*gy*gz*3)+"f4")
         
        dt = np.dtype([head,b,tail])
        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
        #Convert Global-MHD coodinate ->GSM
        bf = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*e0
        ex = -bf[:,:,rz,0]
        ey = -bf[:,:,rz,1]        
        Ephi[:,:,time] = ey*cosphi - ex*sinphi
        
    np.savez(savefile,Ephi=Ephi)
    
main()

module init

  use const

  implicit none

  private

  public :: init__set_param, init__set_sw

  logical, save                :: lexist
  integer, public              :: it0=0, itsw0=0, itsw=0, intvl3=999999
  real(8), public              :: delt
  real(8), public, allocatable :: f(:,:,:,:), gf(:,:,:,:)
  real(8), public, allocatable :: dxf(:,:,:,:), dyf(:,:,:,:), dzf(:,:,:,:)
  real(8), public, allocatable :: gdxf(:,:,:,:), gdyf(:,:,:,:), gdzf(:,:,:,:)
  real(8), public, allocatable :: Db(:,:,:)
  real(8), public, allocatable :: flg(:,:,:,:), wrk(:,:,:,:), bf(:,:,:,:)


contains


  subroutine init__set_param

    use mpi_set
    use functions, only     : functions__init, n0, bdy, bdz, n0
    use fio, only           : fio__param, fio__input
    use boundary, only      : boundary__init, boundary__f, boundary__df
    use advection, only     : advection__init
    use non_advection, only : non_advection__init
    use crct, only          : crct__init
    integer :: i, j, k, ieq
    real(8) :: r0, rm, x0, xm, y0, ym, z0, zm, dr05, drm5
    real(8) :: vamax

!************** MPI SETTINGS  *******************!
    call mpi_set__init(nxgs,nxge,nygs,nyge,nzgs,nzge, &
                       nproc,nproc_i,nproc_j,nproc_k)
!*********** END OF MPI SETTINGS  ***************!

    !memory allocations
    ! neq=1-8
    ! ieq=1 : Cs=sqrt(P/N)
    ! ieq=2 : P
    ! ieq=3 : Zx+
    ! ieq=4 : Zx-
    ! ieq=5 : Zy+
    ! ieq=6 : Zy-
    ! ieq=7 : Zz+
    ! ieq=8 : Zz-
    allocate( flg(neq,nxs:nxe,nys:nye,nzs:nze))       ! FLAGS FOR PHYSICAL REGEION
    allocate(   f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)) ! N, P, Z_{xyz}+-
    allocate( dxf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)) ! X GRADIENT OF PHYSICAL QUANTITY
    allocate( dyf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)) ! Y GRADIENT OF PHYSICAL QUANTITY
    allocate( dzf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)) ! Z GRADIENT OF PHYSICAL QUANTITY
    allocate(  gf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)) ! N, P, Z_{XYZ}+- FOR AFTER ADVECTION
    allocate(gdxf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)) ! 
    allocate(gdyf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)) ! 
    allocate(gdzf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)) ! 
    allocate(  Db(nxs1:nxe1,nys1:nye1,nzs1:nze1))     ! BACKGOUND DENSITY
    allocate( wrk(3,nxs1:nxe1,nys1:nye1,nzs1:nze1))   ! WORKING ARRAY
    allocate(  bf(3,nxs1:nxe1,nys1:nye1,nzs1:nze1))   ! WORKING ARRAY FOR MAGNETIC FIELD

    inquire(file=trim(filesw),exist=lexist) !SOLAR WIND DATA FILE CHECK

    if(not(isinit))then
       !START FROM THE PAST CALCULATION
       call fio__input(it0,itsw0,f,dxf,dyf,dzf,Db,flg,delt,&
                       Bm,rin,drs,px0,py0,pz0,pxm,pym,pzm, &
                       unmax,unmin,                        &
                       neq,nx,ny,nz,                       &
                       nxs,nxe,nys,nye,nzs,nze,            &
                       nxs1,nxe1,nys1,nye1,nzs1,nze1,gam,  &
                       nproc,nrank,                        &
                       dir_in,file10)                           

       !INITIALIZATIONS OF FUNCTIONS
       call functions__init(Bm,drs,rin,unmax,unmin)
       call boundary__init(nxs,nxe,nys,nye,nzs,nze,          &
                           nxs1,nxe1,nys1,nye1,nzs1,nze1,    &
                           nxeh,nyeh,nzeh,nxeh1,nyeh1,nzeh1, &
                           ncomw,mproc_null,mnpr,            &
                           iup,idown,jup,jdown,kup,kdown,    &
                           rin,drs,px0,py0,pz0)
       call advection__init(neq,nxs,nxe,nys,nye,nzs,nze,      &
                            nxs1,nxe1,nys1,nye1,nzs1,nze1,    &
                            nxeh,nyeh,nzeh,nxeh1,nyeh1,nzeh1, &
                            delt,drs,                         &
                            px0,py0,pz0,pxm,pym,pzm)
       call non_advection__init(neq,nxs,nxe,nys,nye,nzs,nze,      &
                                nxs1,nxe1,nys1,nye1,nzs1,nze1,    &
                                nxeh,nyeh,nzeh,nxeh1,nyeh1,nzeh1, &
                                gam,dre0,dre1,dre2,drb0,drb1,     &
                                ccr,dccr,                         &
                                delt,rin,drs,                     &
                                px0,py0,pz0,pxm,pym,pzm)
       call crct__init(neq,nxgs,nxge,nygs,nyge,nzgs,nzge, &
                       nxs,nxe,nys,nye,nzs,nze,           &
                       nxs1,nxe1,nys1,nye1,nzs1,nze1,     &
                       nproc_i,nproc_j,nproc_k,           &
                       ncomw,mproc_null,mnpr,opsum,opmax, &
                       iup,idown,jup,jdown,kup,kdown,     &
                       rin,drs,px0,py0,pz0,pxm,pym,pzm)
       !end of

       if(idown == mproc_null)then
          ! COPY SOLAR WIND DATA TO TEMPRAL FILE GF
!$OMP PARALLEL DO PRIVATE(ieq,j,k)
          do k=nzs1,nze1
          do j=nys1,nye1
          do ieq=1,neq
             gf(ieq,nxs1,j,k) = f(ieq,nxs1,j,k)
          enddo
          enddo
          enddo
!$OMP END PARALLEL DO

          un_sw   = 1.D0/(f(1,nxs1,nys,nzs)**2)*f(2,nxs1,nys,nzs)
          uvx_sw  = 0.5D0*(f(3,nxs1,nys,nzs)+f(4,nxs1,nys,nzs))
          ubx_sw  = 0.0D0

          !DIPOLE FIELD AT I,J+1/2,K
          x0 = (nxs1-px0)*drs
          xm = (nxs1-pxm)*drs
          y0 = (nys+0.5-py0)*drs
          ym = (nys+0.5-pym)*drs
          z0 = (nzs-pz0)*drs
          zm = (nzs-pzm)*drs
          r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
          rm = dsqrt(+xm*xm+ym*ym+zm*zm)
          dr05 = r0**(-5)
          drm5 = rm**(-5)
          uvy_sw  = 0.5D0*(f(5,nxs1,nys,nzs)+f(6,nxs1,nys,nzs))
          uby_sw  = ( 0.5D0*(f(5,nxs1,nys,nzs)-f(6,nxs1,nys,nzs)) &
                     +bdy(y0,z0,ym,zm,dr05,drm5)/dsqrt(n0(r0)))*dsqrt(un_sw)

          !DIPOLE FIELD AT I,J,K+1/2
          y0 = (nys-py0)*drs
          ym = (nys-pym)*drs
          z0 = (nzs+0.5-pz0)*drs
          zm = (nzs+0.5-pzm)*drs
          r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
          rm = dsqrt(+xm*xm+ym*ym+zm*zm)
          dr05 = r0**(-5)
          drm5 = rm**(-5)
          uvz_sw  = 0.5D0*(f(7,nxs1,nys,nzs)+f(8,nxs1,nys,nzs))
          ubz_sw  = ( 0.5D0*(f(7,nxs1,nys,nzs)-f(8,nxs1,nys,nzs)) &
                     +bdz(z0,r0,zm,rm,dr05,drm5)/dsqrt(n0(r0)) )*dsqrt(un_sw)

          beta_sw = f(2,nxs1,nys,nzs)/(uby_sw**2+ubz_sw**2)
       endif

       !INTERVAL3 FOR CHANGING SOLAR WIND CONDITIONS
       intvl3 = int(dt_sw/(6380.0D0/rs/220.0D0)/delt+0.5)

       return
    endif

    !DELT FROM CFL
    vamax  = 2.*Bm*rin**(-3)/sqrt(unmax)*va0  !AT R=RIN(RE) AT POLE AT INNNER BOUNDARY
    delt   = cfl*delx/vamax

    !INTERVAL3 FOR CHANGING SOLAR WIND CONDITIONS
    intvl3 = int(dt_sw/(6380.0D0/rs/220.0D0)/delt+0.5)

    !INITIALIZATIONS OF FUNCTIONS
    call functions__init(Bm,drs,rin,unmax,unmin)

    call boundary__init(nxs,nxe,nys,nye,nzs,nze,          &
                        nxs1,nxe1,nys1,nye1,nzs1,nze1,    &
                        nxeh,nyeh,nzeh,nxeh1,nyeh1,nzeh1, &
                        ncomw,mproc_null,mnpr,            &
                        iup,idown,jup,jdown,kup,kdown,    &
                        rin,drs,px0,py0,pz0)

    call advection__init(neq,nxs,nxe,nys,nye,nzs,nze,      &
                         nxs1,nxe1,nys1,nye1,nzs1,nze1,    &
                         nxeh,nyeh,nzeh,nxeh1,nyeh1,nzeh1, &
                         delt,drs,                         &
                         px0,py0,pz0,pxm,pym,pzm)

    call non_advection__init(neq,nxs,nxe,nys,nye,nzs,nze,      &
                             nxs1,nxe1,nys1,nye1,nzs1,nze1,    &
                             nxeh,nyeh,nzeh,nxeh1,nyeh1,nzeh1, &
                             gam,dre0,dre1,dre2,drb0,drb1,     &
                             ccr,dccr,                         &
                             delt,rin,drs,                     &
                             px0,py0,pz0,pxm,pym,pzm)

    call crct__init(neq,nxgs,nxge,nygs,nyge,nzgs,nzge, &
                    nxs,nxe,nys,nye,nzs,nze,           &
                    nxs1,nxe1,nys1,nye1,nzs1,nze1,     &
                    nproc_i,nproc_j,nproc_k,           &
                    ncomw,mproc_null,mnpr,opsum,opmax, &
                    iup,idown,jup,jdown,kup,kdown,     &
                    rin,drs,px0,py0,pz0,pxm,pym,pzm)

    !PRINT INTIAL CONDITIONS
    call fio__param(nx,ny,nz,                          &
                    delt,uvx_sw,un_sw,beta_sw,re0,rb0, &
                    dir_prt,file9)                      

!$OMP PARALLEL PRIVATE(i,j,k)

    !FLAG
!$OMP DO PRIVATE(r0,z0)
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe

       ! SIMULATION DOMAIN :: R > RIN, WHERE |B|(THETA)=CONST.

       !AT (I,J,K) FOR N
       z0 = (k-pz0)*drs
       r0 = dsqrt(+(i-px0)**2+(j-py0)**2+(k-pz0)**2)*drs

       flg(1,i,j,k) = (0.5*(1.+tanh((r0-rin)*2.0)))**2
       
       !AT (I,J,K) FOR P
       flg(2,i,j,k) = (0.5*(1.+tanh((r0-rin)*2.0)))**2
       
       !AT (I+1/2,J,K) FOR ZX+, ZX-
       z0 = (k-pz0)*drs
       r0 = dsqrt(+(i+0.5-px0)**2+(j-py0)**2+(k-pz0)**2)*drs

       flg(3,i,j,k) = (0.5*(1.+tanh((r0-rin)*2.0)))**2
       flg(4,i,j,k) = (0.5*(1.+tanh((r0-rin)*2.0)))**2

       !AT (I,J+1/2,K) FOR ZY+, ZY-
       z0 = (k-pz0)*drs
       r0 = dsqrt(+(i-px0)**2+(j+0.5-py0)**2+(k-pz0)**2)*drs

       flg(5,i,j,k) = (0.5*(1.+tanh((r0-rin)*2.0)))**2
       flg(6,i,j,k) = (0.5*(1.+tanh((r0-rin)*2.0)))**2
       
       !AT (I,J,K+1/2) FOR ZZ+, ZZ-
       z0 = (k+0.5-pz0)*drs
       r0 = dsqrt(+(i-px0)**2+(j-py0)**2+(k+0.5-pz0)**2)*drs

       flg(7,i,j,k) = (0.5*(1.+tanh((r0-rin)*2.0)))**2
       flg(8,i,j,k) = (0.5*(1.+tanh((r0-rin)*2.0)))**2
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP DO PRIVATE(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       x0 = (i-px0)*drs
       xm = (i-pxm)*drs
       y0 = (j-py0)*drs
       ym = (j-pym)*drs
       z0 = (k-pz0)*drs
       zm = (k-pzm)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       dr05 = r0**(-5)
       drm5 = rm**(-5)

       ! BACKGROUND DENSITY
       Db(i,j,k)  = n0(r0)
       ! Cs
       f(1,i,j,k) = dsqrt(up0/n0(r0))
       ! P
       f(2,i,j,k) = up0
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP END PARALLEL

    !SETTING OF SOLAR WIND PLASMA
    if(idown == mproc_null) call init__set_sw(0)

    call boundary__f(f,neq,nerr,nstat)

!$OMP PARALLEL PRIVATE(ieq,i,j,k)
    !GRADIENT GRAD(F)
!$OMP DO
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe
       do ieq=1,neq
          dxf(ieq,i,j,k) = (-f(ieq,i-1,j,k)+f(ieq,i+1,j,k))*0.5
       enddo
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT
!$OMP DO
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe
       do ieq=1,neq
          dyf(ieq,i,j,k) = (-f(ieq,i,j-1,k)+f(ieq,i,j+1,k))*0.5
       enddo
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT
!$OMP DO
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe
       do ieq=1,neq
          dzf(ieq,i,j,k) = (-f(ieq,i,j,k-1)+f(ieq,i,j,k+1))*0.5
       enddo
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP END PARALLEL

    call boundary__df(dxf,dyf,dzf,neq,nerr,nstat)

  end subroutine init__set_param

  
  subroutine init__set_sw(it)

    use functions, only : bdy, bdz, n0
    use mpi_set
    use boundary

    integer, intent(in) :: it
    integer             :: ieq, i, j, k, l, iost
    real(8)             :: r0, rm, x0, xm, y0, ym, z0, zm, dr05, drm5
    real(8), parameter  :: eps = 1.0D-3
    character(len=128)  :: day, time

    if(it /= 0)then
       if(lexist)then 
          if(itsw==0)then
             open(10000,file=trim(filesw),status='old')
             do l=1,itsw0
                read(10000,*,iostat=iost)
             enddo
          endif

          read(10000,*,iostat=iost)day,time,un_sw,beta_sw,uvx_sw,uvy_sw,uvz_sw, &
                                           ubx_sw,uby_sw,ubz_sw

          if(iost /= 0) return

          !Normalizations
          uvx_sw = -uvx_sw/220.D0
          uvy_sw = -uvy_sw/220.D0
          uvz_sw = uvz_sw/220.D0
          ubx_sw = -ubx_sw/10.D0
          uby_sw = -uby_sw/10.D0
          ubz_sw = ubz_sw/10.D0

          itsw = itsw+1

       else
          !if( abs(uvx_sw/uvx_sw_lim-1.0D0) > eps ) uvx_sw = uvx_sw+0.1*(uvx_sw_lim-uvx_sw)/uvx_sw_lim
          !if( abs(un_sw/un_sw_lim-1.0D0) > eps ) un_sw = un_sw+0.1*(un_sw_lim-un_sw)/un_sw_lim 
          if(uvx_sw < uvx_sw_lim) uvx_sw = uvx_sw + duvx_sw
          if(un_sw < un_sw_lim) un_sw = un_sw + dun_sw

          !if( abs(uvx_sw/uvx_sw_lim-1.0D0) < eps  .and. &
          !    abs(un_sw/un_sw_lim-1.0D0) < eps ) return
          if(uvx_sw >= uvx_sw_lim .and. un_sw >= un_sw_lim) return
              
       endif

    endif


!$OMP PARALLEL PRIVATE(j,k)

    x0 = (nxs1-px0)*drs
    xm = (nxs1-pxm)*drs
!$OMP DO PRIVATE(y0,ym,z0,zm,r0,rm,dr05,drm5)
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxs
       f(1,i,j,k) = un_sw
       f(2,i,j,k) = beta_sw*(ubx_sw**2+uby_sw**2+ubz_sw**2)

       !DIPOLE FIELD AT I+1/2,J,K
       f(3,i,j,k) = uvx_sw
       f(4,i,j,k) = 0.0D0

       !DIPOLE FIELD AT I,J+1/2,K
       y0 = (j+0.5-py0)*drs
       ym = (j+0.5-pym)*drs
       z0 = (k-pz0)*drs
       zm = (k-pzm)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       dr05 = r0**(-5)
       drm5 = rm**(-5)

       f(5,i,j,k) = uvy_sw
       f(6,i,j,k) = uby_sw/dsqrt(un_sw) &
                      -bdy(y0,z0,ym,zm,dr05,drm5)/dsqrt(n0(r0))

       !DIPOLE FIELD AT I,J,K+1/2
       y0 = (j-py0)*drs
       ym = (j-pym)*drs
       z0 = (k+0.5-pz0)*drs
       zm = (k+0.5-pzm)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       dr05 = r0**(-5)
       drm5 = rm**(-5)

       f(7,i,j,k) = uvz_sw
       f(8,i,j,k) = ubz_sw/dsqrt(un_sw) &
                      -bdz(z0,r0,zm,rm,dr05,drm5)/dsqrt(n0(r0))
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP DO
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxs
       !N => Cs
       f(1,i,j,k) = dsqrt(f(2,i,j,k)/f(1,i,j,k))

       !V, VA => Z+-
       f(3,i,j,k) = f(3,i,j,k)+f(4,i,j,k)
       f(4,i,j,k) = f(3,i,j,k)-2.*f(4,i,j,k)
       f(5,i,j,k) = f(5,i,j,k)+f(6,i,j,k)
       f(6,i,j,k) = f(5,i,j,k)-2.*f(6,i,j,k)
       f(7,i,j,k) = f(7,i,j,k)+f(8,i,j,k)
       f(8,i,j,k) = f(7,i,j,k)-2.*f(8,i,j,k)
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP DO PRIVATE(ieq)
    do k=nzs1,nze1
    do j=nys1,nye1
    do ieq=1,neq
       gf(ieq,nxs1:nxs,j,k) = f(ieq,nxs1:nxs,j,k)
    enddo
    enddo
    enddo
!$OMP END DO

    !GRADIENT GRAD(F)
!$OMP DO PRIVATE(ieq)
    do k=nzs1,nze1
    do j=nys1,nye1
       do ieq=1,neq
          dxf(ieq,nxs,j,k) = -f(ieq,nxs,j,k)+f(ieq,nxs+1,j,k)
       enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP END PARALLEL

  end subroutine init__set_sw


end module init

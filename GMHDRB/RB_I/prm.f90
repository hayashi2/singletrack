!######################################################################
module prm
!######################################################################

  double precision, parameter :: pi = 3.141592654d0
  double precision, parameter :: q  = -1.6022d-19
  double precision, parameter :: m  = 9.1094d-31
  double precision, parameter :: c  = 2.9979d8
  double precision, parameter :: eps  = 8.8542d-12
  double precision, parameter :: re = 6378d3

!######################################################################
end module prm
!######################################################################

!######################################################################
module readptl
!######################################################################

   implicit none

     integer, parameter :: unit = 110
     integer, save :: maxptl
     character(128), save :: ppath, pname

   private
   public readptl__ini, readptl__exe, readptl__fin

contains
!######################################################################
   subroutine readptl__ini(ppath, pname, imaxptl, irank)

   character(128), intent(in) :: ppath, pname  
   integer, intent(in) :: imaxptl, irank
   character(128) :: filename  
   integer :: ierr

     maxptl = imaxptl

     write(filename, '(a, a, i6.6, a)') trim(ppath), trim(pname), irank, ".dat"
     open(unit=unit+irank, file=filename, form="unformatted", &
        & status="old", action="read", iostat=ierr) 

     print *, "OPEN FILE NAME:", filename, ierr

   end subroutine readptl__ini

!######################################################################

   subroutine readptl__fin(irank)

   integer, intent(in) :: irank

     close(unit+irank)

   end subroutine readptl__fin

!######################################################################


   subroutine readptl__exe &
             &(irank, itime,  np,    &
             & xp,    yp,     zp,    &
             & lp,    xeq,    yeq,   &
             & zeq,   aeq,    beq,   &
             & ppara, pperp,  mu,    &
             & bp,    prebp,  db,    &
             & predb, nump, ierr     )

   integer, intent(in) :: irank
   real(8), intent(in) :: itime
   integer, intent(out) :: np
   real(8), intent(out) :: mu(1:maxptl)
   real(8), intent(out) :: xp(1:maxptl), yp(1:maxptl), zp(1:maxptl)
   real(8), intent(out) :: lp(1:maxptl)
   real(8), intent(out) :: xeq(1:maxptl), yeq(1:maxptl), zeq(1:maxptl), &
                           & aeq(1:maxptl), beq(1:maxptl)
   real(8), intent(out) :: ppara(1:maxptl), pperp(1:maxptl), bp(1:maxptl)
   real(8), intent(out) :: prebp(1:maxptl), db(1:maxptl), predb(1:maxptl)
   integer, intent(out) :: nump(1:maxptl), ierr

   real(8) :: rtime
   integer :: intime, outtime

     ierr = 0

     do 
       read(unit+irank, iostat = ierr) rtime, np, &
                                     & xp,     yp,     zp,  &
                                     & lp,     xeq,    yeq, &
                                     & zeq,    aeq,    beq, &
                                     & ppara,  pperp,  mu,  &
                                     & bp,     prebp,  db,  &
                                     & predb,  nump

       outtime = dint(rtime * 1000 + 0.1d0)
       intime = dint(itime * 1000 + 0.1d0)
       if ((intime <= outtime).or.(ierr /= 0)) exit
     end do

     print *, "read time [s] = ", outtime / 1000d0
     if (outtime > intime) ierr = 999


   end subroutine readptl__exe

!######################################################################
end module readptl
!######################################################################

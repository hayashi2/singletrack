# -*- coding: utf-8 -*-
import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
import matplotlib.ticker as tick
import matplotlib.patches as mpatches
from matplotlib.patches import Circle
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LogNorm
from scipy import interpolate
from scipy.integrate import ode

# BOX SIZE
gx = 500
gy = 300
gz = 300
#Earth position
rx = 120
ry = 150
rz = 150

dx = dy = dz =1./4

e0 =2.2
b0 = 10
path = "/work/m_hayashi/GM/run3/MHD/"
savepath = "/work/m_hayashi/figure/run3/B/"
bname = "MHD_B"

R=0.1
dtt=0.8*R
# plot area
x0, x1=-11,11
z0, z1=-11,11

def main():
    for time in xrange(0,1):
        filename = path + bname + str(time).zfill(3) + ".dat"
        Bx,Bz = readB(filename)
        Bx = Bx[rx+x0/dx:rx+x1/dx,ry,rz+z0/dz:rz+z1/dz] #resize to 11*11 Re
        Bz = Bz[rx+x0/dx:rx+x1/dx,ry,rz+z0/dz:rz+z1/dz]
        
        xx = np.linspace(x0,x1,np.shape(Bx)[0])
        zz = np.linspace(z0,z1,np.shape(Bx)[1])
        X,Z = np.meshgrid(xx,zz)
        # interpolate function of the Bx and Bz as functions of (x,z) position
        fbx = interpolate.interp2d(xx,zz,Bx.T)
        fbz = interpolate.interp2d(xx,zz,Bz.T)
        
        # set the starting point of the magnetic field line
        xstart = np.linspace(x0,x1,10)
        zstart = np.linspace(z0,z0,10)
        places=np.vstack([xstart,zstart]).T        

        # set the ode function
        r=ode(B_dir)
        r.set_integrator('vode')
        r.set_f_params(fbx,fbz)
        
        xs,zs = [],[]
        for p in places:
            x=[p[0]]
            z=[p[1]]
            r.set_initial_value([x[0], z[0]], 0)
            while r.successful():
                r.integrate(r.t+dtt)
                x.append(r.y[0])
                z.append(r.y[1])
                hit_electrode=False
        # check if field line left drwaing area
                if (not (x0<r.y[0] and r.y[0]<x1)) or (not (z0<r.y[1] and r.y[1]<z1)):
                    break
        xs.append(x)
        zs.append(z)

        fig = figure()
        ax = fig.add_subplot(111)
        
        for x,z in zip(xs,zs):
            ax.plot(x,z,color="k")
        
        #ax.streamplot(X, Z, Bx.T, Bz.T, color='k', minlength=0.01,arrowstyle='-')
        ax.set_xlim(x0,x1)
        ax.set_ylim(z0,z1)
        axes().add_patch(Circle((0, 0), radius=4, edgecolor="k",facecolor='w', linewidth=1))
        axes().add_patch(Circle((0, 0), radius=1, edgecolor="k",facecolor='w', linewidth=1))
        axes().set_aspect("equal","datalim")
        savefig('streamlines_continuous.png')

def readB(filename):    
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<"+str(gx*gy*gz*3)+"f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    #  Convert Global-MHD coodinate ->GSM
    bf = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*b0
    
    by = -bf[:,:,:,1]
    bx = bf[:,:,:,0]
    bz = bf[:,:,:,2]
    return bx,bz


def B_dir(t,p,fx,fz):
    ex = fx(p[0],p[1])
    ez = fz(p[0],p[1])
    n = (ex**2+ez**2)**0.5
    return [ex/n,ez/n]    

main()

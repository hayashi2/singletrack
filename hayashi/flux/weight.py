# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("Agg")
from pylab import *
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches
import scipy.optimize
import time
import spacepy.time as spt
import spacepy.coordinates as spc
import spacepy.irbempy as ib
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8
re = 6370.e3
B0 =32000.
ptl = 10000
ptlnum=144

files ="/work/m_hayashi/RBdata/RB_GMrun5_000/ptl"

enest = 0
iniL = 5.0
dL = 0.5
dphi = float(1.0/180.0)*np.pi*15.
da = float(1.0/180.0)*np.pi
de = 0.25
dt = 5

grL = int((10.-iniL)/dL)
grML = 2*int(np.pi/dphi)
gra = int(np.pi/da)
gre = int(10./de)+1
grt = 200


def main():
    count = np.zeros((grL,grML,gra,gre)) #L,mlt,a,ene,
    weight = np.empty((ptlnum,ptl))
    Jmodel = np.zeros((grL,gre))
    # file loop
    for filenum in xrange(0,ptlnum):
        print "counting..."+str(filenum).zfill(3)
        filename = files + str(filenum).zfill(3) +".dat"

    #read file
        xp1, yp1, a1,b1,ene1, frag1,zp1 ,bp1=  readfile(filename)
        x1,y1 = xp1/re,yp1/re
        del xp1,yp1,zp1
    #grid number
        mltg, ag, eg, Lg = griding(x1,y1,a1,ene1,b1*1e9)
    #ptl loop
        for m in xrange(0,ptl):
    #count per bins
            if(Lg[0,m]<grL):
                count[np.int(Lg[0,m]),np.int(mltg[0,m]),np.int(ag[0,m]),np.int(eg[0,m])] +=1
    
    #create AE8 model
    """
    for l in xrange(grL):
        for ene in xrange(gre):
            BBo = 1.0
            L = float(l)*dL + iniL
            E = float(ene)*de
            J = ib.get_AEP8(E,[BBo,L],model="min",fluxtype="diff",particles="e")
            if(np.isnan(J)==True):
                J = 0.
            Jmodel[l,ene] = J
    print "Created AE8model"
    """
    #create VAPs model
    Jmodel = np.load("/work/m_hayashi/Fluxdata/weight_VAPs0716.npz")
    Jmodel = Jmodel["flux"]
    
    #weighting
    for filenum2 in xrange(0,ptlnum):
        print "weighting..."+str(filenum2).zfill(3)
        filename2 = files + str(filenum2).zfill(3) +".dat"

        xp2, yp2, a2, b2,ene2,frag2,zp2,bp2 =  readfile(filename2)
        x2,y2 = xp2/re,yp2/re
        del xp2,yp2,zp2
        #grid number
        mltg2, ag2, eg2,Lg2 = griding(x2,y2,a2,ene2,b2*1e9)

        for m in xrange(0,ptl):                
            if(count[np.int(Lg2[0,m]),np.int(mltg2[0,m]),np.int(ag2[0,m]),np.int(eg2[0,m])] != 0 and Lg2[0,m]<grL):
                #weight[filenum2,m] = Jmodel[Lg2[0,m],eg2[0,m]]/count[Lg2[0,m],mltg2[0,m],ag2[0,m],eg2[0,m]] #AE8
                weight[filenum2,m] = Jmodel[eg2[0,m]]/count[Lg2[0,m],mltg2[0,m],ag2[0,m],eg2[0,m]] #VAPs
                
    np.savez("/work/m_hayashi/Fluxdata/weight_RB_GMrun5_000_VAPs0716.npz",w=weight)

    

def griding(x,y,a,ene,beq):
    #Lg = np.empty((grt,ptl))
    Lg = np.around(((B0/beq)**(1./3) -iniL)/dL).astype(int)
    mltg = np.around((np.degrees(np.arctan2(y,x)+np.pi))/15).astype(int)-1
    ag = np.around(a/da).astype(int)
    eg = np.around((ene/1e6 -enest)/de).astype(int)
 
    return mltg,ag,eg,Lg


def readfile(filename):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")

    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])

    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt)

    #xp = chunk["xp"]
    #yp = chunk["yp"]
    #zp = chunk["zp"]
    #lp = chunk["lp"]
    xeq = chunk["xeq"]
    yeq = chunk["yeq"]
    zeq = chunk["zeq"]
    aeq = chunk["aeq"]
    beq = chunk["beq"]
    ppara = chunk["ppara"]
    pperp = chunk["pperp"]
    #mu = chunk["mu"]
    bp = chunk["bp"]
    #prebp = chunk["prebp"]
    #db = chunk["db"]
    #predb = chunk["predb"]
    #nump = chunk["nump"]
    ierr = chunk["ierr"]

    fd.close()
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)
    return -xeq[0:grt,:] ,-yeq[0:grt,:],aeq[0:grt,:],beq[0:grt,:],\
        ene[0:grt,:],ierr[0:grt,:],zeq[0:grt],bp[0:grt,:]

main()

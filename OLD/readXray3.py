import numpy as np
from pylab import *

path = "/work/m_hayashi/Xray/Vsw600/"
name = "MHDXray_"
#time = 0
time= 18


data = np.load(path+name+str(time).zfill(3)+".npz")
xray = data["Xray"]

sum_top_image=np.empty([297,241])
sum_side_image=np.empty([297,241])

for k in range(0,240):
  for i in range(0,296):
    for j in range(0,240):
      sum_top_image[i,j]=sum_top_image[i,j]+xray[i,j,k]

for j in range(0,240):
  for i in range(0,296):
    for k in range(0,240):
      sum_side_image[i,k]=sum_side_image[i,k]+xray[i,j,k]


figure(1)
imshow(sum_top_image)
figure(2)
imshow(np.transpose(sum_side_image))
show()

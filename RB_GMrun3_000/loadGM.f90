!@@@@@@@@1@@@@@@@@@2@@@@@@@@@3@@@@@@@@@4@@@@@@@@@5@@@@@@@@@6@@@@@@@@@7@@@@@@@@@
module loadGM
!@@@@@@@@1@@@@@@@@@2@@@@@@@@@3@@@@@@@@@4@@@@@@@@@5@@@@@@@@@6@@@@@@@@@7@@@@@@@@@
  use mpi
  use inp
  use status_checker, only: status_checker__exe

  implicit none
contains
!@@@@@@@@1@@@@@@@@@2@@@@@@@@@3@@@@@@@@@4@@@@@@@@@5@@@@@@@@@6@@@@@@@@@7@@@@@@@@@
  subroutine loadGM__exe(rtime, irank, emf)
  double precision, intent(in) :: rtime
  integer, intent(in) :: irank
  double precision, intent(out):: emf(:,:,:,:) 

  integer :: i, j, k, nsize, openstatus, readstatus, ierr
  character(LEN=128) :: filename, txtline
  real, allocatable :: f(:, :, :, :)


  !Set arrays for reading GMHD data.
  allocate(f(nx*2, ny, nz, 3))

  if (irank == 0) then

    !Set a magnetic file name
    i = dint(rtime / dsec + 0.01d0)
    write(filename, '(a, a, i3.3, a)') trim(gmpath), trim(gmb), i, ".dat"

    !Open the file at rank = 0.
    open(unit=555, file=filename, form ="unformatted", status="old", &
       & action="read", iostat=openstatus)
    write(txtline, '(a, a)') "OPEN: ", trim(filename)
    call status_checker__exe(txtline, irank, openstatus)

    !Read data from the file.
    read(555, iostat=readstatus) f
    write(txtline, '(a, a)') "READ: ", trim(filename)
    call status_checker__exe(txtline, irank, readstatus)

    do k = 1, nz
      do j = 1, ny
        do i = 1, nx
          emf(1, i, j, k) = f(i, j, k, 1) * bn0
          emf(2, i, j, k) = f(i, j, k, 2) * bn0
          emf(3, i, j, k) = f(i, j, k, 3) * bn0
        end do
      end do
    end do

    close(555)

    !Set a electric file name
    i = dint(rtime / dsec + 0.01d0)
    write(filename, '(a, a, i3.3, a)') trim(gmpath), trim(gme), i, ".dat"

    !Open the file
    open(unit=555, file=filename, form ="unformatted", status="old", &
       & action="read", iostat=openstatus)
    write(txtline, '(a, a)') "OPEN: ", trim(filename)
    call status_checker__exe(txtline, irank, openstatus)

    !Read the file at rank = 0.
    read(555, iostat=readstatus) f
    write(txtline, '(a, a)') "READ: ", trim(filename)
    call status_checker__exe(txtline, irank, readstatus)

    do k = 1, nz
      do j = 1, ny
        do i = 1, nx
          emf(4, i, j, k) = f(i, j, k, 1) * en0
          emf(5, i, j, k) = f(i, j, k, 2) * en0
          emf(6, i, j, k) = f(i, j, k, 3) * en0
        end do
      end do
    end do

    close(555)

  end if

  !Transport fiel data to other ranks.
  nsize = 6 * nx * ny * nz
  call mpi_bcast(emf(1,1,1,1), nsize, mpi_double_precision, 0, &
                &mpi_comm_world, ierr)

  deallocate(f)

  write(txtline, '(a,f10.4)') "LOAD FILE at", rtime
  call status_checker__exe(txtline, irank, ierr)


  end subroutine loadGM__exe
!@@@@@@@@1@@@@@@@@@2@@@@@@@@@3@@@@@@@@@4@@@@@@@@@5@@@@@@@@@6@@@@@@@@@7@@@@@@@@@
end module loadGM
!@@@@@@@@1@@@@@@@@@2@@@@@@@@@3@@@@@@@@@4@@@@@@@@@5@@@@@@@@@6@@@@@@@@@7@@@@@@@@@

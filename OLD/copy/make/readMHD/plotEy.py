# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *

patha = "/work/ymatumot/simulation/GM-RB"
pathb = "/run_Vsw600/"
bname = "MHD_B"
ename = "MHD_E"
dname = "MHD_"
k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1
"""
-30 <= x <= 69 (Re)
-40 <= y <= 40 (Re)
""" 
x1 = -6
y1 = 0
x2 = -7
y2 = 0
x3 = -8
y3 = 0
def main():
    rtime,ey1,ey2,ey3 =  readE()
    #vx = readDATA(0)*v0
    #vy = readDATA(1)*v0
    #vz = readDATA(2)*v0
    #N  = readDATA(3)
    #P  = readDATA(4)*p0
    """
    T  = (P*1e-9)/(k*(N*1e6))
    B  = np.sqrt(bx**2 + by**2 + bz**2)
    V  = np.sqrt(vx**2 + vy**2 + vz**2 )
    Ma = (V*(N**(0.5)))/(20*B)
    Va = 20.3*B/np.sqrt(N)
    Vth = 3.0*k*T/1.67e-23
    Vs  = 0.12*(T+1.28*10e5)**(0.5)
    Vms = np.sqrt(Vs**2+Va**2)
    Mms = V/Vms
    Pdyn = (2e-6)*N*(V**2)
   # Pt = P + (B**2)/2
   # print P,Pt
   """
    fig,axes = subplots(3,1,sharex=True,sharey=True) 
    #figure(figsize=(10,20))
    subplots_adjust(hspace=0.1)

    axes[0].set_title(pathb + " Ey")
    axes[2].plot(rtime,-ey1)
    axes[2].set_ylabel("6Re_Ey[mV/m]")
    ylim(-10,5)
    axes[2].minorticks_on()
    axes[2].grid()
    yticks(np.arange(-8,5,4))
    rcParams["font.size"]=15
    
    axes[1].plot(rtime,-ey2)
    axes[1].set_ylabel("7Re_Ey[mV/m]")
    axes[1].minorticks_on()
    axes[1].grid()
    
    axes[0].plot(rtime,-ey3)
    axes[0].set_ylabel("8Re_Ey[mV/m]")
    axes[2].set_xlabel("time",fontsize=15)
    axes[0].minorticks_on()
    axes[0].grid()
    #axes[2].locator_params(axis="y",nbins=4)
    """
    subplot(12,1,4)
    plot(rtime,bz)
    ylabel("Bz[nT]")
    #locator_params(axis="y",tight=True,nbins=4)

    subplot(12,1,5)
    plot(rtime,ex)
    ylabel("Ex[mV/m]")
    locator_params(axis="y",tight=True,nbins=4)
    
    subplot(12,1,6)
    plot(rtime,ey)
    ylabel("Ey[mV/m]")
    locator_params(axis="y",tight=True,nbins=4)
    
    subplot(12,1,7)
    plot(rtime,ez)
    ylabel("Ez[mV/m]")
    locator_params(axis="y",tight=True,nbins=4)
   
    subplot(12,1,8)
    plot(rtime,N)
    yscale('log')
    ylabel("log N(/cm^3)")

    subplot(12,1,9)
    plot(rtime,T)
    yscale('log')
    ylabel("log T(K)")

    subplot(12,1,10)
    plot(rtime,Pdyn)
    ylabel("Pdyn(npa)")
    locator_params(axis="y",tight=True,nbins=5)

    subplot(12,1,11)
    plot(rtime,Ma)
    ylabel("Ma")
    locator_params(axis="y",tight=True,nbins=3)
    
    subplot(12,1,12)
    plot(rtime,Mms)
    ylabel("Mms")
    locator_params(axis="y",tight=True,nbins=5)
    xlabel("time[sec]")

    savefig( "Vsw600 plot("+str(x1)+","+str(y1)+").jpeg")
    
    """
    show()

def readE():
    e1,e2,e3 = [],[],[]
    t = []
    time = 0
    while(time <= 100):
        
        efilename = patha + pathb + ename + str(time).zfill(3) + ".dat"
        
        head = ("head","<i")
        tail = ("tail","<i")
        e = ("e","<51750171f4")
         
        dt = np.dtype([head,e,tail])
        fd = open(efilename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
    
        ef = chunk[0]["e"].reshape((297,241,241,3),order="F")*e0
        et = ef.transpose(1,0,2,3)

        e1 = np.append(e1,et[ny(y1),nx(x1),120,1])
        e2 = np.append(e2,et[ny(y2),nx(x2),120,1])
        e3 = np.append(e3,et[ny(y3),nx(x3),120,1])
        t = np.append(t,time*12)
        print "read... E"+str(time)
        time += 1
        
    return t,e1,e2,e3


def nx(x):
    return int(x*3 + 90)

def ny(y):
    return int(y*3 + 121)

main()

!######################################################################
module gkmover
!######################################################################

  use prm, only: q, m, c, re
  use inp, only: dx_re, dy_re, dz_re, dt, rmin_re, rmax_re, loss_hight
  use itp_emf, only: itp_emf__exe

  implicit none
  private
  public :: gkmover__exe

contains
!######################################################################
  subroutine gkmover__exe           &
           &(rtime,                 &
           & emf0,                  &
           & emf1,                  &
           & maxptl,                &
           & xp,     yp,     zp,    &
           & lp,     xeq,    yeq,   &
           & zeq,    aeq,    beq,   &
           & ppara,  pperp,  mu,    &
           & bp,     prebp,  db,    &
           & predb, nump,   ierr    )

  real(8), intent(in) :: rtime
  real(8), intent(in) :: emf0(:, :, :, :)
  real(8), intent(in) :: emf1(:, :, :, :)
  real(8), intent(in) :: mu(:)
  integer, intent(in) :: maxptl
  real(8), intent(inout) :: xp(:), yp(:), zp(:), lp(:)
  real(8), intent(inout) :: xeq(:), yeq(:), zeq(:), aeq(:), beq(:)
  real(8), intent(inout) :: ppara(:), pperp(:), bp(:)
  real(8), intent(inout) :: prebp(:), db(:), predb(:) 
  integer, intent(inout) :: nump(:), ierr(:)

  real(8) :: rkx(4), rkxout(4), xorigin(4), rk0(4), rk1(4), rk2(4), rk3(4)
  real(8) :: bx, by, bz, ex, ey, ez, t, g
  real(8) :: xp0, yp0, zp0, dl
  integer :: i

  real(8) :: r, ratio, alc, a

  double precision :: dx, dy, dz

  dx = dx_re * re
  dy = dy_re * re
  dz = dz_re * re

  prebp(1:maxptl) = bp(1:maxptl)
  predb(1:maxptl) = db(1:maxptl)


  particle_loop: do i = 1, maxptl

    if (ierr(i) == 0) then

       g = dsqrt(1d0 + (ppara(i)**2 + pperp(i)**2) / (m**2 * c**2))
       lp(i) = lp(i) + 0.5d0 * dt * ppara(i) / (m * g)
       xorigin(1) = xp(i)
       xorigin(2) = yp(i)
       xorigin(3) = zp(i)
       xorigin(4) = ppara(i)

       call itp_emf__exe(rtime, emf0, emf1, xp(i), yp(i), zp(i), &
                       & bx, by, bz, ex, ey, ez, ierr(i))


!\\ begin 4th order Runge-Kutta \\

!\\ 1st advance \\
       rkx(1:4) = xorigin(1:4)
       t = rtime
       call dx_dt(t, emf0, emf1, mu(i), rkx, rkxout, ierr(i))
       rk0(1:4) = dt * rkxout(1:4)

 
!\\ 2nd advance \\
       rkx(1:4) = xorigin(1:4) + 0.5d0 * rk0(1:4)
       t = rtime + 0.5d0 * dt
       call dx_dt(t, emf0, emf1, mu(i), rkx, rkxout, ierr(i))
       rk1(1:4) = dt * rkxout(1:4)


!\\ 3rd advance \\
       rkx(1:4) = xorigin(1:4) + 0.5d0 * rk1(1:4)
       t = rtime + 0.5d0 * dt
       call dx_dt(t, emf0, emf1, mu(i), rkx, rkxout, ierr(i))
       rk2(1:4) = dt * rkxout(1:4)


!\\ 4th advance \\
       rkx(1:4) = xorigin(1:4) + rk2(1:4)
       t = rtime + dt
       call dx_dt(t, emf0, emf1, mu(i), rkx, rkxout, ierr(i))
       rk3(1:4) = dt * rkxout(1:4)



!\\ Full advance \\
       xorigin(1:4) = xorigin(1:4) + (rk0(1:4) + 2 * rk1(1:4) &
                   &+ 2 * rk2(1:4) + rk3(1:4)) * 0.1666666666667d0
       xp(i) = xorigin(1)
       yp(i) = xorigin(2)
       zp(i) = xorigin(3)
       ppara(i) = xorigin(4)
       call itp_emf__exe(rtime+dt, emf0, emf1, xp(i), yp(i), zp(i), &
                       & bx, by, bz, ex, ey, ez, ierr(i))
       bp(i) = dsqrt(bx**2 + by**2 + bz**2)
       pperp(i) = dsqrt(2d0 * m * mu(i) * bp(i))

       g = dsqrt(1d0 + (ppara(i)**2 + pperp(i)**2) / (m**2 * c**2))
       lp(i) = lp(i) + 0.5d0 * dt * ppara(i) / (m * g)

    end if

  end do particle_loop

  db(1:maxptl) = bp(1:maxptl) - prebp(1:maxptl)


!\\ Update the equatorial data
  do i=1, maxptl
    if ((predb(i) < 0d0).and.(db(i) >= 0d0).and.(ierr(i)==0)) then
      lp(i) = 0d0
      xeq(i) = xp(i)
      yeq(i) = yp(i)
      zeq(i) = zp(i)
      beq(i) = bp(i)
      aeq(i) = datan2(pperp(i), ppara(i))
    end if
  end do

!\\ Check particles in loss cone.
  SEARCH_PRECIPITATED_ELECTRONS: do i = 1, maxptl

     r = dsqrt(xp(i)**2 + yp(i)**2 + zp(i)**2)

     if ((r/re <= rmin_re + dx_re).and.(ierr(i) == 0)) then

         ratio = dsqrt(bp(i) / &
               & dipole_line_bmag(xp(i), yp(i), zp(i), re + loss_hight))

         alc = dasin(ratio)

         a = datan2(pperp(i), dabs(ppara(i)))

         if (a <= alc) then
            ierr(i) = -100
         else
            ppara(i) = -ppara(i)
         end if

      end if

  end do SEARCH_PRECIPITATED_ELECTRONS

!\\ Check particles out of the simulation box
  do i = 1, maxptl
     r = dsqrt(xp(i)**2 + yp(i)**2 + zp(i)**2)
     if ((r/re >= rmax_re).and.(ierr(i) == 0)) then
       ierr(i) = -1000
     end if 
     if ((r/re <= rmin_re).and.(ierr(i) == 0)) then
       ierr(i) = -10000
     end if 
  end do


end subroutine gkmover__exe

!######################################################################

  subroutine dx_dt(rtime, emf0, emf1, mu, x, xout, ierr)

  real(8), intent(in) :: rtime, emf0(:,:,:,:), emf1(:,:,:,:)
  real(8), intent(in) :: mu
  real(8), intent(in) :: x(4)
  real(8), intent(out):: xout(4)
  integer, intent(inout):: ierr

  real(8) :: xp, yp, zp, ppara
  real(8) :: bx, by, bz, b1, b2, ex, ey, ez, g, b1inv, b2inv, ginv
  real(8) :: bxa, bya, bza, bba
  real(8) :: bpos, bneg, bbpos, bbneg
  real(8) :: dbxdx, dbxdy, dbxdz
  real(8) :: dbydx, dbydy, dbydz
  real(8) :: dbzdx, dbzdy, dbzdz
  real(8) :: db1dx, db1dy, db1dz
  real(8) :: db2dx, db2dy, db2dz
  real(8) :: ecb_x, ecb_y, ecb_z
  real(8) :: bx_star, by_star, bz_star, bpara_star, bpara_starinv, &
           & commonv, qinv, minv

  double precision :: dx, dy, dz

  dx = dx_re * re
  dy = dy_re * re
  dz = dz_re * re

  if (ierr < 0) then
     xout(1:4) = 0d0
     return
  end if

  xp = x(1)
  yp = x(2)
  zp = x(3)
  ppara = x(4)

!\\ Get gradients of B-field \\
  call itp_emf__exe(rtime, emf0, emf1, xp+0.5d0*dx, yp, zp, &
                  & bx, by, bz, ex, ey, ez, ierr)
  dbydx = by
  dbzdx = bz
  db2dx = bx**2 + by**2 + bz**2
  db1dx = dsqrt(db2dx)
  call itp_emf__exe(rtime, emf0, emf1, xp-0.5d0*dx, yp, zp, &
                  & bx, by, bz, ex, ey, ez, ierr)
  dbydx = (dbydx - by) / dx
  dbzdx = (dbzdx - bz) / dx 
  db2dx = (db2dx - (bx**2 + by**2 + bz**2)) / dx
  db1dx = (db1dx - dsqrt(bx**2 + by**2 + bz**2)) / dx
  call itp_emf__exe(rtime, emf0, emf1, xp, yp+0.5d0*dy, zp, &
                  & bx, by, bz, ex, ey, ez, ierr)
  dbxdy = bx
  dbzdy = bz
  db2dy = bx**2 + by**2 + bz**2
  db1dy = dsqrt(db2dy)
  call itp_emf__exe(rtime, emf0, emf1, xp, yp-0.5d0*dy, zp, &
                  & bx, by, bz, ex, ey, ez, ierr)
  dbxdy = (dbxdy - bx) / dy
  dbzdy = (dbzdy - bz) / dy
  db2dy = (db2dy - (bx**2 + by**2 + bz**2)) / dy
  db1dy = (db1dy - dsqrt(bx**2 + by**2 + bz**2)) / dy
  call itp_emf__exe(rtime, emf0, emf1, xp, yp, zp+0.5d0*dz, &
                  & bx, by, bz, ex, ey, ez, ierr)
  dbxdz = bx
  dbydz = by
  db2dz = bx**2 + by**2 + bz**2
  db1dz = dsqrt(db2dz)
  call itp_emf__exe(rtime, emf0, emf1, xp, yp, zp-0.5d0*dz, &
                  & bx, by, bz, ex, ey, ez, ierr)
  dbxdz = (dbxdz - bx) / dz
  dbydz = (dbydz - by) / dz
  db2dz = (db2dz - bx**2 - by**2 - bz**2) / dz
  db1dz = (db1dz - dsqrt(bx**2 + by**2 + bz**2)) / dz

!\\ Get field data \\
  call itp_emf__exe(rtime, emf0, emf1, xp, yp, zp, &
                  & bx, by, bz, ex, ey, ez, ierr)

  b2 = bx**2 + by**2 + bz**2
  b2inv = 1d0 / b2
  b1 = dsqrt(b2)
  b1inv = 1d0 / b1

!\\ Lorentz g \\ 
  g = dsqrt(1d0 + (ppara**2 + 2d0 * m * mu * b1) / (m**2 * c**2))
  ginv = 1d0 / g

!\\ B_star \\
  qinv = 1d0 / q
  bx_star = bx+qinv*ppara*((dbzdy-dbydz)+0.5d0*(by*db2dz-bz*db2dy)*b2inv)*b1inv
  by_star = by+qinv*ppara*((dbxdz-dbzdx)+0.5d0*(bz*db2dx-bx*db2dz)*b2inv)*b1inv
  bz_star = bz+qinv*ppara*((dbydx-dbxdy)+0.5d0*(bx*db2dy-by*db2dx)*b2inv)*b1inv

!\\ Bpara_star inverse \\
  bpara_starinv = b1 / (bx * bx_star + by * by_star + bz * bz_star)

!\\ E cross B component \\
  ecb_x = (ey * bz - ez * by) * b1inv * bpara_starinv
  ecb_y = (ez * bx - ex * bz) * b1inv * bpara_starinv
  ecb_z = (ex * by - ey * bx) * b1inv * bpara_starinv

!\\ dx/dt \\ 
  minv = 1d0 / m
  commonv = mu * qinv * ginv * bpara_starinv * b1inv
  xout(1) = ecb_x + ppara*ginv*minv * bx_star * bpara_starinv &
         &+ commonv * (by*db1dz - bz*db1dy)

!\\ dy/dt \\ 
  xout(2) = ecb_y + ppara*ginv*minv * by_star * bpara_starinv &
         &+ commonv * (bz*db1dx - bx*db1dz)

!\\ dz/dt \\ 
  xout(3) = ecb_z + ppara*ginv*minv * bz_star * bpara_starinv &
         &+ commonv * (bx*db1dy - by*db1dx)

!\\ dppara/dt \\
  xout(4) = -mu * ginv * bpara_starinv * (bx_star*db1dx &
         &+ by_star*db1dy + bz_star*db1dz)

end subroutine dx_dt

!######################################################################

  real(8) function dipole_line_bmag(x, y, z, rp)

    real(8), intent(in) :: x, y, z, rp
    real(8) :: rxz, theta, x0, y0, z0, r0, theta0, sinthetap, thetap


    rxz = dsqrt(x**2 + z**2)
    theta = dasin(x / rxz)
    x0 = x / dsin(theta) * dsin(theta)
    y0 = y
    z0 = z / dcos(theta) * dcos(theta)

    r0 = dsqrt(x0**2 + y0**2 + z0**2)

    theta0 = dasin(dsqrt(x0**2 + y0**2) / r0)

    sinthetap = dsqrt(rp / r0) * dsin(theta0)

    thetap = dasin(sinthetap)

    dipole_line_bmag = 3.12d-5 * (re / rp)**3 &
                    &* dsqrt(1 + 3 * dcos(thetap)**2)


  end function dipole_line_bmag

!######################################################################


!######################################################################
end module gkmover
!######################################################################

!########10########20########30########40########50########60########80########80########
module readfield
!########10########20########30########40########50########60########80########80########

  implicit none

  real(8), parameter :: re = 6378.137d3

  integer, save :: nx, ny, nz
  real(8), save :: dx, dy, dz
  real(8), save :: interval
  character(128), save:: fpath, fname

  private
  public readfield__ini, readfield__exe, readfield_scal__exe


contains
!########10########20########30########40########50########60########80########80########

   subroutine readfield__ini(vnx, vny, vnz, vdx_re, vdy_re, vdz_re, &
                           & vinterval, vfpath, vfname)

     integer, intent(in) :: vnx, vny, vnz
     real(8), intent(in) :: vdx_re, vdy_re, vdz_re, vinterval
     character(128), intent(in) :: vfpath, vfname

     nx = vnx
     ny = vny
     nz = vnz

     dx = vdx_re * re
     dy = vdy_re * re
     dz = vdz_re * re

     interval = vinterval

     fpath = vfpath
     fname = vfname


   end subroutine readfield__ini

!########10########20########30########40########50########60########80########80########

   subroutine readfield__exe(rtime, emf)

   real(8), intent(in) :: rtime
   real(8), intent(out):: emf(:,:,:,:)

   integer :: ix, iy, iz, i, ierr
   real(4), allocatable :: fx(:,:,:)
   real(4), allocatable :: fy(:,:,:)
   real(4), allocatable :: fz(:,:,:)

   character(3) :: ctime
   character(128) :: filename, outpath

     allocate(fx(nx, ny, nz))
     allocate(fy(nx, ny, nz))
     allocate(fz(nx, ny, nz))


     i = dint(rtime + 0.01d0)
     write(filename, '(a, a, i6.6, a)') trim(fpath), trim(fname), i, ".dat"
     print *, "input file name:", trim(filename)
     open(unit=10, file=filename, form ="unformatted", status="old", &
        & action="read", iostat = ierr)
     if (ierr /= 0) then
       print *, "FILE NOT FOUND..."
     end if

     read(10, iostat=ierr) fx, fy, fz
     if (ierr /= 0) then
       print *, "READING ERROR..."
     end if
   
     emf(1, 1:nx, 1:ny, 1:nz) = fx(1:nx, 1:ny, 1:nz)
     emf(2, 1:nx, 1:ny, 1:nz) = fy(1:nx, 1:ny, 1:nz)
     emf(3, 1:nx, 1:ny, 1:nz) = fz(1:nx, 1:ny, 1:nz)

     close(10)

     deallocate(fx, fy, fz)

   end subroutine readfield__exe

!########10########20########30########40########50########60########80########80########

   subroutine readfield_scal__exe(rtime, emf)

   real(8), intent(in) :: rtime
   real(8), intent(out):: emf(:,:,:)

   integer :: ix, iy, iz, i, ierr
   real(4), allocatable :: f(:,:,:)

   character(3) :: ctime
   character(128) :: filename, outpath

     allocate(f(1:nx, 1:ny, 1:nz))


     i = dint(rtime + 0.01d0)
     write(filename, '(a, a, i6.6, a)') trim(fpath), trim(fname), i, ".dat"
     print *, "input file name:", trim(filename)
     open(unit=10, file=filename, form ="unformatted", status="old", &
        & action="read", iostat = ierr)
     if (ierr /= 0) then
       print *, "FILE NOT FOUND..."
     end if

     read(10, iostat=ierr) f
     if (ierr /= 0) then
       print *, "READING ERROR...", trim(filename)
     end if
   
     emf(1:nx, 1:ny, 1:nz) = f(1:nx, 1:ny, 1:nz)

     close(10)

     deallocate(f)

   end subroutine readfield_scal__exe

!########10########20########30########40########50########60########80########80########
end module readfield
!########10########20########30########40########50########60########80########80########

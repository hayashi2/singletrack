# -*- coding: utf-8 -*-
import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
import matplotlib.ticker as tick
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.signal import hilbert,chirp


# BOX SIZE
gx = 500
gy = 300
gz = 300
#Earth position
rx = 120
ry = 150
rz = 150

dx = 1./4
dy = 1./4
e0 =2.2

tmax = 100 #100*11
RUN = "run3"
Efile = "/work/m_hayashi/"+RUN+"_Ephi.npz"
savefile = "/work/m_hayashi/figure/"+RUN+"/mnum/mnum"


def main():
    Ephi = np.load(Efile)
    Ephi = Ephi["Ephi"]
    Phase = np.zeros(np.shape(Ephi))
    mnum = np.zeros(np.shape(Ephi))
    
    #hilbert transform
    print("start hilbert")
    for x in xrange(0,gx):
        for y in xrange(0,gy):
            analytic_E = hilbert(Ephi[x, y, :])
            amplitude_envelope = np.abs(analytic_E)
            ins_phase = np.angle(analytic_E)
            Phase[x,y,:] = ins_phase
    print("end hilbert and start grad")
    dfdx, dfdy, dfdt = np.gradient(Phase)
    dfdx = -dfdx
    dfdy = -dfdy

    for x in xrange(0,gx):
        for y in xrange(0,gy):
            #convert GSM 
            xgsm = -(float(x) - float(rx))
            ygsm = -(float(y) - float(ry))
            r = np.sqrt(xgsm**2 + ygsm**2)
            phi = np.arctan2(ygsm,xgsm)
            if(r !=0.):
                mnum[x,y,:] = 1./r *(-ygsm*dfdx[x,y,:] + xgsm*dfdy[x,y,:])            



    nx = -(np.arange(gx) - rx)
    ny = -(np.arange(gy) - ry)
    NY,NX = np.meshgrid(ny,nx)

    for t in xrange(0,np.shape(Ephi)[2]):
        print("drawing m number...t="+str(t*11))
        mnum[:,:,t] = (-NY*dfdx[:,:,t] + NX*dfdy[:,:,t])

        fig = figure(figsize=(6,5))
        ax0 = fig.add_subplot(111)
        ax0.set_xlim(11,-11)
        ax0.set_ylim(11,-11)
        im0 = ax0.contourf(mnum[rx-11/dx:rx+11/dx+1,ry-11/dy:ry+11/dy+1,t].T,origin = "lower",
                           extent=[11,-11,11,-11],levels=[-3,-2,-1,-0,1,2,3],
                           aspect="auto") # levels=[-2,-1,-0,1,2]

    
        ax0.set_xlabel("X [Re]")
        ax0.set_ylabel("Y [Re]")
        CB0 = colorbar(im0)
        CB0.set_label("m number")
        ax0.set_title("time = "+str(t*11)+" sec")
        wedge0 = mpatches.Wedge((0,0), 4, -180, 180,fc='w',linewidth = 0)
        wedge1 = mpatches.Wedge((0,0),1,-90,90,fc="w")
        wedge2 = mpatches.Wedge((0,0),1,90,-90,fc="k")
        fig.gca().add_artist(wedge0)
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)
        fig.savefig(savefile+str(t).zfill(3)+".png",dpi = 100)
        clf()
        close(fig)
main()

from pylab import *
import numpy as np

x = np.arange(1,100)
y = np.sin(x)

plot(x,y)
show()

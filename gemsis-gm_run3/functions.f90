module functions

  implicit none

  private

  public             :: n0, dxn0, dyn0, dzn0
  public             :: bdx, dxbdx, dybdx, dzbdx
  public             :: bdy, dxbdy, dybdy, dzbdy
  public             :: bdz, dxbdz, dybdz, dzbdz
  public             :: functions__init
  real(8), parameter :: dr = 1.D0/1.D0
  real(8), save      :: Bm, rin, drs, unmax, unmin


contains


  subroutine functions__init(Bmin,drsin,rinin,unmaxin,unminin)
    real(8), intent(in) :: Bmin, drsin, rinin, unmaxin, unminin

    Bm    = Bmin
    drs   = drsin
    rin   = rinin
    unmax = unmaxin
    unmin = unminin

  end subroutine functions__init

  
  real(8) function n0(r)
    real(8), intent(in) :: r
    
!!$    n0 = unmax-(unmax-unmin)*tanh((r-rin)*dr)
    n0 = unmin

  end function n0


  real(8) function dxn0(x,r)
    real(8), intent(in) :: x, r
    
!!$    dxn0 = -x*dr/r*(unmax-unmin)*cosh((r-rin)*dr)**(-2)
    dxn0 = 0.0D0
    
  end function dxn0


  real(8) function dyn0(y,r)
    real(8), intent(in) :: y, r
    
!!$    dyn0 = -y*dr/r*(unmax-unmin)*cosh((r-rin)*dr)**(-2)
    dyn0 = 0.0D0

  end function dyn0


  real(8) function dzn0(z,r)
    real(8), intent(in) :: z, r
    
!!$    dzn0 = -z*dr/r*(unmax-unmin)*cosh((r-rin)*dr)**(-2)
    dzn0 = 0.0D0

  end function dzn0


  real(8) function bdx(x0,z0,xm,zm,dr05,drm5)
    real(8), intent(in) :: x0, z0, xm, zm, dr05, drm5
    bdx = -Bm*(+dr05*3.*x0*z0+drm5*3.*xm*zm)
  end function bdx

  
  real(8) function bdy(y0,z0,ym,zm,dr05,drm5)
    real(8), intent(in) :: y0, z0, ym, zm, dr05, drm5
    bdy = -Bm*(+dr05*3.*y0*z0+drm5*3.*ym*zm)
  end function bdy


  real(8) function bdz(z0,r0,zm,rm,dr05,drm5)
    real(8), intent(in) :: z0, r0, zm, rm, dr05, drm5
    bdz = -Bm*(+dr05*(+3.*z0*z0-r0*r0)+drm5*(+3.*zm*zm-rm*rm))
  end function bdz


  real(8) function dxbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)
    real(8), intent(in) :: x0, z0, r0, xm, zm, rm, dr05, drm5
    dxbdx = -Bm*(+dr05*(+3.*z0-15.*x0*x0*z0*r0**(-2)) &
                 +drm5*(+3.*zm-15.*xm*xm*zm*rm**(-2)))*drs
  end function dxbdx


  real(8) function dybdx(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)
    real(8), intent(in) :: x0, y0, z0, r0, xm, ym, zm, rm, dr05, drm5
    dybdx = -Bm*(+dr05*(-15.*x0*y0*z0*r0**(-2)) &
                 +drm5*(-15.*xm*ym*zm*rm**(-2)))*drs
  end function dybdx


  real(8) function dzbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)
    real(8), intent(in) :: x0, z0, r0, xm, zm, rm, dr05, drm5
    dzbdx = -Bm*(+dr05*(+3.*x0-15.*x0*z0*z0*r0**(-2)) &
                 +drm5*(+3.*xm-15.*xm*zm*zm*rm**(-2)))*drs
  end function dzbdx


  real(8) function dxbdy(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)
    real(8), intent(in) :: x0, y0, z0, r0, xm, ym, zm, rm, dr05, drm5
    dxbdy = -Bm*(+dr05*(-15.*x0*y0*z0*r0**(-2)) &
                 +drm5*(-15.*xm*ym*zm*rm**(-2)))*drs
  end function dxbdy


  real(8) function dybdy(y0,z0,r0,ym,zm,rm,dr05,drm5)
    real(8), intent(in) :: y0, z0, r0, ym, zm, rm, dr05, drm5
    dybdy = -Bm*(+dr05*(+3.*z0-15.*y0*y0*z0*r0**(-2)) &
                 +drm5*(+3.*zm-15.*ym*ym*zm*rm**(-2)))*drs
  end function dybdy


  real(8) function dzbdy(y0,z0,r0,ym,zm,rm,dr05,drm5)
    real(8), intent(in) :: y0, z0, r0, ym, zm, rm, dr05, drm5
    dzbdy = -Bm*(+dr05*(+3.*y0-15.*y0*z0*z0*r0**(-2)) &
                 +drm5*(+3.*ym-15.*ym*zm*zm*rm**(-2)))*drs
  end function dzbdy


  real(8) function dxbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)
    real(8), intent(in) :: x0, y0, z0, r0, xm, ym, zm, rm, dr05, drm5
    dxbdz = -Bm*(+dr05*(-2.*x0-5.*x0*(-x0*x0-y0*y0+2.*z0*z0)*r0**(-2)) &
                 +drm5*(-2.*xm-5.*xm*(-xm*xm-ym*ym+2.*zm*zm)*rm**(-2)))*drs
  end function dxbdz


  real(8) function dybdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)
    real(8), intent(in) :: x0, y0, z0, r0, xm, ym, zm, rm, dr05, drm5
    dybdz = -Bm*(+dr05*(-2.*y0-5.*y0*(-x0*x0-y0*y0+2.*z0*z0)*r0**(-2)) &
                 +drm5*(-2.*ym-5.*ym*(-xm*xm-ym*ym+2.*zm*zm)*rm**(-2)))*drs
  end function dybdz


  real(8) function dzbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)
    real(8), intent(in) :: x0, y0, z0, r0, xm, ym, zm, rm, dr05, drm5
    dzbdz = -Bm*(+dr05*(+4.*z0-5.*z0*(-x0*x0-y0*y0+2.*z0*z0)*r0**(-2)) &
                 +drm5*(+4.*zm-5.*zm*(-xm*xm-ym*ym+2.*zm*zm)*rm**(-2)))*drs
  end function dzbdz


end module functions

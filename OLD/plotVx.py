# -*- coding: utf-8 -*-
import numpy as np
from pylab import *
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

path = "/work/ymatumot/simulation/GM-RB/run_Vsw400/"
name = "MHD_Vx"

"""
-30 <= x <= 69 (Re)
-40 <= y <= 40 (Re)
""" 
x1 = -20
y1 = 0
x2 = -7
y2 = 0
x3 = 10
y3 = 0


def main():
    v1,v2,v3 = [],[],[]
    t = []
    time = 0
    while(time <= 208):
        
        filename = path + name + str(time).zfill(3) + ".dat"
        
        head = ("head","<i")
        tail = ("tail","<i")
        vx = ("vx","<17250057f4")
         
        dt = np.dtype([head,vx,tail])
        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
    
        vf = chunk[0]["vx"].reshape((297,241,241),order="F")
        vt = vf.transpose(1,0,2)

        v1 = np.append(v1,vt[ny(y1),nx(x1),120])
        v2 = np.append(v2,vt[ny(y2),nx(x2),120])
        v3 = np.append(v3,vt[ny(y3),nx(x3),120])
        t = np.append(t,time*12)
        time += 1
        
    subplots_adjust(hspace = 0.9)
    subplot(3,1,1)
    plot(t,v1)
    xlabel("time[sec]")
    ylabel("Vx[]")
    title("("+str(x1)+","+str(y1)+")")
    
    subplot(3,1,2)
    plot(t,v2)
    xlabel("time[sec]")
    ylabel("Vx[]")
    title("("+str(x2)+","+str(y2)+")")
    
    subplot(3,1,3)
    plot(t,v3) 
    xlabel("time[sec]")
    ylabel("Vx[]")
    title("("+str(x3)+","+str(y3)+")")
    
    show()
# savefig("EphiL"+str(time/24)+".jpeg")
 #   print "saved ",time

def nx(x):
    return int(x*3 + 90)

def ny(y):
    return int(y*3 + 121)

main()

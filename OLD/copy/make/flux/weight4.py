# -*- coding: utf-8 -*-
from pylab import *
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches

m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 1000
files ="/work/m_hayashi/RBF2/ptl"
savepath ="/home/m_hayashi/output/flux/L_MLT/"
t1=0
enemin = 5
enest = enemin -3
dA = 0.5*0.5
da = float(1.0/360.0)*np.pi
de = 0.2e6
dt = 6
dL = 0.2
dphi = np.pi/60

def main():
    
    for t2 in range(0,200):
        count = np.zeros((122,180,80,120)) #L,a,ene,MTL
        flux1 = np.zeros((122,180,80,120))
        flux = np.zeros((122,120))         # integral of a and ene
        clf()
    # file loop 
        for filenum in range(0,128):
            filename = files + str(filenum).zfill(3) +".dat"

    #read file
            xp1,yp1,a1,mu1,ene1 =  readfile(filename,t1) 
            x1,y1 = xp1/6370000,yp1/6370000
            L1 = np.sqrt((x1)**2 + (y1)**2)

   
            xp2,yp2,a2,mu2,ene2 =  readfile(filename,t2)
            x2,y2 = xp2/6370000,yp2/6370000
            L2 = np.sqrt((x2)**2 + (y2)**2)
            dg2= np.arctan2(y2,x2)*180/np.pi +180
            mlt= dg2*1/3 #gedree to mlt
    #weighting
            w = L2*np.abs(np.sin(a2))*ene2/(L1*np.abs(np.sin(a1))*ene1)/L2
            #w = np.ones((1,ptl))/L2
    #grid number
            xg = (2*x2).astype(int) + 30
            yg = (2*y2).astype(int) + 30
            ag = (((180/np.pi)*a2)).astype(int)
            eg = (5*(ene2/1e6-enest)).astype(int)
            Lg = (5*L2).astype(int)
            MLTg= mlt.astype(int)
        
    
    #ptl loop
            for m in range(0,ptl):
    #count per bins
                count[Lg[0,m],ag[0,m],eg[0,m],MLTg[0,m]] = count[Lg[0,m],ag[0,m],eg[0,m],MLTg[0,m]] + w[0,m]
    # end file loop
          #  print count[35,:,:,:]
    #calicurate flux
        for an in range(0,180):
            ap = (an - 0.5)*np.pi/180
            flux1[:,an,:,:] += count[:,an,:,:]/(dL*dphi*da*de*dt*np.abs((np.cos(ap)))*2*np.pi*(np.sin(ap)))               
    #integral of a and ene                       
        for i in range(0,180):
            for j in range(0,80):
                flux[:,:] +=  flux1[:,i,j,:]
        print "read...",t2
    #show images
        flux2 = np.zeros((122,120))
        flux2[:,0:60] = flux[:,60:120]
        flux2[:,60:120] = flux[:,0:60]
        figure(1,figsize=(10,10))
        title("time="+str((t2+1)*6).zfill(3)+"[sec]")
        imshow(flux2[25:50,:],extent=[0,24,5,10],origin="lower",aspect="auto",vmin=0,vmax=20)
        xlabel("MLT",fontsize=13)
        ylabel("L",fontsize=13)
        CB=colorbar()   
        CB.set_label('flux [/Re$^2$ sec str MeV]',fontsize=15)
        CB.ax.tick_params(labelsize=13)
        savefig(savepath + "5MeV_L_MLT"+str(t2).zfill(3)+".jpeg")
        print flux[35:50,:]
def readfile(filename,t):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
    xp = chunk[t]["xp"].reshape((1,ptl),order="F")
    yp = chunk[t]["yp"].reshape((1,ptl),order="F")
    zp = chunk[t]["zp"].reshape((1,ptl),order="F")
    lp = chunk[t]["lp"].reshape((1,ptl),order="F")
    xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
    yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
    zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
    aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
    beq = chunk[t]["beq"].reshape((1,ptl),order="F")
    ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
    pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
    mu = chunk[t]["mu"].reshape((1,ptl),order="F")
    bp = chunk[t]["bp"].reshape((1,ptl),order="F")
    prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
    db = chunk[t]["db"].reshape((1,ptl),order="F")
    predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
    nump = chunk[t]["nump"].reshape((1,ptl),order="F")
    ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   
    fd.close()     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)

    return xeq ,yeq,aeq,mu,ene

main()

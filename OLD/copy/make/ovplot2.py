# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 10:59:43 2016

@author: m_hayashi
"""
import numpy as np
from pylab import *
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

epath = "/work/ymatumot/simulation/GM-RB/run_Vsw600/"
ename = "MHD_E"
ppath1 = "/work/m_hayashi/RB024/ptl"
ppath2 = "/work/m_hayashi/RB033/ptl"
outpath ="/home/m_hayashi/output/demo/"
ptl = 100
irank1 = 1
number1 = 90
irank2 = 55
number2 = 98

nx = np.arange(90,-207,-1)
ny = (np.ones(297))*(120)
na = np.empty([241,297])
nb = np.empty([241,297])

for n in range(0,241):
    na[n] = nx
    nb[n] = ny - n
    
    
cosphi=na/np.sqrt((na)**2+(nb)**2)
sinphi=nb/np.sqrt((na)**2+(nb)**2)

def main():
    for time in range(1,101):
        Ephi = readE(time)
        xp,yp = readp(time*2-1,ppath1,irank1,number1)
        xq,yq = readp(time*2-1,ppath2,irank2,number2)
        
        figure(figsize=(12,10))
        scatter(xp,yp,c="orange",s=1000,alpha=1.0,lw=0)
        scatter(xq,yq,c="red",s=1000,alpha=1.0,lw=0)
    #Convert Global-MHD coodinate ->GSM
        xlim(11,-11)
        ylim(11,-11)
        imshow(Ephi[88:154,57:123],extent=[11,-11,11,-11],origin='lower', vmax=10,vmin=-10)
        xlabel("x(GSM)[Re]")
        ylabel("y(GSM)[Re]")
        title("time="+str(time*12)+"s")
        CB=colorbar()   
        CB.set_label('Ephi [mV/m]',fontsize=15)
    #CB.set_clim(-4.84,2.64)  
        
        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
        fig = gcf()
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)
        
        savefig(outpath+"overw"+str(time-1)+".jpeg")
        print "saved ",time*12
        
def readE(t):
    filename = epath + ename + str(t).zfill(3) + ".dat"
    
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<51750171f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
     #Convert Global-MHD coodinate ->GSM
    bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*2.2
    bt=bf.transpose(1,0,2,3)
    ey = -bt[:,:,120,1]
    ex = -bt[:,:,120,0]
    
    ephi = ey*cosphi - ex*sinphi
    return ephi

def readp(t,ppath,irank,number):
    filename =ppath +str(irank).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=t+1)
    time = chunk[t]["rtime"]
    xp = chunk[t]["xp"].reshape((1,ptl),order="F")
    yp = chunk[t]["yp"].reshape((1,ptl),order="F")
    zp = chunk[t]["zp"].reshape((1,ptl),order="F")
    lp = chunk[t]["lp"].reshape((1,ptl),order="F")
    xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
    yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
    zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
    aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
    beq = chunk[t]["beq"].reshape((1,ptl),order="F")
    ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
    pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
    mu = chunk[t]["mu"].reshape((1,ptl),order="F")
    bp = chunk[t]["bp"].reshape((1,ptl),order="F")
    prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
    db = chunk[t]["db"].reshape((1,ptl),order="F")
    predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
    nump = chunk[t]["nump"].reshape((1,ptl),order="F")
    ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
    fd.close()     
    print time
    #convert GSM
    return -xp[0,number]/6380000,-yp[0,number]/6380000

main()

# -*- coding: utf-8 -*-
from pylab import *
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches
import matplotlib.cm as cm 
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 1000
files ="/work/m_hayashi/RBF2/ptl"
t1=0
iniene = 5
enest = iniene -1
dA = 0.5*0.5
da = float(1.0/360.0)*np.pi
de = 0.4e6
dt = 6
dL = 0.1

def main():
    for t2 in range(0,200):
        print t2
        parr = np.array([[0],[0],[0],[0],[0],[0],[0]])
        clf()
        for filenum in range(0,128):
            filename = files + str(filenum).zfill(3) +".dat"
    #read file   
            xp,yp,ene,eneperp,enepara =  readfile(filename,t2)
            for m in range(0,ptl):
                if(6.4 >= ene[0,m] >= 6.0):
                    b = np.array([[ene[0,m],eneperp[0,m],enepara[0,m],xp[0,m],yp[0,m],filenum,m]])
                    parr = concatenate((parr,b.T),axis=1)

        figure(figsize=(8,6.5))
        ax=scatter(-parr[3,:],-parr[4,:],c = parr[2,:],cmap=cm.jet,vmax=2.,vmin=0.,alpha=0.7,linewidth=0)
        CB=colorbar(ax)
        CB.set_label("Wpara [MeV]",fontsize=14)
        title("W0=5MeV \n 6.0 < W < 6.4  time=" +str(6*t2)+" sec")
        
        grid()
        xlabel("x [Re]",fontsize=14)
        ylabel("y [Re]",fontsize=14)
        xlim(11,-11)
        ylim(11,-11)

        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
        fig = gcf()
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)    

        savefig("/work/m_hayashi/output/energy/ene_sc" + str(t2).zfill(3) + ".jpeg")
        del parr
    
    """
    title("time= " +str((t2+1)*6)+"sec")
    plot(ene[0,:],color="k",label="W")
    plot(eneperp[0,:],color="b",label="Wperp")
    plot(enepara[0,:],color="g",label="Wpara")
    xlabel("num")
    ylim(0,8)
    ylabel("[MeV]")
    legend()
        
    savefig("/work/m_hayashi/output/energy/enedis_"+str(t2).zfill(3)+".jpeg")
    """    

def readfile(filename,time):
    t=time
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
    xp = chunk[t]["xp"].reshape((1,ptl),order="F")
    yp = chunk[t]["yp"].reshape((1,ptl),order="F")
    zp = chunk[t]["zp"].reshape((1,ptl),order="F")
    lp = chunk[t]["lp"].reshape((1,ptl),order="F")
    xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
    yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
    zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
    aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
    beq = chunk[t]["beq"].reshape((1,ptl),order="F")
    ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
    pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
    mu = chunk[t]["mu"].reshape((1,ptl),order="F")
    bp = chunk[t]["bp"].reshape((1,ptl),order="F")
    prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
    db = chunk[t]["db"].reshape((1,ptl),order="F")
    predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
    nump = chunk[t]["nump"].reshape((1,ptl),order="F")
    ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   
    fd.close() 
    
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)

    gperp = np.sqrt(1 + (pperp**2) / (m*c)**2)
    eneperp = (m * c**2 * (gperp - 1) / e)

    gpara = np.sqrt(1 + (ppara**2) / (m*c)**2)
    enepara = (m * c**2 * (gpara - 1) / e)
    
    del chunk
    return xeq/6370000 ,yeq/6370000,ene/1e6,eneperp/1e6,enepara/1e6

def griding():
    L = np.sqrt((x)**2 + (y)**2)
    dg2= np.arctan2(y,x)*180/np.pi +180
    mlt= dg2*1/3 #gedree to mlt
    
    #grid number
    xg = (2*x).astype(int) + 30
    yg = (2*y).astype(int) + 30
    ag = ((180/np.pi)*a).astype(int)
    eg = (5*(ene/1e6-enest)).astype(int)
    Lg = (10*L).astype(int)
    MLTg= mlt.astype(int)    #ptl loop

main()

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
time = 0
while(time < 2400):
    
    path = "/work/saito/GMHDRB/inputdata/"
    ename = "MHDE_RB"

    filename = path + ename + str(time).zfill(6) + ".dat"

    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<6291456f4")
         
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["b"].reshape((128,128,128,3),order="F")
    bt=bf.transpose(2,1,0,3)*(1e3)
    ey = bt[:,64,:,1]

    figure(figsize=(12,10))
    imshow(ey,extent=[-21.33,21.33,-21.33,21.33],origin='lower',vmax=13,vmin=-13)  #Ey
   # imshow(ey[31:97,31:97],extent=[-11,11,-11,11],origin ='lower',vmax=13,vmin=-13)
    xlabel("x(Re)")
    ylabel("z(Re)")
    title("time="+str(time)+"s")

    CB=colorbar()   
    CB.set_label('Ey [mV/m]',fontsize=15)
    CB.set_clim(-13,13)  
    
    wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
    wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
    fig = gcf()
    fig.gca().add_artist(wedge1)
    fig.gca().add_artist(wedge2)

    savefig("Ey(xz)"+str(time/24)+".jpeg")
    print "saved ",time
    time += 24

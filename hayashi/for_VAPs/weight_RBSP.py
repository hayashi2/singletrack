# -*- coding: utf-8 -*-

import numpy as np
from scipy.interpolate import interp1d
def main():

    w = np.array([5.5,5.07,4.43,3.78,3,2.36,1.78])
    x = np.linspace(1,4,np.size(w))
    x2 = np.linspace(1,4,2*np.size(w)-1)
    f = interp1d(x,w)
    w2 = f(x2)
    delf = w2[-1]-w2[-2]
    w = np.zeros(np.size(w2)+4)
    w[0] = 0
    w[1] = 5.64
    w[2] = 5.78
    w[3] = 5.71
    w[4:] = w2
    delf = w[-1]-w[-2]
    for i in xrange(np.size(w)-1+8):
        w = np.append(w,w[-1]+delf)
    print w
    w = 10**w
    x = np.arange(0,float(np.size(w))/4,0.25)
    
    np.savez("/work/m_hayashi/Fluxdata/weight_VAPs0716.npz",energy=x,flux=w)
main()

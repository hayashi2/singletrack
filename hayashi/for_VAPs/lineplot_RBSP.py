# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("Agg")
from pylab import *
import numpy as np
from spacepy import pycdf
import matplotlib.ticker as tic
import matplotlib.colors as col
import matplotlib.cm as cm
import sys
import subprocess
import os
from datetime import datetime,date,timedelta
from matplotlib import gridspec

la_fontsize = 19
ti_fontsize = 16
def main():
    args = sys.argv
    print datetime.today()

    
    ###### Input check ######
    
    if (len(args)!=3):
        print "#########WARNING######### Incorrect input"
        print "Arguments  1st: start date, 2nd: end date"
        sys.exit()
        
    if(len(args[1])!= 12 or len(args[2])!=12):
        print "#########WARNING######### Incorrect input"
        print "start_date or end_date is incorrect"
        sys.exit()

    
    ###### Data acquisition ######
    start_date = args[1]
    end_date = args[2]

    s_date = datetime.strptime(start_date,"%Y%m%d%H%M")
    e_date = datetime.strptime(end_date,"%Y%m%d%H%M")

    
    for single_date in daterange(s_date, e_date):        
        year = str(single_date.year).zfill(4)
        month = str(single_date.month).zfill(2)
        day = str(single_date.day).zfill(2)
        
        mfilename = "rbspb_rel03_ect-mageis-L3_"+year+month+day+"_v7.1.0.cdf"
        rfilename = "rbspb_rel03_ect-rept-sci-L3_"+year+month+day+"_v5.1.0.cdf"
        #pfilename = "erg_orb_l2_"+year+month+day+"_v01.cdf"
        
        Mdata = ReadMagEIS(year,mfilename,"B")
        print Mdata
        #position = ReadPosition(year,pfilename)
        M_FLUX = Mdata["FEDU"]
        M_Epoch = Mdata["Epoch"][:]
        Alpha = Mdata["FEDU_Alpha"][:]
        E_label = Mdata["FEDU_ENERGY_LABL"][:,:]
        L = Mdata["L"][:]
        MLT = Mdata["MLT"][:]
        MLAT = Mdata["MLAT"][:]

        Adata = ReadPEPT(year,rfilename,"B")
        print Adata
        R_FLUX = Adata["FEDU"]
        R_Epoch = Adata["Epoch"][:]
        R_Alpha = Adata["FEDU_Alpha"][:]
        R_E_label = Adata["FEDU_Energy"][:]


        print np.diff(M_Epoch)
        break #one loop
    
    ###### Plot range ######
    delta_m = int(((e_date - s_date).total_seconds())/60.)
    s_hour = s_date.hour 
    s_min = s_date.minute 
    
    ep_st = int((s_hour*60.*60.+s_min*60.)/10.789000) 
    ep_ed = ep_st + int(delta_m*60./10.789000)

    FLUX = Recreate(M_FLUX[:,5,:],M_Epoch,s_date) #FLUX of a =90
    L,MLT,MLAT = Reposition(L,MLT,MLAT,M_Epoch,s_date)
    R_FLUX = Recreate(R_FLUX[:,8,:],R_Epoch,s_date)

    ind = np.argmin(np.abs(FLUX[ep_st,16] - M_FLUX[:,5,16]))
    print M_Epoch[ind],"!!!!!!"



###### Figure1 Setting 1 ######
    fig1 = figure(1,figsize=(10,5))
    #gs = gridspec.GridSpec(4,1,height_ratios=[12,3,3,3])
    ax1 = fig1.add_subplot(111)
    
    ax1xticks = np.linspace(0,delta_m,np.shape(FLUX[ep_st:ep_ed,:])[0])
    ### Draw and Plot ###
    FLUX = FLUX[:,10:-8]
    E_label = E_label[:,10:-8]
    R_FLUX = R_FLUX[:,1:-6]
    R_E_label =R_E_label[1:-6]
    for i in xrange((np.shape(FLUX)[1]+1)/2 +np.shape(R_FLUX)[1]):
        c = cm.jet(i/np.float((np.shape(FLUX)[1]+1)/2+np.shape(R_FLUX)[1]),1)
        if (i<4):
            E = E_label[0,i*2]
            E = "MagEIS "+ "%.2f"%np.around(float(E[:-26])/1e3,decimals=2)+" MeV"

            ax1.plot(ax1xticks,FLUX[ep_st-30:ep_ed-30,i*2]*1e3,label=E,color=c,lw=2)  #keV^-1 => MeV^-1
        else:
            ax1.plot(ax1xticks,R_FLUX[ep_st:ep_ed,i-4],label="REPT    %.2f"%R_E_label[i-4]+" MeV",color=c,lw=2)

  ###### Figure1 setting 2 ######
#    for ax in (ax1,ax2,ax3):
#        setp(ax.get_xticklabels(),visible=False)

    ax1.set_title(year+"-"+month+"-"+day+"  05:50 - 06:20",fontsize=la_fontsize)
    ax1.set_ylabel("FEDU_Alpha = "+str(np.round(Alpha[5],decimals=3))+" deg\n"\
                       +"$cm^{-2}s^{-1}str^{-1}MeV^{-1}$",fontsize=la_fontsize)
    #"Unidirectional Differential Electron Flux"\n 
    ax1.grid()
    #ax1.legend(bbox_to_anchor=(1.03, 1), loc=2, borderaxespad=0.,fontsize=15)
    ax1.legend(loc="lower right",fontsize=la_fontsize-7)
    ax1.set_yscale("log")
    ax1.xaxis.set_minor_locator(tic.AutoMinorLocator())
    ax1.tick_params(axis='y', which='major', labelsize=ti_fontsize)

    #ax1.set_yticklabels(np.logspace(0,5,6),fontsize=13)
    ax1.set_xlim(0,delta_m)
    ax1.set_ylim(1e0,1e7)
    left, width = -0.04, .5
    bottom, height = -0.02, .5
    right = left + width
    top = bottom + height

    ax1.text(left, bottom, "hh:mm\nL\nMLT\nMLAT",horizontalalignment='right',\
                 verticalalignment='top', transform=ax1.transAxes,fontsize=ti_fontsize)

    xticks = np.arange(0,delta_m+10,10)
    ticks_L, ticks_MLT, ticks_MLAT = Position_ticks(L[ep_st:ep_ed], MLT[ep_st:ep_ed], MLAT[ep_st:ep_ed], 10)
    x = [((s_date + timedelta(minutes=i)).time()).strftime("%H:%M") \
             +"\n"+ticks_L[count] +"\n"+ticks_MLT[count] +"\n"+ticks_MLAT[count]\
             for count,i in enumerate(xrange(0,delta_m+10,10))]

    ax1.set_xticks(xticks)
    ax1.set_xticklabels(x,fontsize=ti_fontsize)
    ax1.yaxis.set_label_coords(-0.07,0.5)

    fig1.subplots_adjust(hspace=0.1,bottom=0.2)
    fig1.savefig("RBSP_A"+s_date.strftime("%Y%m%d%H%M")+"-"+e_date.strftime("%Y%m%d%H%M")+".png",dpi=300)
    close(fig1)


def ReadPEPT(year,filename,NUM):
    #URL = "https://rbsp-ect.lanl.gov/data_pub/rbspa/rept/level3/pitchangle/"+year+"/"+filename
    filepath = "/work/m_hayashi/VAPs_data/REPT_"+NUM+"/"+filename
    #if(os.path.exists(filepath)==True):
    #     pass
    #else:
    #    print "Download file..."
    #    cmd = "wget "+ URL+" -O "+filepath
    #    subprocess.call(cmd,shell=True)
    data = pycdf.CDF(filepath)
    return data

def ReadMagEIS(year,filename,NUM):

    #URL = "https://rbsp-ect.lanl.gov/data_pub/rbspa/mageis/level3/pitchangle/"+year+"/"+filename
    filepath = "/work/m_hayashi/VAPs_data/MagEIS_"+NUM+"/"+filename
    #if(os.path.exists(filepath)==True):
    #     pass
    #else:
    #    print "Download file..."
    #    cmd = "wget "+ URL+" -O "+filepath
    #    subprocess.call(cmd,shell=True)
    data = pycdf.CDF(filepath)
    return data


def ReadPosition(year,filename):
    URL = "https://ergsc.isee.nagoya-u.ac.jp/data/ergsc/satellite/erg/orb/def/"+year+"/"+filename
    filepath = "/work/m_hayashi/ERG_data/def/"+filename
    if(os.path.exists(filepath)==True):
         pass
    else:
        print "Download file..."
        cmd = "wget"+ " --http-user=erg_project --http-passwd=geospace "+ URL+" -O "+filepath
        subprocess.call(cmd,shell=True)
    data = pycdf.CDF(filepath)
    return data



def Recreate(DATA,Epoch,s_date):
    NEW_DATA = np.zeros((int(24*60*60/10.789000),np.shape(DATA)[1]))
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(DATA[:,:])[0]):
        if (np.round((Epoch[0] -basetime).total_seconds()) < 0):
            DATA = DATA[1:,:]
            #last_Ep = Epoch[0]
            Epoch = Epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(DATA[:,:])[0]):
        #gr = int(np.round((Epoch[i] -basetime).total_seconds()))/10
        gr += int(np.round((Epoch[i] - last).total_seconds()/10.789000))
        last = Epoch[i]
        
        if(gr>=24*60*60/10.789000 ):
            print Epoch[i]
            break
        
        NEW_DATA[gr,:] = DATA[i,:]
    return NEW_DATA




def Reposition(Lm,MLT,MLAT,epoch,s_date):
    NEW_L = np.zeros(24*60*60/10.789000)
    NEW_MLT = np.zeros(24*60*60/10.789000)
    NEW_MLAT = np.zeros(24*60*60/10.789000)
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(epoch[:])[0]-1):
        if (np.round((epoch[0] -basetime).total_seconds()) < 0):
            Lm = Lm[1:]
            MLT = MLT[1:]
            MLAT = MLAT[1:]
            #last_Ep = Epoch[0]
            epoch = epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(epoch[:])[0]-1):
        gr += int(np.round((epoch[i] -last).total_seconds()/10.789000))
        last = epoch[i]
        if(gr>=24*60*60/10.789000):
            print epoch[i]
            break
        NEW_L[gr] = Lm[i]
        NEW_MLT[gr] = MLT[i]
        NEW_MLAT[gr] = MLAT[i]
    return NEW_L,NEW_MLT,NEW_MLAT


def Position_ticks(L,MLT,MLAT,num):
    L_ticks, MLT_ticks,MLAT_ticks = [], [], []
    t = 0
    i = 0
    while(i <= np.size(L) ):
        #print t
        if(i == np.size(L)):
            L_ticks.append(str(round(L[i-1],1)))
            MLT_ticks.append(str(round(MLT[i-1],1)))
            MLAT_ticks.append(str(round(MLAT[i-1],1)))
        else:
            L_ticks.append(str(round(L[i],1)))
            MLT_ticks.append(str(round(MLT[i],1)))
            MLAT_ticks.append(str(round(MLAT[i],1)))
        t += num
        i = int(t*60./10.789000)

    return L_ticks, MLT_ticks,MLAT_ticks



def daterange(s_date,e_date):
    return (s_date + timedelta(days=i) for i in range((e_date-s_date).days+1))

main()

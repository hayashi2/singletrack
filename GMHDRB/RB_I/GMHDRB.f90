!######################################################################
program GMHDRB
!######################################################################

  use mpi
  use prm
  use inp
  use loadGM, only: loadGM__exe
  use gkmover, only: gkmover__exe
  use init, only: init_single, init_uniform_local_time

  implicit none

  double precision, allocatable :: emf0(:,:,:,:)
  double precision, allocatable :: emf1(:,:,:,:)

  integer, allocatable :: nump(:), ierr(:)
  double precision, allocatable :: mu(:), xp(:), yp(:), zp(:), lp(:)
  double precision, allocatable :: xeq(:), yeq(:), zeq(:), aeq(:), beq(:)
  double precision, allocatable :: ppara(:), pperp(:)
  double precision, allocatable :: bp(:), prebp(:), db(:), predb(:)
 
  integer :: i, nstep, iecode, irank, nprocs, openstatus, nstart, nend, np
  double precision :: rtime
  character(LEN=128) :: filename

!######################################################################

  call mpi_init(iecode)
  call mpi_comm_rank(mpi_comm_world, irank, iecode)
  call mpi_comm_size(mpi_comm_world, nprocs, iecode)

!Read .INP file
  call read_inp_file(irank)

!Initialize
  allocate(emf0(6, nx, ny, nz)) 
  allocate(emf1(6, nx, ny, nz)) 
  allocate(mu(maxptl))
  allocate(xp(maxptl))
  allocate(yp(maxptl))
  allocate(zp(maxptl))
  allocate(lp(maxptl))
  allocate(xeq(maxptl))
  allocate(yeq(maxptl))
  allocate(zeq(maxptl))
  allocate(aeq(maxptl))
  allocate(beq(maxptl))
  allocate(ppara(maxptl))
  allocate(pperp(maxptl))
  allocate(bp(maxptl))
  allocate(prebp(maxptl))
  allocate(db(maxptl))
  allocate(predb(maxptl))
  allocate(ierr(maxptl))
  allocate(nump(maxptl))

!######################################################################


  nstart = dint(calc_start / dt)
  nend = dint(calc_end / dt)

  rtime = calc_start

  call loadGM__exe(rtime, irank, emf0)
  call loadGM__exe(rtime+dsec, irank, emf1)

  emf0(4:6, 1:nx, 1:ny, 1:nz) = 0d0
  emf1(4:6, 1:nx, 1:ny, 1:nz) = 0d0

  !call init_single(rtime, irank, emf0,           &
  call init_uniform_local_time(rtime, irank, emf0, &
                 & xp,     yp,     zp,    &
                 & lp,     xeq,    yeq,   &
                 & zeq,    aeq,    beq,   &
                 & ppara,  pperp,  mu,    &
                 & bp,     prebp,  db,    &
                 & predb, nump,   ierr    )
  
  call loadGM__exe(rtime, irank, emf0)
  call loadGM__exe(rtime+dsec, irank, emf1)

  TIME_INTEGRATION: do i = nstart, nend !---------------------------------------

  ! Calc. GK motion: including Precipitation loss and system-out loss.
    call gkmover__exe(rtime,                 &
                    & emf0,                  &
                    & emf1,                  &
                    & maxptl,                &
                    & xp,     yp,     zp,    &
                    & lp,     xeq,    yeq,   &
                    & zeq,    aeq,    beq,   &
                    & ppara,  pperp,  mu,    &
                    & bp,     prebp,  db,    &
                    & predb, nump,    ierr   )

    rtime = rtime + dt
    if (dmod(rtime+0.01*dt, dsec) < dt) then
      emf0 = emf1
      call loadGM__exe(rtime+dsec, irank, emf1)
      write(filename, '(a, a, i3.3, a)') trim(outpath), "stdout", irank, ".txt"
      open(unit=99, file=filename, form="formatted", &
         & status="new", action="write", iostat=openstatus)
      if (openstatus /= 0) then
        open(unit=99, file=filename, form="formatted", &
           & status="old", action="write", position="append", iostat=openstatus)
      end if
      write(99,*) "Load GM fields at"
      write(99,*) "rtime=", rtime, rtime + dsec
      write(99,*) "------------------------------"
      close(99)
    end if

  !BEGIN: SAVE DATA ------------------------------------------------------------
    if (dmod(rtime+0.01*dt, ptlout) < dt) then
      write(filename, '(a, a, i3.3, a)') trim(outpath), "ptl", irank, ".dat"
      open(unit=100, file=filename, form="unformatted", &
         & status="new", action="write", iostat=openstatus)
      if (openstatus /= 0) then
        open(unit=100, file=filename, form="unformatted", &
           & status="old", action="write", position="append", iostat=openstatus)
      end if
      write(100) rtime,  np,     maxptl, &
               & xp,     yp,     zp,     &
               & lp,     xeq,    yeq,    &
               & zeq,    aeq,    beq,    &
               & ppara,  pperp,  mu,     &
               & bp,     prebp,  db,     &
               & predb,  nump,   ierr 
      close(100)
    end if
  !END: SAVE DATA --------------------------------------------------------------

  !BEGIN: TEMP OUTPUT ------------------------------------------------------------
    !if (dmod(rtime+0.01*dt, 1d0) < dt) then
    !  write(777,*) xp(1)/re, yp(1)/re, beq(1)
    !end if
  !END: TEMP OUTPUT ------------------------------------------------------------

  end do TIME_INTEGRATION !-----------------------------------------------------


!Finalize
  deallocate(emf0)
  deallocate(emf1)
  deallocate(mu)
  deallocate(xp)
  deallocate(yp)
  deallocate(zp)
  deallocate(lp)
  deallocate(xeq)
  deallocate(yeq)
  deallocate(zeq)
  deallocate(aeq)
  deallocate(beq)
  deallocate(ppara)
  deallocate(pperp)
  deallocate(bp)
  deallocate(prebp)
  deallocate(db)
  deallocate(predb)
  deallocate(ierr)
  deallocate(nump)

  call mpi_finalize(iecode)

!######################################################################
end program GMHDRB
!######################################################################

!######################################################################
program singletrak
!######################################################################

  use readfield, only: readfield__ini, readfield__exe, readfield_scal__exe
  use readptl, only: readptl__ini, readptl__exe, readptl__fin

  implicit none
  real(8), parameter :: re = 6378d3
  real(8), parameter :: q  = -1.6022d-19
  real(8), parameter :: m  = 9.1094d-31
  real(8), parameter :: c  = 2.9979d8

  integer, parameter :: maxptl = 100
  integer, parameter :: irank = 116
  integer, parameter :: number = 90
  real(8), parameter :: tracktime = 2400d0
  real(8), parameter :: ptlout = 0.1d0
  real(8), parameter :: interval = 12d0

   integer, parameter :: nx = 128
   integer, parameter :: ny = 128
   integer, parameter :: nz = 128
   real(8), parameter :: dx_re = 1d0/3d0
   real(8), parameter :: dy_re = 1d0/3d0
   real(8), parameter :: dz_re = 1d0/3d0

   character(128), parameter :: ppath = "/work/saito/GMHDRB/r7d0/"
   character(128), parameter :: pname = "ptldata"
   character(128), parameter :: bpath = "/work/saito/GMHDRB/inputdata/" 
   character(128), parameter :: bname = "MHDB_RB"
   character(128), parameter :: epath = "/work/saito/GMHDRB/inputdata/" 
   character(128), parameter :: ename = "MHDE_RB"
   character(128), parameter :: dbpath = "/work/saito/GMHDRB/inputdata/" 
   character(128), parameter :: dbname = "dBdt_"

   real(8) :: mu(1:maxptl)
   real(8) :: xp(1:maxptl), yp(1:maxptl), zp(1:maxptl)
   real(8) :: lp(1:maxptl)
   real(8) :: xeq(1:maxptl), yeq(1:maxptl), zeq(1:maxptl)
   real(8) :: aeq(1:maxptl), beq(1:maxptl)
   real(8) :: ppara(1:maxptl), pperp(1:maxptl), bp(1:maxptl)
   real(8) :: prebp(1:maxptl), db(1:maxptl), predb(1:maxptl)
   integer :: nump(1:maxptl), ierr
 
   real(8) :: g, rtime, e, gbx, gby, gbz
   real(8) :: emf0(6, nx, ny, nz), emf1(6, nx, ny, nz)
   real(8) :: dbdt0(nx, ny, nz), dbdt1(nx, ny, nz)
   real(8) :: bx0, by0, bz0, ex0, ey0, ez0, drtime, xi, yi, zi
   real(8) :: bx1, by1, bz1, ex1, ey1, ez1
   real(8) :: bx, by, bz, ex, ey, ez
   real(8) :: deltabt0, deltabt1, deltabt

   integer :: i, np 
   character(128) :: filename

! OPEN FILES
  call readptl__ini(ppath, pname, maxptl, irank)


  write(filename, '(a, i3.3, a, i3.3, a)') "p", irank, "_", number, ".txt"
  open(unit=555, file=filename, form="formatted", status="unknown")

  write(filename, '(a, i3.3, a, i3.3, a)') "b", irank, "_", number, ".txt"
  open(unit=556, file=filename, form="formatted", status="unknown")

  write(filename, '(a, i3.3, a, i3.3, a)') "e", irank, "_", number, ".txt"
  open(unit=557, file=filename, form="formatted", status="unknown")

  write(filename, '(a, i3.3, a, i3.3, a)') "ene", irank, "_", number, ".txt"
  open(unit=558, file=filename, form="formatted", status="unknown")

  write(filename, '(a, i3.3, a, i3.3, a)') "dBdt", irank, "_", number, ".txt"
  open(unit=559, file=filename, form="formatted", status="unknown")

!  write(filename, '(a, i3.3, a, i3.3, a)') "divb", irank, "_", number, ".txt"
!  open(unit=600, file=filename, form="formatted", status="unknown")

!  write(filename, '(a)') "Bnight.txt"
!  open(unit=700, file=filename, form="formatted", status="unknown")
!  write(filename, '(a)') "Bdawn.txt"
!  open(unit=701, file=filename, form="formatted", status="unknown")
!  write(filename, '(a)') "Bday.txt"
!  open(unit=702, file=filename, form="formatted", status="unknown")
!  write(filename, '(a)') "Bdusk.txt"
!  open(unit=703, file=filename, form="formatted", status="unknown")

!  write(filename, '(a)') "Enight.txt"
!  open(unit=800, file=filename, form="formatted", status="unknown")
!  write(filename, '(a)') "Edawn.txt"
!  open(unit=801, file=filename, form="formatted", status="unknown")
!  write(filename, '(a)') "Eday.txt"
!  open(unit=802, file=filename, form="formatted", status="unknown")
!  write(filename, '(a)') "Edusk.txt"
!  open(unit=803, file=filename, form="formatted", status="unknown")

  rtime = 0d0
  drtime = 0d0

  !########## Read Magnetic Field
  call readfield__ini(nx, ny, nz, dx_re, dy_re, dz_re, &
                    & interval, bpath, bname)
  call readfield__exe(rtime, emf0(1:3, 1:nx, 1:ny, 1:nz))
  call readfield__exe(rtime, emf1(1:3, 1:nx, 1:ny, 1:nz))

  !########## Read Electric Field
  call readfield__ini(nx, ny, nz, dx_re, dy_re, dz_re, &
                    & interval, epath, ename)
  call readfield__exe(rtime, emf0(4:6, 1:nx, 1:ny, 1:nz))
  call readfield__exe(rtime, emf1(4:6, 1:nx, 1:ny, 1:nz))

  call readfield__ini(nx, ny, nz, dx_re, dy_re, dz_re, &
                    & interval, dbpath, dbname)
  call readfield_scal__exe(rtime, dbdt0(1:nx, 1:ny, 1:nz))
  call readfield_scal__exe(rtime, dbdt1(1:nx, 1:ny, 1:nz))


  do 
    call readptl__exe &
        &(irank, rtime,  np,    &
        & xp,    yp,     zp,    &
        & lp,    xeq,    yeq,   &
        & zeq,   aeq,    beq,   &
        & ppara, pperp,  mu,    &
        & bp,    prebp,  db,    &
        & predb, nump, ierr     )

    drtime = dmod(rtime + 0.01*ptlout, interval)
    print *, "drtime=", drtime

    if (dmod(rtime + 0.1d0*ptlout, interval) < ptlout) then

      !########## Read Magnetic Field
      call readfield__ini(&
                        & nx, ny, nz, dx_re, dy_re, dz_re, &
                        & interval, bpath, bname)
      emf0(1:3, 1:nx, 1:ny, 1:nz) = emf1(1:3, 1:nx, 1:ny, 1:nz)
      call readfield__exe(rtime+interval, emf1(1:3, 1:nx, 1:ny, 1:nz))
      print *, "rtime=", rtime

      !########## Read Electric Field
      call readfield__ini(&
                        & nx, ny, nz, dx_re, dy_re, dz_re, &
                        & interval, epath, ename)
      emf0(4:6, 1:nx, 1:ny, 1:nz) = emf1(4:6, 1:nx, 1:ny, 1:nz)
      call readfield__exe(rtime+interval, emf1(4:6, 1:nx, 1:ny, 1:nz))
      print *, "rtime=", rtime

      call readfield__ini(&
                        & nx, ny, nz, dx_re, dy_re, dz_re, &
                        & interval, dbpath, dbname)
      dbdt0(1:nx, 1:ny, 1:nz) = dbdt1(1:nx, 1:ny, 1:nz)
      call readfield_scal__exe(rtime+interval, dbdt1(1:nx, 1:ny, 1:nz))
      print *, "rtime=", rtime

!      call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                 & emf0(1:3, 1:nx, 1:ny, 1:nz), -7*re, 0d0, 0d0, bx0, by0, bz0)
!      write(700, *) real(bx0), real(by0), real(bz0)
!
!      call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                 & emf0(1:3, 1:nx, 1:ny, 1:nz), 0d0, -7*re, 0d0, bx0, by0, bz0)
!      write(701, *) real(bx0), real(by0), real(bz0)
!
!      call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                 & emf0(1:3, 1:nx, 1:ny, 1:nz), 7*re, 0d0, 0d0, bx0, by0, bz0)
!      write(702, *) real(bx0), real(by0), real(bz0)
!
!      call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                 & emf0(1:3, 1:nx, 1:ny, 1:nz), 0d0, 7*re, 0d0, bx0, by0, bz0)
!      write(703, *) real(bx0), real(by0), real(bz0)
!
!      call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                 & emf0(4:6, 1:nx, 1:ny, 1:nz), -7*re, 0d0, 0d0, bx0, by0, bz0)
!      write(800, *) real(bx0), real(by0), real(bz0)
!
!      call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                 & emf0(4:6, 1:nx, 1:ny, 1:nz), 0d0, -7*re, 0d0, bx0, by0, bz0)
!      write(801, *) real(bx0), real(by0), real(bz0)
!
!      call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                 & emf0(4:6, 1:nx, 1:ny, 1:nz), 7*re, 0d0, 0d0, bx0, by0, bz0)
!      write(802, *) real(bx0), real(by0), real(bz0)
!
!      call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                 & emf0(4:6, 1:nx, 1:ny, 1:nz), 0d0, 7*re, 0d0, bx0, by0, bz0)
!      write(803, *) real(bx0), real(by0), real(bz0)


    end if


!TMP------------------
!    call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!               & emf0(1:3, 1:nx, 1:ny, 1:nz), -7*re, 0d0, 0d0, bx0, by0, bz0)
!    print *, "B @ 7Re =", dsqrt(bx0**2 + by0**2 + bz0**2)
!   stop
!TMP------------------



    do i = 1, np

      if (nump(i) == number) then
        g = dsqrt(1 + (ppara(i)**2 + pperp(i)**2) / (m*c)**2)
        e = m * c**2 * (g - 1) / dabs(q)

        call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
                   & emf0(1:3, 1:nx, 1:ny, 1:nz), xp(i), yp(i), zp(i), bx0, by0, bz0)
        call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
                   & emf1(1:3, 1:nx, 1:ny, 1:nz), xp(i), yp(i), zp(i), bx1, by1, bz1)
        bx = bx0 + (bx1 - bx0) * drtime / interval
        by = by0 + (by1 - by0) * drtime / interval
        bz = bz0 + (bz1 - bz0) * drtime / interval

!        call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                   & emf0(1:3, 1:nx, 1:ny, 1:nz), xp(i)+dx_re*re, yp(i), zp(i), bx0, by0, bz0)
!        call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                   & emf1(1:3, 1:nx, 1:ny, 1:nz), xp(i)+dx_re*re, yp(i), zp(i), bx1, by1, bz1)
!        gbx = (bx - (bx0 + (bx1 - bx0) * drtime / interval)) / dx_re
!        call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                   & emf0(1:3, 1:nx, 1:ny, 1:nz), xp(i), yp(i)+dy_re*re, zp(i), bx0, by0, bz0)
!        call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                   & emf1(1:3, 1:nx, 1:ny, 1:nz), xp(i), yp(i)+dy_re*re, zp(i), bx1, by1, bz1)
!        gby = (by - (by0 + (by1 - by0) * drtime / interval)) / dy_re
!        call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                   & emf0(1:3, 1:nx, 1:ny, 1:nz), xp(i), yp(i), zp(i)+dz_re, bx0, by0, bz0)
!        call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
!                   & emf1(1:3, 1:nx, 1:ny, 1:nz), xp(i), yp(i), zp(i)+dz_re, bx1, by1, bz1)
!        gbz = (bz - (bz0 + (bz1 - bz0) * drtime / interval)) / dz_re


        call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
                   & emf0(4:6, 1:nx, 1:ny, 1:nz), xp(i), yp(i), zp(i), ex0, ey0, ez0)
        call itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
                   & emf1(4:6, 1:nx, 1:ny, 1:nz), xp(i), yp(i), zp(i), ex1, ey1, ez1)
        ex = ex0 + (ex1 - ex0) * drtime / interval
        ey = ey0 + (ey1 - ey0) * drtime / interval
        ez = ez0 + (ez1 - ez0) * drtime / interval
       
        call scal_itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
                   & dbdt0(1:nx, 1:ny, 1:nz), xp(i), yp(i), zp(i), deltabt0)
        call scal_itp_emf(nx, ny, nz, dx_re*re, dy_re*re, dz_re*re, &
                   & dbdt1(1:nx, 1:ny, 1:nz), xp(i), yp(i), zp(i), deltabt1)

        deltabt = deltabt0 + (deltabt1 - deltabt0) * drtime / interval

        write(555, *) real(rtime), real(xp(i)), real(yp(i)), real(zp(i))
        write(556, *) real(rtime), real(bx), real(by), real(bz)
        write(557, *) real(rtime), real(ex), real(ey), real(ez)
        write(558, *) real(rtime), real(e)
        write(559, *) real(rtime), real(mu(i) * deltabt / g)
!        write(600, *) real(rtime), real(gbx + gby + gbz)

      end if

    end do

    rtime = rtime + ptlout
    print *, "rtime = ", rtime
    if (rtime > tracktime) exit

  end do

  close(555)
  close(556)
  close(557)
  close(558)
  close(559)

contains
!########10########20########30########40########50########60########80########80########

  subroutine itp_emf(nx, ny, nz, dx, dy, dz, emf, xp, yp, zp, bx, by, bz)

    integer, intent(in) :: nx, ny, nz
    real(8), intent(in) :: dx, dy, dz
    real(8), intent(in) :: emf(:, :, :, :)
    real(8), intent(in) :: xp, yp, zp
    real(8), intent(out):: bx, by, bz

    real(8) :: x0(6,2), x1(6,2), x2(6,2), x3(6,2), y0(6,2), y1(6,2), z0(6,2)
    real(8) :: deltax, deltay, deltaz, deltat, time
    integer :: ix, iy, iz, i, iem, n, nxhlf, nyhlf, nzhlf

    nxhlf = (nx - 1) / 2 + 1
    nyhlf = (ny - 1) / 2 + 1
    nzhlf = (nz - 1) / 2 + 1

    ix = dint(xp / dx + nxhlf)
    iy = dint(yp / dy + nyhlf)
    iz = dint(zp / dz + nzhlf)
    deltax = xp / dx + nxhlf - ix
    deltay = yp / dy + nyhlf - iy
    deltaz = zp / dz + nzhlf - iz

    do iem=1, 3
      x0(iem,1) = emf(iem,ix,iy,iz) + deltax*(emf(iem,ix+1,iy,iz) &
              & - emf(iem,ix,iy,iz))
      x1(iem,1) = emf(iem,ix,iy+1,iz) + deltax*(emf(iem,ix+1,iy+1,iz) &
              & - emf(iem,ix,iy+1,iz))
      x2(iem,1) = emf(iem,ix,iy,iz+1) + deltax*(emf(iem,ix+1,iy,iz+1) &
              & - emf(iem,ix,iy,iz+1))
      x3(iem,1) = emf(iem,ix,iy+1,iz+1) &
              & + deltax*(emf(iem,ix+1,iy+1,iz+1)-emf(iem,ix,iy+1,iz+1))
    end do
    do iem=1, 3
      y0(iem,1) = x0(iem,1) + deltay * (x1(iem,1) - x0(iem,1))
      y1(iem,1) = x2(iem,1) + deltay * (x3(iem,1) - x2(iem,1))
    end do
    do iem=1, 3
      z0(iem,1) = y0(iem,1) + deltaz * (y1(iem,1) - y0(iem,1))
    end do

    bx = z0(1,1)
    by = z0(2,1)
    bz = z0(3,1)

  end subroutine itp_emf

!######################################################################

  subroutine scal_itp_emf(nx, ny, nz, dx, dy, dz, emf, xp, yp, zp, bx)

    integer, intent(in) :: nx, ny, nz
    real(8), intent(in) :: dx, dy, dz
    real(8), intent(in) :: emf(:, :, :)
    real(8), intent(in) :: xp, yp, zp
    real(8), intent(out):: bx

    real(8) :: x0, x1, x2, x3, y0, y1, z0
    real(8) :: deltax, deltay, deltaz, deltat, time
    integer :: ix, iy, iz, i, iem, n, nxhlf, nyhlf, nzhlf

    nxhlf = (nx - 1) / 2 + 1
    nyhlf = (ny - 1) / 2 + 1
    nzhlf = (nz - 1) / 2 + 1

    ix = dint(xp / dx + nxhlf)
    iy = dint(yp / dy + nyhlf)
    iz = dint(zp / dz + nzhlf)
    deltax = xp / dx + nxhlf - ix
    deltay = yp / dy + nyhlf - iy
    deltaz = zp / dz + nzhlf - iz

      x0 = emf(ix,iy,iz) + deltax*(emf(ix+1,iy,iz) &
              & - emf(ix,iy,iz))
      x1 = emf(ix,iy+1,iz) + deltax*(emf(ix+1,iy+1,iz) &
              & - emf(ix,iy+1,iz))
      x2 = emf(ix,iy,iz+1) + deltax*(emf(ix+1,iy,iz+1) &
              & - emf(ix,iy,iz+1))
      x3 = emf(ix,iy+1,iz+1) &
              & + deltax*(emf(ix+1,iy+1,iz+1)-emf(ix,iy+1,iz+1))
      y0 = x0 + deltay * (x1 - x0)
      y1 = x2 + deltay * (x3 - x2)
      z0 = y0 + deltaz * (y1 - y0)

    bx = z0

  end subroutine scal_itp_emf

!######################################################################
end program singletrak
!######################################################################

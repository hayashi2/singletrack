# -*- coding: utf-8 -*-
from pylab import *
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches

m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 1000

files ="/work/m_hayashi/RBF4/ptl"
outputpath= "/work/m_hayashi/output/flux/xy_600keV/"
outputname= "RBF4dW"
outputtitle= "RBF4_dW<0.6   "
iniene = 2
enest = iniene-1
t1=0
dA = 0.2*0.2
da = float(1.0/360.0)*np.pi
de = 0.2e6
dt = 6 

def main():
    w = weighting()

    for t2 in range(0,200):
        clf()
        count = np.zeros((150,150,180,80)) #x,y,a,ene,
        flux1 = np.zeros((150,150,180,80))
        flux = np.zeros((150,150))         # integral of a and ene

    # file loop 
        for filenum in range(0,128):
            filename = files + str(filenum).zfill(3) +".dat"

    #read file
            xp1,yp1,a1,ene1,ierr1 =  readfile(filename,t1) 
            x1,y1 = xp1/6370000,yp1/6370000
            L1 = np.sqrt((x1)**2 + (y1)**2)

   
            xp2,yp2,a2,ene2,ierr2 =  readfile(filename,t2)
            x2,y2 = xp2/6370000,yp2/6370000
            energy=ene2/1e6
            L2 = np.sqrt((x2)**2 + (y2)**2)
    
    #grid number
            xg, yg, ag, eg = griding(x2,y2,a2,ene2)   
    #ptl loop
            for m in range(0,ptl):
    #count per bins
                if(ierr2[0,m]==0 and (xg[0,m] <150 and yg[0,m] <150) and energy[0,m] < (iniene+0.6)) :
                    count[xg[0,m],yg[0,m],ag[0,m],eg[0,m]] = count[xg[0,m],yg[0,m],ag[0,m],eg[0,m]] + w[filenum,m]
    # end file loop

    #calicurate flix
        for an in range(0,180):
            ap = (an - 0.5)*np.pi/180
            flux1[:,:,an,:] += count[:,:,an,:]/(dA*da*de*dt*(np.cos(ap))*2*np.pi*(sin(ap)))               
    #integral of a and ene                       
        for i in range(0,180):
            for j in range(0,80):
                flux +=  flux1[:,:,i,j]
    #show images
        imshow(np.absolute(flux[25:125,25:125].T),origin='lower',extent=[10,-10,10,-10],norm=LogNorm(vmin=1e0,vmax=1e2))

        grid()
        title(outputtitle+"time=" + str((t2+1)*6)+"sec")
        xlabel("x(GSM)[Re]",fontsize=13)
        ylabel("y(GSM)[Re]",fontsize=13)
        CB=colorbar()   
        CB.set_label('flux [/Re$^2$ sec str MeV]',fontsize=15)
        CB.ax.tick_params(labelsize=13)
        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
        fig = gcf()
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)    
        draw()
        savefig(outputpath+outputname+str(t2).zfill(3)+".jpeg")
        print("saved..."+str(t2))

def readfile(filename,t):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
    xp = chunk[t]["xp"].reshape((1,ptl),order="F")
    yp = chunk[t]["yp"].reshape((1,ptl),order="F")
    zp = chunk[t]["zp"].reshape((1,ptl),order="F")
    lp = chunk[t]["lp"].reshape((1,ptl),order="F")
    xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
    yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
    zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
    aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
    beq = chunk[t]["beq"].reshape((1,ptl),order="F")
    ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
    pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
    mu = chunk[t]["mu"].reshape((1,ptl),order="F")
    bp = chunk[t]["bp"].reshape((1,ptl),order="F")
    prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
    db = chunk[t]["db"].reshape((1,ptl),order="F")
    predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
    nump = chunk[t]["nump"].reshape((1,ptl),order="F")
    ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   
    fd.close()     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)

    return xeq ,yeq,aeq,ene,ierr

def weighting():
    count = np.zeros((150,150,180,80)) #x,y,a,ene,
    weight = np.empty((128,ptl))

    # file loop 
    for filenum in range(0,128):
        filename = files + str(filenum).zfill(3) +".dat"

    #read file
        xp1, yp1, a1,ene1, frag1 =  readfile(filename,0) 
        x1,y1 = xp1/6370000,yp1/6370000
        L1 = np.sqrt((x1)**2 + (y1)**2)

    #grid number
        xg, yg, ag, eg = griding(x1,y1,a1,ene1)
   
    #ptl loop
        for m in range(0,ptl):
    #count per bins
                count[xg[0,m],yg[0,m],ag[0,m],eg[0,m]] +=1 

    #weighting
    for filenum2 in range(0,128):
        filename2 = files + str(filenum2).zfill(3) +".dat"
        
        xp2, yp2, a2, ene2, frag2 =  readfile(filename2,0) 
        x2,y2 = xp2/6370000,yp2/6370000
        L2 = np.sqrt((x2)**2 + (y2)**2)
        #grid number
        xg2, yg2, ag2, eg2 = griding(x2,y2,a2,ene2)

        for m in range(0,ptl):
            if(count[xg2[0,m],yg2[0,m],ag2[0,m],eg2[0,m]]):
                 weight[filenum2,m] = L2[0,m]*np.abs(np.sin(a2[0,m]))*5/count[xg2[0,m],yg2[0,m],ag2[0,m],eg2[0,m]]
    return weight

def griding(x,y,a,ene):
    xg = np.around(5*x).astype(int) + 75
    yg = np.around(5*y).astype(int) + 75
    ag = np.around(((180/np.pi)*a)).astype(int)
    eg = np.around(5*(ene/1e6 - enest)).astype(int)
    
    return xg,yg,ag,eg


main()

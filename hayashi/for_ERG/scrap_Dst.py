# -*- codind: utf-8 -*-

import urllib2
from bs4 import BeautifulSoup
import datetime
from datetime import datetime,date,timedelta
import sys
import re
import numpy as np

def main():
    args = sys.argv
    ###### Input check ######
    if (len(args)!=2):
        print "#########WARNING######### Incorrect input"
        print "Arguments  1st: Data date (YYYY)"
        sys.exit()
    if(len(args[1])!= 4):
        print "#########WARNING######### Incorrect input"
        print "start_date is incorrect. Prease input YYYY"
        sys.exit()
    YYYY = args[1]
    #YYYY =  datetime.strptime(YYYY,"%Y")
    today = date.today()
    t_year,t_month,t_day = today.year,today.month,today.day

    f = open("Dst_"+str(YYYY).zfill(4)+".txt","w")
    for mm in xrange(1,13): #1 year
        if(int(YYYY)==t_year and mm > t_month):
            break
        YYYYmm = str(YYYY).zfill(4)+str(mm).zfill(2)
        print "Reading... "+YYYYmm +"'s Dst"

        DATA = Read_Month_Dst(YYYY,mm)
        DATA = DATA[2]
        DATAline = DATA.split("\n")
        DATAlabel = DATAline[:7]
        if(mm == 1):
            label1,label2 = Create_Label(DATAlabel[4:6])
            for s in label1:
                f.write("%s "%str(s).rjust(4))
            f.write("\n")
    
            for s in label2:
                f.write("%s "%str(s).rjust(4))
            f.write("\n")
        DATAline = DATAline[7:]
        for line in DATAline:
            print line
            if len(line)!=0 :
                newlist = []
                li = line.split()
                for l in li:
                    newlist.append(l.encode())
                if(int(YYYY)== t_year and mm == t_month and int(li[0]) == t_day):
                    break
                newlist.insert(0,YYYYmm.encode())
                for s in newlist:
                    if(len(s) >= 7):
                        SIZE = int(float(len(s))/4)
                        n = len(s)%4
                        
                        if(n==0):
                            for j in xrange(0,SIZE-1):
                                f.write("%s "%str(s[-(SIZE-j)*4:-(SIZE-j-1)*4]).rjust(4))
                            f.write("%s "%str(s[-4:]).rjust(4))
                        else:
                            f.write("%s "%str(s[-n-SIZE*4:-(SIZE)*4]).rjust(4))
                            for j in xrange(0,SIZE-1):
                                f.write("%s "%str(s[-(SIZE-j)*4:-(SIZE-j-1)*4]).rjust(4))
                            f.write("%s "%str(s[-n-1:]).rjust(4))
                    else:
                        f.write("%s "%str(s).rjust(4))
                f.write("\n")
    f.close()


def Create_Label(DATAlabel):
    line1 = DATAlabel[0]
    label1 = []
    li = line1.split()
    for l in li:
        label1.append(l.encode())
    label1.insert(0,"      ")
    for i in xrange(2,24):
        label1.insert(i," ")

    line2 = DATAlabel[1]
    label2 = []
    li = line2.split()
    for l in li:
        label2.append(l.encode())

    label2.insert(0,"YYYYmm")
    label2.insert(1,"DAY")
    return label1,label2
    
def Read_Month_Dst(YYYY,mm):
    YYYYmm = str(YYYY).zfill(4)+str(mm).zfill(2)
    htmlpath = "http://wdc.kugi.kyoto-u.ac.jp/dst_realtime/"+YYYYmm+"/index.html"    
    html = urllib2.urlopen(htmlpath) 
       
    soup = BeautifulSoup(html)
    DATA = soup.find(class_=re.compile("data"))
    DATA = np.array((DATA))
    return DATA


main()

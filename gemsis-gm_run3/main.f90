!!$**************************************************************!
!!$                                                              !
!!$     CIP-MHD code for Global MHD simulation model of the      !
!!$     terrestrial magnetosphere                                !
!!$                               by Yosuke Matsumoto @ STEL     !
!!$                                                              !
!!$                                                              !
!!$     - MPI parallelization                                    !
!!$       on Feb 22,2010                                         !
!!$     - OpenMP parallelization                                 !
!!$       on Feb 4, 2010                                         !
!!$     - Rewritten in F90 under MRI conding rule                !
!!$       on Jan 27, 2010                                        !
!!$     - Development started in Feb, 2008 supported by          !
!!$       Grant-in-aid 19740302 from MEXT, Japan                 !
!!$                                                              !
!!$**************************************************************!
      
program main

  use mpi_set
  use const
  use init
  use fio, only           : fio__output, fio__print
  use advection, only     : advection__rcip
  use non_advection, only : non_advection__rrk2, &
                            non_advection__df
  use boundary, only      : boundary__f, boundary__df, &
                            boundary__fdmp, boundary__dfdmp
  use crct, only          : crct__exec

  implicit none

  integer :: it
  real(8) :: etime, etime0
  real(8) :: omp_get_wtime

  !RECORDING THE INTIIAL TIME
  etime0 =  omp_get_wtime()

  !-------------------------------------------------------------
  ! INITIALIZATIONS
  !-------------------------------------------------------------
  call init__set_param
  call MPI_BCAST(etime0,1,mnpr,0,ncomw,nerr)

  !-------------------------------------------------------------
  ! START
  !-------------------------------------------------------------
  loop: do it=1,itmax-it0

     !-------------------------------------------------------------
     ! TIME COUNTER
     !-------------------------------------------------------------
     if(nrank == 0) etime = omp_get_wtime()

     call MPI_BCAST(etime,1,mnpr,0,ncomw,nerr)
     if(etime-etime0 >= etlim)then
        if(nrank == 0) &
             write(*,'(a,i6.6,a,i6.6)') '*** elapse time over ***  ',it-1+it0,'/',itmax
        call fio__print(it+it0-1,neq,nxs,nxe,nys,nye,nzs,nze, &
                        nxs1,nxe1,nys1,nye1,nzs1,nze1,        &
                        nrank,nerr,nstat,                     &
                        f,gf,Db,flg,wrk,bf,                   &
                        drs,px0,py0,pz0,pxm,pym,pzm,          &
                        dir_prt)                               

        call fio__output(it+it0-1,itsw0+itsw,neq,nx,ny,nz,            &
                         nxs,nxe,nys,nye,nzs,nze,                     &
                         nxs1,nxe1,nys1,nye1,nzs1,nze1,               &
                         nproc,nrank,                                 &
                         f,dxf,dyf,dzf,Db,flg,                        &
                         delt,gam,Bm,rin,drs,px0,py0,pz0,pxm,pym,pzm, &
                         unmax,unmin,                                 &
                         dir_out,file11)                                   

        exit loop
     endif

     !-------------------------------------------------------------
     ! ADVECTION
     !-------------------------------------------------------------
     call advection__rcip(gf,gdxf,gdyf,gdzf, &
                          f,dxf,dyf,dzf,flg)
     call boundary__f(gf,neq,nerr,nstat)
     call boundary__df(gdxf,gdyf,gdzf,neq,nerr,nstat)

     !-------------------------------------------------------------
     ! NON-ADVECTION OF F
     !-------------------------------------------------------------
!     call non_advection__ssprk3(f,            &
!                                nerr,nstat,   &
!                                gf,Db,wrk,bf,flg)
     call non_advection__rrk2(f,            &
                              ncomw,nerr,nstat,mnpr,opsum, &
                              gf,Db,wrk,bf,flg)
     call boundary__f(f,neq,nerr,nstat)

     !-------------------------------------------------------------
     ! DAMPING ARROUND THE INNER BOUNDARY FOR F
     !-------------------------------------------------------------
     call boundary__fdmp(f,neq,wrk,flg)
     call boundary__f(f,neq,nerr,nstat)

     !-------------------------------------------------------------
     ! NON-ADVECTION OF DF
     !-------------------------------------------------------------
     call non_advection__df(dxf,dyf,dzf,            &
                            f,gf,gdxf,gdyf,gdzf,flg)

     !-------------------------------------------------------------
     ! MAGNETIC FIELD CORRECTION BY PROJECTION
     !-------------------------------------------------------------
     call crct__exec(gf,f,dxf,dyf,dzf, &
                     nerr,nstat,       &
                     Db,bf,flg)

     !-------------------------------------------------------------
     ! DAMPING ARROUND THE INNER BOUNDARY FOR DF
     !-------------------------------------------------------------
     call boundary__dfdmp(dxf,dyf,dzf,neq,f)
     call boundary__df(dxf,dyf,dzf,neq,nerr,nstat)

     !-------------------------------------------------------------
     ! SETTING SOLAR WIND PARAMETERS
     !-------------------------------------------------------------
     if(mod(it+it0,intvl3) == 0)then
        if(idown == mproc_null) call init__set_sw(it+it0)
     endif

     !-------------------------------------------------------------
     ! SAVING DATA
     !-------------------------------------------------------------
     if(mod(it+it0,intvl1) == 0)                               &
          call fio__print(it+it0,neq,nxs,nxe,nys,nye,nzs,nze, &
                          nxs1,nxe1,nys1,nye1,nzs1,nze1,      &
                          nrank,nerr,nstat,                   &
                          f,gf,Db,flg,wrk,bf,                 &
                          drs,px0,py0,pz0,pxm,pym,pzm,        &
                          dir_prt)                             
     if(mod(it+it0,intvl2) == 0)                                        &
          call fio__output(it+it0,itsw0+itsw,neq,nx,ny,nz,              &
                           nxs,nxe,nys,nye,nzs,nze,                     &
                           nxs1,nxe1,nys1,nye1,nzs1,nze1,               &
                           nproc,nrank,                                 &
                           f,dxf,dyf,dzf,Db,flg,                        &
                           delt,gam,Bm,rin,drs,px0,py0,pz0,pxm,pym,pzm, &
                           unmax,unmin,                                 &
                           dir_out,file11)                                   

  enddo loop

  call MPI_FINALIZE(nerr)

end program main


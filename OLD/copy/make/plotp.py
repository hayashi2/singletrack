# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 13:49:29 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 100
ppath = "/work/m_hayashi/RB033/"
fname = "ptl"
iniene = 8
outpath ="/home/m_hayashi/output/RB033/position/"
irank = 55
number = 98
def main():
   t = 1
   while(t <= 414):
      clf()
      energy,x,y=[],[],[]
      num=0
      setfig(t)
      while(num <= 127):
         filename= ppath + fname +str(num).zfill(3) +".dat"
         energy = np.append(energy,readfile(filename,t)[0])
         x = np.append(x,readfile(filename,t)[1])
         y = np.append(y,readfile(filename,t)[2])
         num+=1
      
      enenum = readfile( ppath+fname+str(irank).zfill(3)+".dat",t)[0]
      xnum = readfile( ppath+fname+str(irank).zfill(3)+".dat",t)[1]
      ynum = readfile( ppath+fname+str(irank).zfill(3)+".dat",t)[2]
      scatter(xnum[0,number],ynum[0,number],c="r",cmap=cm.jet,s=500,alpha=0.9,lw=0,vmin=-1.8,vmax=1.8)
      scatter(x,y,c=(energy-iniene),cmap=cm.jet,s=5,alpha=0.9,lw=0,vmin=-1.8,vmax=1.8 )
     
      ax=colorbar()    
      ax.set_label('delta Energy [MeV]',fontsize=15)
     # ax.set_clim(1.40,2.20)
      ax.set_clim(-1.8,1.8)
      draw()
      savefig(outpath+'position%03d.jpeg' %((t-1)/4))
      print 'saved %d'%(t*6+6)
      t+=4
      
def setfig(n):
   figure(figsize=(12,10))  
   wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='k')
   wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='w')    
   fig = gcf()
   fig.gca().add_artist(wedge1)
   fig.gca().add_artist(wedge2)    
   
   plot((-10,10), (0,0 ), color='gray',linestyle='--')
   plot((0,0),(-10,10),'gray',linestyle='--')
   
   xlim(-11,11)
   ylim(-11,11)
   setp(gca().get_xticklabels(), fontsize=20, visible=True)
   setp(gca().get_yticklabels(), fontsize=20, visible=True)
   ylabel("y[Re]",fontsize=25)
   xlabel("x[Re]",fontsize=25)
    #Convert Global-MHD coordinate ->GSM
   xticks([-10,-5,0,5,10],["10","5","0","-5","-10"])
   yticks([-10,-5,0,5,10],["10","5","0","-5","-10"])
   title("time="+str(n*6+6)+"s")
  

def readfile(filename,t):
   ene=[]      
   head = ("head","<i")
   tail = ("tail","<i")
   rtime = ("rtime","<f8")
   nnp = ("np","<i4")
   maxptl =("maxptl","<i4")
   xxp = ("xp","<"+str(ptl)+"f8")
   yyp = ("yp","<"+str(ptl)+"f8")
   zzp = ("zp","<"+str(ptl)+"f8")
   llp = ("lp","<"+str(ptl)+"f8")
   xxeq = ("xeq","<"+str(ptl)+"f8")
   yyeq = ("yeq","<"+str(ptl)+"f8")
   zzeq = ("zeq","<"+str(ptl)+"f8")
   aaeq = ("aeq","<"+str(ptl)+"f8")
   bbeq = ("beq","<"+str(ptl)+"f8")
   p_ppara = ("ppara","<"+str(ptl)+"f8")
   p_pperp = ("pperp","<"+str(ptl)+"f8")
   mmu = ("mu","<"+str(ptl)+"f8")
   bbp = ("bp","<"+str(ptl)+"f8")
   pprebp = ("prebp","<"+str(ptl)+"f8")
   ddb = ("db","<"+str(ptl)+"f8")
   ppredb = ("predb","<"+str(ptl)+"f8")
   nnump = ("nump","<"+str(ptl)+"i4")
   iierr = ("ierr","<"+str(ptl)+"i4")

   dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                  aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                  iierr,tail])
               
   fd = open(filename,"r")
   chunk = np.fromfile(fd, dtype=dt,count=t+1)
   time = chunk[t]["rtime"]
   xp = chunk[t]["xp"].reshape((1,ptl),order="F")
   yp = chunk[t]["yp"].reshape((1,ptl),order="F")
   zp = chunk[t]["zp"].reshape((1,ptl),order="F")
   lp = chunk[t]["lp"].reshape((1,ptl),order="F")
   xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
   yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
   zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
   aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
   beq = chunk[t]["beq"].reshape((1,ptl),order="F")
   ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
   pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
   mu = chunk[t]["mu"].reshape((1,ptl),order="F")
   bp = chunk[t]["bp"].reshape((1,ptl),order="F")
   prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
   db = chunk[t]["db"].reshape((1,ptl),order="F")
   predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
   nump = chunk[t]["nump"].reshape((1,ptl),order="F")
   ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   if (t == 1 and filename==ppath+fname+str(irank).zfill(3)+".dat"):
      print xp[0,29]/6380000
   fd.close()     
   g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
   ene = (m * c**2 * (g - 1) / e)/1000000
   return ene,xp/6380000,yp/6380000
    
main()

# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 13:49:29 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 

def main():
   t = 5
   while(t <= 498):
      clf()
      energy,x,y=[],[],[]
      num=0
      setfig(t)
      while(num <= 127):
         filename= "/work/m_hayashi/RB000/ptl%03i.dat"%num
         energy = np.append(energy,readfile(filename,t)[0])
         x = np.append(x,readfile(filename,t)[1])
         y = np.append(y,readfile(filename,t)[2])
         num+=1
      
      scatter(x,y,c=energy,cmap=cm.jet,s=5,alpha=0.9,lw=0,vmin=1.70,vmax=2.50)
      ax=colorbar(ticks=[1.80,2.00,2.20,2.40])    
      ax.set_label('energy [MeV]',fontsize=15)
      ax.set_clim(1.70,2.50)
      draw()
      savefig('position%04d.jpeg' %(t*5+5))
      print 'saved %d'%(t*5+5)
      t+=6
      
def setfig(n):
   figure(figsize=(12,10))  
   wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='k')
   wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='w')    
   fig = gcf()
   fig.gca().add_artist(wedge1)
   fig.gca().add_artist(wedge2)    
   
   plot((-10,10), (0,0 ), color='gray',linestyle='--')
   plot((0,0),(-10,10),'gray',linestyle='--')
   
   xlim(-11,11)
   ylim(-11,11)
   setp(gca().get_xticklabels(), fontsize=20, visible=True)
   setp(gca().get_yticklabels(), fontsize=20, visible=True)
   ylabel("y[Re]",fontsize=25)
   xlabel("x[Re]",fontsize=25)
   title("time="+str(n*5+5)+"s")
  

def readfile(filename,t):
   ene=[]      
   head = ("head","<i")
   tail = ("tail","<i")
   rtime = ("rtime","<f8")
   nnp = ("np","<i4")
   maxptl =("maxptl","<i4")

   dt = np.dtype([head,rtime,nnp,maxptl,
                  ("xp","<128f8"),("yp","<128f8"),("zp","<128f8"),
                  ("lp","<128f8"),("xeq","<128f8"),("yeq","<128f8"),
                  ("zeq","<128f8"),("aeq","<128f8"),("beq","<128f8"),
                  ("ppara","<128f8"),("pperp","<128f8"),("mu","<128f8"),
                  ("bp","<128f8"),("prebp","<128f8"),("db","<128f8"),
                  ("predb","<128f8"),("nump","<128i4"),("ierr","<128i4"),tail])
    
   fd = open(filename,"r")
   chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
   xp = chunk[t]["xp"].reshape((1,128),order="F")
   yp = chunk[t]["yp"].reshape((1,128),order="F")
   zp = chunk[t]["zp"].reshape((1,128),order="F")
   lp = chunk[t]["lp"].reshape((1,128),order="F")
   xeq = chunk[t]["xeq"].reshape((1,128),order="F")
   yeq = chunk[t]["yeq"].reshape((1,128),order="F")
   zeq = chunk[t]["zeq"].reshape((1,128),order="F")
   aeq = chunk[t]["aeq"].reshape((1,128),order="F")
   beq = chunk[t]["beq"].reshape((1,128),order="F")
   ppara = chunk[t]["ppara"].reshape((1,128),order="F")
   pperp = chunk[t]["pperp"].reshape((1,128),order="F")
   mu = chunk[t]["mu"].reshape((1,128),order="F")
   bp = chunk[t]["bp"].reshape((1,128),order="F")
   prebp = chunk[t]["prebp"].reshape((1,128),order="F")
   db = chunk[t]["db"].reshape((1,128),order="F")
   predb = chunk[t]["predb"].reshape((1,128),order="F") 
   nump = chunk[t]["nump"].reshape((1,128),order="F")
   ierr = chunk[t]["ierr"].reshape((1,128),order="F")
   
   fd.close()     
   g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
   ene = (m * c**2 * (g - 1) / e)/1000000
   return ene,xp/6380000,yp/6380000
    
main()

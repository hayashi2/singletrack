module boundary

  implicit none

  private

  public :: boundary__init, boundary__f, boundary__df, boundary__bf, boundary__phi, boundary__divva
  public :: boundary__fdmp, boundary__dfdmp

  integer, save      :: nxs, nxe, nys, nye, nzs, nze
  integer, save      :: nxs1, nxe1, nys1, nye1, nzs1, nze1
  integer, save      :: nxeh, nyeh, nzeh, nxeh1, nyeh1, nzeh1
  integer, save      :: ncomw, mproc_null, mnpr
  integer, save      :: iup, idown, jup, jdown, kup, kdown
  integer, save      :: isize, jsize, ksize
  real(8), save      :: rin, drs, px0, py0, pz0


contains


  subroutine boundary__init(nxsin,nxein,nysin,nyein,nzsin,nzein,          &
                            nxs1in,nxe1in,nys1in,nye1in,nzs1in,nze1in,    &
                            nxehin,nyehin,nzehin,nxeh1in,nyeh1in,nzeh1in, &
                            ncomwin,mproc_nullin,mnprin,                  &
                            iupin,idownin,jupin,jdownin,kupin,kdownin,    &
                            rinin,drsin,px0in,py0in,pz0in)
    integer, intent(in) :: nxsin, nxein, nysin, nyein, nzsin, nzein
    integer, intent(in) :: nxs1in, nxe1in, nys1in, nye1in, nzs1in, nze1in
    integer, intent(in) :: nxehin, nyehin, nzehin, nxeh1in, nyeh1in, nzeh1in
    integer, intent(in) :: ncomwin, mproc_nullin, mnprin
    integer, intent(in) :: iupin, idownin, jupin, jdownin, kupin, kdownin
    real(8), intent(in) :: rinin, drsin, px0in, py0in, pz0in

    nxs   = nxsin
    nxe   = nxein
    nys   = nysin
    nye   = nyein
    nzs   = nzsin
    nze   = nzein
    isize = nxe-nxs+1
    jsize = nye-nys+1
    ksize = nze-nzs+1
    nxs1  = nxs1in
    nxe1  = nxe1in
    nys1  = nys1in
    nye1  = nye1in
    nzs1  = nzs1in
    nze1  = nze1in
    nxeh  = nxehin
    nyeh  = nyehin
    nzeh  = nzehin
    nxeh1 = nxeh1in
    nyeh1 = nyeh1in
    nzeh1 = nzeh1in
    ncomw = ncomwin
    mproc_null = mproc_nullin
    iup   = iupin
    idown = idownin
    jup   = jupin
    jdown = jdownin
    kup   = kupin
    kdown = kdownin
    mnpr  = mnprin
    rin   = rinin
    drs   = drsin
    px0   = px0in
    py0   = py0in
    pz0   = pz0in
    
  end subroutine boundary__init


  subroutine boundary__f(f,neq,nerr,nstat)
                         

    real(8), intent(inout) :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    integer, intent(in)    :: neq
    integer, intent(inout) :: nerr, nstat(:)
    integer                :: i, j, k, ieq, zeq, dir

    !Mirror boundary conditions at y=ny, z=0. Other sides are free boundary conditions.

    dir = -1
    call boundary__mpi(f,dir,neq,nerr,nstat)

    dir = +1
    call boundary__mpi(f,dir,neq,nerr,nstat)

    if(iup == mproc_null)then
!$OMP PARALLEL PRIVATE(ieq,zeq,j,k)
       do ieq=1,neq
          select case(ieq)
          case(1,2,5,6,7,8)
!$OMP DO
             do k=nzs,nze
             do j=nys,nye
                f(ieq,nxe1,j,k) = f(ieq,nxe,j,k)
             enddo
             enddo
!$OMP END DO

          case(3,4)
             zeq = -ieq+7
!$OMP DO
             do k=nzs,nze
             do j=nys,nye
                f(ieq,nxeh1,j,k) = 0.5*max(0.0D0,                            &
                                           f(ieq,nxeh,j,k)+f(zeq,nxeh,j,k) ) &
                                  +0.5*(f(ieq,nxeh,j,k)-f(zeq,nxeh,j,k))
             enddo
             enddo
!$OMP END DO
          end select
       enddo
!$OMP END PARALLEL
    endif

    dir = -2
    call boundary__mpi(f,dir,neq,nerr,nstat)

    if(jdown == mproc_null)then
!$OMP PARALLEL DO PRIVATE(ieq,i,k)
       do k=nzs,nze
       do i=nxs1,nxe1
          do ieq=1,neq
             f(ieq,i,nys1,k) = f(ieq,i,nys,k)
          enddo
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = +2
    call boundary__mpi(f,dir,neq,nerr,nstat)

    if(jup == mproc_null)then
!$OMP PARALLEL PRIVATE(ieq,i,k)
       do ieq=1,neq
          select case(ieq)
          case(1,2,3,4,7,8)
!$OMP DO
             do k=nzs,nze
             do i=nxs1,nxe1
                f(ieq,i,nye1,k) = f(ieq,i,nye-1,k)
             enddo
             enddo
!$OMP END DO

          case(5,6)
!$OMP DO
             do k=nzs,nze
             do i=nxs1,nxe1
                f(ieq,i,nyeh1,k) = -f(ieq,i,nyeh,k)
             enddo
             enddo
!$OMP END DO
          end select
       enddo
!$OMP END PARALLEL
    endif

    dir = -3
    call boundary__mpi(f,dir,neq,nerr,nstat)

    if(kdown == mproc_null)then
!$OMP PARALLEL PRIVATE(ieq,zeq,i,j)

       !At equatorial plane
!$OMP DO
       do j=nys1,nye1
       do i=nxs1,nxe1
          f(3,i,j,nzs) = 0.5*(+f(3,i,j,nzs)+f(4,i,j,nzs))
          f(4,i,j,nzs) = f(3,i,j,nzs)
          f(5,i,j,nzs) = 0.5*(+f(5,i,j,nzs)+f(6,i,j,nzs))
          f(6,i,j,nzs) = f(5,i,j,nzs)
       enddo
       enddo
!$OMP END DO

       do ieq=1,neq
          select case(ieq)
          case(1,2)
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                f(ieq,i,j,nzs1) = f(ieq,i,j,nzs+1) 
             enddo
             enddo
!$OMP END DO

          case(3,4)
             zeq = -ieq+7
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                f(ieq,i,j,nzs1) = f(zeq,i,j,nzs+1)
             enddo
             enddo
!$OMP END DO

          case(5,6)
             zeq = -ieq+11
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                f(ieq,i,j,nzs1) = f(zeq,i,j,nzs+1)
             enddo
             enddo
!$OMP END DO
             
          case(7,8)
             zeq = -ieq+15
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                f(ieq,i,j,nzs1) = -f(zeq,i,j,nzs)
             enddo
             enddo
!$OMP END DO
          end select

       enddo

!$OMP END PARALLEL
    endif

    dir = +3
    call boundary__mpi(f,dir,neq,nerr,nstat)

    if(kup == mproc_null)then
!$OMP PARALLEL PRIVATE(ieq,i,j)
       do ieq=1,neq
          select case(ieq)
          case(1,2,3,4,5,6)
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                f(ieq,i,j,nze1) = f(ieq,i,j,nze)
             enddo
             enddo
!$OMP END DO

          case(7,8)
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                f(ieq,i,j,nzeh1) = f(ieq,i,j,nzeh)
             enddo
             enddo
!$OMP END DO
          end select
       enddo
!$OMP END PARALLEL
    endif

  end subroutine boundary__f


  subroutine boundary__df(dxf,dyf,dzf,neq,nerr,nstat)


    real(8), intent(inout) :: dxf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: dyf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: dzf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    integer, intent(in)    :: neq
    integer, intent(inout) :: nerr, nstat(:)
    integer                :: i, j, k, ieq, zeq, dir

    !Mirror boundary conditions at y=ny, z=0. Other sides are free boundary conditions.

    dir = -1
    call boundary__mpi(dxf,dir,neq,nerr,nstat)
    call boundary__mpi(dyf,dir,neq,nerr,nstat)
    call boundary__mpi(dzf,dir,neq,nerr,nstat)

    dir = +1
    call boundary__mpi(dxf,dir,neq,nerr,nstat)
    call boundary__mpi(dyf,dir,neq,nerr,nstat)
    call boundary__mpi(dzf,dir,neq,nerr,nstat)

    if(iup == mproc_null)then
!$OMP PARALLEL PRIVATE(ieq,j,k)
       do ieq=1,neq
          select case(ieq)
          case(1,2,5,6,7,8)
!$OMP DO
             do k=nzs,nze
             do j=nys,nye
                dxf(ieq,nxe1,j,k) = 0.0D0
                dyf(ieq,nxe1,j,k) = +dyf(ieq,nxe,j,k)
                dzf(ieq,nxe1,j,k) = +dzf(ieq,nxe,j,k)
             enddo
             enddo
!$OMP END DO

          case(3,4)
!$OMP DO
             do k=nzs,nze
             do j=nys,nye
                dxf(ieq,nxeh1,j,k) = 0.0D0
                dyf(ieq,nxeh1,j,k) = +dyf(ieq,nxeh,j,k)
                dzf(ieq,nxeh1,j,k) = +dzf(ieq,nxeh,j,k)
             enddo
             enddo
!$OMP END DO
          end select
       enddo
!$OMP END PARALLEL
    endif

    dir = -2
    call boundary__mpi(dxf,dir,neq,nerr,nstat)
    call boundary__mpi(dyf,dir,neq,nerr,nstat)
    call boundary__mpi(dzf,dir,neq,nerr,nstat)

    if(jdown == mproc_null)then
!$OMP PARALLEL DO PRIVATE(ieq,i,k)
       do k=nzs,nze
       do i=nxs1,nxe1
          do ieq=1,neq
             dxf(ieq,i,nys1,k) = +dxf(ieq,i,nys,k)
             dyf(ieq,i,nys1,k) = 0.0D0
             dzf(ieq,i,nys1,k) = +dzf(ieq,i,nys,k)
          enddo
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = +2
    call boundary__mpi(dxf,dir,neq,nerr,nstat)
    call boundary__mpi(dyf,dir,neq,nerr,nstat)
    call boundary__mpi(dzf,dir,neq,nerr,nstat)

    if(jup == mproc_null)then
!$OMP PARALLEL PRIVATE(ieq,i,k)
       do ieq=1,neq
          select case(ieq)
          case(1,2,3,4,7,8)
!$OMP DO
             do k=nzs,nze
             do i=nxs1,nxe1
                dxf(ieq,i,nye1,k) = +dxf(ieq,i,nye-1,k)
                dyf(ieq,i,nye1,k) = -dyf(ieq,i,nye-1,k)
                dzf(ieq,i,nye1,k) = +dzf(ieq,i,nye-1,k)
             enddo
             enddo
!$OMP END DO

          case(5,6)
!$OMP DO
             do k=nzs,nze
             do i=nxs1,nxe1
                dxf(ieq,i,nyeh1,k) = -dxf(ieq,i,nyeh,k)
                dyf(ieq,i,nyeh1,k) = +dyf(ieq,i,nyeh,k)
                dzf(ieq,i,nyeh1,k) = -dzf(ieq,i,nyeh,k)
             enddo
             enddo
!$OMP END DO
          end select
       enddo
!$OMP END PARALLEL
    endif

    dir = -3
    call boundary__mpi(dxf,dir,neq,nerr,nstat)
    call boundary__mpi(dyf,dir,neq,nerr,nstat)
    call boundary__mpi(dzf,dir,neq,nerr,nstat)

    if(kdown == mproc_null)then
!$OMP PARALLEL PRIVATE(ieq,zeq,i,j)

       !At equatorial plane
!$OMP DO
       do j=nys1,nye1
       do i=nxs1,nxe1
          dxf(3,i,j,nzs) = +0.5*(+dxf(3,i,j,nzs)+dxf(4,i,j,nzs))
          dyf(3,i,j,nzs) = +0.5*(+dyf(3,i,j,nzs)+dyf(4,i,j,nzs))
          dzf(3,i,j,nzs) = +0.5*(+dzf(3,i,j,nzs)-dzf(4,i,j,nzs))
          dxf(4,i,j,nzs) = +dxf(3,i,j,nzs)
          dyf(4,i,j,nzs) = +dyf(3,i,j,nzs)
          dzf(4,i,j,nzs) = -dzf(3,i,j,nzs)
          dxf(5,i,j,nzs) = +0.5*(+dxf(5,i,j,nzs)+dxf(6,i,j,nzs))
          dyf(5,i,j,nzs) = +0.5*(+dyf(5,i,j,nzs)+dyf(6,i,j,nzs))
          dzf(5,i,j,nzs) = +0.5*(+dzf(5,i,j,nzs)-dzf(6,i,j,nzs))
          dxf(6,i,j,nzs) = +dxf(5,i,j,nzs)
          dyf(6,i,j,nzs) = +dyf(5,i,j,nzs)
          dzf(6,i,j,nzs) = -dzf(5,i,j,nzs)
       enddo
       enddo
!$OMP END DO

       do ieq=1,neq
          select case(ieq)
          case(1,2)
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                dxf(ieq,i,j,nzs1) = +dxf(ieq,i,j,nzs+1)
                dyf(ieq,i,j,nzs1) = +dyf(ieq,i,j,nzs+1)
                dzf(ieq,i,j,nzs1) = -dzf(ieq,i,j,nzs+1)
             enddo
             enddo
!$OMP END DO

          case(3,4)
             zeq = -ieq+7
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                dxf(ieq,i,j,nzs1) = +dxf(zeq,i,j,nzs+1)
                dyf(ieq,i,j,nzs1) = +dyf(zeq,i,j,nzs+1)
                dzf(ieq,i,j,nzs1) = -dzf(zeq,i,j,nzs+1)
             enddo
             enddo
!$OMP END DO
             
          case(5,6)
             zeq = -ieq+11
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                dxf(ieq,i,j,nzs1) = +dxf(zeq,i,j,nzs+1)
                dyf(ieq,i,j,nzs1) = +dyf(zeq,i,j,nzs+1)
                dzf(ieq,i,j,nzs1) = -dzf(zeq,i,j,nzs+1)
             enddo
             enddo
!$OMP END DO

          case(7,8)
             zeq = -ieq+15
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                dxf(ieq,i,j,nzs1) = -dxf(zeq,i,j,nzs) 
                dyf(ieq,i,j,nzs1) = -dyf(zeq,i,j,nzs) 
                dzf(ieq,i,j,nzs1) = +dzf(zeq,i,j,nzs)
             enddo
             enddo
!$OMP END DO
          end select
       enddo
!$OMP END PARALLEL
    endif

    dir = +3
    call boundary__mpi(dxf,dir,neq,nerr,nstat)
    call boundary__mpi(dyf,dir,neq,nerr,nstat)
    call boundary__mpi(dzf,dir,neq,nerr,nstat)

    if(kup == mproc_null)then
!$OMP PARALLEL PRIVATE(i,j,ieq)
       do ieq=1,neq
          select case(ieq)
          case(1,2,3,4,5,6)
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                dxf(ieq,i,j,nze1) = +dxf(ieq,i,j,nze)
                dyf(ieq,i,j,nze1) = +dyf(ieq,i,j,nze)
                dzf(ieq,i,j,nze1) = 0.0D0
             enddo
             enddo
!$OMP END DO
            
          case(7,8)
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                dxf(ieq,i,j,nzeh1) = +dxf(ieq,i,j,nzeh)
                dyf(ieq,i,j,nzeh1) = +dyf(ieq,i,j,nzeh)
                dzf(ieq,i,j,nzeh1) = 0.0D0
             enddo
             enddo
!$OMP END DO
          end select
       enddo
!$OMP END PARALLEL
    endif

  end subroutine boundary__df


  subroutine boundary__bf(f,neq,nerr,nstat)
                         

    real(8), intent(inout) :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    integer, intent(in)    :: neq
    integer, intent(inout) :: nerr, nstat(:)
    integer                :: i, j, k, ieq, dir

    !Mirror boundary conditions at y=ny, z=0. Other sides are free boundary conditions.

    dir = -1
    call boundary__mpi(f,dir,neq,nerr,nstat)

    dir = +1
    call boundary__mpi(f,dir,neq,nerr,nstat)

    if(iup == mproc_null)then
!$OMP PARALLEL PRIVATE(ieq,j,k)
       do ieq=1,neq
          select case(ieq)
          case(1)
!$OMP DO
             do k=nzs,nze
             do j=nys,nye
                f(ieq,nxeh1,j,k) = f(ieq,nxeh,j,k)
             enddo
             enddo
!$OMP END DO

          case(2,3)
!$OMP DO
             do k=nzs,nze
             do j=nys,nye
                f(ieq,nxe1,j,k) = f(ieq,nxe,j,k)
             enddo
             enddo
!$OMP END DO
          end select
       enddo
!$OMP END PARALLEL
    endif

    dir = -2
    call boundary__mpi(f,dir,neq,nerr,nstat)

    if(jdown == mproc_null)then
!$OMP PARALLEL DO PRIVATE(ieq,i,k)
       do k=nzs,nze
       do i=nxs1,nxe1
          do ieq=1,neq
             f(ieq,i,nys1,k) = f(ieq,i,nys,k)
          enddo
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = +2
    call boundary__mpi(f,dir,neq,nerr,nstat)

    if(jup == mproc_null)then
!$OMP PARALLEL PRIVATE(ieq,i,k)
       do ieq=1,neq
          select case(ieq)
          case(1,3)
!$OMP DO
             do k=nzs,nze
             do i=nxs1,nxe1
                f(ieq,i,nye1,k) = f(ieq,i,nye-1,k)
             enddo
             enddo
!$OMP END DO

          case(2)
!$OMP DO
             do k=nzs,nze
             do i=nxs1,nxe1
                f(ieq,i,nyeh1,k) = -f(ieq,i,nyeh,k)
             enddo
             enddo
!$OMP END DO
          end select
       enddo
!$OMP END PARALLEL
    endif

    dir = -3
    call boundary__mpi(f,dir,neq,nerr,nstat)

    if(kdown == mproc_null)then
!$OMP PARALLEL PRIVATE(ieq,i,j)
       do ieq=1,neq
          select case(ieq)
          case(1,2)
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                f(ieq,i,j,nzs) = 0.0D0
                f(ieq,i,j,nzs1) = -f(ieq,i,j,nzs+1)
             enddo
             enddo
!$OMP END DO
             
          case(3)
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                f(ieq,i,j,nzs1) = f(ieq,i,j,nzs)
             enddo
             enddo
!$OMP END DO
          end select
       enddo
!$OMP END PARALLEL
    endif

    dir = +3
    call boundary__mpi(f,dir,neq,nerr,nstat)

    if(kup == mproc_null)then
!$OMP PARALLEL PRIVATE(ieq,i,j)
       do ieq=1,neq
          select case(ieq)
          case(1,2)
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                f(ieq,i,j,nze1) = f(ieq,i,j,nze)
             enddo
             enddo
!$OMP END DO

          case(3)
!$OMP DO
             do j=nys1,nye1
             do i=nxs1,nxe1
                f(ieq,i,j,nzeh1) = f(ieq,i,j,nzeh)
             enddo
             enddo
!$OMP END DO
          end select
       enddo
!$OMP END PARALLEL
    endif

  end subroutine boundary__bf


  subroutine boundary__phi(f,nerr,nstat)
                         

    real(8), intent(inout) :: f(nxs1:nxe1,nys1:nye1,nzs1:nze1)
    integer, intent(inout) :: nerr, nstat(:)
    integer                :: i, j, k, dir

    !Mirror boundary conditions at y=ny, z=0. Other sides are free boundary conditions.

    dir = -1
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(idown == mproc_null)then
!$OMP PARALLEL DO PRIVATE(j,k)
       do k=nzs,nze
       do j=nys,nye
          f(nxs1,j,k) = f(nxs,j,k)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = +1
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(iup == mproc_null)then
!$OMP PARALLEL DO PRIVATE(j,k)
       do k=nzs,nze
       do j=nys,nye
          f(nxe1,j,k) = 2.*f(nxe,j,k)-f(nxe-1,j,k)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = -2
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(jdown == mproc_null)then
!$OMP PARALLEL DO PRIVATE(i,k)
       do k=nzs,nze
       do i=nxs1,nxe1
          f(i,nys1,k) = 2.*f(i,nys,k)-f(i,nys+1,k)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = +2
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(jup == mproc_null)then
!$OMP PARALLEL DO PRIVATE(i,k)
       do k=nzs,nze
       do i=nxs1,nxe1
          f(i,nye1,k) = f(i,nye-1,k)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = -3
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(kdown == mproc_null)then
!$OMP PARALLEL DO PRIVATE(i,j)
       do j=nys1,nye1
       do i=nxs1,nxe1
          f(i,j,nzs1) = -f(i,j,nzs+1)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = +3
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(kup == mproc_null)then
!$OMP PARALLEL DO PRIVATE(i,j)
       do j=nys1,nye1
       do i=nxs1,nxe1
          f(i,j,nze1) = 2.*f(i,j,nze)-f(i,j,nze-1)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

  end subroutine boundary__phi


  subroutine boundary__divva(f,nerr,nstat)
                         

    real(8), intent(inout) :: f(nxs1:nxe1,nys1:nye1,nzs1:nze1)
    integer, intent(inout) :: nerr, nstat(:)
    integer                :: i, j, k, dir

    !Mirror boundary conditions at y=ny, z=0. Other sides are free boundary conditions.

    dir = -1
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(idown == mproc_null)then
!$OMP PARALLEL DO PRIVATE(j,k)
       do k=nzs,nze
       do j=nys,nye
          f(nxs1,j,k) = f(nxs,j,k)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = +1
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(iup == mproc_null)then
!$OMP PARALLEL DO PRIVATE(j,k)
       do k=nzs,nze
       do j=nys,nye
          f(nxe1,j,k) = f(nxe,j,k)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = -2
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(jdown == mproc_null)then
!$OMP PARALLEL DO PRIVATE(i,k)
       do k=nzs,nze
       do i=nxs1,nxe1
          f(i,nys1,k) = f(i,nys,k)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = +2
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(jup == mproc_null)then
!$OMP PARALLEL DO PRIVATE(i,k)
       do k=nzs,nze
       do i=nxs1,nxe1
          f(i,nye1,k) = f(i,nye,k)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = -3
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(kdown == mproc_null)then
!$OMP PARALLEL DO PRIVATE(i,j)
       do j=nys1,nye1
       do i=nxs1,nxe1
          f(i,j,nzs1) = f(i,j,nzs)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

    dir = +3
    call boundary__mpi(f,dir,1,nerr,nstat)

    if(kup == mproc_null)then
!$OMP PARALLEL DO PRIVATE(i,j)
       do j=nys1,nye1
       do i=nxs1,nxe1
          f(i,j,nze1) = f(i,j,nze)
       enddo
       enddo
!$OMP END PARALLEL DO
    endif

  end subroutine boundary__divva


  subroutine boundary__fdmp(f,neq,wrk,flg)

    real(8), intent(inout) :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    integer, intent(in)    :: neq
    real(8), intent(out)   :: wrk(3,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: flg(neq,nxs:nxe,nys:nye,nzs:nze)
    real(8), parameter     :: fac=1.D0/12.D0, rout=1.0D0, dr=1.0D0, ddr=1./dr
    integer                :: i, j, k, ip, im, jp, jm, kp, km
    real(8)                :: r0, x0, y0, z0, fac2
    
!$OMP PARALLEL PRIVATE(i,j,k)

    !z+- -> v, va
!$OMP DO
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       f(3,i,j,k) = 0.5*(+f(3,i,j,k)+f(4,i,j,k))
       f(4,i,j,k) = +f(3,i,j,k)-f(4,i,j,k)

       f(5,i,j,k) = 0.5*(+f(5,i,j,k)+f(6,i,j,k))
       f(6,i,j,k) = +f(5,i,j,k)-f(6,i,j,k)

       f(7,i,j,k) = 0.5*(+f(7,i,j,k)+f(8,i,j,k))
       f(8,i,j,k) = +f(7,i,j,k)-f(8,i,j,k)
    enddo
    enddo
    enddo
!$OMP END DO
    !smoothing area: r0<r<r0+dr (RE) for f
!$OMP DO PRIVATE(ip,im,jp,jm,kp,km,r0,x0,y0,z0,fac2)
    do k=nzs,nze
       km = k-1
       kp = k+1
    do j=nys,nye
       jm = j-1
       jp = j+1
    do i=nxs,nxe
       im = i-1
       ip = i+1

       x0 = (i+0.5-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       fac2 = sqrt(0.5*(1.-tanh((r0-(rin+rout))*ddr)))
       wrk(1,i,j,k) = (+f(3,i,j,km)+f(3,i,jm,k)           &
                       +f(3,im,j,k)+f(3,ip,j,k)           &
                       +f(3,i,jp,k)+f(3,i,j,kp))*fac*fac2 &
                     +6.*f(3,i,j,k)*fac*(2.-fac2)

       x0 = (i-px0)*drs
       y0 = (j+0.5-py0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       fac2 = sqrt(0.5*(1.-tanh((r0-(rin+rout))*ddr)))
       wrk(2,i,j,k) = (+f(5,i,j,km)+f(5,i,jm,k)           &
                       +f(5,im,j,k)+f(5,ip,j,k)           &
                       +f(5,i,jp,k)+f(5,i,j,kp))*fac*fac2 &
                     +6.*f(5,i,j,k)*fac*(2.-fac2)

       y0 = (j-py0)*drs
       z0 = (k+0.5-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       fac2 = sqrt(0.5*(1.-tanh((r0-(rin+rout))*ddr)))
       wrk(3,i,j,k) = (+f(7,i,j,km)+f(7,i,jm,k)           &
                       +f(7,im,j,k)+f(7,ip,j,k)           &
                       +f(7,i,jp,k)+f(7,i,j,kp))*fac*fac2 &
                     +6.*f(7,i,j,k)*fac*(2.-fac2)
    enddo
    enddo
    enddo
!$OMP END DO

!$OMP DO
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe
       f(3,i,j,k) = wrk(1,i,j,k)*flg(3,i,j,k)
       f(5,i,j,k) = wrk(2,i,j,k)*flg(5,i,j,k)
       f(7,i,j,k) = wrk(3,i,j,k)*flg(7,i,j,k)
    enddo
    enddo
    enddo
!$OMP END DO

    !v, va -> z+-
!$OMP DO
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       f(3,i,j,k) = +f(3,i,j,k)+f(4,i,j,k)
       f(4,i,j,k) = +f(3,i,j,k)-2.*f(4,i,j,k)

       f(5,i,j,k) = +f(5,i,j,k)+f(6,i,j,k)
       f(6,i,j,k) = +f(5,i,j,k)-2.*f(6,i,j,k)

       f(7,i,j,k) = +f(7,i,j,k)+f(8,i,j,k)
       f(8,i,j,k) = +f(7,i,j,k)-2.*f(8,i,j,k)
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP END PARALLEL 

  end subroutine boundary__fdmp


  subroutine boundary__dfdmp(dxf,dyf,dzf,neq,f)
                             
    real(8), intent(inout) :: dxf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: dyf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: dzf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    integer, intent(in)    :: neq
    real(8), intent(in)    :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), parameter     :: rout=1.0D0, dr=1.0D0, ddr=1./dr, dflim=10.0
    integer                :: ieq, i, j, k, ip, im, jp, jm, kp, km
    real(8)                :: r0, x0, y0, z0, fac2, fac2i, fac2j, fac2k
    real(8)                :: dxf1, dyf1, dzf1

!$OMP PARALLEL

!$OMP DO PRIVATE(i,j,k,ip,im,jp,jm,kp,km,fac2,fac2i,fac2j,fac2k,r0,x0,y0,z0)
    do k=nzs,nze
       km = k-1
       kp = k+1
    do j=nys,nye
       jm = j-1
       jp = j+1
    do i=nxs,nxe
       im = i-1
       ip = i+1

       !(i+1/2,j,k)
       x0 = (i+0.5-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       fac2i = sqrt(0.5*(1.+tanh((r0-(rin+rout))*ddr)))

       !(i,j+1/2,k)
       x0 = (i-px0)*drs
       y0 = (j+0.5-py0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       fac2j = sqrt(0.5*(1.+tanh((r0-(rin+rout))*ddr)))

       !(i,j,k+1/2)
       y0 = (j-py0)*drs
       z0 = (k+0.5-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       fac2k = sqrt(0.5*(1.+tanh((r0-(rin+rout))*ddr)))

       dzf(3,i,j,k) = ( dzf(3,i,j,k)*fac2i                        &
                       +(-f(3,i,j,km)+f(3,i,j,kp))*0.5*(1.-fac2i) &
                      )
       dzf(4,i,j,k) = ( dzf(4,i,j,k)*fac2i                        &
                       +(-f(4,i,j,km)+f(4,i,j,kp))*0.5*(1.-fac2i) &
                      )
       dzf(5,i,j,k) = ( dzf(5,i,j,k)*fac2j                        &
                       +(-f(5,i,j,km)+f(5,i,j,kp))*0.5*(1.-fac2j) &
                      )
       dzf(6,i,j,k) = ( dzf(6,i,j,k)*fac2j                        &
                       +(-f(6,i,j,km)+f(6,i,j,kp))*0.5*(1.-fac2j) &
                      )
       dzf(7,i,j,k) = ( dzf(7,i,j,k)*fac2k                        &
                       +(-f(7,i,j,km)+f(7,i,j,kp))*0.5*(1.-fac2k) &
                      )
       dzf(8,i,j,k) = ( dzf(8,i,j,k)*fac2k                        &
                       +(-f(8,i,j,km)+f(8,i,j,kp))*0.5*(1.-fac2k) &
                      )

       dyf(3,i,j,k) = ( dyf(3,i,j,k)*fac2i                        &
                       +(-f(3,i,jm,k)+f(3,i,jp,k))*0.5*(1.-fac2i) &
                      )
       dyf(4,i,j,k) = ( dyf(4,i,j,k)*fac2i                        &
                       +(-f(4,i,jm,k)+f(4,i,jp,k))*0.5*(1.-fac2i) &
                      )
       dyf(5,i,j,k) = ( dyf(5,i,j,k)*fac2j                        &
                       +(-f(5,i,jm,k)+f(5,i,jp,k))*0.5*(1.-fac2j) &
                      )
       dyf(6,i,j,k) = ( dyf(6,i,j,k)*fac2j                        &
                       +(-f(6,i,jm,k)+f(6,i,jp,k))*0.5*(1.-fac2j) &
                      )
       dyf(7,i,j,k) = ( dyf(7,i,j,k)*fac2k                        &
                       +(-f(7,i,jm,k)+f(7,i,jp,k))*0.5*(1.-fac2k) &
                      )
       dyf(8,i,j,k) = ( dyf(8,i,j,k)*fac2k                        &
                       +(-f(8,i,jm,k)+f(8,i,jp,k))*0.5*(1.-fac2k) &
                      )

       dxf(3,i,j,k) = ( dxf(3,i,j,k)*fac2i                        &
                       +(-f(3,im,j,k)+f(3,ip,j,k))*0.5*(1.-fac2i) &
                      )
       dxf(4,i,j,k) = ( dxf(4,i,j,k)*fac2i                        &
                       +(-f(4,im,j,k)+f(4,ip,j,k))*0.5*(1.-fac2i) &
                      )
       dxf(5,i,j,k) = ( dxf(5,i,j,k)*fac2j                        &
                       +(-f(5,im,j,k)+f(5,ip,j,k))*0.5*(1.-fac2j) &
                      )
       dxf(6,i,j,k) = ( dxf(6,i,j,k)*fac2j                        &
                       +(-f(6,im,j,k)+f(6,ip,j,k))*0.5*(1.-fac2j) &
                      )
       dxf(7,i,j,k) = ( dxf(7,i,j,k)*fac2k                        &
                       +(-f(7,im,j,k)+f(7,ip,j,k))*0.5*(1.-fac2k) &
                      )
       dxf(8,i,j,k) = ( dxf(8,i,j,k)*fac2k                        &
                       +(-f(8,im,j,k)+f(8,ip,j,k))*0.5*(1.-fac2k) &
                      )
    enddo
    enddo
    enddo
!$OMP END DO

!$OMP DO PRIVATE(ieq,i,j,k,ip,im,jp,jm,kp,km,dxf1,dyf1,dzf1)
    do k=nzs,nze
       km = k-1
       kp = k+1
    do j=nys,nye
       jm = j-1
       jp = j+1
    do i=nxs,nxe
       im = i-1
       ip = i+1
       do ieq=1,neq
          dxf1 = 0.5*(-f(ieq,im,j,k)+f(ieq,ip,j,k))
          dyf1 = 0.5*(-f(ieq,i,jm,k)+f(ieq,i,jp,k))
          dzf1 = 0.5*(-f(ieq,i,j,km)+f(ieq,i,j,kp))

          if(abs(dxf(ieq,i,j,k)) > dflim*abs(dxf1)) &
               dxf(ieq,i,j,k) = dxf1
          if(abs(dyf(ieq,i,j,k)) > dflim*abs(dyf1)) &
               dyf(ieq,i,j,k) = dyf1
          if(abs(dzf(ieq,i,j,k)) > dflim*abs(dzf1)) &
               dzf(ieq,i,j,k) = dzf1
       enddo
    enddo
    enddo
    enddo
!$OMP END DO

!$OMP END PARALLEL

  end subroutine boundary__dfdmp


  subroutine boundary__mpi(f,dir,neq,nerr,nstat)

    real(8), intent(inout) :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    integer, intent(in)    :: dir, neq
    integer, intent(inout) :: nerr, nstat(:)
    integer              :: i, j, k, l, ieq
    real(8), allocatable :: bff_snd_i(:), bff_rcv_i(:)
    real(8), allocatable :: bff_snd_j(:), bff_rcv_j(:)
    real(8), allocatable :: bff_snd_k(:), bff_rcv_k(:)

    select case(dir)

    case(+1,-1)
       allocate(bff_snd_i(neq*jsize*ksize))
       allocate(bff_rcv_i(neq*jsize*ksize))
       
    case(+2,-2)
       allocate(bff_snd_j(neq*(isize+2)*ksize))
       allocate(bff_rcv_j(neq*(isize+2)*ksize))
       
    case(+3,-3)
       allocate(bff_snd_k(neq*(isize+2)*(jsize+2)))
       allocate(bff_rcv_k(neq*(isize+2)*(jsize+2)))

    end select
    
    select case(dir)

    !Mpi Send-RECEIVE in +-x direction
    case(+1)

!$OMP PARALLEL DO PRIVATE(ieq,j,k,l)
       do k=nzs,nze
       do j=nys,nye
          do ieq=1,neq
             l = ieq+(j-nys)*neq+(k-nzs)*jsize*neq
             bff_snd_i(l) = f(ieq,nxs,j,k)
          enddo
       enddo
       enddo
!$OMP END PARALLEL DO
       call MPI_SENDRECV(bff_snd_i(1),neq*jsize*ksize,mnpr,idown,100, &
                         bff_rcv_i(1),neq*jsize*ksize,mnpr,iup  ,100, &
                         ncomw,nstat,nerr)
       if(iup /= mproc_null)then
!$OMP PARALLEL DO PRIVATE(ieq,j,k,l)
          do k=nzs,nze
          do j=nys,nye
             do ieq=1,neq
                l = ieq+(j-nys)*neq+(k-nzs)*jsize*neq
                f(ieq,nxe1,j,k) = bff_rcv_i(l)
             enddo
          enddo
          enddo
!$OMP END PARALLEL DO
       endif

       deallocate(bff_snd_i)
       deallocate(bff_rcv_i)

    case(-1)
!$OMP PARALLEL DO PRIVATE(ieq,j,k,l)
       do k=nzs,nze
       do j=nys,nye
          do ieq=1,neq
             l = ieq+(j-nys)*neq+(k-nzs)*jsize*neq
             bff_snd_i(l) = f(ieq,nxe,j,k)
          enddo
       enddo
       enddo
!$OMP END PARALLEL DO
       call MPI_SENDRECV(bff_snd_i(1),neq*jsize*ksize,mnpr,iup  ,200, &
                         bff_rcv_i(1),neq*jsize*ksize,mnpr,idown,200, &
                         ncomw,nstat,nerr)
       if(idown /= mproc_null)then
!$OMP PARALLEL DO PRIVATE(ieq,j,k,l)
          do k=nzs,nze
          do j=nys,nye
             do ieq=1,neq
                l = ieq+(j-nys)*neq+(k-nzs)*jsize*neq
                f(ieq,nxs1,j,k) = bff_rcv_i(l)
             enddo
          enddo
          enddo
!$OMP END PARALLEL DO
       endif

       deallocate(bff_snd_i)
       deallocate(bff_rcv_i)

    !MPI SEND-RECEIVE in +-y direction
    case(+2)
!$OMP PARALLEL DO PRIVATE(ieq,i,k,l)
       do k=nzs,nze
       do i=nxs1,nxe1
          do ieq=1,neq
             l = ieq+(i-nxs1)*neq+(k-nzs)*(isize+2)*neq
             bff_snd_j(l) = f(ieq,i,nys,k)
          enddo
       enddo
       enddo
!$OMP END PARALLEL DO
       call MPI_SENDRECV(bff_snd_j(1),neq*(isize+2)*ksize,mnpr,jdown,300, &
                         bff_rcv_j(1),neq*(isize+2)*ksize,mnpr,jup  ,300, &
                         ncomw,nstat,nerr)
       if(jup /= mproc_null)then
!$OMP PARALLEL DO PRIVATE(ieq,i,k,l)
          do k=nzs,nze
          do i=nxs1,nxe1
             do ieq=1,neq
                l = ieq+(i-nxs1)*neq+(k-nzs)*(isize+2)*neq
                f(ieq,i,nye1,k) = bff_rcv_j(l)
             enddo
          enddo
          enddo
!$OMP END PARALLEL DO
       endif

       deallocate(bff_snd_j)
       deallocate(bff_rcv_j)

    case(-2)
!$OMP PARALLEL DO PRIVATE(ieq,i,k,l)
       do k=nzs,nze
       do i=nxs1,nxe1
          do ieq=1,neq
             l = ieq+(i-nxs1)*neq+(k-nzs)*(isize+2)*neq
             bff_snd_j(l) = f(ieq,i,nye,k)
          enddo
       enddo
       enddo
!$OMP END PARALLEL DO
       call MPI_SENDRECV(bff_snd_j(1),neq*(isize+2)*ksize,mnpr,jup  ,400, &
                         bff_rcv_j(1),neq*(isize+2)*ksize,mnpr,jdown,400, &
                         ncomw,nstat,nerr)
       if(jdown /= mproc_null)then
!$OMP PARALLEL DO PRIVATE(ieq,i,k,l)
          do k=nzs,nze
          do i=nxs1,nxe1
             do ieq=1,neq
                l = ieq+(i-nxs1)*neq+(k-nzs)*(isize+2)*neq
                f(ieq,i,nys1,k) = bff_rcv_j(l)
             enddo
          enddo
          enddo
!$OMP END PARALLEL DO
       endif

       deallocate(bff_snd_j)
       deallocate(bff_rcv_j)

    !MPI SEND-RECEIVE in +-z direction
    case(+3)
!$OMP PARALLEL DO PRIVATE(ieq,i,j,l)
       do j=nys1,nye1
       do i=nxs1,nxe1
          do ieq=1,neq
             l = ieq+(i-nxs1)*neq+(j-nys1)*(isize+2)*neq
             bff_snd_k(l) = f(ieq,i,j,nzs)
          enddo
       enddo
       enddo
!$OMP END PARALLEL DO
       call MPI_SENDRECV(bff_snd_k(1),neq*(isize+2)*(jsize+2),mnpr,kdown,500, &
                         bff_rcv_k(1),neq*(isize+2)*(jsize+2),mnpr,kup  ,500, &
                         ncomw,nstat,nerr)
       if(kup /= mproc_null)then
!$OMP PARALLEL DO PRIVATE(ieq,i,j,l)
          do j=nys1,nye1
          do i=nxs1,nxe1
             do ieq=1,neq
                l = ieq+(i-nxs1)*neq+(j-nys1)*(isize+2)*neq
                f(ieq,i,j,nze1) = bff_rcv_k(l)
             enddo
          enddo
          enddo
!$OMP END PARALLEL DO
       endif

       deallocate(bff_snd_k)
       deallocate(bff_rcv_k)

    case(-3)
!$OMP PARALLEL DO PRIVATE(ieq,i,j,l)
       do j=nys1,nye1
       do i=nxs1,nxe1
          do ieq=1,neq
             l = ieq+(i-nxs1)*neq+(j-nys1)*(isize+2)*neq
             bff_snd_k(l) = f(ieq,i,j,nze)
          enddo
       enddo
       enddo
!$OMP END PARALLEL DO
       call MPI_SENDRECV(bff_snd_k(1),neq*(isize+2)*(jsize+2),mnpr,kup  ,600, &
                         bff_rcv_k(1),neq*(isize+2)*(jsize+2),mnpr,kdown,600, &
                         ncomw,nstat,nerr)
       if(kdown /= mproc_null)then
!$OMP PARALLEL DO PRIVATE(ieq,i,j,l)
          do j=nys1,nye1
          do i=nxs1,nxe1
             do ieq=1,neq
                l = ieq+(i-nxs1)*neq+(j-nys1)*(isize+2)*neq
                f(ieq,i,j,nzs1) = bff_rcv_k(l)
             enddo
          enddo
          enddo
!$OMP END PARALLEL DO
       endif

       deallocate(bff_snd_k)
       deallocate(bff_rcv_k)

    end select

  end subroutine boundary__mpi


end module boundary

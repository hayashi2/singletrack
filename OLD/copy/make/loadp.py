import numpy as np
from pylab import *

ppath ="/work/m_hayashi/"
patha = "/work/ymatumot/simulation/GM-RB"
pathb = "/run_Vsw600/"
ename = "MHD_E"
dirname="RB033"
savename="MAXposition"


ptl = 100
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
e0 = 2.2
w=[]
t=[]
def main():
    data=np.loadtxt(ppath + dirname +"/"+ savename +".txt")
    vp = ((data[1:414,:]-data[0:413,:])/6)/1000
    Gdata = data/6380000
    #v = np.sqrt(dp**2)
    i=1
    
    while(i <= 400): 
        time =data[i,0] #6s
        xg = nx(Gdata[i,1])
        yg = ny(Gdata[i,2])
        zg = -ny(Gdata[i,3])
        ex , ey = readE((i-1)/2 +1,xg,yg,zg)
        w.append(ex*vp[i,1]+ey*vp[i,2])
        t.append(data[i,0])
        i += 2
    
    #t=np.arange(6,2400,6)
    #v=np.sqrt(vp[:,1]**2 + vp[:,2]**2)
    #plot(t[0:200],v[0:200])
    plot(t[0:100],w[0:100])
    show()
    
def nx(x1):
    return int((-x1)*3 + 90)

def ny(y1):
    return int((-y1)*3 + 121)

def readE(time,x,y,z):
    
    efilename = patha +pathb + ename + str(time).zfill(3) + ".dat"
    
    head = ("head","<i")
    tail = ("tail","<i")
    e = ("e","<51750171f4")
    
    dt = np.dtype([head,e,tail])
    fd = open(efilename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    ef = chunk[0]["e"].reshape((297,241,241,3),order="F")*e0
    #convert GSM
    ex = -ef[x,y,z,0]
    ey = -ef[x,y,z,1]
    
    print "read... E"+str(time)
        
    return ex,ey
main()

!######################################################################
module itp_emf
!######################################################################
  use prm, only: re
  use inp, only: nx, ny, nz, nxc, nyc, nzc, dsec, dx_re, dy_re, dz_re

  implicit none
  private
  public itp_emf__exe

contains

!######################################################################
  subroutine itp_emf__exe(rtime, emf0, emf1, &
                        & xp, yp, zp, bx, by, bz, ex, ey, ez, ierr)

    real(8), intent(in) :: rtime, emf0(:,:,:,:), emf1(:,:,:,:)
    real(8), intent(in) :: xp, yp, zp
    real(8), intent(out):: bx, by, bz, ex, ey, ez
    integer, intent(inout):: ierr

    real(8) :: x0(6,2), x1(6,2), x2(6,2), x3(6,2), y0(6,2), y1(6,2), z0(6,2)
    real(8) :: deltax, deltay, deltaz, deltat, r, dxinv, dyinv, dzinv
    integer :: ix, iy, iz, i, iem, n, nxhlf, nyhlf, nzhlf
    double precision :: dx, dy, dz

    dx = dx_re * re
    dy = dy_re * re
    dz = dz_re * re


    dxinv = 1d0/dx
    dyinv = 1d0/dy
    dzinv = 1d0/dz
    nxhlf = nxc
    nyhlf = nyc
    nzhlf = nzc


    r = dsqrt(xp**2 + yp**2 + zp**2)

    deltat = dmod(rtime, dsec) / dsec

    ix = dint(xp * dxinv + nxhlf )
    iy = dint(yp * dyinv + nyhlf )
    iz = dint(zp * dzinv + nzhlf )

    deltax = xp * dxinv + nxhlf - ix
    deltay = yp * dyinv + nyhlf - iy
    deltaz = zp * dzinv + nzhlf - iz




    if ((ix < 1).or.(ix >= nx).or. &
     &  (iy < 1).or.(iy >= ny).or. &
     &  (iz < 1).or.(iz >= nz).or.(ierr < 0)) then
       bx = 1d-6
       by = 1d-6
       bz = 1d-6
       ex = 0d0
       ey = 0d0
       ez = 0d0
       ierr = -1
       return
    end if



    do iem=1, 6
      x0(iem,1)=emf0(iem,ix,iy,iz) &
              &+ deltax*(emf0(iem,ix+1,iy,iz) - emf0(iem,ix,iy,iz))
      x1(iem,1)=emf0(iem,ix,iy+1,iz) &
              &+ deltax*(emf0(iem,ix+1,iy+1,iz) - emf0(iem,ix,iy+1,iz))
      x2(iem,1)=emf0(iem,ix,iy,iz+1) &
              &+ deltax*(emf0(iem,ix+1,iy,iz+1) - emf0(iem,ix,iy,iz+1))
      x3(iem,1)=emf0(iem,ix,iy+1,iz+1) &
              &+ deltax*(emf0(iem,ix+1,iy+1,iz+1)-emf0(iem,ix,iy+1,iz+1))
    end do


    do iem=1, 6
      y0(iem,1) = x0(iem,1) + deltay * (x1(iem,1) - x0(iem,1))
      y1(iem,1) = x2(iem,1) + deltay * (x3(iem,1) - x2(iem,1))
    end do
    do iem=1, 6
      z0(iem,1) = y0(iem,1) + deltaz * (y1(iem,1) - y0(iem,1))
    end do


    do iem=1, 6
      x0(iem,2)=emf1(iem,ix,iy,iz) &
              &+ deltax*(emf1(iem,ix+1,iy,iz) - emf1(iem,ix,iy,iz))
      x1(iem,2)=emf1(iem,ix,iy+1,iz) &
              &+ deltax*(emf1(iem,ix+1,iy+1,iz) - emf1(iem,ix,iy+1,iz))
      x2(iem,2)=emf1(iem,ix,iy,iz+1) &
              &+ deltax*(emf1(iem,ix+1,iy,iz+1) - emf1(iem,ix,iy,iz+1))
      x3(iem,2)=emf1(iem,ix,iy+1,iz+1) &
              &+ deltax*(emf1(iem,ix+1,iy+1,iz+1)-emf1(iem,ix,iy+1,iz+1))
    end do


    do iem=1, 6
      y0(iem,2) = x0(iem,2) + deltay * (x1(iem,2) - x0(iem,2))
      y1(iem,2) = x2(iem,2) + deltay * (x3(iem,2) - x2(iem,2))
    end do
    do iem=1, 6
      z0(iem,2) = y0(iem,2) + deltaz * (y1(iem,2) - y0(iem,2))
    end do


    bx = z0(1,1) + (z0(1,2) - z0(1,1)) * deltat
    by = z0(2,1) + (z0(2,2) - z0(2,1)) * deltat
    bz = z0(3,1) + (z0(3,2) - z0(3,1)) * deltat
    ex = z0(4,1) + (z0(4,2) - z0(4,1)) * deltat
    ey = z0(5,1) + (z0(5,2) - z0(5,1)) * deltat
    ez = z0(6,1) + (z0(6,2) - z0(6,1)) * deltat

  end subroutine itp_emf__exe

!######################################################################
end module itp_emf
!######################################################################

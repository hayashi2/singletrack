# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
import matplotlib.patches as mpatches
import matplotlib.colors as col

ptl = 10000
f_num = 144
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
re = 6370e3
e0 = 2.2
B0 = 32000. #nT

RUN = "run3"
pdt = 5

sttime = 220/pdt -1 #run3 0
etime = 360/pdt -1 #run3 0
#sttime = 260/pdt -1 #run5 0
#etime = 495/pdt -1 #run5 0

def main():        
    xedges = np.linspace(0.,24.,49) #MLT
    yedges = np.linspace(0.,10.,41) #ini W
    extents = [xedges[0],xedges[-1],yedges[0],yedges[-1]]
    data = np.load("/work/m_hayashi/hist_"+RUN+"_ver2.npz")
    HIST = data["HIST"]
    print HIST.shape
    HIST = np.nanmean(HIST[:,:,0:etime-sttime],axis=2)
    
    fig = figure(1,figsize=(8,4))
    ax0 = fig.add_subplot(111)
    im0 = ax0.imshow(HIST[:,:-1].T/np.max(HIST),origin="lower",extent=extents,\
                         interpolation="none",aspect="auto",\
                         vmin = 0,vmax = 1)
    CB0 = colorbar(im0)
    ax0.set_xlabel("MLT")
    #ax0.set_ylabel("$W_{init}$ [MeV]")
    ax0.set_ylabel("$W$ [MeV]")
    CB0.set_label(r"<dW> /<dW>$_{MAX}$ ")
    ax0.set_xticks(np.linspace(0,24,5))
    ax0.set_yticks(np.linspace(0,10,11))
    ax0.grid()
    fig.tight_layout()
    fig.savefig("W_MLT_"+RUN+".eps")
    fig.savefig("W_MLT_"+RUN+".png")
    close(fig)

    fig2 = figure(2)
    ax1 = fig2.add_subplot(111)
    HIST = np.nanmean(HIST[:,:-1],axis=0)
    ax1.plot(np.linspace(0,10,np.size(HIST)),HIST)
    ax1.set_yscale("log")
    fig2.savefig("test.png")
    
def readptl(filename):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt)
    #time = chunk["rtime"]
    #xp = chunk["xp"]
    #yp = chunk["yp"]
    #zp = chunk["zp"]
    #lp = chunk["lp"]
    xeq = chunk["xeq"]
    yeq = chunk["yeq"]
    #zeq = chunk["zeq"]
    aeq = chunk["aeq"]
    beq = chunk["beq"]
    ppara = chunk["ppara"]
    pperp = chunk["pperp"]
    #mu = chunk["mu"]
    #bp = chunk["bp"]
    #prebp = chunk["prebp"]
    #db = chunk["db"]
    #predb = chunk["predb"] 
    #nump = chunk["nump"]
    ierr = chunk["ierr"]
    fd.close()
    
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)       
    ene = (m * c**2 * (g - 1) / e)/1e6
    mlt = np.degrees(np.arctan2(-yeq,-xeq)+np.pi)/15
    return mlt,aeq,beq*1e9,ene,ierr  # =>nT

def readE(filepath,t):
    filename = filepath + str(t).zfill(3)+".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<"+str(gx*gy*gz*3)+"f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    E = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*e0
    #Convert Global-MHD coodinate ->GSM
    return -E[:,:,rz,0],-E[:,:,rz,1] 

main()

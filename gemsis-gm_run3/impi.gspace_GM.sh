#!/bin/sh

#$ -S /bin/sh
#$ -cwd
#$ -V
#$ -q all.q
#$ -pe openmpi36 360

export OMP_NUM_THREADS=6
MPISLOTS=`expr $NSLOTS / $OMP_NUM_THREADS`
mpirun -x OMP_NUM_THREADS=$OMP_NUM_THREADS -npernode 6 -np $MPISLOTS ./gm2.out


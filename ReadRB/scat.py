import numpy as np
from pylab import *
import matplotlib.ticker as tick
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.cm as cm
from scipy.interpolate import spline
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
re = 6370000
mptl = 500
tmax = 1200
ppath = "/work/m_hayashi/RBF/"
fname = "ptl"

def main():
    for time in xrange(0,np.round(tmax/6)):

        px,py,pene = Get_position(time)
        clf()
        fig = figure(figsize=(12,10))
        ax = fig.add_subplot(111)
        ax.scatter(px,py,c=pene,s=1,lw=0,cmap=cm.rainbow,vmin=3,vmax=7)
        
        ax.set_xlabel("$X_{GSM}$ [Re]",fontsize=22)
        ax.set_ylabel("$Y_{GSM}$ [Re]",fontsize=22)
        ax.set_title("t ="+str(time*12)+"sec",fontsize=22)
        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='k')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='w')    
        fig = gcf()
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)

        savefig("scat"+str(time).zfill(3)+".jpeg")
        print "saved ",time*6
        close(fig)


def Get_position(t):
    pene,px,py=[],[],[]
    for f in xrange(0,127):
        filename= ppath + fname +str(f).zfill(3) +".dat"
        ene,x,y = readptl(filename,t)
        pene=np.append(pene,ene)
        px=np.append(px,x)
        py=np.append(py,y)
        
    return px,py,pene



def readptl(filename,t):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")

    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
               
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=t+1)
    time = chunk[t]["rtime"]
    xp = chunk[t]["xp"].reshape((1,mptl),order="F")
    yp = chunk[t]["yp"].reshape((1,mptl),order="F")
    zp = chunk[t]["zp"].reshape((1,mptl),order="F")
    lp = chunk[t]["lp"].reshape((1,mptl),order="F")
    xeq = chunk[t]["xeq"].reshape((1,mptl),order="F")
    yeq = chunk[t]["yeq"].reshape((1,mptl),order="F")
    zeq = chunk[t]["zeq"].reshape((1,mptl),order="F")
    aeq = chunk[t]["aeq"].reshape((1,mptl),order="F")
    beq = chunk[t]["beq"].reshape((1,mptl),order="F")
    ppara = chunk[t]["ppara"].reshape((1,mptl),order="F")
    pperp = chunk[t]["pperp"].reshape((1,mptl),order="F")
    mu = chunk[t]["mu"].reshape((1,mptl),order="F")
    bp = chunk[t]["bp"].reshape((1,mptl),order="F")
    prebp = chunk[t]["prebp"].reshape((1,mptl),order="F")
    db = chunk[t]["db"].reshape((1,mptl),order="F")
    predb = chunk[t]["predb"].reshape((1,mptl),order="F") 
    nump = chunk[t]["nump"].reshape((1,mptl),order="F")
    ierr = chunk[t]["ierr"].reshape((1,mptl),order="F")
    fd.close()     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)1e6
    return ene,xp/6370000,yp/6370000

main()

# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *

path = "/work/m_hayashi/Xray/Vsw600/"
name = "MHDXray_"
time = 0
#time= 18

"""
-69 <= x <= 30 (Re)
-40 <= y <= 40 (Re)
-40 <= z <= 40 (Re)
""" 
x=0  #GSM
y=0  #GSM
z=0  #GSM

def main():
    data = np.load(path+name+str(time).zfill(3)+".npz")
    xray = data["Xray"]

    xg = nx(x)
    yg = ny(y)
    zg = ny(z)
   
    
    imshow(xray[:,:,120])
    show()


def nx(x1):
    return int((-x)*3 + 90)

def ny(y1):
    return int((-y)*3 + 121)
main()


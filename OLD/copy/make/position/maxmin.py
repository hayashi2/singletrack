# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 14:06:49 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 

def main():
    n=0
    maxe,mine=[],[]

    while(n<=479):
        num=0
        while(num <= 127):
            filename= "/work/m_hayashi/RB000/ptl%03i.dat"%num
            a,b = readfile(filename,n)
            maxe = np.append(maxe,a)
            mine = np.append(mine,b)
            num+=1        
        print 'finish %i'%n
        n+=1
    print max(maxe),min(mine)
    
def readfile(filename,n):
    
    ene=[]
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,
                   ("xp","<128f8"),("yp","<128f8"),("zp","<128f8"),
                   ("lp","<128f8"),("xeq","<128f8"),("yeq","<128f8"),
                   ("zeq","<128f8"),("aeq","<128f8"),("beq","<128f8"),
                   ("ppara","<128f8"),("pperp","<128f8"),("mu","<128f8"),
                   ("bp","<128f8"),("prebp","<128f8"),("db","<128f8"),
                   ("predb","<128f8"),("nump","<128i4"),("ierr","<128i4"),tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=n+1)
    
    xp = chunk[n]["xp"].reshape((1,128),order="F")
    yp = chunk[n]["yp"].reshape((1,128),order="F")
    zp = chunk[n]["zp"].reshape((1,128),order="F")
    lp = chunk[n]["lp"].reshape((1,128),order="F")
    xeq = chunk[n]["xeq"].reshape((1,128),order="F")
    yeq = chunk[n]["yeq"].reshape((1,128),order="F")
    zeq = chunk[n]["zeq"].reshape((1,128),order="F")
    aeq = chunk[n]["aeq"].reshape((1,128),order="F")
    beq = chunk[n]["beq"].reshape((1,128),order="F")
    ppara = chunk[n]["ppara"].reshape((1,128),order="F")
    pperp = chunk[n]["pperp"].reshape((1,128),order="F")
    mu = chunk[n]["mu"].reshape((1,128),order="F")
    bp = chunk[n]["bp"].reshape((1,128),order="F")
    prebp = chunk[n]["prebp"].reshape((1,128),order="F")
    db = chunk[n]["db"].reshape((1,128),order="F")
    predb = chunk[n]["predb"].reshape((1,128),order="F") 
    nump = chunk[n]["nump"].reshape((1,128),order="F")
    ierr = chunk[n]["ierr"].reshape((1,128),order="F")
    
    close(filename)     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)       
    ene = np.append(ene,  (m * c**2 * (g - 1) / e)/1000000)
        
    return max(ene),min(ene)
    
main()

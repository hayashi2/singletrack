import numpy as np
from pylab import *
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
time = 0
e1,e2,e3=[],[],[]
t=[]

nx = np.arange(1,129,1)-64
na = np.empty([128,128])

n=0
while(n<=127):
    na[n]=nx
    n+=1

nb=na.transpose(1,0)

cosphi=na/np.sqrt((na)**2+(nb)**2)
sinphi=nb/np.sqrt((na)**2+(nb)**2)


while(time <= 2376):
    
    path = "/work/saito/GMHDRB/inputdata/"
    ename = "MHDE_RB"

    filename = path + ename + str(time).zfill(6) + ".dat"

    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<6291456f4")
         
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["b"].reshape((128,128,128,3),order="F")
    bt=bf.transpose(1,0,2,3)*(1e3)
    ey = bt[:,:,64,1]
    ex = bt[:,:,64,0]

    ephi = ey*cosphi -ex*sinphi
    e1 = np.append(e1,ephi[64,109])
    e2 = np.append(e2,ephi[64,88])
    e3 = np.append(e3,ephi[64,40])
    t = np.append(t,time)
    time += 12

figure(1)
plot(t,e1)
xlabel("time[sec]")
ylabel("Ephi[mV/m]")
title("(15,0)")

figure(2)
plot(t,e2)
xlabel("time[sec]")
ylabel("Ephi[mV/m]")
title("(8,0)")

figure(3)
plot(t,e3) 
xlabel("time[sec]")
ylabel("Ephi[mV/m]")
title("(-8,0)")

show()
# savefig("EphiL"+str(time/24)+".jpeg")
 #   print "saved ",time

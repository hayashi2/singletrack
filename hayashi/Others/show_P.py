# -*- coding: utf-8 -*-
import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
import matplotlib.ticker as tick
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LogNorm
# BOX SIZE
gx = 500
gy = 300
gz = 300
#Earth position
#rx = 122
#ry = 151
#rz = 151
rx = 120
ry = 150
rz = 150
e0 =2.2
b0 = 10.0
n0 = 1.0
p0 = 0.04 
path = "/work/m_hayashi/GM/run6/MHD/"
savepath = "/work/m_hayashi/figure/run6/P/"
ename = "MHD_P"

def main():

    for time in xrange(0,400):
            
        filename = path + ename + str(time).zfill(3) + ".dat"
        
        head = ("head","<i")
        tail = ("tail","<i")
        b=("b","<"+str(gx*gy*gz)+"f4")
         
        dt = np.dtype([head,b,tail])
        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
        #Convert Global-MHD coodinate ->GSM
        bf = chunk[0]["b"].reshape((gx,gy,gz),order="F")*p0
        bt=bf.transpose(1,0,2)
        P = bt[:,:,rz]
        
        clf()
        figure(figsize=(11,6))
        
        #imshow(bz[79:223,42:202],extent=[20,-20,18,-18],origin="lower",aspect="auto",interpolation="none",norm=LogNorm(1,1000))
        imshow(P[:,:],extent=[30,-95,150./4,-150./4],origin="lower",aspect="auto",interpolation="none",vmin=0,vmax=2)
        xlabel("x(GSM)[Re]",fontsize=18)
        ylabel("y(GSM)[Re]",fontsize=18)
        xticks(fontsize=18)
        yticks(fontsize=18)
        gca().xaxis.set_major_locator(tick.MultipleLocator(10))
        title("time="+str(time*11)+"sec",fontsize=22)
        #title("step : "+str(1000*(time-1)).zfill(6),fontsize=20)
        grid(color="gray",linestyle="--",linewidth=0.5)
        CB=colorbar()   
        CB.set_label('P [nPa]',fontsize=18)
        CB.ax.tick_params(labelsize=16)
    #CB.set_clim(-4.84,2.64)  
        """
        lin = -readB(time)
        plot(lin[10:98]/3 +21,(-np.arange(np.size(lin),dtype="f")[56]+np.arange(np.size(lin),dtype="f")[10:98])/3,"k")
        xlim(21,-21)
        ylim(18,-18)
        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
        fig = gcf()
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)

        """
        savefig(savepath+"P"+str(time).zfill(3)+".jpeg")
        print "saved ",time*11
        close()
        
"""
def readB(t):
    b0 = 10
    mu0 = 4.*np.pi*1e-7
    bpath ="/work/ymatumot/simulation/GM-RB/run_Vsw600/"
    bname = "MHD_B"
    filename = bpath + bname + str(t).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<51750171f4")

    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*b0
    
    Bx = bf[:,:,:,0]
    By = bf[:,:,:,1]
    Bz = bf[:,:,:,2]
    B = np.sqrt(Bx**2 + By**2 + Bz**2)
    P =  (((B*1e-9)**2)/(2.*mu0))*1e9 #+Pp 

    P = P[27:153,66:174,121]
    #P = np.diff(P,axis=0)
    lin = np.empty(np.shape(P)[1])
    for y in xrange(np.shape(P)[1]):
        for x in xrange(np.shape(P)[0]):
            
            if(P[x,y] > 0.01):
                lin[y] = float(x)
                break
    return lin
"""
main()

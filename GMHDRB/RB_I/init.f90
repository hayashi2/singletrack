!######################################################################
module init
!######################################################################

    use prm, only: q, m, c, re, pi
    use inp, only: set_seed, dx_re, dy_re, dz_re, dt, maxptl, outpath, &
                 & rpmin_re, rpmax_re, apmin_deg, apmax_deg, epmin_ev, epmax_ev
    use itp_emf, only: itp_emf__exe
    use mtmod, only: sgrnd, grnd
    use gkmover, only: gkmover__exe
    use find_mequator, only: find_mequator__exe
    use status_checker, only: status_checker__exe

    implicit none
    private
    public :: init_single, init_uniform_local_time

contains
!######################################################################
    subroutine init_uniform_local_time&
             &(rtime, irank, emf,     &
             & xp,     yp,     zp,    &
             & lp,     xeq,    yeq,   &
             & zeq,    aeq,    beq,   &
             & ppara,  pperp,  mu,    &
             & bp,     prebp,  db,    &
             & predb, nump,   ierr    )

        real(8), intent(in) :: rtime
        real(8), intent(in) :: emf(:, :, :, :)
        integer, intent(in) :: irank
        real(8), intent(out) :: mu(:)
        real(8), intent(out) :: xp(:), yp(:), zp(:), lp(:)
        real(8), intent(out) :: xeq(:), yeq(:), zeq(:), aeq(:), beq(:)
        real(8), intent(out) :: ppara(:), pperp(:), bp(:)
        real(8), intent(out) :: prebp(:), db(:), predb(:) 
        integer, intent(out) :: nump(:), ierr(:)

        integer :: iseed, ierr_counter, i, nt, openstatus
        integer :: icount, ic, nstep
        real(8) :: rpmin, rpmax, apmin, apmax, epmin, epmax
        real(8) :: xp0, yp0, zp0, b0, bp0, db0
        real(8) :: e0, p0
        real(8) :: Lmi(maxptl)
        real(8) :: lp0, xeq0, yeq0, zeq0, aeq0, beq0
        real(8) :: ppara0, pperp0, prebp0, predb0
        real(8) :: preyp, r, td, beta, g
        real(8) :: bx, by, bz, ex, ey, ez
        character(128) :: memo, filename

        iseed = set_seed + irank

        call sgrnd(iseed)

        rpmin = rpmin_re * re
        rpmax = rpmax_re * re
        apmin = apmin_deg * pi / 180d0
        apmax = apmax_deg * pi / 180d0
        epmin = epmin_ev * (-q)
        epmax = epmax_ev * (-q) 

        i = 1
        PARTICLE_LOOP: do
            ierr_counter = 0

        !##### SEARCH EQUATRIAL POSITION AT MIDNIGHT #####
            FIND_EQUATOR: do
                r = rpmin + (rpmax - rpmin) * grnd()
                xp(i) = r
                yp(i) = 0d0
                zp(i) = 0d0
                call find_mequator__exe(rtime, emf, emf,     &
                                      & xp(i), yp(i), zp(i), &
                                      & xp0,   yp0,   zp0,   &
                                      & b0, ierr(i)          )
                call itp_emf__exe(rtime, emf, emf, xp(i), yp(i), zp(i), &
                                & bx, by, bz, ex, ey, ez, ierr(i))
                b0 = dsqrt(bx**2 + by**2 + bz**2)

                if (ierr(i) == 0) then
                    Lmi(i) = (3.12d-5/b0)**0.33333
                    exit
                end if

                ierr_counter = ierr_counter + 1

                if (ierr_counter > 100) then
                    memo = "A MAGNETIC EQUATOR IS NOT FOUND IN THE FIELD AREA..."
                    call status_checker__exe(memo, irank, ierr_counter)
                end if
            end do FIND_EQUATOR
           !END FIND_EQUATOR

        !#### SET ENERGY AND MOMENTUM
            e0 = epmin + (epmax - epmin) * grnd()
            p0 = dsqrt((e0 + m * c**2)**2 - m**2 * c**4) / c

        !##### SET PARTICLE DATA #####
            nump(i) = i
            xeq(i) = xp0
            yeq(i) = yp0
            zeq(i) = zp0
            lp(i)  = 0d0
            xp(i)  = xeq(i)
            yp(i)  = yeq(i)
            zp(i)  = zeq(i)
            beq(i) = b0
            bp(i)  = b0
            aeq(i) = apmin + (apmax - apmin) * grnd()
            ppara(i) = p0 * dcos(aeq(i))
            pperp(i) = p0 * dsin(aeq(i))
            mu(i) = pperp(i)**2 / (2 * m * bp(i))
            prebp(i) = bp(i)
            predb(i) = 0d0
            db(i) = 0d0


        !##### CALCULATE DRIFT TIME #####
            nt = 0
            icount = 0
            ic = 0
            xp0 = xp(i)
            yp0 = yp(i)
            zp0 = zp(i)
            bp0 = bp(i)
            db0 = db(i)
            prebp0 = prebp(i)
            predb0 = predb(i)
            ppara0 = ppara(i)
            pperp0 = pperp(i)
            xeq0 = xeq(i)
            yeq0 = yeq(i)
            zeq0 = zeq(i)
            aeq0 = aeq(i)
            lp0 = lp(i)
            beq0 = beq(i)
            preyp = yp(i)
            ierr(i) = 0
            g = dsqrt(1d0 + (ppara(i)**2 + pperp(i)**2)/(m**2 * c**2))
            beta = dsqrt(ppara(i)**2 + pperp(i)**2) / (m * c * g)
            td = 1.557d4 * (re / (dsqrt(xeq(i)**2 + yeq(i)**2))) / g / beta**2 &
            &  * (1d0 - 0.3333d0*dsin(aeq(i))**0.62)

            CALC_TD: do
                call gkmover__exe           &
                    &(0d0,                 &
                    & emf,                  &
                    & emf,                  &
                    & 1,                  &
                    & xp(i:i),     yp(i:i),     zp(i:i),    &
                    & lp(i:i),     xeq(i:i),    yeq(i:i),   &
                    & zeq(i:i),    aeq(i:i),    beq(i:i),   &
                    & ppara(i:i),  pperp(i:i),  mu(i:i),    &
                    & bp(i:i),     prebp(i:i),  db(i:i),    &
                    & predb(i:i),  nump(i:i),   ierr(i:i)   )
                if (ierr(i) /= 0) exit

                !If the particle at the noon, EXIT.
                if ((nt*dt > 10d0).and.(xp(i) <  0d0).and. &
                  & (preyp >  0d0).and.(yp(i) <= 0d0)) exit

                !Count the number of calculation.
                nt = nt + 1

                !If the particle cannot reach at the noon, ERROR and EXIT.
                if (nt*dt > 3600d0) then
                    ierr(i) = -10
                    exit
                end if

                preyp = yp(i)
            end do CALC_TD

            !Set the start position again
            xp(i) = xp0
            yp(i) = yp0
            zp(i) = zp0
            bp(i) = bp0
            db(i) = db0
            prebp(i) = prebp0
            predb(i) = predb0
            ppara(i) = ppara0
            pperp(i) = pperp0
            xeq(i) = xeq0
            yeq(i) = yeq0
            zeq(i) = zeq0
            aeq(i) = aeq0
            lp(i) = lp0
            beq(i) = beq0

            if (ierr(i) /= 0) then
                i = i - 1
            else
                nt = dint(2 * nt * grnd())
                if (nt > 0) then
                    !Set the initial position finally.
                    do nstep = 1, nt
                        call gkmover__exe           &
                            &(0d0,                 &
                            & emf,                  &
                            & emf,                  &
                            & 1,                  &
                            & xp(i:i),     yp(i:i),     zp(i:i),    &
                            & lp(i:i),     xeq(i:i),    yeq(i:i),   &
                            & zeq(i:i),    aeq(i:i),    beq(i:i),   &
                            & ppara(i:i),  pperp(i:i),  mu(i:i),    &
                            & bp(i:i),     prebp(i:i),  db(i:i),    &
                            & predb(i:i),  nump(i:i),   ierr(i:i)   )
                    end do
                end if
            end if

            write(filename, '(a, a, i3.3, a)') trim(outpath), "stdout", irank, ".txt"
            open(unit=99, file=filename, form="formatted", &
               & status="new", action="write", iostat=openstatus)
            if (openstatus /= 0) then
                open(unit=99, file=filename, form="formatted", &
                   & status="old", action="write", position="append", iostat=openstatus)
            end if
            write(99, *) "calc_position end:", i, &
                        & real(xeq(i)/re), real(yeq(i)/re), real(zeq(i)/re)
            close(99)
            i = i + 1
            if (i > maxptl) exit

        end do PARTICLE_LOOP

        ierr(1:maxptl) = 0

    end subroutine init_uniform_local_time

!######################################################################
!######################################################################
!######################################################################

    subroutine init_single          &
           &(rtime, irank, emf,     &
           & xp,     yp,     zp,    &
           & lp,     xeq,    yeq,   &
           & zeq,    aeq,    beq,   &
           & ppara,  pperp,  mu,    &
           & bp,     prebp,  db,    &
           & predb, nump,   ierr    )

        real(8), intent(in) :: rtime, emf(:, :, :, :)
        integer, intent(in) :: irank
        real(8), intent(out) :: mu(:)
        real(8), intent(out) :: xp(:), yp(:), zp(:), lp(:)
        real(8), intent(out) :: xeq(:), yeq(:), zeq(:), aeq(:), beq(:)
        real(8), intent(out) :: ppara(:), pperp(:), bp(:)
        real(8), intent(out) :: prebp(:), db(:), predb(:) 
        integer, intent(out) :: nump(:), ierr(:)

        real(8) :: dx, dy, dz
        real(8) :: bx, by, bz, ex, ey, ez, e0, p0
        real(8) :: r0re, e0ev, a0deg 
        integer :: iseed, i


        ierr = 0
        dx = dx_re * re
        dy = dy_re * re
        dz = dz_re * re
        r0re = rpmin_re
        e0ev = epmin_ev
        a0deg = apmin_deg

        xp(1:maxptl) = r0re * re
        yp(1:maxptl) = 0d0
        zp(1:maxptl) = 0d0
        lp(1:maxptl) = 0d0
        xeq(1:maxptl) = xp(1:maxptl)
        yeq(1:maxptl) = yp(1:maxptl)
        zeq(1:maxptl) = zp(1:maxptl)

        do i = 1, maxptl 
            call itp_emf__exe(rtime, emf, emf, xp(i), yp(i), zp(i), &
                            & bx, by, bz, ex, ey, ez, ierr(i))
            beq(i) = dsqrt(bx**2 + by**2 + bz**2)
            bp(i) = beq(i)
        end do

        e0 = e0ev * dabs(q)
        p0 = dsqrt((e0 + m * c**2)**2 - m**2 * c**4) / c

        aeq(1:maxptl) = a0deg * pi / 180d0

        do i = 1, maxptl
            ppara(i) = p0 * dcos(aeq(i))
            pperp(i) = p0 * dsin(aeq(i))
            mu(i) = pperp(i)**2 / (2 * m * bp(i))
            prebp(i) = bp(i)
            predb(i) = 0d0
            db(i) = 0d0
            nump(i) = i
            ierr(i) = 0
        end do

end subroutine init_single

!######################################################################
end module init

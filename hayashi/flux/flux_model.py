# -*- coding: utf-8 -*-

import numpy as np
import scipy.optimize
import time

m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
re = 6370.e3
ptl = 10000
ptlnum=144
B0 = 32000. #nT 
files ="/work/m_hayashi/RBdata/RB_GMrun5_000/ptl"
savefile = "/work/m_hayashi/Fluxdata/Data_RB_GMrun5_000_VAPs0716"
wfile = "/work/m_hayashi/Fluxdata/weight_RB_GMrun5_000_VAPs0716.npz"

enest = 0
dL = 0.5
dphi = float(1.0/180.0)*np.pi*15.
da = float(1.0/180.0)*np.pi
de = 0.25
dt = 5
iniL = 5.0
grL = int((10.-iniL)/dL)
grML = 2*int(np.pi/dphi)
gra = int(np.pi/da)
gre = int(10./de)+1
grt = 200

def main():
    start =time.time()
    w = np.load(wfile)
    w = w["w"]
    print w.shape
    count = np.zeros((grL,grML,gra,gre,grt)) #L,phi,a,ene,t
    flux1 = np.zeros((grL,grML,gra,gre,grt))
    #count[:,:,:,:,:] = np.nan
    # file loop 
    for filenum in xrange(0,ptlnum):
        print "counting...", filenum
        filename = files + str(filenum).zfill(3) +".dat"

    #read file
        xp,yp,aeq,beq,ene,ierr,zeq=  readfile(filename)
        x, y,zeq= xp/re,yp/re,zeq/re
        energy=ene/1e6
    
    #grid number
        mltg, ag, eg ,Lg= griding(x,y,aeq,ene,beq*1e9)   
    #ptl loop
        for t in xrange(0,grt):
            for m in xrange(0,ptl):
    #count per bins
                Flag = (Lg[t,m] < grL and mltg[t,m] < grML and ag[t,m] < gra and eg[t,m] < gre)
                if(ierr[t,m]==0 and np.abs(zeq[t,m])< 1.5 and Flag) :
                   # if(np.isnan(count[Lg[t,m],mltg[t,m],ag[t,m],eg[t,m],t])==True):
                    # count[Lg[t,m],mltg[t,m],ag[t,m],eg[t,m],t] = w[filenum,m]/(float(Lg[t,m])/2 +5.5)
                    # else:
                    count[Lg[t,m],mltg[t,m],ag[t,m],eg[t,m],t] += w[filenum,m]/(float(Lg[t,m])*dL +iniL)

    


    del mltg, ag, eg ,Lg
    #calicurate flux
    for an in xrange(1,gra-1):
        ap = (an)*np.pi/gra
        flux1[:,:,an,:,:] = count[:,:,an,:,:]/np.abs(dL*dphi*da*de*dt*2*np.pi*(np.sin(ap))) 

    np.savez(savefile+".npz",flux=flux1)
    

    elapsed_time = time.time()-start
    print "elapsed_time:{0}".format(elapsed_time) +" [sec]"
    
def readfile(filename):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt)
   
    #xp = chunk["xp"]
    #yp = chunk["yp"]
    #zp = chunk["zp"]
    #lp = chunk["lp"]
    xeq = chunk["xeq"]
    yeq = chunk["yeq"]
    zeq = chunk["zeq"]
    aeq = chunk["aeq"]
    beq = chunk["beq"]
    ppara = chunk["ppara"]
    pperp = chunk["pperp"]
    #mu = chunk["mu"]
    #bp = chunk["bp"]
    #prebp = chunk["prebp"]
    #db = chunk["db"]
    #predb = chunk["predb"] 
    #nump = chunk["nump"]
    ierr = chunk["ierr"]
  
    fd.close()     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)
    return -xeq[0:grt,:] ,-yeq[0:grt,:],aeq[0:grt,:],beq[0:grt,:],ene[0:grt,:],ierr[0:grt,:],zeq[0:grt,:]



def griding(x,y,a,ene,beq):
    Lg = np.around(((B0/beq)**(1./3) -iniL)/dL).astype(int)    
    mltg = np.around((np.degrees(np.arctan2(y,x)+np.pi))/15).astype(int)-1
    ag = np.around(a/da).astype(int)
    eg = np.around((ene/1e6 )/de).astype(int)
            


    return mltg,ag,eg,Lg



def Save_para():
    phi = dphi/np.pi/15.*180
    a = da/np.pi*180.
    f = open(savefile+".txt", "w")
    f.write("ptl = "+str(ptl)+"\n")
    f.write("ptlnum = "+str(ptlnum)+"\n")
    f.write("dL = "+str(dL)+"\n")
    f.write("dphi = float("+str(phi)+"/180.)*np.pi*15\n")
    f.write("da = float("+str(a)+"/180.)*np.pi\n")
    f.write("de = "+str(de)+"\n")
    f.write("dt = "+str(dt)+"\n")
    f.write("iniene = "+str(iniene)+"\n")
    f.write("enest = "+str(enest)+"\n")
    f.write("iniL = "+str(iniL)+"\n")
    f.write("grL = "+str(grL)+"\n")
    f.write("grML = "+str(grML)+"\n")
    f.write("gra = "+str(gra)+"\n")
    f.write("gre = "+str(gre)+"\n")
    f.write("grt = "+str(grt)+"\n")
    f.close()


main()

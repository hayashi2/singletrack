# -*- coding: utf-8 -*-

import numpy as np
from spacepy import pycdf
import subprocess
import sys
import os
from datetime import datetime,date,timedelta

username = "erg_project"
pswd = "geospace"

def main():
    args = sys.argv
    print datetime.today()

    
    ###### Input check ######
    
    if (len(args)!=3):
        print "#########WARNING######### Incorrect input"
        print "Arguments  1st: start date, 2nd: end date"
        sys.exit()
        
    if(len(args[1])!= 8 or len(args[2])!=8):
        print "#########WARNING######### Incorrect input"
        print "start_date or end_date is incorrect"
        sys.exit()

    
    ###### Data acquisition ######
    start_date = args[1]
    end_date = args[2]

    s_date = datetime.strptime(start_date,"%Y%m%d")
    e_date = datetime.strptime(end_date,"%Y%m%d")

    i=0
    for single_date in daterange(s_date, e_date):        
        year = str(single_date.year).zfill(4)
        month = str(single_date.month).zfill(2)
        day = str(single_date.day).zfill(2)
        
        filename = "erg_hep_pre_omniflux_"+year+month+day+"_v01.cdf"
        URL = "http://ergsc.isee.nagoya-u.ac.jp/data/ergsc/satellite/erg/hep/l2pre/omni/"+year+"/"+filename
        filepath = "/home/m_hayashi/ERG_data/hep/"+filename
        cmd = "wget"+ " --http-user="+username+" --http-passwd="+pswd +" "+ URL+" -O "+filepath
                
        subprocess.call(cmd,shell=True)
        


def daterange(s_date,e_date):
    return (s_date + timedelta(days=i) for i in range((e_date-s_date).days+1))

main()

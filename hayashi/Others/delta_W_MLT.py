# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
import matplotlib.patches as mpatches
import matplotlib.colors as col

ptl = 10000
f_num = 144
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
re = 6370e3
e0 = 2.2
B0 = 32000. #nT

RUN = "run5"
pdt = 5
savedir = "/1"

#sttime = 220/pdt -1 #run3 1
#etime = 470/pdt -1 #run3 1
sttime = 260/pdt -1 #run5 1
etime = 585/pdt -1 #run5 1

def main():        
    xedges = np.linspace(0.,24.,49) #MLT
    yedges = np.linspace(0.,10.,41) #ini W
    data = np.zeros((np.size(xedges)-1,np.size(yedges)-1,etime-sttime+1))

    for t in xrange(sttime,etime+1):
        print t
        ini_ene, mlt,d_ene = [],[] ,[]
        for f in xrange(f_num):
            filename = "/work/m_hayashi/RBdata/RB_GM"+RUN+"_000/ptl"+str(f).zfill(3)+".dat"
            print "reading......" + filename
            p_mlt,aeq,beq,p_ene,ierr = readptl(filename)
            aeq = np.degrees(aeq)
            Lg = (B0/beq)**(1./3)

            for i in xrange(ptl):
                if(85 <= aeq[0,i] < 95 and 6.5<= Lg[t,i] < 7 and ierr[t,i] == 0):
                    ini_ene.append(p_ene[t,i])
                    mlt.append(p_mlt[t,i])
                    if(t == sttime): d_ene.append(0)
                    else: d_ene.append(p_ene[t,i]-p_ene[t-1,i])

                    #mg = round(mlt[-1]*2)
                    #eg = round(ini_ene[-1]*4)
                    
                    #data[mg,eg,t-sttime] += d_ene[-1]

        H, xedges, yedges = np.histogram2d(mlt, ini_ene, bins=(xedges, yedges))
        HIST , xedges, yedges = np.histogram2d(mlt, ini_ene, bins=(xedges, yedges),weights=d_ene)
        
        HIST = HIST/H
        data[:,:,t-sttime] = HIST 
        #data[data == np.nan] = 0
    np.savez("/work/m_hayashi/hist_"+RUN+"_ver2.npz",HIST=data)
           

def readptl(filename):
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt)
    #time = chunk["rtime"]
    #xp = chunk["xp"]
    #yp = chunk["yp"]
    #zp = chunk["zp"]
    #lp = chunk["lp"]
    xeq = chunk["xeq"]
    yeq = chunk["yeq"]
    #zeq = chunk["zeq"]
    aeq = chunk["aeq"]
    beq = chunk["beq"]
    ppara = chunk["ppara"]
    pperp = chunk["pperp"]
    #mu = chunk["mu"]
    #bp = chunk["bp"]
    #prebp = chunk["prebp"]
    #db = chunk["db"]
    #predb = chunk["predb"] 
    #nump = chunk["nump"]
    ierr = chunk["ierr"]
    fd.close()
    
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)       
    ene = (m * c**2 * (g - 1) / e)/1e6
    mlt = np.degrees(np.arctan2(-yeq,-xeq)+np.pi)/15
    return mlt,aeq,beq*1e9,ene,ierr  # =>nT

def readE(filepath,t):
    filename = filepath + str(t).zfill(3)+".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<"+str(gx*gy*gz*3)+"f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    E = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*e0
    #Convert Global-MHD coodinate ->GSM
    return -E[:,:,rz,0],-E[:,:,rz,1] 

main()

# coding: UTF-8

import numpy as np
import pylab as plt

xray=np.empty([297,241,241])
for line in open("/work/m_hayashi/xraytest.dat","r"):
    item =line[:-1].split("\t")
    print round((float(item[2]))*(-3)) + 120
    xg = round((float(item[0]))*(-3)) + 89
    yg = round((float(item[1]))*(-3)) + 119
    zg = round((float(item[2]))*(-3)) + 119
    
    xray[xg,yg,zg]= item[3]
plt.figure(1)
plt.imshow(xray[:,:,120].T,origin='lower',extent =[30,-69,40,-40])
plt.xlabel('x [Re]')
plt.ylabel('y [Re]')

plt.figure(2)
plt.imshow(xray[:,120,:].T,origin='lower',extent =[30,-69,40,-40])
plt.xlabel('x [Re]')
plt.ylabel('z [Re]')

plt.show()
print xray[230,0,0]
    

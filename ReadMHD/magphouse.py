# -*- coding: utf-8 -*-
import numpy as np
from scipy.interpolate import spline
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8
b0 = 10
mu0 = 4.*np.pi*1e-7
bpath ="/work/m_hayashi/GM/run_Vsw600/"
bname = "MHD_B"
gx = 297
gy = 241
gz = 241
tmax =2400.
# Earth position
ex = 90
ey = 120
ez = 120
def main():
    for t in xrange(0,int(tmax/12)):
        B = readB(t)
        P =  (((B*1e-9)**2)/(2.*mu0))*1e9 #+Pp 
        P = P[27:153,66:174,ez] # -21Re <x<21Re  -18Re <y< 18Re
        for y in xrange(np.shape(P)[1]):
            for x in xrange(np.shape(P)[0]):
                if(P[x,y] > 0.01):
                    print("x: ",x," y: ",y)
                    break

    

def readB(t):
    filename = bpath + bname + str(t).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<"+str(gx*gy*gz*3)+"f4")

    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    bf = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*b0
    
    Bx = bf[:,:,:,0]
    By = bf[:,:,:,1]
    Bz = bf[:,:,:,2]
    B = np.sqrt(Bx**2 + By**2 + Bz**2)
    return B


    
main()

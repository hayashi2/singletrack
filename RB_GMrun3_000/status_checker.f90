!######################################################################
module status_checker
!######################################################################
  use mpi
  use inp, only: outpath

  implicit none

  private
  public status_checker__exe

contains
!######################################################################
  subroutine status_checker__exe(txtline, irank, istatus)

    character(128), intent(in) :: txtline
    integer, intent(in) :: irank, istatus
    integer :: ierr, openstatus
    character(128) :: filename

    write(filename, '(a, a, i3.3, a)') trim(outpath), "stdout", irank, ".txt"
    open(unit=99, file=filename, form="formatted", &
       & status="new", action="write", iostat=openstatus) 
    if (openstatus /= 0) then
      open(unit=99, file=filename, form="formatted", &
         & status="old", action="write", position="append", iostat=openstatus) 
    end if

    if (istatus /= 0) then
      write(99, *) "STATUS CHECK: ERROR"
      write(99, *) "SUM STATUS =", istatus
      write(99, *) "CALCULATION STOPPED."
      write(99, *) txtline
      close(99)
      call mpi_abort(mpi_comm_world, 9, ierr)
      call mpi_finalize(ierr)
      stop
    else
      write(99, *) "STATUS CHECK: OK"
      write(99, *) txtline
    end if

  end subroutine status_checker__exe

!######################################################################
end module status_checker
!######################################################################

# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 13:30:36 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches

bpath ="/work/ymatumot/simulation/GM-RB/run_Vsw600/"
bname = "MHD_B"


def main():
    for num in range(0,209):
        Bz = readB(num)
        figure(figsize=(12,10))
        imshow(Bz[:,:,120,2].T , extent=[-30,69,-40,40] , origin="lower",norm=LogNorm(vmax=1e3,vmin=1e-2))
        title("time="+str(num*12)+"s")
        ax=colorbar()    
        ax.set_label('Bz [nT]',fontsize=15)
    #ax.set_clim(0,480)
        xlabel("x(Re)")
        ylabel("y(Re)")
        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='k')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='w')    
        fig = gcf()
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)
    
        savefig('Bz%04i.jpeg'%(num/2))
        print 'saved %d'%(num*12)

def readB(num):
    filename = bpath + bname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<51750171f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*10
    #convert GSM
    bx = -bf[:,:,:,0]
    by = -bf[:,:,:,1]
    bz = bf[:,:,:,2] 
   
    return bz
   
main()

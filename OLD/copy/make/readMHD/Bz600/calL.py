# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 13:30:36 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches

bpath ="/work/ymatumot/simulation/GM-RB/run_Vsw600/"
bname = "MHD_B"
b0=10

def main():               
    x0= 68               
    x1= 110              
    y0= 99               
    y1= 141        
    xy=np.empty((200,4))
    for t in range(0,200):
        print "time= " + str(t) +" sec"
        Beq = readB(t)
       """ 
        for x in range(0,297):
            if (int(Beq[x,120,120])>=91 and int(Beq[x,120,120])<=97 ):
                #print "x=" ,x
                 
                if(x<90):
                    x0=x
                else:
                    x1=x
                 
        
        for y in range(0,241):
            if (int(Beq[90,y,120])>=91 and int(Beq[90,y,120])<=97 ):
                #print "y=" ,y
                
                if(y<120):
                    y0=y
                else:
                    y1=y    
                
        print x0,x1,y0,y1
        
        xy[t,0]=x0
        xy[t,1]=x1
        xy[t,2]=y0
        xy[t,3]=y1
    np.savez("L7.npz", xy=xy) 
    
    """
def readB(num):
    filename = bpath + bname + str(num).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<51750171f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*b0
    #convert GSM
    bx = -bf[:,:,:,0]
    by = -bf[:,:,:,1]
    bz = bf[:,:,:,2] 
    beq=np.sqrt(bx**2 +by**2 +bz**2)
    
    return beq
   
main()

# -*- coding: utf-8 -*-
"""
Created on Fri Dec 18 11:32:35 2015

@author: m_hayashi
"""

import os
import os.path

path = "/home/m_hayashi/output/RB"
newdir = "hist"
dirnum = 1
while(dirnum<=45):
    dirname = path + str(dirnum).zfill(3) + "/" + newdir
    if os.path.exists(dirname) == False:
        os.makedirs(dirname)
    else:
        print "RB"+ str(dirnum).zfill(3)+ "  directory already exists"
    dirnum += 1

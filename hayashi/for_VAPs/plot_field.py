# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("Agg")
from pylab import *
import numpy as np
from spacepy import pycdf
import matplotlib.ticker as tic
import matplotlib.colors as col
import matplotlib.cm as cm
import sys
import subprocess
import os
import scipy.signal
from datetime import datetime,date,timedelta
from matplotlib import gridspec



la_fontsize = 22
ti_fontsize = 17



def main():
    args = sys.argv
    print datetime.today()
    start_time = args[1]
    end_time = args[2]
    delta_m = (int(end_time[0:2])-int(start_time[0:2]))*60 +  (int(end_time[2:4])- int(start_time[2:4]))

    a_st = (int(start_time[0:2])*60*60 + int(start_time[2:4])*60)/4 +2 
    a_ed = (int(end_time[0:2])*60*60 + int(end_time[2:4])*60)/4  +2 +1

    b_st = (int(start_time[0:2])*60*60 + int(start_time[2:4])*60)/4 +14 
    b_ed = (int(end_time[0:2])*60*60 + int(end_time[2:4])*60)/4  +14 +1

    e_st = (int(start_time[0:2])*60*60 + int(start_time[2:4])*60)/8 +3
    e_ed = (int(end_time[0:2])*60*60 + int(end_time[2:4])*60)/8  +4



    s_date = datetime.strptime("20170716"+start_time,"%Y%m%d%H%M")
    mfilename_a = "/home/m_hayashi/data/rbspa_emfisis_l3_4sec_maf_Mag_20170716.txt"
    mfilename_b = "/home/m_hayashi/data/rbspb_emfisis_l3_4sec_maf_Mag_20170716.txt"
    mfilename_e = "/home/m_hayashi/data/erg_mgf_l2_mag_8sec_maf_20170716.txt" 
    efilename_e = "/home/m_hayashi/data/erg_efd_eu_maf_interp_efd_csv_20170716.txt"
    #efilename_e = "/home/m_hayashi/data/erg_pwe_efd_l3_Eu_sm-VxB.csv"

    Ba = np.loadtxt(mfilename_a,skiprows = 0,dtype="str")
    Bb = np.loadtxt(mfilename_b,skiprows = 0,dtype="str")
    Be = np.loadtxt(mfilename_e,skiprows = 0,dtype="str")
    Ee = np.loadtxt(efilename_e,skiprows = 0,dtype="str")
    #Ee = np.loadtxt(efilename_e,skiprows = 0,dtype="str",delimiter=",")
    print Ee.shape

    for i in xrange(10):
        #print Ba[a_ed+i,0],Bb[b_ed+i,0],Be[e_ed+i,0],Ee[e_ed+i,0]
        print Ba[a_st+i,1:4],Bb[b_st+i,1:4],Be[e_st+i,1:4]
    
    Ba_x = Ba[:,1].astype(float)
    Bb_x = Bb[:,1].astype(float)
    Be_x = Be[:,1].astype(float)
    Be_y = Be[:,2].astype(float)
    Ee_y = Ee[:,2].astype(float)

    print np.linalg.norm(Ba[a_st,1:4].astype(float)),np.linalg.norm(Bb[b_st,1:4].astype(float)),np.linalg.norm(Be[e_st,1:4].astype(float)) 
    Ba_t = np.sqrt( (Ba[:,1].astype(float))**2+ (Ba[:,2].astype(float))**2) 
    Bb_t = np.sqrt((Bb[:,1].astype(float))**2+ (Bb[:,2].astype(float))**2) 
    Be_t = np.sqrt((Be[:,1].astype(float))**2+ (Be[:,2].astype(float))**2) 
    
    Ba_z = cal_B(Ba,112)
    Bb_z = cal_B(Bb,112)
    Be_z = cal_B(Be,56)
#    Ba_z = Ba[:,3].astype(float)
#    Bb_z = Bb[:,3].astype(float)
#    Be_z = Be[:,3].astype(float)

    
    
    Ba_x,delay_v = l_filter(Ba_x)
    Bb_x,delay_v = l_filter(Bb_x)
    Be_x,delay_e = l_filter(Be_x)
    Be_y,delay_e = l_filter(Be_y)
    
    #Ee_y,delay = l_filter(Ee_y)
    Ba_t,delay_v = l_filter(Ba_t)
    Bb_t,delay_v = l_filter(Bb_t)
    Be_t,delay_e = l_filter(Be_t)
                   

    Ba_z,delay_v = l_filter(Ba_z)
    Bb_z,delay_v = l_filter(Bb_z)
    Be_z,delay_e = l_filter(Be_z)


    
    #### plot Bx (Br) ####
    fig = figure(figsize=(12,4))
    ax1 = fig.add_subplot(111)
    
    e_xticks = np.linspace(0,delta_m,np.shape(Be[e_st:e_ed,:])[0])
     
    #ax1.plot(e_xticks,Be_x[e_st+delay_e:e_ed+delay_e],color="b",lw = 1.5,label=r"$B_{r}$")
    #ax1.plot(e_xticks,Be_y[e_st+delay_e:e_ed+delay_e],color="green",lw = 1.5,label=r"$B_{\phi}$")
    ax1.plot(e_xticks,Be_z[e_st+delay_e:e_ed+delay_e],color="k",lw = 1.5,label=r"$B_{\parallel}$")    
    ax1.set_ylabel("  MGF - $B_{\parallel}$ [nT]",fontsize = la_fontsize)

    ax2 = ax1.twinx()
    ax2.plot(e_xticks,Ee_y[e_st:e_ed],color="r",lw = 1.5,label=r"$E_{\phi}$")
    ax2.set_ylabel(" EFD - $E_{\phi}$"+" [mV/m]",fontsize=la_fontsize,color="r")
    ax2.tick_params("y",colors="r")
    
    ax2.set_ylim(-10,10)



    left, width = -0.04, .5
    bottom, height = -0.01, .5
    right = left + width
    top = bottom + height

    ax1.text(left, bottom, "hh:mm",horizontalalignment='right',\
                 verticalalignment='top', transform=ax1.transAxes,fontsize = ti_fontsize)


    xticks = np.arange(0,delta_m+10,10)
    

    x = [((s_date + timedelta(minutes=i)).time()).strftime("%H:%M") \
             for count,i in enumerate(xrange(0,delta_m+10,10))]
        
    ax1.set_xlim(0,delta_m)
    ax1.set_xticks(xticks)
    ax1.set_xticklabels(x,fontsize = ti_fontsize)
    ax1.grid()
    ax1.xaxis.set_minor_locator(tic.AutoMinorLocator())
    ax1.yaxis.set_minor_locator(tic.AutoMinorLocator())
    ax1.tick_params(axis='y', which='major', labelsize=ti_fontsize)
    ax2.tick_params(axis='y', which='major', labelsize=ti_fontsize)
    ax1.locator_params(axis="y",tight=False,nbins=5)
    h1, l1 = ax1.get_legend_handles_labels()
    h2, l2 = ax2.get_legend_handles_labels()
    #ax2.legend(h1+h2, l1+l2,loc="upper right",fontsize=la_fontsize)
        
    fig.subplots_adjust(hspace=0.1,bottom=0.2)
    fig.savefig("ERG_B_E.png",dpi=300)
    close(fig)



def l_filter(Bx):
    fs = np.shape(Bx)[0]
    nyq = fs/2.0

    f1 = 1500/nyq
    f2 = 300.0 / nyq      #カットオフ周波数2
    numtaps = 255       #フィルタ係数の数(奇数)
    b = scipy.signal.firwin(numtaps, f1)                         # ローパス
    delay = (numtaps-1)/2
    #b = scipy.signal.firwin(numtaps, f2, pass_zero=False)        # ハイパス
    #b = scipy.signal.firwin(numtaps, [f1, f2], pass_zero=False) # バンドパス
    return scipy.signal.lfilter(b, 1, Bx),delay


def cal_B(B,delt):
    Bz = np.zeros(np.shape(B[:,3]))
    bx = B[:,1].astype(float)
    by = B[:,2].astype(float)
    bz = B[:,3].astype(float)
    for t in xrange(delt,np.size(bz)-delt):
        b_ave = np.array([np.average(bx[t-delt:t+delt]),np.average(by[t-delt:t+delt]),np.average(bz[t-delt:t+delt])])
        b_ave_ave = np.linalg.norm(b_ave)
        B = np.array([bx[t],by[t],bz[t]])
        #Bz[t] =  np.linalg.norm(B) - b_ave_ave
        Bz[t] =  np.linalg.norm(bz[t]) - b_ave_ave #たぶんこっち

    return Bz

def daterange(s_date,e_date):
    return (s_date + timedelta(days=i) for i in range((e_date-s_date).days+1))

main()
 

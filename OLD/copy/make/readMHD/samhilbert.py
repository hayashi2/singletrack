# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *
from scipy.signal import hilbert,chirp
from matplotlib.ticker import AutoMinorLocator


def main():
   
    duration = 1.0
    fs = 400.0
    samples = int(fs*duration)
    t = np.arange(samples)/fs

    signal = chirp(t,20.0,t[-1],100.0)
    signal *= (1.0 + 0.5*np.sin(2.0*np.pi*2.0*t) )
    #signal *= 1.0

    analytic_signal = hilbert(signal)
    amplitude_envelope = np.abs(analytic_signal)
    ins_phase = np.unwrap(np.angle(analytic_signal))
    ins_fre = np.diff(ins_phase)/(2.0*np.pi)*fs

    #figure
    fig = figure(figsize=(12,13))
    subplots_adjust(hspace=0.3)
    ax0 = fig.add_subplot(311)
    ax0.plot(t,signal,label="signal")
    ax0.plot(t,amplitude_envelope,label="envelope")
    ax0.set_xlabel("time(sec)")
    #ax0.set_ylabel("[nT]",fontsize=15)
    ax0.legend(loc=4)
    grid()
    ax0.xaxis.set_minor_locator(AutoMinorLocator())
    ax0.yaxis.set_minor_locator(AutoMinorLocator())

    ax1 = fig.add_subplot(312)
    ax1.plot(t[1:],ins_fre,label="frequency")
    ax1.set_xlabel("time(sec)")
   # ax1.set_ylabel("[Hz]",fontsize=15)
    ax1.legend(loc=4)
    grid()
    ax1.xaxis.set_minor_locator(AutoMinorLocator())
    ax1.yaxis.set_minor_locator(AutoMinorLocator())

    ax2 = fig.add_subplot(313)
    ax2.plot(t,ins_phase,label="phase")
    ax2.set_xlabel("time(sec)")
    ax2.legend(loc=4)
    ax2.xaxis.set_minor_locator(AutoMinorLocator())
    ax2.yaxis.set_minor_locator(AutoMinorLocator())
    grid()
    
    show()
        

main()

# -*- coding: utf-8 -*-
import numpy as np
import matplotlib
matplotlib.use("Agg")
from matplotlib.pyplot import *
from scipy.signal import hilbert,chirp

nx = np.arange(-50,50)
ny = np.arange(-50,50)
NY,NX = np.meshgrid(nx,ny)
print NX[:,0]
R = np.sqrt(NX**2 +NY**2).astype(float)
PHI = np.arctan2(NY,NX)

k = 2.
w = 0.05 * np.pi

Ephi = np.zeros((100,100,100))

for t in xrange(100):
    Ephi[:,:,t] = 10./(R**0.5) *np.sin(-k*PHI +w*t)


    fig = figure()
    ax = fig.add_subplot(111)
    ax.imshow(Ephi[:,:,t],vmin=-10,vmax=10)
    fig.savefig("/work/m_hayashi/figure/test/E/E"+str(t)+".png")

    clf()
    close(fig)


Phase = np.zeros(np.shape(Ephi))
mnum = np.zeros(np.shape(Ephi))

    #hilbert transform
print("start hilbert")
for x in xrange(0,100):
    for y in xrange(0,100):
        analytic_E = hilbert(Ephi[x, y, :])
        print analytic_E.dtype
        amplitude_envelope = np.abs(analytic_E)
        ins_phase = np.angle(analytic_E)
        print ins_phase.dtype
        xgsm = (float(x) - float(50))
        ygsm = (float(y) - float(50))
        r = np.sqrt(xgsm**2 + ygsm**2)
        if(r > 3):
            Phase[x,y,:] = ins_phase
print("end hilbert and start grad")
dfdx, dfdy, dfdt = np.gradient(Phase)
dfdx = dfdx
dfdy = dfdy
"""
for x in xrange(0,100):
    for y in xrange(0,100):
            #convert GSM
        xgsm = (float(x) - float(50))
        ygsm = (float(y) - float(50))
        r = np.sqrt(xgsm**2 + ygsm**2)
        phi = np.arctan2(ygsm,xgsm)
        if(r !=0.):
            mnum[x,y,:] = 1./r *(-ygsm*dfdx[x,y,:] + xgsm*dfdy[x,y,:])
"""


for t in xrange(0,100):
    fig1 = figure(figsize=(6,5))
    ax0 = fig1.add_subplot(111)
    im0 = ax0.imshow(Phase[:,:,t],vmin=-4,vmax=4,\
                             aspect="auto") # levels=[-2,-1,-0,1,2]
    
    fig1.savefig("/work/m_hayashi/figure/test/Phase/phase"+str(t).zfill(3)+".png",dpi = 100)
    clf()
    close(fig1)
    


    mnum[:,:,t] = (-NY*dfdx[:,:,t] + NX*dfdy[:,:,t])
    #print mnum[:,:,t]
    print("drawing m number...t="+str(t))
    fig2 = figure(figsize=(6,5))
    ax0 = fig2.add_subplot(111)
    im0 = ax0.imshow(mnum[:,:,t],vmin=-4,vmax=4,\
                             aspect="auto") # levels=[-2,-1,-0,1,2]
    CB0 = colorbar(im0)
    fig2.savefig("/work/m_hayashi/figure/test/mnum/mnum"+str(t).zfill(3)+".png",dpi = 100)
    clf()
    close(fig2)

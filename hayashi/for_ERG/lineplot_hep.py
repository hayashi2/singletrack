# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("Agg")
from pylab import *
import numpy as np
from spacepy import pycdf
import matplotlib.ticker as tic
import matplotlib.colors as col
import matplotlib.cm as cm
import sys
import subprocess
import os

from datetime import datetime,date,timedelta
from matplotlib import gridspec

la_fontsize = 19
ti_fontsize = 16

def main():
    args = sys.argv
    print datetime.today()

    
    ###### Input check ######
    
    if (len(args)!=3):
        print "#########WARNING######### Incorrect input"
        print "Arguments  1st: start date, 2nd: end date"
        sys.exit()
        
    if(len(args[1])!= 12 or len(args[2])!=12):
        print "#########WARNING######### Incorrect input"
        print "start_date or end_date is incorrect"
        sys.exit()

    
    ###### Data acquisition ######
    start_date = args[1]
    end_date = args[2]

    s_date = datetime.strptime(start_date,"%Y%m%d%H%M")
    e_date = datetime.strptime(end_date,"%Y%m%d%H%M")

    
    for single_date in daterange(s_date, e_date):        
        year = str(single_date.year).zfill(4)
        month = str(single_date.month).zfill(2)
        day = str(single_date.day).zfill(2)
        
        hfilename = "erg_hep_pre_omniflux_"+year+month+day+"_v01.cdf"
        pfilename = "erg_orb_l2_"+year+month+day+"_v01.cdf"
      
        Hdata = ReadHEP(year,hfilename)
        position = ReadPosition(year,pfilename)
       
        HEP_L_count = Hdata["FEDO_L"]
        HEP_L_count_e = Hdata["FEDO_L_Energy"]
        HEP_L_count_e_la = Hdata["FEDO_L_Energy_LABL"][:]
        HEP_H_count = Hdata["FEDO_H"]
        HEP_H_count_e = Hdata["FEDO_H_Energy"]
        HEP_H_count_e_la = Hdata["FEDO_H_Energy_LABL"][:]
        Epoch_L = Hdata["Epoch_L"]
        Epoch_H = Hdata["Epoch_H"]
        Lm = position["pos_Lm"]
        pos = position["pos_rmlatmlt"]
        MLT = pos[:,2]
        MLAT = pos[:,1]
        p_epoch = position["epoch"]
       
        break #one loop
    
    HEP_L_count = Recreate(HEP_L_count,Epoch_L,s_date)
    HEP_H_count = Recreate(HEP_H_count,Epoch_H,s_date)
    
    Lm,MLT,MLAT = Reposition(Lm[:,0],MLT,MLAT,p_epoch,s_date)

    ###### Plot range ######
    delta_m = int(((e_date - s_date).total_seconds())/60.)
    s_hour = s_date.hour
    s_min = s_date.minute
    
    ep_st = int((s_hour*60.*60.+s_min*60.)/8)
    pos_st = int((s_hour*60.*60.+s_min*60.)/6)
    ep_ed = ep_st + int(delta_m*60./8)
    pos_ed = pos_st + int(delta_m*60./6)
    
    ###### Figure1 Setting 1 ######
    fig1 = figure(1,figsize=(10,5))
    #gs = gridspec.GridSpec(6,1,height_ratios=[12,12,3,3,3,3])
    ax1 = fig1.add_subplot(111)
    
    ax1xticks = np.linspace(0,delta_m,np.shape(HEP_L_count[ep_st:ep_ed,:])[0])
    ### Draw and Plot ###
    for i in xrange(0,5):
        c = cm.jet(i/np.float(8),1)
        HEP_L_label = HEP_L_count_e_la[i*2]
        
        if(i==0): HEP_L_label = "HEP-L < 70 keV"
        else:
            print float(HEP_L_label)
            print round(float(HEP_L_label))
            HEP_L_label = "HEP-L %.0f"%np.round(float(HEP_L_label[:])) +" keV"

        ax1.plot(ax1xticks,HEP_L_count[ep_st:ep_ed,i*2],label=HEP_L_label,color=c,lw=2)

    ax1xticks = np.linspace(0,delta_m,np.shape(HEP_H_count[ep_st:ep_ed,:])[0])
    ### Draw and Plot ###
    for i in xrange(0,3):
        c = cm.jet((i+5.)/np.float(8),1)
        HEP_H_label = HEP_H_count_e_la[i*2+1]
        HEP_H_label = "HEP-H %.0f"%np.round(float(HEP_H_label[:])) +" keV"
        ax1.plot(ax1xticks,HEP_H_count[ep_st:ep_ed,i*2+1],label=HEP_H_label,color=c,lw=2)
        
    
    ax1.set_title(str(s_date.year).zfill(4)+"-"+str(s_date.month).zfill(2)\
                      +"-"+str(s_date.day).zfill(2)+"   05:50 - 06:20"\
                      ,fontsize = la_fontsize)
    ax1.set_ylabel("HEP \ncounts",fontsize = la_fontsize)
    ax1.grid()
    ax1.legend(loc ="lower right",fontsize=la_fontsize-7 )
    ax1.set_yscale("log")
    ax1.xaxis.set_minor_locator(tic.AutoMinorLocator())
    ax1.set_xlim(0,delta_m)
    left, width = -0.04, .5
    bottom, height = -0.02, .5
    right = left + width
    top = bottom + height
    
    ax1.text(left, bottom, "hh:mm\nL\nMLT\nMLAT",horizontalalignment='right',\
                 verticalalignment='top', transform=ax1.transAxes,fontsize = ti_fontsize)
    xticks = np.arange(0,delta_m+10,10)
    ticks_L, ticks_MLT, ticks_MLAT = \
        Position_ticks(Lm[pos_st:pos_ed], MLT[pos_st:pos_ed], MLAT[pos_st:pos_ed], 10)

    x = [((s_date + timedelta(minutes=i)).time()).strftime("%H:%M") \
             +"\n"+ticks_L[count] +"\n"+ticks_MLT[count] +"\n"+ticks_MLAT[count]\
             for count,i in enumerate(xrange(0,delta_m+10,10))]
    
    ax1.set_xticks(xticks)
    ax1.set_xticklabels(x,fontsize = ti_fontsize)
    ax1.yaxis.set_label_coords(-0.07,0.5)
    ax1.xaxis.set_minor_locator(tic.AutoMinorLocator())
    ax1.tick_params(axis='y', which='major', labelsize=ti_fontsize)

    fig1.subplots_adjust(hspace=0.1,bottom=0.15)
    fig1.tight_layout()
    fig1.savefig("HEP_"+str(s_date.year).zfill(4)+str(s_date.month).zfill(2)+str(s_date.day).zfill(2)+".png",dpi=300)
    
    close(fig1)

def ReadHEP(year,filename):
    filepath = "/home/m_hayashi/ERG_data/hep/"+filename
    data = pycdf.CDF(filepath)
    return data


def ReadPosition(year,filename):
    filepath = "/work/m_hayashi/ERG_data/def/"+filename
    data = pycdf.CDF(filepath)
    return data


def Sym(s_date,e_date):
    filename = "/home/m_hayashi/ERG/Dst/sym-H20170716.txt"
    st = s_date.hour*60 + s_date.minute
    ed = e_date.hour*60 + e_date.minute
    Sym = np.loadtxt(filename,skiprows = 15,dtype="str")
    return Sym[st:ed,-1].astype("float")


def Recreate(DATA,Epoch,s_date):
    NEW_DATA = np.zeros((24*60*60/8,np.shape(DATA)[1]))
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(DATA[:,:])[0]):
        if (np.round((Epoch[0] -basetime).total_seconds()) < 0):
            DATA = DATA[1:,:]
            #last_Ep = Epoch[0]
            Epoch = Epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(DATA[:,:])[0]):
        #gr = int(np.round((Epoch[i] -basetime).total_seconds()))/8
        gr += int(np.round((Epoch[i] - last).total_seconds()/8))
        last = Epoch[i]
        if(gr>=24*60*60/8):
            print Epoch[i]
            break
        NEW_DATA[gr,:] = DATA[i,:]
    return NEW_DATA




def Reposition(Lm,MLT,MLAT,epoch,s_date):
    NEW_L = np.zeros(24*60*60/6)
    NEW_MLT = np.zeros(24*60*60/6)
    NEW_MLAT = np.zeros(24*60*60/6)
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(epoch[:])[0]-1):
        if (np.round((epoch[0] -basetime).total_seconds()) < 0):
            Lm = Lm[1:]
            MLT = MLT[1:]
            MLAT = MLAT[1:]
            #last_Ep = Epoch[0]
            epoch = epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(epoch[:])[0]-1):
        gr += int(np.round((epoch[i] -last).total_seconds()/6))
        last = epoch[i]
        if(gr>=24*60*60/6):
            print epoch[i]
            break
        NEW_L[gr] = Lm[i]
        NEW_MLT[gr] = MLT[i]
        NEW_MLAT[gr] = MLAT[i]
    return NEW_L,NEW_MLT,NEW_MLAT




def Position_ticks(L,MLT,MLAT,num):
    L_ticks, MLT_ticks,MLAT_ticks = [], [], []
    t = 0
    i = 0
    while(i <= np.size(L) ):
        #print t
        if(i == np.size(L)):
            L_ticks.append(str(round(L[i-1],1)))
            MLT_ticks.append(str(round(MLT[i-1],1)))
            MLAT_ticks.append(str(round(MLAT[i-1],1)))
        else:
            L_ticks.append(str(round(L[i],1)))
            MLT_ticks.append(str(round(MLT[i],1)))
            MLAT_ticks.append(str(round(MLAT[i],1)))
        t += num
        i = int(t*60./6)

    return L_ticks, MLT_ticks,MLAT_ticks



def daterange(s_date,e_date):
    return (s_date + timedelta(days=i) for i in range((e_date-s_date).days+1))

main()

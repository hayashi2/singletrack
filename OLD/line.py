# -*- coding: utf-8 -*-

import numpy as np
import yt
import matplotlib.pyplot as plt

bpath ="/work/ymatumot/simulation/GM-RB/run_Vsw600/"
bname = "MHD_B"
b0=10
def main():
    for t in xrange(0,100):
        
        plot_line(t)
        

def plot_line(t):
    #read magnetic field
    filename = bpath + bname + str(t).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<51750171f4")

    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*b0
    
    Bx = bf[:,:,:,0]
    By = bf[:,:,:,1]
    Bz = bf[:,:,:,2]
    
    del bf
    
    Btol = np.sqrt(Bx**2 +By**2 + Bz**2)
    
    data = dict(Bz = (Bz, "nT"),By = (By, "nT"),Bx = (Bx, "nT"))
    
    # make images
    bbox = np.array([[-30, 69], [-40, 40], [-40, 40]])
    ds = yt.load_uniform_grid(data,Bz.shape, bbox=bbox, nprocs=1)
    slc = yt.SlicePlot(ds, "y", "Bz")
    slc.set_log("Bz",True, linthresh=1.e1) #symlog scale
    slc.set_cmap("Bz", "RdBu_r")
    slc.set_zlim(field="Bz",zmin=-100,zmax=100)
    slc.annotate_line_integral_convolution("Bz","Bx",lim=(0.5,0.7)) #LIC
    slc.annotate_title("time= " +str(t*12) +"  [sec]")
    
    # save to "png" or "eps"
    slc.save("/work/m_hayashi/output/filine/line_"+str(t).zfill(3) + ".png")
    
main()
    


# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 11:48:43 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 100
ppath1 = "/work/m_hayashi/RB059/"
ppath2 = "/work/m_hayashi/RB060/"

fname = "ptl"
outpath = "/home/m_hayashi/output/hist/"
iniene1 = 5.5
iniene2 = 5.5


def main():
   t = 1
   while(t <= 200):
      clf()
      energy1,energy2=[],[]
      num=0
      
      while(num <= 127):
         filename1 = ppath1 + fname +str(num).zfill(3) +".dat"
         energy1 = np.append(energy1,readfile(filename1,t)[0])
         filename2 = ppath2 + fname +str(num).zfill(3) +".dat"
         energy2 = np.append(energy2,readfile(filename2,t)[0])         
         num+=1
      
      
      subplot(211)
      title("time="+str(t*6+6)+"s")
      hist(energy1-iniene1,bins=330,range=(-0.6,2.7),color="w",edgecolor="r",lw=1.5)
     # ylabel(str(iniene1)+"[MeV]",fontsize=20)
      xlim(-0.6,2.7)
      ylim(0,1200)
      xticks(fontsize=18)
      yticks(fontsize=18)

      subplot(212)
      hist(energy2-iniene2,bins=330,range=(-0.6,2.7),color="w",edgecolor="r",lw=1.5)
     # ylabel(str(iniene2)+"[MeV]",fontsize=20)
      xlim(-0.6,2.7)
      ylim(0,1200)
      xticks(fontsize=18)
      yticks(fontsize=18)

      draw()
      savefig(outpath+'enehis%03d.jpeg' %((t-1)/2))
      print 'saved %d'%(t*6+6)
      t+=2
      
  
  

def readfile(filename,t):
   ene=[]      
   head = ("head","<i")
   tail = ("tail","<i")
   rtime = ("rtime","<f8")
   nnp = ("np","<i4")
   maxptl =("maxptl","<i4")
   xxp = ("xp","<"+str(ptl)+"f8")
   yyp = ("yp","<"+str(ptl)+"f8")
   zzp = ("zp","<"+str(ptl)+"f8")
   llp = ("lp","<"+str(ptl)+"f8")
   xxeq = ("xeq","<"+str(ptl)+"f8")
   yyeq = ("yeq","<"+str(ptl)+"f8")
   zzeq = ("zeq","<"+str(ptl)+"f8")
   aaeq = ("aeq","<"+str(ptl)+"f8")
   bbeq = ("beq","<"+str(ptl)+"f8")
   p_ppara = ("ppara","<"+str(ptl)+"f8")
   p_pperp = ("pperp","<"+str(ptl)+"f8")
   mmu = ("mu","<"+str(ptl)+"f8")
   bbp = ("bp","<"+str(ptl)+"f8")
   pprebp = ("prebp","<"+str(ptl)+"f8")
   ddb = ("db","<"+str(ptl)+"f8")
   ppredb = ("predb","<"+str(ptl)+"f8")
   nnump = ("nump","<"+str(ptl)+"i4")
   iierr = ("ierr","<"+str(ptl)+"i4")

   dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                  aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                  iierr,tail])
               
   fd = open(filename,"r")
   chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
   xp = chunk[t]["xp"].reshape((1,ptl),order="F")
   yp = chunk[t]["yp"].reshape((1,ptl),order="F")
   zp = chunk[t]["zp"].reshape((1,ptl),order="F")
   lp = chunk[t]["lp"].reshape((1,ptl),order="F")
   xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
   yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
   zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
   aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
   beq = chunk[t]["beq"].reshape((1,ptl),order="F")
   ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
   pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
   mu = chunk[t]["mu"].reshape((1,ptl),order="F")
   bp = chunk[t]["bp"].reshape((1,ptl),order="F")
   prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
   db = chunk[t]["db"].reshape((1,ptl),order="F")
   predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
   nump = chunk[t]["nump"].reshape((1,ptl),order="F")
   ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   
   fd.close()     
   g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
   ene = (m * c**2 * (g - 1) / e)/1000000
   return ene
    
main()

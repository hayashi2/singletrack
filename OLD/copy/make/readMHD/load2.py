# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *

path = "/work/m_hayashi/MHDdata/"
name = "Vsw600_6Re(noon).npz"

k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1
"""
savd...
 np.savez("/work/m_hayashi/MHDdata/Vsw400_6Re(noon).npz)",time=rtime, bx=bx, by=by, bz=bz, ex=ex, ey=ey, ez=ez, vx=vx,vy=vy, vz=vz, N=N, P=P)
"""
def main():
    data=np.load(path+name)
    
    rtime = data["time"]
    bx = -data["bx"]
    by = -data["by"]
    bz = data["bz"]
    ex = -data["ex"]
    ey = -data["ey"]
    ez = data["ez"] 
    vx = -data["vx"]
    vy = -data["vy"]
    vz = data["vz"]
    N  = data["N"]
    P  = data["P"]
    

    T  = (P*1e-9)/(k*(N*1e6))
    B  = np.sqrt(bx**2 + by**2 + bz**2)
    V  = np.sqrt(vx**2 + vy**2 + vz**2 )
    Ma = (V*(N**(0.5)))/(20*B)
    Va = 20.3*B/np.sqrt(N)
    Vth = 3.0*k*T/1.67e-23
    Vs  = 0.12*(T+1.28*10e5)**(0.5)
    Vms = np.sqrt(Vs**2+Va**2)
    Mms = V/Vms
    Pdyn = (2e-6)*N*(V**2)
   # Pt = P + (B**2)/2
         
    subplots_adjust(hspace=0.7)
    subplot(3,1,1)
    title("Vsw600_(7,4)")
    plot(rtime,Va)
    ylabel("Va[km/s]")
   # ylim(1200,2800)
    ylim(3000,5000)
    locator_params(axis="x",tight=True,nbins=20)
    locator_params(axis="y",tight=True,nbins=5)
    
    subplot (3,1,2)
    plot(rtime,Vms)
    ylabel("Vms[km/s]")
    #ylim(1200,2800)
    ylim(3000,5000)
    locator_params(axis="x",tight=True,nbins=20)
    locator_params(axis="y",tight=True,nbins=5)
    
    subplot(3,1,3)
    plot(rtime,ey)
    ylabel("Ey[mV/m]")
    xlabel("time[sec]")
    ylim(np.min(ey)*1.1,np.max(ey)*1.1)
    locator_params(axis="y",tight=True,nbins=5)
    locator_params(axis="x",tight=True,nbins=20)


    show()
    #savefig( "Vsw600 plot("+str(x1)+","+str(y1)+").jpeg")
    
main()

# -*- coding: utf-8 -*-

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

path = "/work/ymatumot/simulation/GM-RB/run_Vsw400/"
name = "MHD_B"

"""
-30 <= x <= 69 (Re)
-40 <= y <= 40 (Re)
""" 
x1 = -20
y1 = 0
x2 = -7
y2 = 0
x3 = 10
y3 = 0


def main():
    b1,b2,b3 = [],[],[]
    t = []
    time = 0
    while(time <= 208):
        
        filename = path + name + str(time).zfill(3) + ".dat"
        
        head = ("head","<i")
        tail = ("tail","<i")
        bz = ("bz","<51750171f4")
         
        dt = np.dtype([head,bz,tail])
        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
    
        bf = chunk[0]["bz"].reshape((297,241,241,3),order="F")
        bt = bf.transpose(1,0,2,3)

        b1 = np.append(b1,bt[ny(y1),nx(x1),120,2])
        b2 = np.append(b2,bt[ny(y2),nx(x2),120,2])
        b3 = np.append(b3,bt[ny(y3),nx(x3),120,2])
        t = np.append(t,time*12)
        time += 1
        
    subplots_adjust(hspace = 0.9)
    subplot(3,1,1)
    plot(t,b1)
    xlabel("time[sec]")
    ylabel("Bz[nT]")
    title("("+str(x1)+","+str(y1)+")")
    
    subplot(3,1,2)
    plot(t,b2)
    xlabel("time[sec]")
    ylabel("Bz[nT]")
    title("("+str(x2)+","+str(y2)+")")
    
    subplot(3,1,3)
    plot(t,b3) 
    xlabel("time[sec]")
    ylabel("Bz[nT]")
    title("("+str(x3)+","+str(y3)+")")
    
    show()
# savefig("EphiL"+str(time/24)+".jpeg")
 #   print "saved ",time

def nx(x):
    return int(x*3 + 90)

def ny(y):
    return int(y*3 + 121)

main()

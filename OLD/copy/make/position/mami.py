# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 10:30:51 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 100
ppath = /work/m_hayashi/RB001/
fname = ptl
def main():
    maxe,mine=[],[]
    num=0
    while(num <= 127):
         filename= ppath + fname +str(num).zifll(3) +".dat"
        a,b = readfile(filename)
        maxe = np.append(maxe,a)
        mine = np.append(mine,b)
        num+=1        
        print 'finish %i'%num
    print max(maxe),min(mine)
        
def readfile(filename):
    
    ene=[]
       head = ("head","<i")
tail = ("tail","<i")
rtime = ("rtime","<f8")
nnp = ("np","<i4")
maxptl =("maxptl","<i4")
xxp = ("xp","<"+str(ptl)+"f8")
yyp = ("yp","<"+str(ptl)+"f8")
zzp = ("zp","<"+str(ptl)+"f8")
llp = ("lp","<"+str(ptl)+"f8")
xxeq = ("xeq","<"+str(ptl)+"f8")
yyeq = ("yeq","<"+str(ptl)+"f8")
zzeq = ("zeq","<"+str(ptl)+"f8")
aaeq = ("aeq","<"+str(ptl)+"f8")
bbeq = ("beq","<"+str(ptl)+"f8")
p_ppara = ("ppara","<"+str(ptl)+"f8")
p_pperp = ("pperp","<"+str(ptl)+"f8")
mmu = ("mu","<"+str(ptl)+"f8")
bbp = ("bp","<"+str(ptl)+"f8")
pprebp = ("prebp","<"+str(ptl)+"f8")
ddb = ("db","<"+str(ptl)+"f8")
ppredb = ("predb","<"+str(ptl)+"f8")
nnump = ("nump","<"+str(ptl)+"i4")
iierr = ("ierr","<"+str(ptl)+"i4")

  dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
               aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
               iierr,tail])
               
               fd = open(filename,"r")
chunk = np.fromfile(fd, dtype=dt,count=414)
    
    xp = chunk[n]["xp"].reshape((1,ptl),order="F")
yp = chunk[n]["yp"].reshape((1,ptl),order="F")
zp = chunk[n]["zp"].reshape((1,ptl),order="F")
lp = chunk[n]["lp"].reshape((1,ptl),order="F")
xeq = chunk[n]["xeq"].reshape((1,ptl),order="F")
yeq = chunk[n]["yeq"].reshape((1,ptl),order="F")
zeq = chunk[n]["zeq"].reshape((1,ptl),order="F")
aeq = chunk[n]["aeq"].reshape((1,ptl),order="F")
beq = chunk[n]["beq"].reshape((1,ptl),order="F")
ppara = chunk[n]["ppara"].reshape((1,ptl),order="F")
pperp = chunk[n]["pperp"].reshape((1,ptl),order="F")
mu = chunk[n]["mu"].reshape((1,ptl),order="F")
bp = chunk[n]["bp"].reshape((1,ptl),order="F")
prebp = chunk[n]["prebp"].reshape((1,ptl),order="F")
db = chunk[n]["db"].reshape((1,ptl),order="F")
predb = chunk[n]["predb"].reshape((1,ptl),order="F") 
nump = chunk[n]["nump"].reshape((1,ptl),order="F")
ierr = chunk[n]["ierr"].reshape((1,ptl),order="F")
    
    close(filename)     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)       
    ene = np.append(ene,  (m * c**2 * (g - 1) / e)/1000000)
    print max(ene)
    return max(ene),min(ene)
    
main()

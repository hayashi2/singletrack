from pylab import*
import numpy as np
import matplotlib.cm as cm

def main():
    erange = np.array([0.5,1,1.5,2,3,4,4.5,5,5.5,6,8,12])
    erange2 = np.array([0.5,1,1.5,2,3,4,4.5,5,5.5,6,7,8,12])
    RE6 = np.array([0.137 ,0.238 ,0.321 ,0.395 ,0.597 ,0.730 ,0.774 ,0.815 ,0.841 ,0.848 ,1.058 ,1.595 ])
    RE7 = np.array([0.316 ,0.660 ,0.858 ,0.997 ,1.137 ,1.232 ,1.273 ,1.290 ,1.303 ,1.375 ,1.734 ,2.597 ])
    RE8 = np.array([0.568 ,0.980 ,1.224 ,1.429 ,1.604 ,1.661 ,1.770 ,1.769 ,1.969 ,2.145 ,2.450 ,2.680 ,3.650 ])
    
    rrange = np.array([6,7,8])
    MeV1 = np.array([0.238 ,0.660 ,0.980])
    MeV2 = np.array([0.395 ,0.997 ,1.429 ])
    MeV3 = np.array([0.597 ,1.137 ,1.604 ])
    MeV4 = np.array([0.730 ,1.232 ,1.661 ])
    MeV5 = np.array([0.815 ,1.290 ,1.769 ])
    #5.5MeV = np.array([0.841 ,1.303 ,1.969])
    #6MeV = np.array([0.848 ,1.375,2.145])
    MeV8 = np.array([1.058 ,1.734 ,2.680 ])
    MeV12 = np.array([1.595 ,2.597 ,3.650 ])
    
    figure(1,figsize=(7,7))
    plot(erange2,RE8,label="8Re",marker="o",linestyle="--",lw=2,color="g")
    plot(erange,RE7,label="7Re",marker="o",linestyle="--",lw=2,color="r")
    plot(erange,RE6,label="6Re",marker="o",linestyle="--",lw=2,color="b")
   
    legend(loc="lower right",fontsize=16)
    xticks([0,5,10],fontsize=18)
    yticks(fontsize=18)
    minorticks_on()
    grid()
    xlabel("W$_0$ [MeV]",fontsize=22)
    ylabel("$ \Delta $W [MeV]",fontsize=22)

    figure(2,figsize=(7,7))
    plot(rrange,MeV12,label="12MeV",marker="o",linestyle="-",lw=1.5,color=cm.jet(6./6.))  
    plot(rrange,MeV8,label="8MeV",marker="o",linestyle="-",lw=1.5,color=cm.jet(5./6.))
    plot(rrange,MeV5,label="5MeV",marker="o",linestyle="-",lw=1.5,color=cm.jet(4./6.))
    plot(rrange,MeV4,label="4MeV",marker="o",linestyle="-",lw=1.5,color=cm.jet(3./6.))
    plot(rrange,MeV3,label="3MeV",marker="o",linestyle="-",lw=1.5,color=cm.jet(2./6.))
    plot(rrange,MeV2,label="2MeV",marker="o",linestyle="-",lw=1.5,color=cm.jet(1./6.))
    plot(rrange,MeV1,label="1MeV",marker="o",linestyle="-",lw=1.5,color=cm.jet(0./6.))

    legend(loc="upper left",fontsize=14)
    xticks([6,7,8],fontsize=18)
    yticks(fontsize=18)
    minorticks_on()
    grid()
    xlabel("L$_0$ ",fontsize=22)
    ylabel("$ \Delta $W [MeV]",fontsize=22)
    

    show()

main()

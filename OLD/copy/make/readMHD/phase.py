# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *
from scipy.signal import hilbert,chirp
from matplotlib.ticker import AutoMinorLocator
import matplotlib.patches as mpatches
import matplotlib.ticker as tick
path = "/work/m_hayashi/MHDdata/"
name = "Vsw600_E.npz"

k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1

phase=np.empty([241,297,201])

def main():
    data=np.load(path+name)
    ex = data["ex"]
    ey = data["ey"]
    ez = data["ez"] 
    ephi = data["ephi"]

    for y in range(0,241):
        for x in range(0,297):
            
            analytic_E = hilbert(ephi[y,x,:])
            amplitude_envelope = np.abs(analytic_E)
            #ins_phase = np.unwrap(np.angle(analytic_E))
            ins_phase = np.angle(analytic_E)
            ins_fre =np.diff(ins_phase)/(2.0*np.pi)*201
    
            phase[y,x,:] = ins_phase

    for t in range(0,101):
        print t
    #figure
        figure(figsize=(10,8))
        #plot(phase[120,69,:])
        
        imshow(phase[88:154,57:123,t],extent=[11,-11,11,-11],origin="lower", vmin=-3.14,vmax=3.14)
        title("time= "+str(t*12),fontsize=22)
        xlabel("x [Re]",fontsize=22)
        ylabel("y [Re]",fontsize=22)
        xticks(fontsize=20)
        yticks(fontsize=20)
        gca().xaxis.set_major_locator(tick.MultipleLocator(10))
        CB = colorbar(shrink=1)
        CB.set_label("phase [rad]",fontsize=22)
        CB.set_ticks(np.arange(-np.pi,np.pi*3/2,np.pi/2))
        CB.ax.set_yticklabels(["-$ \pi $","-$ \pi/2 $","$0$","$ \pi/2 $","$ \pi $"],fontsize=22)

        wedge0 = mpatches.Wedge((0,0), 5, -180, 180,fc='w',linewidth = 0)
        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
        fig = gcf()
        fig.gca().add_artist(wedge0)
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)
    
        #ax0.xaxis.set_minor_locator(AutoMinorLocator())
        #ax0.yaxis.set_minor_locator(AutoMinorLocator())    
        savefig("/home/m_hayashi/output/phase/phase"+str(t).zfill(3)+".jpeg")
        
main()

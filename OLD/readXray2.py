# -*- coding: utf-8 -*-
"""
Created on Sat Jan 23 17:46:25 2016

@author: Miyoshi
"""

import numpy as np
from pylab import *

path = "/work/m_hayashi/Xray/Vsw600/"
name = "MHDXray_"
time = 1
#time=18

# xray=np.empty([297,241,241])

image_view=np.empty([19,19])

data = np.load(path+name+str(time).zfill(3)+".npz")
xray = data["Xray"]

for i in range(0,297):
  for j in range(0,241):
    for k in range(0,241):
      xpoint=30.0-(i-1)*3./10.
      ypoint=40.0-(j-1)*3./10.
      zpoint=-40.0+(k-1)*3./10.
      r=np.sqrt(xpoint**2+ypoint**2+zpoint**2)
      if (r >= 5.) and (r <= 20.):
        if (xpoint != 0.):
          theta=np.arcsin(zpoint/r)*180./np.pi
          phi=np.arctan2(ypoint,xpoint)*180./np.pi
          for l in range(0,19):
            theta_min=-90.0+(l-1)*10.0
            theta_max=-90.0+l*10.
            if (theta_min <= theta) and (theta < theta_max):
              for m in range(0,19):
                phi_min=-180.0+(m-1)*20.0
                phi_max=-180.0+m*20.
                if (phi_min <= phi) and (phi < phi_max):
                  image_view[l,m]=image_view[l,m]+xray[i,j,k]

imshow(image_view)
show()

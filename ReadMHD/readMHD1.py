c# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 13:30:36 2015

@author: m_hayashi
"""

import numpy as np
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.image as mpimg

path = "/work/m_hayashi/GM/run_Vsw600/"  
gx = 297
gy = 241
gz = 241
BOXSIZE = gx*gy*gz*3


def main():
    for i in xrange(0,200):

        filename = "MHD_B" + str(i).zfill(3) + ".dat"
        B=readB(filename)
        fig = plt.figure(1)
        ax = fig.add_subplot(111)
        
        imgplot = ax.imshow(B[:,:,121,2].T,origin="lower", cmap = 'jet',vmin=-1,vmax=6)
        fig.colorbar(imgplot)
        
        plt.xticks([0,90,297],["-30","0","69"])
        plt.yticks([0,120,241],["-40","0","40"])

        ax.set_xlabel("X[Re]",fontsize=22)
        ax.set_ylabel("Y[Re]",fontsize=22)

        fig = plt.savefig("/work/c0099ito/figure/GM/Bre/Bz"+str(i).zfill(3)+".png")
        print ("save",i)
        plt.close()
        plt.clf()





def readB(filename):
     head = ("head","<i")
     tail = ("tail","<i")
     b=("b","<"+str(BOXSIZE)+"f4")
     dt = np.dtype([head,b,tail])
     fd = open(path + filename,"r")
     chunk = np.fromfile(fd, dtype=dt,count=1)
     fd.close()
     bf = chunk[0]["b"].reshape((297,241,241,3),order="F")
     return bf

main()

# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 13:30:36 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
time = 0

while(time < 2400):
    
    path = "/work/saito/GMHDRB/inputdata/"
    ename = "MHDE_RB"

    filename = path + ename + str(time).zfill(6) + ".dat"

    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<6291456f4")
         
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["b"].reshape((128,128,128,3),order="F")
    list=bf.transpose(1,0,2,3)*(1e3)

    figure(figsize=(10,10))
   
    imshow(list[31:97,31:97,64,1],extent=[-21.33,21.33,-21.33,21.33],origin='lower',vmax=8,vmin=-14)  #Ey
    title("time="+str(time)+"s")
    ax=colorbar()    
    ax.set_label('Ey [mV/m]',fontsize=15)
    ax.set_clim(-14,8)  
    xlabel("x(Re)")
    ylabel("y(Re)")
    
    wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
    wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
    fig = gcf()
    fig.gca().add_artist(wedge1)
    fig.gca().add_artist(wedge2)

    savefig("Ey"+str(time/24)+".jpeg")
    print "saved ",time
    time += 24


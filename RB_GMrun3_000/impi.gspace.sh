#!/bin/sh

#$ -S /bin/sh
#$ -cwd
#$ -V
#$ -q all.q
#$ -pe openmpi36 144

export OMP_NUM_THREADS=1
#MPISLOTS=`expr $NSLOTS / $OMP_NUM_THREADS`
mpirun -x OMP_NUM_THREADS=$OMP_NUM_THREADS -np  $NSLOTS ./rb.exe


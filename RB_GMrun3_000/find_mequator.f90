!######################################################################
module find_mequator
!######################################################################

  use prm, only: re
  use inp, only: rmin_re, rmax_re
  use itp_emf, only: itp_emf__exe

  implicit none
  private
  public :: find_mequator__exe

contains
!######################################################################

   subroutine find_mequator__exe(rtime, emf0, emf1, x, y, z, &
                               & x0, y0, z0, b0, ierr)
     
   real(8), intent(in) :: rtime, emf0(:,:,:,:), emf1(:,:,:,:)
   real(8), intent(in) :: x, y, z
   real(8), intent(out):: x0, y0, z0, b0
   integer, intent(out):: ierr

   real(8) :: f(3), bx, by, bz, ex, ey, ez, b1, b1min, r, ds
   real(8) :: xmin, ymin, zmin, rmin, rmax
   real(8) :: k0(3), k1(3), k2(3), k3(3)
   integer :: n, ds_counter

   rmin = rmin_re * re
   rmax = rmax_re * re

   ierr = 0
   ds_counter = 0
   ds = 0.01d0 * re
   x0 = x
   y0 = y
   z0 = z
   xmin = x
   ymin = y
   zmin = z
   n = 0

   call itp_emf__exe(rtime, emf0, emf1, x0, y0, z0, &
                   & bx, by, bz, ex, ey, ez, ierr)
   b1min = dsqrt(bx**2 + by**2 + bz**2)

   trace_loop : do
     f(1) = x0
     f(2) = y0
     f(3) = z0
     call itp_emf__exe(rtime, emf0, emf1, f(1), f(2), f(3), &
                     & bx, by, bz, ex, ey, ez, ierr)
     b1 = dsqrt(bx**2 + by**2 + bz**2)
     k0(1) = ds * bx / b1
     k0(2) = ds * by / b1
     k0(3) = ds * bz / b1
     f(1) = x0 + 0.5d0 * k0(1)
     f(2) = y0 + 0.5d0 * k0(2)
     f(3) = z0 + 0.5d0 * k0(3)
     call itp_emf__exe(rtime, emf0, emf1, f(1), f(2), f(3), &
                     & bx, by, bz, ex, ey, ez, ierr)
     b1 = dsqrt(bx**2 + by**2 + bz**2)
     k1(1) = ds * bx / b1
     k1(2) = ds * by / b1
     k1(3) = ds * bz / b1
     f(1) = x0 + 0.5d0 * k1(1)
     f(2) = y0 + 0.5d0 * k1(2)
     f(3) = z0 + 0.5d0 * k1(3)
     call itp_emf__exe(rtime, emf0, emf1, f(1), f(2), f(3), &
                     & bx, by, bz, ex, ey, ez, ierr)
     b1 = dsqrt(bx**2 + by**2 + bz**2)
     k2(1) = ds * bx / b1
     k2(2) = ds * by / b1
     k2(3) = ds * bz / b1
     f(1) = x0 + k2(1)
     f(2) = y0 + k2(2)
     f(3) = z0 + k2(3)
     call itp_emf__exe(rtime, emf0, emf1, f(1), f(2), f(3), &
                     & bx, by, bz, ex, ey, ez, ierr)
     b1 = dsqrt(bx**2 + by**2 + bz**2)
     k3(1) = ds * bx / b1
     k3(2) = ds * by / b1
     k3(3) = ds * bz / b1
     x0 = x0 + (k0(1) + 2 * k1(1) + 2 * k2(1) + k3(1)) / 6d0
     y0 = y0 + (k0(2) + 2 * k1(2) + 2 * k2(2) + k3(2)) / 6d0
     z0 = z0 + (k0(3) + 2 * k1(3) + 2 * k2(3) + k3(3)) / 6d0
     call itp_emf__exe(rtime, emf0, emf1, x0, y0, z0, &
                     & bx, by, bz, ex, ey, ez, ierr)
     b1 = dsqrt(bx**2 + by**2 + bz**2)
     if (b1<b1min) then
       b1min = b1
       xmin = x0
       ymin = y0
       zmin = z0
     end if

     r = dsqrt(x0**2 + y0**2 + z0**2)
     if (r < rmin) then
       ds = -ds
       ds_counter = ds_counter + 1
     end if

     if (r > rmax) then
       ierr = -1 + ierr
       exit
     end if

     if (ds_counter >= 2) exit

   end do trace_loop

   x0 = xmin
   y0 = ymin
   z0 = zmin
   b0 = b1min

 end subroutine find_mequator__exe

!######################################################################
end module find_mequator
!######################################################################

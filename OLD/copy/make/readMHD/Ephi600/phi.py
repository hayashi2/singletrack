import numpy as np
from pylab import *
import matplotlib.ticker as tick
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable


time = 0
nx = np.arange(90,-207,-1)
ny = (np.ones(297))*120
na = np.empty([241,297])
nb = np.empty([241,297])
n=0
while(n<=240):
    na[n] = nx
    nb[n] = ny - n
    n+=1
    
cosphi=na/np.sqrt((na)**2+(nb)**2)
sinphi=nb/np.sqrt((na)**2+(nb)**2)

print cosphi
print sinphi

while(time <= 100 ):
    
    path = "/work/ymatumot/simulation/GM-RB/run_Vsw600/"
    ename = "MHD_E"

    filename = path + ename + str(time).zfill(3) + ".dat"

    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<51750171f4")
         
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
     #Convert Global-MHD coodinate ->GSM
    bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*2.2
    bt=bf.transpose(1,0,2,3)
    ey = -bt[:,:,120,1]
    ex = -bt[:,:,120,0]

    ephi = ey*cosphi - ex*sinphi

    figure(figsize=(11,7.2))
    #Convert Global-MHD coodinate ->GSM
    """
    xlim(30,-69)
    ylim(40,-40)
    imshow(ephi,extent=[30,-69,40,-40],origin='lower', vmax=10,vmin=-10)
    """
    clf()
    imshow(ephi[66:174,27:153,],extent=[21,-21,18,-18],origin="lower",aspect="auto",vmax=10,vmin=-10)
    xlabel("x(GSM)[Re]",fontsize=18)
    ylabel("y(GSM)[Re]",fontsize=18)
    xticks(fontsize=18)
    yticks(fontsize=18)
    gca().xaxis.set_major_locator(tick.MultipleLocator(10))
    title("time="+str(time*12)+"sec",fontsize=18)
    grid(color="gray",linestyle="--",linewidth=0.5)
    CB=colorbar()   
    CB.set_label('Ephi [mV/m]',fontsize=20)
    CB.ax.tick_params(labelsize=15)
    #CB.set_clim(-4.84,2.64)  
    
    wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
    wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
    fig = gcf()
    fig.gca().add_artist(wedge1)
    fig.gca().add_artist(wedge2)

    savefig("/work/m_hayashi/output/Ephi_0/Ephi"+str(time).zfill(3)+".jpeg")
    print "saved ",time*12
    time += 1


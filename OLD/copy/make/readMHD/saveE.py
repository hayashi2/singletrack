# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
#save Eq plane's E data 
import numpy as np
from pylab import *

patha = "/work/ymatumot/simulation/GM-RB"
pathb = "/run_Vsw600/"
bname = "MHD_B"
ename = "MHD_E"
dname = "MHD_"
k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1
ex = np.empty((241,297,201))
ey = np.empty((241,297,201))
ez = np.empty((241,297,201))
ephi = np.empty((241,297,201))
#create cos and sin
nx = np.arange(90,-207,-1)
ny = (np.ones(297))*120
na = np.empty([241,297])
nb = np.empty([241,297])
i=0
while(i<=240):
    na[i] = nx
    nb[i] = ny - i
    i+=1

cosphi=na/np.sqrt((na)**2+(nb)**2)
sinphi=nb/np.sqrt((na)**2+(nb)**2)

def main():
    for t in range(0,201) :
        ex[:,:,t],ey[:,:,t],ez[:,:,t],ephi[:,:,t] =  readE(t)

    np.savez("/work/m_hayashi/MHDdata/Vsw600_E.npz", ex=ex, ey=ey, ez=ez,ephi=ephi) 


def readE(time):
    efilename = patha + pathb + ename + str(time).zfill(3) + ".dat"
    
    head = ("head","<i")
    tail = ("tail","<i")
    e = ("e","<51750171f4")
    
    dt = np.dtype([head,e,tail])
    fd = open(efilename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    ef = chunk[0]["e"].reshape((297,241,241,3),order="F")*e0
    #vertical=>y  horizontal=>x
    et = ef.transpose(1,0,2,3)
    e1 = -et[:,:,120,0] #convert GSM
    e2 = -et[:,:,120,1]
    e3 = et[:,:,120,2]
    phi = e2*cosphi - e1*sinphi

    print "read... E"+str(time)
        
    return e1[:,:],e2[:,:],e3[:,:],phi[:,:]
main()

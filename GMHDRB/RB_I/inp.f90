!######################################################################
module inp
!######################################################################
  implicit none
  character(LEN=128), save :: gmpath, gmb, gme, outpath
  integer, save :: set_seed
  real(8), save :: dsec, bn0, en0
  integer, save :: nx, ny, nz, nxc, nyc, nzc
  real(8), save :: dx_re, dy_re, dz_re
  real(8), save :: dt, calc_start, calc_end, ptlout
  integer, save :: maxptl
  real(8), save :: loss_hight, rmin_re, rmax_re , &
                 & rpmin_re, rpmax_re, apmin_deg, apmax_deg, epmin_ev, epmax_ev

  namelist/GMHDINFO/gmpath, gmb, gme, dsec, bn0, en0
  namelist/BOXSIZE/nx, ny, nz, nxc, nyc, nzc, dx_re, dy_re, dz_re
  namelist/TIMES/dt, calc_start, calc_end, ptlout
  namelist/PARTICLES/maxptl, set_seed, rpmin_re, rpmax_re, &
                   & apmin_deg, apmax_deg, epmin_ev, epmax_ev
  namelist/SYSTEMRANGE/loss_hight, rmin_re, rmax_re
  namelist/FILES/outpath

  private status_checker__exe

contains
!######################################################################
  subroutine read_inp_file(irank)
  integer, intent(in) :: irank
  integer :: openstatus, readstatus, ierr
  character(LEN=128) :: txtline

! FILE OPEN ----------------------
! OPEN PARAMETER FILE: "RB.INP"
  open(unit = 100, file = "rb.inp", form = "formatted", &
     & status = "old", action = "read", iostat = openstatus)
  txtline="   [ Read: rb.inp ]   "
  call status_checker__exe(txtline, irank, openstatus)
! --------------------------------

! READ TIME STEP FROM "RB.INP"
  read(unit = 100, nml  = GMHDINFO, iostat = readstatus)
  txtline = "INP FILE READ: GMHDINFO"
  call status_checker__exe(txtline, irank, readstatus)
! --------------------------------
  read(unit = 100, nml  = BOXSIZE, iostat = readstatus)
  txtline = "INP FILE READ: BOXSIZE"
  call status_checker__exe(txtline, irank, readstatus)
! --------------------------------
  read(unit = 100, nml  = TIMES, iostat = readstatus)
  txtline = "INP FILE READ: TIMES"
  call status_checker__exe(txtline, irank, readstatus)
! --------------------------------
  read(unit = 100, nml  = PARTICLES, iostat = readstatus)
  txtline = "INP FILE READ: PARTICLES"
  call status_checker__exe(txtline, irank, readstatus)
! --------------------------------
  read(unit = 100, nml  = SYSTEMRANGE, iostat = readstatus)
  txtline = "INP FILE READ: SYSTEMRANGE"
  call status_checker__exe(txtline, irank, readstatus)
! --------------------------------
  read(unit = 100, nml  = FILES, iostat = readstatus)
  txtline = "INP FILE READ: FILES"
  call status_checker__exe(txtline, irank, readstatus)
! --------------------------------

  close(100)

  end subroutine read_inp_file
!######################################################################
  subroutine status_checker__exe(txtline, irank, istatus)
    use mpi
    character(128), intent(in) :: txtline
    integer, intent(in) :: irank, istatus
    integer :: ierr, openstatus
    character(128) :: filename

    if (istatus /= 0) then
      write(filename, '(a, i3.3, a)')  "inp.ERROR", irank, ".txt"
      open(unit=99, file=filename, form="formatted", &
         & status="new", action="write", iostat=openstatus)
      if (openstatus /= 0) then
        open(unit=99, file=filename, form="formatted", &
           & status="old", action="write", position="append", iostat=openstatus)
      end if
      write(99, *) "STATUS CHECK: ERROR"
      write(99, *) "SUM STATUS =", istatus
      write(99, *) "CALCULATION STOPPED."
      write(99, *) txtline
      close(99)
      call mpi_abort(mpi_comm_world, 9, ierr)
      call mpi_finalize(ierr)
    end if

  end subroutine status_checker__exe
!######################################################################
end module inp
!######################################################################

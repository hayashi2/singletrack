# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("Agg")
from pylab import *
import numpy as np
from spacepy import pycdf
import matplotlib.ticker as tic
import matplotlib.colors as col
import matplotlib.cm as cm
import sys
import subprocess
import os
from datetime import datetime,date,timedelta
from matplotlib import gridspec
from scipy import signal
from scipy.signal import find_peaks_cwt

def main():
    args = sys.argv
    print datetime.today()

    
    ###### Input check ######
    
    if (len(args)!=3):
        print "#########WARNING######### Incorrect input"
        print "Arguments  1st: start date, 2nd: end date"
        sys.exit()
        
    if(len(args[1])!= 12 or len(args[2])!=12):
        print "#########WARNING######### Incorrect input"
        print "start_date or end_date is incorrect"
        sys.exit()

    
    ###### Data acquisition ######
    start_date = args[1]
    end_date = args[2]

    s_date = datetime.strptime(start_date,"%Y%m%d%H%M")
    e_date = datetime.strptime(end_date,"%Y%m%d%H%M")

    
    for single_date in daterange(s_date, e_date):        
        year = str(single_date.year).zfill(4)
        month = str(single_date.month).zfill(2)
        day = str(single_date.day).zfill(2)
        
        filename = "erg_hep_pre_omniflux_"+year+month+day+"_v01.cdf"
        pfilename = "erg_orb_l2_"+year+month+day+"_v01.cdf"
        Hdata = ReadHEP(year,filename)
        position = ReadPosition(year,pfilename)
        
        HEP_L_count = Hdata["FEDO_L"]
        HEP_L_count_e = Hdata["FEDO_L_Energy"]
        HEP_L_count_e_la = Hdata["FEDO_L_Energy_LABL"]
        HEP_H_count = Hdata["FEDO_H"]
        HEP_H_count_e = Hdata["FEDO_H_Energy"]
        HEP_H_count_e_la = Hdata["FEDO_H_Energy_LABL"]
        Epoch_L = Hdata["Epoch_L"]
        Epoch_H = Hdata["Epoch_H"]
        Lm = position["pos_Lm"]
        pos = position["pos_rmlatmlt"]
        MLT = pos[:,2]
        MLAT = pos[:,1]
        p_epoch = position["epoch"]
        break #one loop
    
    HEP_L_count = Recreate(HEP_L_count,Epoch_L,s_date)
    HEP_H_count = Recreate(HEP_H_count,Epoch_H,s_date)
    Lm,MLT,MLAT = Reposition(Lm[:,0],MLT,MLAT,p_epoch,s_date)
    
    ###### Plot range ######
    delta_m = int(((e_date - s_date).total_seconds())/60.)
    s_hour = s_date.hour
    s_min = s_date.minute
    
    ep_st = int((s_hour*60.*60.+s_min*60.)/8)
    pos_st = int((s_hour*60.*60.+s_min*60.)/6)
    ep_ed = ep_st + int(delta_m*60./8)
    pos_ed = pos_st + int(delta_m*60./6)
    
    #HEP_L_count = np.loadtxt("/home/m_hayashi/data/erg_hep_l2ql_FEDO_L_clip.txt",dtype=str)
    #HEP_L_count = HEP_L_count[:,1:].astype(float)

    ###### Figure1 Setting 1 ######
    fig1 = figure(1)
    ax1 = fig1.add_subplot(111)
    
    ### Draw and Plot ###
    x_t,y_u = [],[]
    w,f = [],[]
    ini_f =[] 
    for i in xrange(1,np.shape(HEP_L_count)[1]-5):
        c = cm.jet(i/np.float(np.shape(HEP_L_count)[1]-5-4),1)
        HEP_L_label = str(int(HEP_L_count_e[0,i]))+" - "+str(int(HEP_L_count_e[1,i])) 
        yhat = savitzky_golay(HEP_L_count[2*60*60/8+1:2*60*60/8+1+40*60/8,i],21,3)
        maxId = signal.argrelmax(yhat)
        maxIdm = signal.argrelmin(yhat)
        W = HEP_L_count_e_la[i].astype(float)
        print maxId
        W = W*1e3
        ini_f.append(yhat[0])

        for m in maxId[0]:
            if(30 < m < 50):   #目打ちしている
                v = calv(W,R=6)
                print v
                w.append(W/1e3)
                f.append(yhat[m])
                x_t.append(m*8)
                y_u.append(1./v)
                label=str(HEP_L_count_e_la[i])+" keV"
                ax1.scatter(m*8,1./v,label=label,color=c)
                break


    fig = figure()
    ax = fig.add_subplot(111)
    ax.plot(w,ini_f,"o-",color ="b",label="pre-shock")
    ax.plot(w,f,"o-",color ="r",label="shock injected")
    ax.set_xlabel("Energy [keV]")
    ax.legend(loc="upper right")
    #ax.set_ylabel("$cm^{-2} str^{-1} s^{-1} MeV^{-1} $",fontsize =18 )
    ax.set_ylabel("counts",fontsize=18)
    ax.grid()
    ax.set_yscale("log")
    ax.xaxis.set_minor_locator(tic.AutoMinorLocator())
    #ax.set_title("Arase HEP-L Electron Omni flux")
    ax.set_titile("Arase HEP-L")
    fig.savefig("HEP_L_spec.png")

def calv(W,R):
    c = 3.0e8 #m/s
    m0 = 9.109e-31 #kg
    e = 1.602e-19
    Cb = 1.557e4
    B0 = 3.12e-5
    Re = 6370e3 #m
    W = W*e  #eV => J
    R = R*Re # Re =>m
    #gamma = W/(m0*c**2)
    #print "gamma:",gamma
    #gbb = (gamma**2-1.)/gamma
    #td = Cb*(Re/R)*(1.-0.3333)/gbb
    td = np.pi*e*B0*Re**3/W/R*(1-0.3333)
    vd = 2.*np.pi*R/td
    vd = vd/1e3 #m/s => km/s
    return vd


def ReadHEP(year,filename):
    filepath = "/home/m_hayashi/ERG_data/hep/"+filename
    data = pycdf.CDF(filepath)
    return data


def ReadPosition(year,filename):
    filepath = "/work/m_hayashi/ERG_data/def/"+filename
    data = pycdf.CDF(filepath)
    return data



def Recreate(DATA,Epoch,s_date):
    NEW_DATA = np.zeros((24*60*60/8,np.shape(DATA)[1]))
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(DATA[:,:])[0]):
        if (np.round((Epoch[0] -basetime).total_seconds()) < 0):
            DATA = DATA[1:,:]
            #last_Ep = Epoch[0]
            Epoch = Epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(DATA[:,:])[0]):
        #gr = int(np.round((Epoch[i] -basetime).total_seconds()))/8
        gr += int(np.round((Epoch[i] - last).total_seconds()/8))
        last = Epoch[i]
        if(gr>=24*60*60/8):
            print Epoch[i]
            break
        NEW_DATA[gr,:] = DATA[i,:]
    return NEW_DATA




def Reposition(Lm,MLT,MLAT,epoch,s_date):
    NEW_L = np.zeros(24*60*60/6)
    NEW_MLT = np.zeros(24*60*60/6)
    NEW_MLAT = np.zeros(24*60*60/6)
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(epoch[:])[0]-1):
        if (np.round((epoch[0] -basetime).total_seconds()) < 0):
            Lm = Lm[1:]
            MLT = MLT[1:]
            MLAT = MLAT[1:]
            #last_Ep = Epoch[0]
            epoch = epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(epoch[:])[0]-1):
        gr += int(np.round((epoch[i] -last).total_seconds()/6))
        last = epoch[i]
        if(gr>=24*60*60/6):
            print epoch[i]
            break
        NEW_L[gr] = Lm[i]
        NEW_MLT[gr] = MLT[i]
        NEW_MLAT[gr] = MLAT[i]
    return NEW_L,NEW_MLT,NEW_MLAT




def daterange(s_date,e_date):
    return (s_date + timedelta(days=i) for i in range((e_date-s_date).days+1))



def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
    the values of the time history of the signal.
    window_size : int
    the length of the window. Must be an odd integer number.
    order : int
    the order of the polynomial used in the filtering.
    Must be less then `window_size` - 1.
    deriv: int
    the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
    the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
    Data by Simplified Least Squares Procedures. Analytical
    Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
    W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
    Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial
    
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
       # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')


main()

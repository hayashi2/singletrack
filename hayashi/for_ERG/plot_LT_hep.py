# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("Agg")
from pylab import *
import numpy as np
from spacepy import pycdf
import matplotlib.ticker as tic
import matplotlib.colors as col
import matplotlib.cm as cm
import sys
import subprocess
import os
from datetime import datetime,date,timedelta
from matplotlib import gridspec

dL = 1./5.
grL = int(np.round((11./dL)))

def main():
    args = sys.argv
    print datetime.today()

    
    ###### Input check ######
    
    if (len(args)!=3):
        print "#########WARNING######### Incorrect input"
        print "Arguments  1st: start date, 2nd: end date"
        sys.exit()
        
    if(len(args[1])!= 8 or len(args[2])!=8):
        print "#########WARNING######### Incorrect input"
        print "start_date or end_date is incorrect"
        sys.exit()
    
    ###### Data acquisition ######
    start_date = args[1]
    end_date = args[2]

    s_date = datetime.strptime(start_date,"%Y%m%d")
    e_date = datetime.strptime(end_date,"%Y%m%d")

    LT = np.empty((6,grL,1))
    DATE = []
    for i,single_date in enumerate(daterange(s_date, e_date)):        
        print single_date
        year = str(single_date.year).zfill(4)
        month = str(single_date.month).zfill(2)
        day = str(single_date.day).zfill(2)
        #if((single_date == datetime.strptime("20170417","%Y%m%d")) or \
        #       (single_date == datetime.strptime("20170813","%Y%m%d"))or\
        #       (single_date == datetime.strptime("20170527","%Y%m%d"))or\
        #       (single_date == datetime.strptime("20170528","%Y%m%d"))):
        if(single_date==1000000000):
            lt = np.empty((6,LT[0,:,:].shape[0],LT[0,:,:].shape[1]+1))
            lt[:,:,0:-1] = LT[:,:,:]
            lt[:,:,-1] = np.zeros((6,grL))*np.nan
            LT = lt[:,:,:]
            DATE.append(" ")
        else:
            filename = "erg_hep_pre_omniflux_"+year+month+day+"_v01.cdf"
            pfilename = "erg_orb_l2_"+year+month+day+"_v01.cdf"
            Hdata = ReadHEP(year, filename)
            position = ReadPosition(year,pfilename)    
            #HEP_L_count = Hdata["FEDO_L"]
            #HEP_L_count_e = Hdata["FEDO_L_Energy"]
            #HEP_L_count_e_la = Hdata["FEDO_L_Energy_LABL"]
            HEP_H_count = Hdata["FEDO_H"]
            HEP_H_count_e = Hdata["FEDO_H_Energy"]
            print HEP_H_count_e[:,:]
            HEP_H_count_e_la = Hdata["FEDO_H_Energy_LABL"]
            #Epoch_L = Hdata["Epoch_L"]
            Epoch_H = Hdata["Epoch_H"]
            

            DATE.append(str(Epoch_H[100].month).zfill(2)+"-"+str(Epoch_H[100].day).zfill(2))
            pos = position["pos_rmlatmlt"]
            Lm = position["pos_Lm"]
            MLT = pos[:,2]
            MLAT = pos[:,1]
            p_epoch = position["epoch"]
            HEP_H_count = Recreate(HEP_H_count,Epoch_H,single_date)
            Lm,MLT,MLAT = Reposition(Lm[:,0],MLT,MLAT,p_epoch,single_date)
        
            SIZE = min(np.size(Lm[::4]),np.size(HEP_H_count[::3,0]))
            L = Lm[0:SIZE*4:4]  #30deg
            Lcolor = np.empty((6,SIZE))
            Llabel = np.empty(6,dtype="S16")
            for j in xrange(0,6):
                Lcolor[j,:] = HEP_H_count[0:SIZE*3:3,j+4]
                Llabel[j] = HEP_H_count_e_la[j+4]#+np.array(["\nkeV"],dtype="S7")
            lt = np.empty((6,LT[0,:,:].shape[0],LT[0,:,:].shape[1]+1))         
            yedges = np.arange(0,grL+1)
            xedges = np.arange(0,SIZE+1)
            H, xedges, yedges = np.histogram2d(np.arange(SIZE),L/dL,bins=(xedges,yedges))
            DATA = np.zeros((6,SIZE,grL))
            for t in xrange(SIZE):
                DATA[:,t,np.trunc(L[t]/dL).astype(int)] += Lcolor[:,t]
            DATA = np.sum(DATA[:,:,:],axis=1)
            H = np.sum(H[:,:],axis=0)
            DATA = np.divide(np.float64(DATA),np.float64(H))
            #DATA = np.float64(DATA)/np.float64(H)
            if(i == 0):
                LT[:,:,0] = DATA[:,:]
            else:
                lt[:,:,0:-1] = LT[:,:,:]
                lt[:,:,-1] = DATA[:,:]
                LT = lt[:,:,:]
            Hdata.close()
            position.close()

    Dst = Read_Dst(s_date,e_date)

    LT = np.mean(LT[:,:,:],axis=0)
    ###### Figure Setting 1 ######
    fig = figure(1)

    fig.subplots_adjust(hspace=0.1,right=0.85)
    gs = gridspec.GridSpec(2,1,height_ratios=[5,2])
    ax0 = fig.add_subplot(gs[0,0])
    ax5 = fig.add_subplot(gs[1,0])
    #ax0 = fig.add_subplot(615)
    #ax1 = fig.add_subplot(614)
    #ax2 = fig.add_subplot(613)
    #ax3 = fig.add_subplot(612)
    #ax4 = fig.add_subplot(611)
    #ax0 = fig.add_subplot(211)
    #ax5 = fig.add_subplot(212)
    ### Draw and Plot ###
    ay0 = ax0.imshow(LT[:,:],origin="lower",aspect="auto",interpolation="none",\
                         norm=col.LogNorm(vmin=1e-2,vmax=1e4))
    #ay1 = ax1.imshow(LT[1,:-4,:],origin="lower",aspect="auto",interpolation="none",\
    #                     norm=col.LogNorm(vmin=1e-2,vmax=1e4))
    #ay2 = ax2.imshow(LT[2,:-4,:],origin="lower",aspect="auto",interpolation="none",\
    #                     norm=col.LogNorm(vmin=1e-2,vmax=1e4))
    #ay3 = ax3.imshow(LT[3,:-4,:],origin="lower",aspect="auto",interpolation="none",\
    #                     norm=col.LogNorm(vmin=1e-2,vmax=1e4))
    #ay4 = ax4.imshow(LT[4,:-4,:],origin="lower",aspect="auto",interpolation="none",\
    #                     norm=col.LogNorm(vmin=1e-2,vmax=1e4))
    ax5.plot(np.linspace(0,np.shape(LT[:,:])[1],len(Dst)),Dst,"k")
    ###### Figure setting 2 ######
    
    ax0.set_title("HEP-H   1.0 - 2.0 MeV  "+s_date.strftime("%Y%m%d")+"-"+e_date.strftime("%Y%m%d"),fontsize=15)
    box0 = ax0.get_position()
    #box1 = ax1.get_position()
    #box2 = ax2.get_position()
    #box3 = ax3.get_position()
    #box4 = ax4.get_position()
    pad, width=0.02,0.01
    cax0 = fig.add_axes([box0.xmax+pad, box0.ymin,width,box0.height])
    #cax1 = fig.add_axes([box1.xmax+pad, box1.ymin,width,box1.height])
    #cax2 = fig.add_axes([box2.xmax+pad, box2.ymin,width,box2.height])
    #cax3 = fig.add_axes([box3.xmax+pad, box3.ymin,width,box3.height])
    #cax4 = fig.add_axes([box4.xmax+pad, box4.ymin,width,box4.height])
    CB0 = colorbar(ay0,cax=cax0,ticks=np.logspace(-2,4,7))
    #CB1 = colorbar(ay1,cax=cax1,ticks=np.logspace(-2,4,7))
    #CB2 = colorbar(ay2,cax=cax2,ticks=np.logspace(-2,4,7))
    #CB3 = colorbar(ay3,cax=cax3,ticks=np.logspace(-2,4,7))
    #CB4 = colorbar(ay4,cax=cax4,ticks=np.logspace(-2,4,7))
    
    CB0.set_label("counts",fontsize=11)
    CB0.ax.tick_params(labelsize=10)
    
    xticks = np.linspace(0,np.shape(LT[:,:])[1],7)
    delD  = int(((e_date - s_date).total_seconds())/60./60./24.)
    x = [((s_date + timedelta(days=i)).date()).strftime("%m/%d") \
             for i in np.linspace(0,delD,7).astype(int)]


    ax0.yaxis.set_label_coords(-0.06,0.5)
    ax0.set_yticks(np.linspace(-0.5,grL-1-0.5,6))       
    ax0.set_yticklabels(np.linspace(0,10,6).astype(int),fontsize=10)
    ax0.set_xticks(xticks)
    ax0.set_xlim(0,np.shape(LT[:,:])[1])
        #ax.set_xticklabels(DATE[::int(len(DATE)/5)],fontsize=13)
    ax0.set_xticklabels(x,fontsize=13)
    ax0.grid()
    
    ax5.set_xlabel("date",fontsize=15)
    ax5.set_xticks(xticks)
    ax5.set_yticks(np.linspace(-150,100,6))
    ax5.set_xlim(0,np.shape(LT[:,:])[1])
    ax5.set_xticklabels(x,fontsize=13)
    ax5.set_yticklabels(np.linspace(-150,100,6).astype(int),fontsize=10)
    #ax5.set_yticklabels(fontsize=10)
    ax5.grid()
    ax5.xaxis.set_minor_locator(tic.AutoMinorLocator())
    ax5.yaxis.set_minor_locator(tic.AutoMinorLocator())
    ax5.set_ylabel("Dst [nT]",fontsize=13)
    ax0.set_ylabel("L",fontsize=15)
    
    setp(ax0.get_xticklabels(),visible=False)
    
    ###### Save Figure ######
    fig.savefig("LT_HEP_H_"+s_date.strftime("%Y%m%d")+"-"+e_date.strftime("%Y%m%d")+".png")
    close(fig)


def ReadHEP(year,filename):

    filepath = "/home/m_hayashi/ERG_data/hep/"+filename
    data = pycdf.CDF(filepath)
    return data

def ReadPosition(year,filename):
    filepath = "/work/m_hayashi/ERG_data/def/"+filename
    data = pycdf.CDF(filepath)
    return data


def ReadXEP(year,filename):
    
    filepath = "/home/m_hayashi/ERG_data/xep/"+filename
    data = pycdf.CDF(filepath)
    return data



def Recreate(DATA,Epoch,s_date):
    NEW_DATA = np.zeros((24*60*60/8,np.shape(DATA)[1]))
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(DATA[:,:])[0]):
        if (np.round((Epoch[0] -basetime).total_seconds()) < 0):
            DATA = DATA[1:,:]
            #last_Ep = Epoch[0]
            Epoch = Epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(DATA[:,:])[0]):
        #gr = int(np.round((Epoch[i] -basetime).total_seconds()))/8
        gr += int(np.round((Epoch[i] - last).total_seconds()/8))
        last = Epoch[i]
        if(gr>=24*60*60/8):
            print Epoch[i]
            break
        NEW_DATA[gr,:] = DATA[i,:]
    return NEW_DATA



def Reposition(Lm,MLT,MLAT,epoch,s_date):
    NEW_L = np.zeros(24*60*60/6)
    NEW_MLT = np.zeros(24*60*60/6)
    NEW_MLAT = np.zeros(24*60*60/6)
    s_year = str(s_date.year).zfill(4)
    s_month = str(s_date.month).zfill(2)
    s_day = str(s_date.day).zfill(2)
    basetime = datetime.strptime(s_year+s_month+s_day+"00","%Y%m%d%H")
    for i in xrange(np.shape(epoch[:])[0]-1):
        if (np.round((epoch[0] -basetime).total_seconds()) < 0):
            Lm = Lm[1:]
            MLT = MLT[1:]
            MLAT = MLAT[1:]
            #last_Ep = Epoch[0]
            epoch = epoch[1:]
        else:
            break
    gr = 0
    last = basetime
    for i in xrange(np.shape(epoch[:])[0]-1):
        gr += int(np.round((epoch[i] -last).total_seconds()/6))
        last = epoch[i]
        if(gr>=24*60*60/6):
            print epoch[i]
            break
        NEW_L[gr] = Lm[i]
        NEW_MLT[gr] = MLT[i]
        NEW_MLAT[gr] = MLAT[i]
    return NEW_L,NEW_MLT,NEW_MLAT



def Read_Dst(s_date,e_date):
    filename = "/home/m_hayashi/ERG/Dst/Dst_"+str(s_date.year)+".txt"
    day_st = int(s_date.strftime("%j")) -1
    day_ed = int(s_date.strftime("%j")) -1
    delD  = int(((e_date - s_date).total_seconds())/60./60./24.)
    Dst = np.loadtxt(filename,skiprows = 2).astype(int)
    #dst = np.arange(delD*24 + 1)
    dst=[]
    dst.append(Dst[day_st-1,-1])
    for num in range(delD):
        #dst[delD*24+1:delD*24 +24] = Dst[day_st + num,2:]
        dst.extend(Dst[day_st + num,2:].tolist())
    print dst
    return dst


def daterange(s_date,e_date):
    return (s_date + timedelta(days=i) for i in range((e_date-s_date).days+1))

main()

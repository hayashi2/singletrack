# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

filename ="/work/ymatumot/simulation/GM-RB/run_Vsw400/MHD_B025.dat"
nx = 297
ny = 241
nz = 241
layer = 3
size = nx * ny * nz * layer
head = ("head","<i4")
tail = ("tail","<i4")
b=("bfield", '<'+str(size)+'f4')
dt = np.dtype([head, b, tail])
fd = open(filename, 'r')
chunk = np.fromfile(fd, dtype=dt, count=1)
bf = chunk[0]['bfield'].reshape((nx, ny, nz, layer),order='F')
#plt.imshow(bf[:,:,120,0].T, origin='lower', interpolation='none')
#plt.show()

# -*- coding: utf-8 -*-
import matplotlib 
matplotlib.use("Agg")
from pylab import *
import numpy as np
import matplotlib.colors as col
import  matplotlib.cm as cm
from matplotlib.colors import LogNorm
import matplotlib.ticker as tic
from matplotlib.ticker import MaxNLocator 
import time
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable


m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 10000

#files ="/work/m_hayashi/RBF6/ptl"
iniene = 1
enest = iniene-1
dx = 0.5
dy = 0.5
#dL = 0.5
dphi = float(1.0/180.0)*np.pi*15.
#dA = dx*dy
da = float(1.0/180.0)*np.pi
de = 0.25
dt = 5 
grx = 2*int(15/dx)
gry = 2*int(15/dx)
#grL = int((10.-5.5)/dL)
#grML = 2*int(np.pi/dphi)
gra = int(np.pi/da)
gre = int(10./de)+1
grt = 200

def main():
    start =time.time()
    data = np.load("/work/m_hayashi/Fluxdata/Data_RB_GMrun3_VAPs0716_xy.npz")
    flux1= data["flux"]
    """
    flux1 = np.sum(flux1[:,:,:,:,:],axis=2)
    flux1 = np.sum(flux1[:,:,:,:],axis=2) +1
    
    for t in xrange(0,grt):
        clf()
        title("time=" +str((t+1)*6)+" [sec]")
        ax=imshow(flux1[:,:,t]/flux1[:,:,0])
        colorbar(ax)
        savefig("/work/m_hayashi/output/flux/xytest/test_"+str(t).zfill(3)+".png")
            
    """
    
    flux1 = np.average(flux1[:,:,85:96,:,:],axis=2) # integral for a
    #flux1 = flux1[1,:,:] #L=6
    print flux1.shape
    """
    div = np.empty((flux1.shape))
    for t in xrange(grt):
        div[:,:,t] = flux1[:,:,0]
    
    flux1 = flux1/div
    """


    for t in xrange(grt):
        clf()
        fig = figure(1)
        ax1 = fig.add_subplot(1,1,1)
        print t
        #ax1.plot(flux1[3,:],label="105 keV",color=cm.jet(0/4))  
        im1 = ax1.imshow(np.sum(flux1[6:-6,6:-6,2:4,t],axis=2).T,origin="lower",extent=[12,-12,12,-12],interpolation="none",norm=col.LogNorm(vmin=1e6,vmax=1e9))
         
        ax1.set_title("0.5 - 1.0 MeV t = "+str(t*dt)+" sec",fontsize=15)
        ax1.set_ylabel("Y [Re] ")
        ax1.set_xlabel("X[Re]")
        CB1 = colorbar(im1)
        CB1.set_label("$cm^{-2} s^{-1} str^{-1} MeV^{-1}$",fontsize=15)

        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')
        #fig = gcf()
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)

        fig.savefig("/work/m_hayashi/figure/RB/run3/XY/05_1_"+str(t).zfill(3)+".png")
        close(fig)

    elapsed_time = time.time()-start
    print "elapsed_time:{0}".format(elapsed_time) +" [sec]"
    

main()

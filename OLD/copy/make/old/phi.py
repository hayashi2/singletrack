import numpy as np
from pylab import *
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
time = 0


nx = np.arange(1,129,1)-64
na = np.empty([128,128])
n=0
while(n<=127):
    na[n]=nx
    n+=1

nb=na.transpose(1,0)

cosphi=na/np.sqrt((na)**2+(nb)**2)
sinphi=nb/np.sqrt((na)**2+(nb)**2)


while(time < 2400):
    
    path = "/work/saito/GMHDRB/inputdata/"
    ename = "MHDE_RB"

    filename = path + ename + str(time).zfill(6) + ".dat"

    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<6291456f4")
         
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["b"].reshape((128,128,128,3),order="F")
    bt=bf.transpose(1,0,2,3)*(1e3)
    ey = bt[:,:,64,1]
    ex = bt[:,:,64,0]

    ephi = ey*cosphi -ex*sinphi

    figure(figsize=(12,10))
    #imshow(ephi,extent=[-21.33,21.33,-21.33,21.33],origin='lower',vmax=8,vmin=-14)  #Ey
    imshow(ephi[31:97,31:97],extent=[-11,11,-11,11],origin ='lower',vmax=8,vmin=-14)
    xlabel("x(Re)")
    ylabel("y(Re)")
    title("time="+str(time)+"s")

    CB=colorbar()   
    CB.set_label('Ephi [mV/m]',fontsize=15)
    CB.set_clim(-14,8)  
    
    wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
    wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
    fig = gcf()
    fig.gca().add_artist(wedge1)
    fig.gca().add_artist(wedge2)

    savefig("EphiL"+str(time/24)+".jpeg")
    print "saved ",time
    time += 24

# -*- coding: utf-8 -*-
from pylab import *
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches
import matplotlib.cm as cm
from matplotlib.ticker import FormatStrFormatter 
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 1000
files ="/work/m_hayashi/RBF2/ptl"
t1=0
iniene = 5
enest = iniene -1
dA = 0.5*0.5
da = float(1.0/360.0)*np.pi
de = 0.4e6
dt = 6
dL = 0.1

def main():
    data0 = np.empty((3,200))
    data1 = np.empty((3,200))
    
    for t2 in range(0,200):
        print t2
        clf()
        for filenum in range(0,128):
            filename = files + str(filenum).zfill(3) +".dat"
    #read file   
            xp,yp,ene,eneperp,enepara =  readfile(filename,t2)
           # for m in range(0,ptl):
            if(filenum == 118):
                data0[0,t2] = ene[0,840]
                data0[1,t2] = eneperp[0,840]
                data0[2,t2] = enepara[0,840]
            if(filenum == 110):
                data1[0,t2] = ene[0,170]
                data1[1,t2] = eneperp[0,170]
                data1[2,t2] = enepara[0,170]
                
    fig,axes = subplots(2,1,sharex=True, sharey=True)
    time = np.arange(0,1200,6)
    axes[0].plot(time,data0[0,:],label="W")
    axes[0].plot(time,data0[1,:],label="Wperp")
    axes[0].plot(time,data0[2,:],label="Wpara")
    axes[0].set_ylabel("MeV",fontsize=12)
    axes[0].minorticks_on()
    axes[0].grid()
    axes[0].legend(fontsize=10)

    axes[1].plot(time,data1[0,:],label="W")
    axes[1].plot(time,data1[1,:],label="Wperp")
    axes[1].plot(time,data1[2,:],label="Wpara")
    axes[1].minorticks_on()
    axes[1].grid()
    axes[1].set_xlabel("time [sec]",fontsize=12)
    axes[1].set_ylabel("MeV",fontsize=12)
    axes[1].legend(fontsize=10)
    subplots_adjust(bottom=None,top=None)
    
    show()
    
def readfile(filename,time):
    t=time
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=t+1)

    xp = chunk[t]["xp"].reshape((1,ptl),order="F")
    yp = chunk[t]["yp"].reshape((1,ptl),order="F")
    zp = chunk[t]["zp"].reshape((1,ptl),order="F")
    lp = chunk[t]["lp"].reshape((1,ptl),order="F")
    xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
    yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
    zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
    aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
    beq = chunk[t]["beq"].reshape((1,ptl),order="F")
    ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
    pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
    mu = chunk[t]["mu"].reshape((1,ptl),order="F")
    bp = chunk[t]["bp"].reshape((1,ptl),order="F")
    prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
    db = chunk[t]["db"].reshape((1,ptl),order="F")
    predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
    nump = chunk[t]["nump"].reshape((1,ptl),order="F")
    ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   
    fd.close() 
    
   # print xp.shape,yp.shape
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)

    gperp = np.sqrt(1 + (pperp**2) / (m*c)**2)
    eneperp = (m * c**2 * (gperp - 1) / e)

    gpara = np.sqrt(1 + (ppara**2) / (m*c)**2)
    enepara = (m * c**2 * (gpara - 1) / e)
    
    del chunk
    return xeq/6370000 ,yeq/6370000,ene/1e6,eneperp/1e6,enepara/1e6

def griding():
    L = np.sqrt((x)**2 + (y)**2)
    dg2= np.arctan2(y,x)*180/np.pi +180
    mlt= dg2*1/3 #gedree to mlt
    
    #grid number
    xg = (2*x).astype(int) + 30
    yg = (2*y).astype(int) + 30
    ag = ((180/np.pi)*a).astype(int)
    eg = (5*(ene/1e6-enest)).astype(int)
    Lg = (10*L).astype(int)
    MLTg= mlt.astype(int)    #ptl loop

main()

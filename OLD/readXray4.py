# -*- coding: utf-8 -*-
"""
Created on Sat Jan 23 17:46:25 2016

@author: Miyoshi
"""

import numpy as np
from pylab import *

path = "/work/m_hayashi/Xray/Vsw600/"
name = "MHDXray_"
outpath = "/home/m_hayashi/output/Xray/"
#time = 0


# xray=np.empty([297,241,241])
for time in range(0,101):
  image_view=np.zeros([19,19])
  data = np.load(path+name+str(time).zfill(3)+".npz")
  xray = data["Xray"]
  for i in range(0,297):
    for j in range(0,241):
      for k in range(0,241):
        xpoint=30.0-(i-1)*3./10.
        ypoint=40.0-(j-1)*3./10.
        zpoint=-40.0+(k-1)*3./10.
        r=np.sqrt(xpoint**2+ypoint**2+zpoint**2)
        if (r >= 5.) and (r <= 20.):
          if (xpoint != 0.):
            theta=np.arcsin(zpoint/r)*180./np.pi
            phi=np.arctan2(ypoint,xpoint)*180./np.pi
            for l in range(0,19):
              theta_min=-90.0+(l-1)*10.0
              theta_max=-90.0+l*10.
              if (theta_min <= theta) and (theta < theta_max):
                for m in range(0,19):
                  phi_min=-180.0+(m-1)*20.0
                  phi_max=-180.0+m*20.
                  if (phi_min <= phi) and (phi < phi_max):
                    image_view[l,m]=image_view[l,m]+xray[i,j,k]

  clf()
  title("time= " +str(time*12)+" s")
  imshow(image_view,vmin=0,vmax=0.016)
  ax=colorbar()    
#ax.set_label('',fontsize=15)
  savefig(outpath + str(time).zfill(3)+".jpeg")
  print "saved..."+str(time)
#s]how()


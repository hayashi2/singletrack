module fio

  implicit none

  private

  public :: fio__param, fio__input, fio__output, fio__print


contains


  subroutine fio__param(nx,ny,nz, &
                        delt,uv_sw,un_sw,beta_sw,re0,rb0, &
                        dir,file)

    integer, intent(in)          :: nx, ny, nz
    real(8), intent(in)          :: delt
    real(8), intent(in)          :: uv_sw, un_sw, beta_sw
    real(8), intent(in)          :: re0,rb0
    character(len=*), intent(in) :: dir, file


    open(9,file=trim(dir)//trim(file),status='unknown')

    write(9,*)'nx, ny, nz: ',nx, ny, nz
    write(9,*)'dt: ',delt
    write(9,*)'V_sw, N_sw, P_sw, Beta_sw: ',uv_sw,un_sw,beta_sw
    write(9,*)'Reynolds number: ',re0
    write(9,*)'Magnetic Reynolds number: ',rb0

    close(9)

  end subroutine fio__param


  subroutine fio__input(it0,itsw0,f,dxf,dyf,dzf,Db,flg,delt,&
                        Bm,rin,drs,px0,py0,pz0,pxm,pym,pzm, &
                        unmax,unmin,                        &
                        neq,nx,ny,nz,                       &
                        nxs,nxe,nys,nye,nzs,nze,            &
                        nxs1,nxe1,nys1,nye1,nzs1,nze1,gam,  &
                        nproc,nrank,                        &
                        dir,file)

    integer, intent(out)         :: it0,itsw0
    real(8), intent(out)         :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)         :: dxf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)         :: dyf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)         :: dzf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)         :: Db(nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)         :: flg(neq,nxs:nxe,nys:nye,nzs:nze)
    real(8), intent(out)         :: delt
    real(8), intent(out)         :: Bm, rin, drs, px0, py0, pz0, pxm, pym, pzm
    real(8), intent(out)         :: unmax, unmin
    integer, intent(in)          :: neq, nx, ny, nz
    integer, intent(in)          :: nxs, nxe, nys, nye, nzs, nze
    integer, intent(in)          :: nxs1, nxe1, nys1, nye1, nzs1, nze1
    integer, intent(in)          :: nproc, nrank
    real(8), intent(in)          :: gam
    character(len=*), intent(in) :: dir, file
    integer                      :: inx, iny, inz, inproc, ineq
    real(8)                      :: igam
    character(len=256)           :: fin

    write(fin,'(a,a,a,i3.3,a)')trim(dir),trim(file),'_rank=',nrank,'.dat'
    open(100+nrank,file=fin,form='unformatted')

!    read(100+nrank)it0,itsw0,ineq,inx,iny,inz,inproc,igam   !Swのファイル使うとき
    read(100+nrank)it0,ineq,inx,iny,inz,inproc,igam
    if(ineq /= neq .or. inx /= nx .or. iny /= ny .or. inz /= nz .or. &
         inproc /= nproc .or. igam /= gam) then
       write(*,*)'parameter mismatch'
       stop
    endif
    read(100+nrank)f
    read(100+nrank)dxf
    read(100+nrank)dyf
    read(100+nrank)dzf
    read(100+nrank)Db
    read(100+nrank)flg
    read(100+nrank)delt
    read(100+nrank)Bm,rin,drs,px0,py0,pz0,pxm,pym,pzm,unmax,unmin

    close(100+nrank)

    itsw0=0

  end subroutine fio__input


  subroutine fio__output(it0,itsw0,neq,nx,ny,nz,                      & 
                         nxs,nxe,nys,nye,nzs,nze,                     &
                         nxs1,nxe1,nys1,nye1,nzs1,nze1,               &
                         nproc, nrank,                                &
                         f,dxf,dyf,dzf,Db,flg,                        &
                         delt,gam,Bm,rin,drs,px0,py0,pz0,pxm,pym,pzm, &
                         unmax,unmin,                                 &
                         dir,file)

    integer, intent(in)          :: it0, itsw0
    integer, intent(in)          :: neq, nx, ny, nz
    integer, intent(in)          :: nxs, nxe, nys, nye, nzs, nze
    integer, intent(in)          :: nxs1, nxe1, nys1, nye1, nzs1, nze1
    integer, intent(in)          :: nproc, nrank
    real(8), intent(in)          :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)          :: dxf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)          :: dyf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)          :: dzf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)          :: Db(nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)          :: flg(neq,nxs:nxe,nys:nye,nzs:nze)
    real(8), intent(in)          :: delt, gam
    real(8), intent(in)          :: Bm, rin, drs, px0, py0, pz0, pxm, pym, pzm
    real(8), intent(in)          :: unmax, unmin
    character(len=*), intent(in) :: dir, file
    character(len=256) :: fout

    write(fout,'(a,a,a,i3.3,a)')trim(dir),trim(file),'_rank=',nrank,'.dat'
    open(100+nrank,file=fout,form='unformatted')

    write(100+nrank)it0,itsw0,neq,nx,ny,nz,nproc,gam
    write(100+nrank)f
    write(100+nrank)dxf
    write(100+nrank)dyf
    write(100+nrank)dzf
    write(100+nrank)Db
    write(100+nrank)flg
    write(100+nrank)delt
    write(100+nrank)Bm,rin,drs,px0,py0,pz0,pxm,pym,pzm,unmax,unmin

    close(100+nrank)

  end subroutine fio__output

  
  subroutine fio__print(it,neq,nxs,nxe,nys,nye,nzs,nze, &
                        nxs1,nxe1,nys1,nye1,nzs1,nze1,  &
                        nrank,nerr,nstat,               &   
                        f,gf,Db,flg,wrk,bf,             &
                        drs,px0,py0,pz0,pxm,pym,pzm,    &
                        dir)

    use boundary, only  : boundary__f, boundary__bf
    use functions, only : bdx, bdy, bdz
    integer, intent(in)          :: it, neq
    integer, intent(in)          :: nxs, nxe, nys, nye, nzs, nze
    integer, intent(in)          :: nxs1, nxe1, nys1, nye1, nzs1, nze1, nrank
    integer, intent(inout)       :: nerr, nstat(:)
    real(8), intent(inout)       ::  f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout)       :: gf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout)       :: Db(nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout)       :: wrk(3,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout)       :: bf(3,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)          :: flg(neq,nxs:nxe,nys:nye,nzs:nze)
    real(8), intent(in)          :: drs, px0, py0, pz0, pxm, pym, pzm
    character(len=*), intent(in) :: dir
    integer            :: i, j, k
    real(8), parameter :: c1=1.0D0/3.0D0, c2=0.5*c1
    real(8)            :: r0, rm, x0, xm, y0, ym, z0, zm, dr05, drm5
    real(8)            :: dn, dn0
    character(len=256) :: file1, file2, file3, file4, file5, file6, file7, file8, file9

    write(file1,'(a,i6.6,a,a,i3.3,a)') trim(dir),it,'_dn','_rank=',nrank,'.dat'
    write(file2,'(a,i6.6,a,a,i3.3,a)') trim(dir),it,'_pr','_rank=',nrank,'.dat'
    write(file3,'(a,i6.6,a,a,i3.3,a)') trim(dir),it,'_db','_rank=',nrank,'.dat'
    write(file4,'(a,i6.6,a,a,i3.3,a)') trim(dir),it,'_vx','_rank=',nrank,'.dat'
    write(file5,'(a,i6.6,a,a,i3.3,a)') trim(dir),it,'_vy','_rank=',nrank,'.dat'
    write(file6,'(a,i6.6,a,a,i3.3,a)') trim(dir),it,'_vz','_rank=',nrank,'.dat'
    write(file7,'(a,i6.6,a,a,i3.3,a)') trim(dir),it,'_bx','_rank=',nrank,'.dat'
    write(file8,'(a,i6.6,a,a,i3.3,a)') trim(dir),it,'_by','_rank=',nrank,'.dat'
    write(file9,'(a,i6.6,a,a,i3.3,a)') trim(dir),it,'_bz','_rank=',nrank,'.dat'

    open(1000+nrank,file=file1,status='unknown',form='unformatted')
    open(2000+nrank,file=file2,status='unknown',form='unformatted')
    open(3000+nrank,file=file3,status='unknown',form='unformatted')
    open(4000+nrank,file=file4,status='unknown',form='unformatted')
    open(5000+nrank,file=file5,status='unknown',form='unformatted')
    open(6000+nrank,file=file6,status='unknown',form='unformatted')
    open(7000+nrank,file=file7,status='unknown',form='unformatted')
    open(8000+nrank,file=file8,status='unknown',form='unformatted')
    open(9000+nrank,file=file9,status='unknown',form='unformatted')

    call boundary__f(f,neq,nerr,nstat)

!$OMP PARALLEL PRIVATE(i,j,k)

!$OMP DO
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       !Cs to N
       f(1,i,j,k) = 1.D0/(f(1,i,j,k)*f(1,i,j,k))*f(2,i,j,k)
       !N -> ln(N)
       wrk(1,i,j,k) = dlog(f(1,i,j,k))
       !N0 -> ln(N0)
       wrk(2,i,j,k) = dlog(Db(i,j,k))

       !z+- -> v, va
       f(3,i,j,k) = 0.5*(+f(3,i,j,k)+f(4,i,j,k))
       f(4,i,j,k) = +f(3,i,j,k)-f(4,i,j,k)
       f(5,i,j,k) = 0.5*(+f(5,i,j,k)+f(6,i,j,k)) 
       f(6,i,j,k) = +f(5,i,j,k)-f(6,i,j,k)
       f(7,i,j,k) = 0.5*(+f(7,i,j,k)+f(8,i,j,k))
       f(8,i,j,k) = +f(7,i,j,k)-f(8,i,j,k)
    enddo
    enddo
    enddo
!$OMP END DO

    !Va -> B
    !background density
    !end of
!$OMP DO PRIVATE(x0,xm,y0,ym,z0,zm,r0,rm,dr05,drm5,dn,dn0)
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe

       !at (i+1/2,j,k)
       x0 = (i+0.5-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0**2+y0**2+z0**2)
       dr05 = r0**(-5)

       xm = (i+0.5-pxm)*drs
       ym = (j-pym)*drs
       zm = (k-pzm)*drs
       rm = dsqrt(+xm**2+ym**2+zm**2)
       drm5 = rm**(-5)

       dn  = dsqrt(0.5*(+f(1,i,j,k)+f(1,i+1,j,k)))
       dn0 = dsqrt(0.5*(+Db(i,j,k)+Db(i+1,j,k)))
       bf(1,i,j,k) = f(4,i,j,k)*dn &
                    +(dn/dn0-1.)*bdx(x0,z0,xm,zm,dr05,drm5)

       !at (i,j+1/2,k)
       x0 = (i-px0)*drs
       y0 = (j+0.5-py0)*drs
       r0 = dsqrt(+x0**2+y0**2+z0**2)
       dr05 = r0**(-5)
       xm = (i-pxm)*drs
       ym = (j+0.5-pym)*drs
       rm = dsqrt(+xm**2+ym**2+zm**2)
       drm5 = rm**(-5)

       dn = dsqrt(0.5*(+f(1,i,j,k)+f(1,i,j+1,k)))
       dn0 = dsqrt(0.5*(+Db(i,j,k)+Db(i,j+1,k)))
       bf(2,i,j,k) = f(6,i,j,k)*dn &
                    +(dn/dn0-1.)*bdy(y0,z0,ym,zm,dr05,drm5)

       !at (i,j,k+1/2)
       y0 = (j-py0)*drs
       z0 = (k+0.5-pz0)*drs
       r0 = dsqrt(+x0**2+y0**2+z0**2)
       dr05 = r0**(-5)

       ym = (j-pym)*drs
       zm = (k+0.5-pzm)*drs
       rm = dsqrt(+xm**2+ym**2+zm**2)
       drm5 = rm**(-5)

       dn = dsqrt(0.5*(+f(1,i,j,k)+f(1,i,j,k+1)))
       dn0 = dsqrt(0.5*(+Db(i,j,k)+Db(i,j,k+1)))
       bf(3,i,j,k) = f(8,i,j,k)*dn &
                    +(dn/dn0-1.)*bdz(z0,r0,zm,rm,dr05,drm5)
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP END PARALLEL

    call boundary__bf(bf,3,nerr,nstat)

!$OMP PARALLEL PRIVATE(i,j,k)

    !divb
!!$OMP DO PRIVATE(x0,y0,z0,r0,dr05,xm,ym,zm,rm,drm5,dn0)
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe
       gf(2,i,j,k) = (-bf(1,i-1,j,k)+bf(1,i,j,k) &
                      -bf(2,i,j-1,k)+bf(2,i,j,k) &
                      -bf(3,i,j,k-1)+bf(3,i,j,k) &
                     )*flg(1,i,j,k)
!       x0 = (i-px0)*drs
!       y0 = (j-py0)*drs
!       z0 = (k-pz0)*drs
!       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
!       dr05 = r0**(-5)

!       xm = (i-pxm)*drs
!       ym = (j-pym)*drs
!       zm = (k-pzm)*drs
!       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
!       drm5 = rm**(-5)

!       dn0 = 1./dsqrt(Db(i,j,k))

!       gf(2,i,j,k) = (-f(4,i-1,j,k)-f(6,i,j-1,k)-f(8,i,j,k-1)             &
!                      +f(4,i,j,k)+f(6,i,j,k)+f(8,i,j,k)                   &
!                      +0.25*(+f(4,i-1,j,k)*(-wrk(1,i-1,j,k)+wrk(1,i,j,k)) &
!                             +f(4,i,j,k)*(-wrk(1,i,j,k)+wrk(1,i+1,j,k))   &
!                             +f(6,i,j-1,k)*(-wrk(1,i,j-1,k)+wrk(1,i,j,k)) & 
!                             +f(6,i,j,k)*(-wrk(1,i,j,k)+wrk(1,i,j+1,k))   &
!                             +f(8,i,j,k-1)*(-wrk(1,i,j,k-1)+wrk(1,i,j,k)) &
!                             +f(8,i,j,k)*(-wrk(1,i,j,k)+wrk(1,i,j,k+1))   &
!                             +bdx(x0,z0,xm,zm,dr05,drm5)*dn0              &
!                             *(-wrk(1,i-1,j,k)+wrk(1,i+1,j,k)             &
!                               +wrk(2,i-1,j,k)-wrk(2,i+1,j,k))            &
!                             +bdy(y0,z0,ym,zm,dr05,drm5)*dn0              &
!                             *(-wrk(1,i,j-1,k)+wrk(1,i,j+1,k)             &
!                               +wrk(2,i,j-1,k)-wrk(2,i,j+1,k))            &
!                             +bdz(z0,r0,zm,rm,dr05,drm5)*dn0              &
!                             *(-wrk(1,i,j,k-1)+wrk(1,i,j,k+1)             &
!                               +wrk(2,i,j,k-1)-wrk(2,i,j,k+1))            &
!                            )                                             &
!                     )*dsqrt(f(1,i,j,k))*flg(1,i,j,k)
    enddo
    enddo
    enddo
!!$OMP END DO NOWAIT

    !value at (i,j,k)
!$OMP DO
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe
       gf(3,i,j,k) = 0.5*(+f(3,i-1,j,k)+f(3,i,j,k))
       gf(4,i,j,k) = 0.5*(+f(5,i,j-1,k)+f(5,i,j,k))
       gf(5,i,j,k) = 0.5*(+f(7,i,j,k-1)+f(7,i,j,k))
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP DO
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe
       gf(6,i,j,k) = +0.5*(+bf(1,i-1,j,k)+bf(1,i,j,k)) 
       gf(7,i,j,k) = +0.5*(+bf(2,i,j-1,k)+bf(2,i,j,k))
       gf(8,i,j,k) = +0.5*(+bf(3,i,j,k-1)+bf(3,i,j,k))
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP END PARALLEL

    write(1000+nrank)sngl(  f(1,nxs:nxe,nys:nye,nzs:nze))
    write(2000+nrank)sngl(  f(2,nxs:nxe,nys:nye,nzs:nze))
    write(3000+nrank)sngl( gf(2,nxs:nxe,nys:nye,nzs:nze))
    write(4000+nrank)sngl( gf(3,nxs:nxe,nys:nye,nzs:nze))
    write(5000+nrank)sngl( gf(4,nxs:nxe,nys:nye,nzs:nze))
    write(6000+nrank)sngl( gf(5,nxs:nxe,nys:nye,nzs:nze))
    write(7000+nrank)sngl( gf(6,nxs:nxe,nys:nye,nzs:nze))
    write(8000+nrank)sngl( gf(7,nxs:nxe,nys:nye,nzs:nze))
    write(9000+nrank)sngl( gf(8,nxs:nxe,nys:nye,nzs:nze))

    close(1000+nrank)
    close(2000+nrank)
    close(3000+nrank)
    close(4000+nrank)
    close(5000+nrank)
    close(6000+nrank)
    close(7000+nrank)
    close(8000+nrank)
    close(9000+nrank)

!$OMP PARALLEL DO PRIVATE(i,j,k)
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       !N => Cs
       f(1,i,j,k) = dsqrt(f(2,i,j,k)/f(1,i,j,k))

       !v, va -> z+-
       f(3,i,j,k) = f(3,i,j,k)+f(4,i,j,k)
       f(4,i,j,k) = f(3,i,j,k)-2.*f(4,i,j,k)
       f(5,i,j,k) = f(5,i,j,k)+f(6,i,j,k)
       f(6,i,j,k) = f(5,i,j,k)-2.*f(6,i,j,k)
       f(7,i,j,k) = f(7,i,j,k)+f(8,i,j,k)
       f(8,i,j,k) = f(7,i,j,k)-2.*f(8,i,j,k)
    enddo
    enddo
    enddo
!$OMP END PARALLEL DO
   
  end subroutine fio__print


end module fio

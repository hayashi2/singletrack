# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 14:27:16 2015

@author: m_hayashi
"""

"""
ピッチ角のヒストグラム作成のプログラムです。
nの値によって時間を変えられます。
time=(n*5+5)

"""
import numpy as np
from pylab import *

def main():
    angle = readfile()
    
    hist(angle,bins=90,range=(0,90))
    show()
 

def readfile():
    n=250
    num=0
    a=[]
    while(num <= 127):
        filename= "/work/m_hayashi/RB000/ptl%03i.dat"%num
        
        head = ("head","<i")
        tail = ("tail","<i")
        rtime = ("rtime","<f8")
        nnp = ("np","<i4")
        maxptl =("maxptl","<i4")
        
        dt = np.dtype([head,rtime,nnp,maxptl,
                       ("xp","<128f8"),("yp","<128f8"),("zp","<128f8"),
                       ("lp","<128f8"),("xeq","<128f8"),("yeq","<128f8"),
                       ("zeq","<128f8"),("aeq","<128f8"),("beq","<128f8"),
                       ("ppara","<128f8"),("pperp","<128f8"),("mu","<128f8"),
                       ("bp","<128f8"),("prebp","<128f8"),("db","<128f8"),
                       ("predb","<128f8"),("nump","<128i4"),("ierr","<128i4"),tail])

        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=n+1)
        
        xp = chunk[n]["xp"].reshape((1,128),order="F")
        yp = chunk[n]["yp"].reshape((1,128),order="F")
        zp = chunk[n]["zp"].reshape((1,128),order="F")
        lp = chunk[n]["lp"].reshape((1,128),order="F")
        xeq = chunk[n]["xeq"].reshape((1,128),order="F")
        yeq = chunk[n]["yeq"].reshape((1,128),order="F")
        zeq = chunk[n]["zeq"].reshape((1,128),order="F")
        aeq = chunk[n]["aeq"].reshape((1,128),order="F")
        beq = chunk[n]["beq"].reshape((1,128),order="F")
        ppara = chunk[n]["ppara"].reshape((1,128),order="F")
        pperp = chunk[n]["pperp"].reshape((1,128),order="F")
        mu = chunk[n]["mu"].reshape((1,128),order="F")
        bp = chunk[n]["bp"].reshape((1,128),order="F")
        prebp = chunk[n]["prebp"].reshape((1,128),order="F")
        db = chunk[n]["db"].reshape((1,128),order="F")
        predb = chunk[n]["predb"].reshape((1,128),order="F") 
        nump = chunk[n]["nump"].reshape((1,128),order="F")
        ierr = chunk[n]["ierr"].reshape((1,128),order="F")
        

        a = np.append(a, (np.arctan(np.fabs(pperp/ppara)))*180/np.pi)
        num+=1
    return a

main()

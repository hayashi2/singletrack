# -*- coding: utf-8 -*-
import numpy as np
import matplotlib
matplotlib.use("Agg")
from pylab import *
import matplotlib.ticker as tick
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

# BOX SIZE
gx = 500
gy = 300
gz = 300
#Earth position
rx = 120
ry = 150
rz = 150

dx = dy = dz = 1./4
e0 =2.2
delt = 11
path = "/work/m_hayashi/GM/run3/MHD/"
ename = "MHD_E"

def main():

    nx = -(np.arange(0,gx)-rx)
    ny = -(np.arange(0,gy)-ry)
    NY,NX = np.meshgrid(ny,nx)
    cosphi = NX.astype(float)/np.sqrt(NX**2 + NY**2)
    sinphi = NY.astype(float)/np.sqrt(NX**2 + NY**2)

    for time in xrange(0,220):
            
        filename = path + ename + str(time).zfill(3) + ".dat"
        
        head = ("head","<i")
        tail = ("tail","<i")
        b=("b","<"+str(gx*gy*gz*3)+"f4")
         
        dt = np.dtype([head,b,tail])
        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=1)
        fd.close()
        #Convert Global-MHD coodinate ->GSM
        bf = chunk[0]["b"].reshape((gx,gy,gz,3),order="F")*e0
    
        ey = -bf[:,:,rz,1]
        ex = -bf[:,:,rz,0]
        
        ephi = ey*cosphi - ex*sinphi
        
        clf()
        figure(figsize=(9,6))
        
        imshow(ephi[rx-20/dx:rx+20/dx+1,ry-20/dy:ry+20/dy+1].T,extent=[20,-20,20,-20],\
                   origin="lower",aspect="equal",vmax=10,vmin=-10,\
                   interpolation="none",cmap = "bwr")
        xlabel("x(GSM)[Re]",fontsize=22)
        ylabel("y(GSM)[Re]",fontsize=22)
        xticks(fontsize=20)
        yticks(fontsize=20)
        gca().xaxis.set_major_locator(tick.MultipleLocator(10))
        title("time="+str(time*11)+"sec",fontsize=22)
        grid(color="gray",linestyle="--",linewidth=0.5)
        CB=colorbar()   
        CB.set_label('Ephi [mV/m]',fontsize=22)
        CB.ax.tick_params(labelsize=20)
    #CB.set_clim(-4.84,2.64)  
        """
        lin = -readB(time)
        plot(lin[10:98]/3 +21,(-np.arange(np.size(lin),dtype="f")[56]+np.arange(np.size(lin),dtype="f")[10:98])/3,"k")
        xlim(21,-21)
        ylim(18,-18)
        """
        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
        fig = gcf()
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)

        
        savefig("/work/m_hayashi/figure/run3/Ephi/Ephi_L"+str(time).zfill(3)+".jpeg")
        print "saved ",time*delt
        close()
        
"""
def readB(t):
    b0 = 10
    mu0 = 4.*np.pi*1e-7
    bpath ="/work/ymatumot/simulation/GM-RB/run_Vsw600/"
    bname = "MHD_B"
    filename = bpath + bname + str(t).zfill(3) +".dat"
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<51750171f4")

    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*b0
    
    Bx = bf[:,:,:,0]
    By = bf[:,:,:,1]
    Bz = bf[:,:,:,2]
    B = np.sqrt(Bx**2 + By**2 + Bz**2)
    P =  (((B*1e-9)**2)/(2.*mu0))*1e9 #+Pp 

    P = P[27:153,66:174,121]
    #P = np.diff(P,axis=0)
    lin = np.empty(np.shape(P)[1])
    for y in xrange(np.shape(P)[1]):
        for x in xrange(np.shape(P)[0]):
            
            if(P[x,y] > 0.01):
                lin[y] = float(x)
                break
    return lin
"""
main()

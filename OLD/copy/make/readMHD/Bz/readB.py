# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 13:30:36 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches

bpath ="/work/ymatumot/simulation/GM-RB/run_Vsw400/"
bname = "MHD_B"
num = 0
while(num <=208):
    filename = bpath + bname + str(num).zfill(3) +".dat"
    
    head = ("head","<i")
    tail = ("tail","<i")
    b=("b","<51750171f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    
    bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*10
    list=bf.transpose(1,0,2,3)
    print np.min(list[:,:,120,2]) 
    figure(figsize=(12,10))
    #imshow(list[:,:,120,2],extent=[30,-69,-40,40],origin='lower',vmax=480)
    imshow(list[:,:,120,2] , extent=[-30,69,-40,40] , origin="lower",norm=LogNorm(vmax=1e3,vmin=1e-2))
    title("time="+str(num*12)+"s")
    ax=colorbar()    
    ax.set_label('Bz [nT]',fontsize=15)
    #ax.set_clim(0,480)
    xlabel("x(Re)")
    ylabel("y(Re)")
    #xticks([-20,0,20,40,60],["20","0","-20","40","60"])
    #yticks([-40,-30,-20,-10,0,10,20,30,40],["40","30","20","10","0","-10","-20","-30","-40"])

    wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='k')
    wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='w')    
    fig = gcf()
    fig.gca().add_artist(wedge1)
    fig.gca().add_artist(wedge2)

    savefig('Bz%04i.jpeg'%(num/2))
    print 'saved %d'%(num*12)
    num+=2
   

# -*- coding: utf-8 -*-
from pylab import *
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches

m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 1000
files ="/work/m_hayashi/RBF4/ptl"
t1=0
iniene = 2
enest = iniene -1
dA = 0.5*0.5
da = float(1.0/360.0)*np.pi
de = 0.4e6
dt = 6
dL = 0.1

def main():
    count = np.zeros((180,200,4)) #a,t
    data1=np.load("L7.npz")
    xy7= data1["xy"]
    xy7[:,0]=np.abs(xy7[:,0]-90)/3
    xy7[:,1]=np.abs(xy7[:,1]-90)/3
    xy7[:,2]=np.abs(xy7[:,2]-120)/3
    xy7[:,3]=np.abs(xy7[:,3]-120)/3
    
    
    data2=np.load("L8.npz")
    xy8= data2["xy"]
    xy8[:,0]=np.abs(xy8[:,0]-90)/3
    xy8[:,1]=np.abs(xy8[:,1]-90)/3
    xy8[:,2]=np.abs(xy8[:,2]-120)/3
    xy8[:,3]=np.abs(xy8[:,3]-120)/3
   
    for t2 in range(0,200):
        clf()
        print t2
    # file loop 
        for filenum in range(0,128):
            filename = files + str(filenum).zfill(3) +".dat"

    #read file   
            xp,yp,a,mu,ene =  readfile(filename,t2)
            x,y = xp/6370000,yp/6370000
            L = np.sqrt((x)**2 + (y)**2)
    	    dg2= np.arctan2(y,x)*180/np.pi +180
            mlt= dg2*1/3 #gedree to mlt

    #grid number
            xg = (2*x).astype(int) + 30
            yg = (2*y).astype(int) + 30
            ag = ((180/np.pi)*a).astype(int)
            eg = (5*(ene/1e6-enest)).astype(int)
            Lg = (10*L).astype(int)
	    MLTg= mlt.astype(int)
    #ptl loop
            for m in range(0,ptl):

                #count per bins
                if((L[0,m]<=xy8[t2,1] and L[0,m]>=xy7[t2,1])and (MLTg[0,m] <= 62 and MLTg[0,m] >=58)):  #0MLT
                    count[ag[0,m],t2,0] +=1

                if((L[0,m]<=xy8[t2,2] and L[0,m]>=xy7[t2,2]) and MLTg[0,m] <= 92 and MLTg[0,m] >= 88 ): # 6MLT
                    count[ag[0,m],t2,1] +=1

                if((L[0,m]<=xy8[t2,0] and L[0,m]>=xy7[t2,0]) and (MLTg[0,m] <= 1 or MLTg[0,m] >= 117)): #12MLT
                    count[ag[0,m],t2,2] += 1
      
                if((L[0,m]<=xy8[t2,3] and L[0,m]>=xy7[t2,3]) and(MLTg[0,m] <= 32 and MLTg[0,m] >= 28) ): #18MLT
                    count[ag[0,m],t2,3] += 1
            
    # end file loop          
    #show images
    figure(1,figsize=(10,11))
    imshow([(1,2),(1,2)],origin="lower",aspect="auto",norm=LogNorm(vmin=1,vmax=1e3))
    CB=colorbar()
    CB.set_label('count',fontsize=15)
    CB.ax.tick_params(labelsize=13)
    
    f, ((ax3,ax1),(ax4,ax2)) = subplots(2,2,sharex="col",sharey="row")
    suptitle("RBF2_L8.0")
   # f,
    ax1.imshow(count[:,:,0],extent=[0,2400,0,180],origin="lower",aspect="auto",norm=LogNorm(vmin=1,vmax=1e3))
    ax1.set_title("0MLT",fontsize=15)
   #ax1.set_xlabel("time [sec]",fontsize=13)
    #ax1.set_ylabel("aeq [deg]",fontsize=13)
   

    ax2.imshow(count[:,:,1],extent=[0,2400,0,180],origin="lower",aspect="auto",norm=LogNorm(vmin=1,vmax=1e3))
    ax2.set_title("6MLT",fontsize=15)
    ax2.set_xlabel("time [sec]",fontsize=13)
   # ax2.set_ylabel("aeq [deg]",fontsize=13)

    ax3.imshow(count[:,:,2],extent=[0,2400,0,180],origin="lower",aspect="auto",norm=LogNorm(vmin=1,vmax=1e3))
    ax3.set_title("12MLT",fontsize=15)
   # ax3.set_xlabel("time [sec]",fontsize=13)
    ax3.set_ylabel("aeq [deg]",fontsize=13)
    
    ax4.imshow(count[:,:,3],extent=[0,2400,0,180],origin="lower",aspect="auto",norm=LogNorm(vmin=1,vmax=1e3))
    ax4.set_title("18MLT",fontsize=15)
    ax4.set_xlabel("time [sec]",fontsize=13)
    ax4.set_ylabel("aeq [deg]",fontsize=13)
    
    show()

def readfile(filename,time):
    t=2*time
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
    xp = chunk[t]["xp"].reshape((1,ptl),order="F")
    yp = chunk[t]["yp"].reshape((1,ptl),order="F")
    zp = chunk[t]["zp"].reshape((1,ptl),order="F")
    lp = chunk[t]["lp"].reshape((1,ptl),order="F")
    xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
    yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
    zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
    aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
    beq = chunk[t]["beq"].reshape((1,ptl),order="F")
    ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
    pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
    mu = chunk[t]["mu"].reshape((1,ptl),order="F")
    bp = chunk[t]["bp"].reshape((1,ptl),order="F")
    prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
    db = chunk[t]["db"].reshape((1,ptl),order="F")
    predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
    nump = chunk[t]["nump"].reshape((1,ptl),order="F")
    ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   
    fd.close()     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)

    return xeq ,yeq,aeq,mu,ene

main()

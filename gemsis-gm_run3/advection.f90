module advection

  implicit none

  private

  public        :: advection__init, advection__rcip
  
  integer, save :: neq, nxs, nxe, nys, nye, nzs, nze
  integer, save :: nxs1, nxe1, nys1, nye1, nzs1, nze1
  integer, save :: nxeh, nyeh, nzeh, nxeh1, nyeh1, nzeh1
  real(8), save :: delt, drs, px0, py0, pz0, pxm, pym, pzm


contains


  subroutine advection__init(neqin,nxsin,nxein,nysin,nyein,nzsin,nzein,    &
                             nxs1in,nxe1in,nys1in,nye1in,nzs1in,nze1in,    &
                             nxehin,nyehin,nzehin,nxeh1in,nyeh1in,nzeh1in, &
                             deltin,drsin,px0in,py0in,pz0in,pxmin,pymin,pzmin)
    integer, intent(in) :: neqin, nxsin, nxein, nysin, nyein, nzsin, nzein
    integer, intent(in) :: nxs1in, nxe1in, nys1in, nye1in, nzs1in, nze1in
    integer, intent(in) :: nxehin, nyehin, nzehin, nxeh1in, nyeh1in, nzeh1in
    real(8), intent(in) :: deltin, drsin, px0in, py0in, pz0in, pxmin, pymin, pzmin

    neq   = neqin
    nxs   = nxsin
    nxe   = nxein
    nys   = nysin
    nye   = nyein
    nzs   = nzsin
    nze   = nzein
    nxs1  = nxs1in
    nxe1  = nxe1in
    nys1  = nys1in
    nye1  = nye1in
    nzs1  = nzs1in
    nze1  = nze1in
    nxeh  = nxehin
    nyeh  = nyehin
    nzeh  = nzehin
    nxeh1 = nxeh1in
    nyeh1 = nyeh1in
    nzeh1 = nzeh1in
    delt  = deltin
    drs   = drsin
    px0   = px0in
    py0   = py0in
    pz0   = pz0in
    pxm   = pxmin
    pym   = pymin
    pzm   = pzmin

  end subroutine advection__init


  subroutine advection__rcip(gf,gdxf,gdyf,gdzf, &
                             f,dxf,dyf,dzf,flg)

    use functions

    real(8), intent(out)   :: gf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)   :: gdxf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)   :: gdyf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)   :: gdzf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: dxf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: dyf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: dzf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(in)    :: flg(neq,nxs:nxe,nys:nye,nzs:nze)
    !***local parameters***!
    real(8), parameter :: eps=1.d-2, pmin=0.1, dp=1.D0/(0.05*pmin), nmin=0.1, dn=1.D0/(0.05*nmin)
    real(8), parameter :: a100=1.0D0, a010=1.0D0, a001=1.0D0 !RCIP control parameter
    integer            :: ieq, i, j, k, isn, jsn, ksn, im, ip, jm, jp, km, kp, iuw, juw, kuw
    real(8)            :: r0, rm, x0, xm, y0, ym, z0, zm, dr05, drm5
    real(8)            :: vax, vay, vaz, dn0, dsn0, vaxu, vayu, vazu, fac
    real(8)            :: dxvax, dxvay, dxvaz, dyvax, dyvay, dyvaz, dzvax, dzvay, dzvaz 
    real(8)            :: dxvaxu, dxvayu, dxvazu, dyvaxu, dyvayu, dyvazu, dzvaxu, dzvayu, dzvazu 
    real(8)            :: tmp4, tmp5, tmp6, tmp7, tmp8, tmp9, tmp10, tmp11, tmp12
    real(8)            :: dlx, dly, dlz, uvx, uvy, uvz
    real(8)            :: dx, dy, dz, dx2, dy2, dz2, ddx, ddy, ddz, ddx2, ddy2, ddz2
    real(8)            :: sx, sy, sz, sumxy, sumxz, sumyz, ab100, ab010, ab001
    real(8)            :: b100, b010, b001
    real(8)            :: c100, c010, c001, c300, c030, c003, c200, c020, c002
    real(8)            :: c210, c120, c201, c102, c021, c012, c110, c101, c011, c111

!$OMP PARALLEL

!$OMP DO PRIVATE(i,j,k,ieq,isn,jsn,ksn,im,ip,jm,jp,km,kp,iuw,juw,kuw,               &
!$OMP            r0,rm,x0,xm,y0,ym,z0,zm,dr05,drm5,dn0,dsn0,fac,                    &
!$OMP            vaxu,vayu,vazu,                                                    &
!$OMP            dxvaxu,dxvayu,dxvazu,dyvaxu,dyvayu,dyvazu,dzvaxu,dzvayu,dzvazu,    & 
!$OMP            vax,vay,vaz,dxvax,dxvay,dxvaz,dyvax,dyvay,dyvaz,dzvax,dzvay,dzvaz, & 
!$OMP            tmp4,tmp5,tmp6,tmp7,tmp8,tmp9,tmp10,tmp11,tmp12,                   &
!$OMP            dlx,dly,dlz,uvx,uvy,uvz,                                           &
!$OMP            dx,dy,dz,dx2,dy2,dz2,ddx,ddy,ddz,ddx2,ddy2,ddz2,                   &
!$OMP            sx,sy,sz,sumxy,sumxz,sumyz,ab100,ab010,ab001,b100,b010,b001,       &
!$OMP            c100,c010,c001,c300,c030,c003,c200,c020,c002,                      &
!$OMP            c210,c120,c201,c102,c021,c012,c110,c101,c011,c111)
    do k=nzs,nze
       km = k-1
       kp = k+1
    do j=nys,nye
       jm = j-1
       jp = j+1
    do i=nxs,nxe
       im = i-1
       ip = i+1

       fac = min(min(0.5*(1.0+tanh((f(2,i,j,k)-pmin)*dp)), &
                     0.5*(1.0+tanh((f(1,i,j,k)-nmin)*dn))), &
                 0.5*(1.0+tanh((1.D0/(f(1,i,j,k)*f(1,i,j,k))*f(2,i,j,k)-nmin)*dn)))

       ieq = 1

       uvz = 0.25*(+f(7,i,j,km)+f(8,i,j,km) &
                   +f(7,i,j,k)+f(8,i,j,k))
       uvy = 0.25*(+f(5,i,jm,k)+f(6,i,jm,k) &
                   +f(5,i,j,k)+f(6,i,j,k))
       uvx = 0.25*(+f(3,im,j,k)+f(4,im,j,k) &
                   +f(3,i,j,k)+f(4,i,j,k))

       isn = -sign(1.D0,uvx)
       iuw = i+isn
       dlx = -delt*uvx*flg(ieq,i,j,k)

       jsn = -sign(1.D0,uvy)
       juw = j+jsn
       dly = -delt*uvy*flg(ieq,i,j,k)
       
       ksn = -sign(1.D0,uvz)
       kuw = k+ksn
       dlz = -delt*uvz*flg(ieq,i,j,k)
          
       dx = dble(isn)
       ddx = 1./dx
       dx2 = dx*dx
       ddx2 = ddx*ddx
       dy = dble(jsn)
       ddy = 1./dy
       dy2 = dy*dy
       ddy2 = ddy*ddy
       dz = dble(ksn)
       ddz = 1./dz
       dz2 = dz*dz
       ddz2 = ddz*ddz
          
       sx = (f(ieq,iuw,j,k)-f(ieq,i,j,k))*ddx
       tmp4 = dxf(ieq,iuw,j,k)-sx 
       tmp4 = tmp4 + 1.D-18*sign(1.D0,tmp4)
       b100 = 0.5*(1.+dtanh((dabs(tmp4)-eps)/(0.05*eps))) &
             *(dabs((sx-dxf(ieq,i,j,k))/tmp4)-1.0)*ddx
       ab100 = a100*b100
       tmp7 = 1.0+ab100*dx
       c100 = dxf(ieq,i,j,k)+ab100*f(ieq,i,j,k)
       c300 = ( tmp7*tmp4+dxf(ieq,i,j,k)-sx )*ddx2
       c200 = ( tmp7*f(ieq,iuw,j,k)-f(ieq,i,j,k)-c100*dx )*ddx2-c300*dx
       
       sy = (f(ieq,i,juw,k)-f(ieq,i,j,k))*ddy
       tmp5 = dyf(ieq,i,juw,k)-sy
       tmp5 = tmp5 + 1.D-18*sign(1.D0,tmp5)
       b010 = 0.5*(1.+dtanh((dabs(tmp5)-eps)/(0.05*eps))) &
             *(dabs((sy-dyf(ieq,i,j,k))/tmp5)-1.0)*ddy
       ab010 = a010*b010
       tmp8 = 1.0+ab010*dy
       c010 = dyf(ieq,i,j,k)+ab010*f(ieq,i,j,k)
       c030 = ( tmp8*tmp5+dyf(ieq,i,j,k)-sy )*ddy2
       c020 = ( tmp8*f(ieq,i,juw,k)-f(ieq,i,j,k)-c010*dy )*ddy2-c030*dy

       sz = (f(ieq,i,j,kuw)-f(ieq,i,j,k))*ddz
       tmp6 = dzf(ieq,i,j,kuw)-sz
       tmp6 = tmp6 + 1.D-18*sign(1.D0,tmp6)
       b001 = 0.5*(1.+dtanh((dabs(tmp6)-eps)/(0.05*eps))) &
             *(dabs((sz-dzf(ieq,i,j,k))/tmp6)-1.0)*ddz
       ab001 = a001*b001
       tmp9 = 1.0+ab001*dz
       c001 = dzf(ieq,i,j,k)+ab001*f(ieq,i,j,k)
       c003 = ( tmp9*tmp6+dzf(ieq,i,j,k)-sz )*ddz2
       c002 = ( tmp9*f(ieq,i,j,kuw)-f(ieq,i,j,k)-c001*dz )*ddz2-c003*dz

       sumxy = +(c100+c200*dx+c300*dx2)*dx &
               +(c010+c020*dy+c030*dy2)*dy+f(ieq,i,j,k)
       sumxz = +(c100+c200*dx+c300*dx2)*dx &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)
       sumyz = +(c010+c020*dy+c030*dy2)*dy &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)

       c210 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab010*dy)*dx*dxf(ieq,i,juw,k)       &
               -ab100*dx*f(ieq,i,juw,k)+c100*dx         &
               -sumxy )*ddx2*ddy
       c120 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab100*dx)*dy*dyf(ieq,iuw,j,k)       &
               -ab010*dy*f(ieq,iuw,j,k)+c010*dy         &
               -sumxy )*ddx*ddy2
       c110 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -c210*dx2*dy-c120*dx*dy2                 &
               -sumxy )*ddx*ddy

       c201 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab001*dz)*dx*dxf(ieq,i,j,kuw)       &
               -ab100*dx*f(ieq,i,j,kuw)+c100*dx         &
               -sumxz )*ddx2*ddz
       c102 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab100*dx)*dz*dzf(ieq,iuw,j,k)       &
               -ab001*dz*f(ieq,iuw,j,k)+c001*dz         &
               -sumxz )*ddx*ddz2
       c101 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -c201*dx2*dz-c102*dx*dz2                 &
               -sumxz )*ddx*ddz

       c021 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab001*dz)*dy*dyf(ieq,i,j,kuw)       &
               -ab010*dy*f(ieq,i,j,kuw)+c010*dy         &
               -sumyz )*ddy2*ddz
       c012 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab010*dy)*dz*dzf(ieq,i,juw,k)       &
               -ab001*dz*f(ieq,i,juw,k)+c001*dz         &
               -sumyz )*ddy*ddz2
       c011 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -c021*dy2*dz-c012*dy*dz2                 &
               -sumyz )*ddy*ddz

       c111 = ( (1.+ab100*dx+ab010*dy+ab001*dz)*f(ieq,iuw,juw,kuw) &
               -0.5*(sumxy+sumxz+sumyz-f(ieq,i,j,k))               &
               -c210*dx2*dy-c120*dx*dy2                            &
               -c201*dx2*dz-c102*dx*dz2                            &
               -c021*dy2*dz-c012*dy*dz2                            &
               -c110*dx*dy-c101*dx*dz                              &
               -c011*dy*dz                                         &
              )*ddx*ddy*ddz

       tmp10 = ( (c300*dlx+c210*dly+c201*dlz+c200)*dlx+c110*dly+c100 )*dlx &
              +( (c030*dly+c021*dlz+c120*dlx+c020)*dly+c011*dlz+c010 )*dly &
              +( (c003*dlz+c102*dlx+c012*dly+c002)*dlz+c101*dlx+c001 )*dlz &
              +c111*dlx*dly*dlz                                            &
              +f(ieq,i,j,k)

       tmp11 = 1./(1.0D0+ab100*dlx+ab010*dly+ab001*dlz)
       tmp12 = tmp10*tmp11

       gf(ieq,i,j,k) = tmp12*fac                                  &
                      +(f(ieq,i,j,k)+sx*dlx+sy*dly+sz*dlz)*(1.-fac)

       gdxf(ieq,i,j,k) = tmp11*(                               &
             (3.*c300*dlx+2.*c210*dly+2.*c201*dlz+2.*c200)*dlx &
            +(c120*dly+c111*dlz+c110)*dly                      & 
            +(c102*dlz+c101)*dlz+c100                          &
            -ab100*tmp12 )*fac!                                 &
!            +sx*(1.-fac)

       gdyf(ieq,i,j,k) = tmp11*(                               &
             (3.*c030*dly+2.*c021*dlz+2.*c120*dlx+2.*c020)*dly &
            +(c210*dlx+c111*dlz+c110)*dlx                      &
            +(c012*dlz+c011)*dlz+c010                          &
            -ab010*tmp12 )*fac!                                 &
!            +sy*(1.-fac)

       gdzf(ieq,i,j,k) = tmp11*(                               &
             (3.*c003*dlz+2.*c102*dlx+2.*c012*dly+2.*c002)*dlz &
            +(c201*dlx+c111*dly+c101)*dlx                      &
            +(c021*dly+c011)*dly+c001                          &
            -ab001*tmp12 )*fac!                                &
!            +sz*(1.-fac)


       ieq = 2

       sx = (f(ieq,iuw,j,k)-f(ieq,i,j,k))*ddx
       tmp4 = dxf(ieq,iuw,j,k)-sx 
       tmp4 = tmp4 + 1.D-18*sign(1.D0,tmp4)
       b100 = 0.5*(1.+dtanh((dabs(tmp4)-eps)/(0.05*eps))) &
             *(dabs((sx-dxf(ieq,i,j,k))/tmp4)-1.0)*ddx
       ab100 = a100*b100
       tmp7 = 1.0+ab100*dx
       c100 = dxf(ieq,i,j,k)+ab100*f(ieq,i,j,k)
       c300 = ( tmp7*tmp4+dxf(ieq,i,j,k)-sx )*ddx2
       c200 = ( tmp7*f(ieq,iuw,j,k)-f(ieq,i,j,k)-c100*dx )*ddx2-c300*dx
       
       sy = (f(ieq,i,juw,k)-f(ieq,i,j,k))*ddy
       tmp5 = dyf(ieq,i,juw,k)-sy
       tmp5 = tmp5 + 1.D-18*sign(1.D0,tmp5)
       b010 = 0.5*(1.+dtanh((dabs(tmp5)-eps)/(0.05*eps))) &
             *(dabs((sy-dyf(ieq,i,j,k))/tmp5)-1.0)*ddy
       ab010 = a010*b010
       tmp8 = 1.0+ab010*dy
       c010 = dyf(ieq,i,j,k)+ab010*f(ieq,i,j,k)
       c030 = ( tmp8*tmp5+dyf(ieq,i,j,k)-sy )*ddy2
       c020 = ( tmp8*f(ieq,i,juw,k)-f(ieq,i,j,k)-c010*dy )*ddy2-c030*dy

       sz = (f(ieq,i,j,kuw)-f(ieq,i,j,k))*ddz
       tmp6 = dzf(ieq,i,j,kuw)-sz
       tmp6 = tmp6 + 1.D-18*sign(1.D0,tmp6)
       b001 = 0.5*(1.+dtanh((dabs(tmp6)-eps)/(0.05*eps))) &
             *(dabs((sz-dzf(ieq,i,j,k))/tmp6)-1.0)*ddz
       ab001 = a001*b001
       tmp9 = 1.0+ab001*dz
       c001 = dzf(ieq,i,j,k)+ab001*f(ieq,i,j,k)
       c003 = ( tmp9*tmp6+dzf(ieq,i,j,k)-sz )*ddz2
       c002 = ( tmp9*f(ieq,i,j,kuw)-f(ieq,i,j,k)-c001*dz )*ddz2-c003*dz

       sumxy = +(c100+c200*dx+c300*dx2)*dx &
               +(c010+c020*dy+c030*dy2)*dy+f(ieq,i,j,k)
       sumxz = +(c100+c200*dx+c300*dx2)*dx &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)
       sumyz = +(c010+c020*dy+c030*dy2)*dy &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)

       c210 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab010*dy)*dx*dxf(ieq,i,juw,k)       &
               -ab100*dx*f(ieq,i,juw,k)+c100*dx         &
               -sumxy )*ddx2*ddy
       c120 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab100*dx)*dy*dyf(ieq,iuw,j,k)       &
               -ab010*dy*f(ieq,iuw,j,k)+c010*dy         &
               -sumxy )*ddx*ddy2
       c110 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -c210*dx2*dy-c120*dx*dy2                 &
               -sumxy )*ddx*ddy

       c201 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab001*dz)*dx*dxf(ieq,i,j,kuw)       &
               -ab100*dx*f(ieq,i,j,kuw)+c100*dx         &
               -sumxz )*ddx2*ddz
       c102 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab100*dx)*dz*dzf(ieq,iuw,j,k)       &
               -ab001*dz*f(ieq,iuw,j,k)+c001*dz         &
               -sumxz )*ddx*ddz2
       c101 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -c201*dx2*dz-c102*dx*dz2                 &
               -sumxz )*ddx*ddz

       c021 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab001*dz)*dy*dyf(ieq,i,j,kuw)       &
               -ab010*dy*f(ieq,i,j,kuw)+c010*dy         &
               -sumyz )*ddy2*ddz
       c012 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab010*dy)*dz*dzf(ieq,i,juw,k)       &
               -ab001*dz*f(ieq,i,juw,k)+c001*dz         &
               -sumyz )*ddy*ddz2
       c011 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -c021*dy2*dz-c012*dy*dz2                 &
               -sumyz )*ddy*ddz

       c111 = ( (1.+ab100*dx+ab010*dy+ab001*dz)*f(ieq,iuw,juw,kuw) &
               -0.5*(sumxy+sumxz+sumyz-f(ieq,i,j,k))               &
               -c210*dx2*dy-c120*dx*dy2                            &
               -c201*dx2*dz-c102*dx*dz2                            &
               -c021*dy2*dz-c012*dy*dz2                            &
               -c110*dx*dy-c101*dx*dz                              &
               -c011*dy*dz                                         &
              )*ddx*ddy*ddz

       tmp10 = ( (c300*dlx+c210*dly+c201*dlz+c200)*dlx+c110*dly+c100 )*dlx &
              +( (c030*dly+c021*dlz+c120*dlx+c020)*dly+c011*dlz+c010 )*dly &
              +( (c003*dlz+c102*dlx+c012*dly+c002)*dlz+c101*dlx+c001 )*dlz &
              +c111*dlx*dly*dlz                                            &
              +f(ieq,i,j,k)

       tmp11 = 1./(1.0D0+ab100*dlx+ab010*dly+ab001*dlz)
       tmp12 = tmp10*tmp11

       gf(ieq,i,j,k) = tmp12*fac                                  &
                      +(f(ieq,i,j,k)+sx*dlx+sy*dly+sz*dlz)*(1.-fac)

       gdxf(ieq,i,j,k) = tmp11*(                               &
             (3.*c300*dlx+2.*c210*dly+2.*c201*dlz+2.*c200)*dlx &
            +(c120*dly+c111*dlz+c110)*dly                      & 
            +(c102*dlz+c101)*dlz+c100                          &
            -ab100*tmp12 )*fac!                                 &
!            +sx*(1.-fac)

       gdyf(ieq,i,j,k) = tmp11*(                               &
             (3.*c030*dly+2.*c021*dlz+2.*c120*dlx+2.*c020)*dly &
            +(c210*dlx+c111*dlz+c110)*dlx                      &
            +(c012*dlz+c011)*dlz+c010                          &
            -ab010*tmp12 )*fac!                                 &
!            +sy*(1.-fac)

       gdzf(ieq,i,j,k) = tmp11*(                               &
             (3.*c003*dlz+2.*c102*dlx+2.*c012*dly+2.*c002)*dlz &
            +(c201*dlx+c111*dly+c101)*dlx                      &
            +(c021*dly+c011)*dly+c001                          &
            -ab001*tmp12 )*fac!                                &
!            +sz*(1.-fac)


       ieq = 3

!       fac = 0.5*(1.0+tanh((0.5*(f(2,i,j,k)+f(2,ip,j,k))-pmin)*dp))
       fac = min(min(0.5*(1.0+tanh((0.5*(f(2,i,j,k)+f(2,ip,j,k))-pmin)*dp)), &
                     0.5*(1.0+tanh((0.5*(f(1,i,j,k)+f(1,ip,j,k))-nmin)*dn))), &
                 0.5*(1.0+tanh((0.5*(1.D0/(f(1,i,j,k)*f(1,i,j,k))*f(2,i,j,k)+1.D0/(f(1,ip,j,k)*f(1,ip,j,k))*f(2,ip,j,k))-nmin)*dn)) &
                )

       uvx = 0.5*(+f(3,i,j,k)+f(4,i,j,k))
       uvy = 0.125*(+f(5,i,jm,k)+f(5,ip,jm,k) &
                    +f(5,i,j,k)+f(5,ip,j,k)   &
                    +f(6,i,jm,k)+f(6,ip,jm,k) &
                    +f(6,i,j,k)+f(6,ip,j,k))  
       uvz = 0.125*(+f(7,i,j,km)+f(7,ip,j,km) &
                    +f(7,i,j,k)+f(7,ip,j,k)   &
                    +f(8,i,j,km)+f(8,ip,j,km) &
                    +f(8,i,j,k)+f(8,ip,j,k))  
 
       isn = -sign(1.D0,uvx)
       iuw = i+isn
       dlx = -delt*uvx*flg(ieq,i,j,k)

       jsn = -sign(1.D0,uvy)
       juw = j+jsn
       dly = -delt*uvy*flg(ieq,i,j,k)
       
       ksn = -sign(1.D0,uvz)
       kuw = k+ksn
       dlz = -delt*uvz*flg(ieq,i,j,k)

       !dipole field at upwind position
       x0 = (i+0.5+dlx-px0)*drs
       y0 = (j+dly-py0)*drs
       z0 = (k+dlz-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)
       
       xm = (i+0.5+dlx-pxm)*drs
       ym = (j+dly-pym)*drs
       zm = (k+dlz-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       vaxu   = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       dxvaxu = dxbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)*dsn0 &
               -0.5*vaxu*dn0*dxn0(x0,r0)
       dyvaxu = dybdx(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
               -0.5*vaxu*dn0*dyn0(y0,r0)
       dzvaxu = dzbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)*dsn0 &
               -0.5*vaxu*dn0*dzn0(z0,r0)
       !end dipole 

       !dipole field at i+1/2,j,k
       x0 = (i+0.5-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0   = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)
       
       xm = (i+0.5-pxm)*drs
       ym = (j-pym)*drs
       zm = (k-pzm)*drs
       rm   = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       vax  = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       vay  = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       vaz  = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       dxvax = dxbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dn0*dxn0(x0,r0)
       dyvax = dybdx(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dn0*dyn0(y0,r0)
       dzvax = dzbdx(x0,z0,r0,xm,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vax*dn0*dzn0(z0,r0)
       !end dipole

       uvz = 0.25*(+f(8,i,j,km)+f(8,ip,j,km) &
                   +f(8,i,j,k)+f(8,ip,j,k))-vaz
       uvy = 0.25*(+f(6,i,jm,k)+f(6,ip,jm,k) &
                   +f(6,i,j,k)+f(6,ip,j,k))-vay

       jsn = -sign(1.D0,uvy)
       juw = j+jsn
       dly = -delt*uvy*flg(ieq,i,j,k)

       ksn = -sign(1.D0,uvz)
       kuw = k+ksn
       dlz = -delt*uvz*flg(ieq,i,j,k)
          
       dx = dble(isn)
       ddx = 1./dx
       dx2 = dx*dx
       ddx2 = ddx*ddx
       dy = dble(jsn)
       ddy = 1./dy
       dy2 = dy*dy
       ddy2 = ddy*ddy
       dz = dble(ksn)
       ddz = 1./dz
       dz2 = dz*dz
       ddz2 = ddz*ddz
          
       sx = (f(ieq,iuw,j,k)-f(ieq,i,j,k))*ddx
       tmp4 = dxf(ieq,iuw,j,k)-sx 
       tmp4 = tmp4 + 1.D-18*sign(1.D0,tmp4)
       b100 = 0.5*(1.+dtanh((dabs(tmp4)-eps)/(0.05*eps))) &
             *(dabs((sx-dxf(ieq,i,j,k))/tmp4)-1.0)*ddx
       ab100 = a100*b100
       tmp7 = 1.0+ab100*dx
       c100 = dxf(ieq,i,j,k)+ab100*f(ieq,i,j,k)
       c300 = ( tmp7*tmp4+dxf(ieq,i,j,k)-sx )*ddx2
       c200 = ( tmp7*f(ieq,iuw,j,k)-f(ieq,i,j,k)-c100*dx )*ddx2-c300*dx
       
       sy = (f(ieq,i,juw,k)-f(ieq,i,j,k))*ddy
       tmp5 = dyf(ieq,i,juw,k)-sy
       tmp5 = tmp5 + 1.D-18*sign(1.D0,tmp5)
       b010 = 0.5*(1.+dtanh((dabs(tmp5)-eps)/(0.05*eps))) &
             *(dabs((sy-dyf(ieq,i,j,k))/tmp5)-1.0)*ddy
       ab010 = a010*b010
       tmp8 = 1.0+ab010*dy
       c010 = dyf(ieq,i,j,k)+ab010*f(ieq,i,j,k)
       c030 = ( tmp8*tmp5+dyf(ieq,i,j,k)-sy )*ddy2
       c020 = ( tmp8*f(ieq,i,juw,k)-f(ieq,i,j,k)-c010*dy )*ddy2-c030*dy

       sz = (f(ieq,i,j,kuw)-f(ieq,i,j,k))*ddz
       tmp6 = dzf(ieq,i,j,kuw)-sz
       tmp6 = tmp6 + 1.D-18*sign(1.D0,tmp6)
       b001 = 0.5*(1.+dtanh((dabs(tmp6)-eps)/(0.05*eps))) &
             *(dabs((sz-dzf(ieq,i,j,k))/tmp6)-1.0)*ddz
       ab001 = a001*b001
       tmp9 = 1.0+ab001*dz
       c001 = dzf(ieq,i,j,k)+ab001*f(ieq,i,j,k)
       c003 = ( tmp9*tmp6+dzf(ieq,i,j,k)-sz )*ddz2
       c002 = ( tmp9*f(ieq,i,j,kuw)-f(ieq,i,j,k)-c001*dz )*ddz2-c003*dz

       sumxy = +(c100+c200*dx+c300*dx2)*dx &
               +(c010+c020*dy+c030*dy2)*dy+f(ieq,i,j,k)
       sumxz = +(c100+c200*dx+c300*dx2)*dx &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)
       sumyz = +(c010+c020*dy+c030*dy2)*dy &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)
       c210 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab010*dy)*dx*dxf(ieq,i,juw,k)       &
               -ab100*dx*f(ieq,i,juw,k)+c100*dx         &
               -sumxy )*ddx2*ddy
       c120 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab100*dx)*dy*dyf(ieq,iuw,j,k)       &
               -ab010*dy*f(ieq,iuw,j,k)+c010*dy         &
               -sumxy )*ddx*ddy2
       c110 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -c210*dx2*dy-c120*dx*dy2                 &
               -sumxy )*ddx*ddy

       c201 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab001*dz)*dx*dxf(ieq,i,j,kuw)       &
               -ab100*dx*f(ieq,i,j,kuw)+c100*dx         &
               -sumxz )*ddx2*ddz
       c102 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab100*dx)*dz*dzf(ieq,iuw,j,k)       &
               -ab001*dz*f(ieq,iuw,j,k)+c001*dz         &
               -sumxz )*ddx*ddz2
       c101 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -c201*dx2*dz-c102*dx*dz2                 &
               -sumxz )*ddx*ddz

       c021 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab001*dz)*dy*dyf(ieq,i,j,kuw)       &
               -ab010*dy*f(ieq,i,j,kuw)+c010*dy         &
               -sumyz )*ddy2*ddz
       c012 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab010*dy)*dz*dzf(ieq,i,juw,k)       &
               -ab001*dz*f(ieq,i,juw,k)+c001*dz         &
               -sumyz )*ddy*ddz2
       c011 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -c021*dy2*dz-c012*dy*dz2                 &
               -sumyz )*ddy*ddz

       c111 = ( (1.+ab100*dx+ab010*dy+ab001*dz)*f(ieq,iuw,juw,kuw) &
               -0.5*(sumxy+sumxz+sumyz-f(ieq,i,j,k))               &
               -c210*dx2*dy-c120*dx*dy2                            &
               -c201*dx2*dz-c102*dx*dz2                            &
               -c021*dy2*dz-c012*dy*dz2                            &
               -c110*dx*dy-c101*dx*dz                              &
               -c011*dy*dz                                         &
              )*ddx*ddy*ddz

       tmp10 = ( (c300*dlx+c210*dly+c201*dlz+c200)*dlx+c110*dly+c100 )*dlx &
              +( (c030*dly+c021*dlz+c120*dlx+c020)*dly+c011*dlz+c010 )*dly &
              +( (c003*dlz+c102*dlx+c012*dly+c002)*dlz+c101*dlx+c001 )*dlz &
              +c111*dlx*dly*dlz                                            &
              +f(ieq,i,j,k)

       tmp11 = 1./(1.0D0+ab100*dlx+ab010*dly+ab001*dlz)
       tmp12 = tmp10*tmp11

       gf(ieq,i,j,k) = tmp12*fac                                    &
                      +(f(ieq,i,j,k)+sx*dlx+sy*dly+sz*dlz)*(1.-fac) &
                      +vaxu-vax

       gdxf(ieq,i,j,k) = tmp11*(                               &
             (3.*c300*dlx+2.*c210*dly+2.*c201*dlz+2.*c200)*dlx &
            +(c120*dly+c111*dlz+c110)*dly                      & 
            +(c102*dlz+c101)*dlz+c100                          &
            -ab100*tmp12 )*fac                                 &
            +dxvaxu-dxvax!+sx*(1.-fac)

       gdyf(ieq,i,j,k) = tmp11*(                               &
             (3.*c030*dly+2.*c021*dlz+2.*c120*dlx+2.*c020)*dly &
            +(c210*dlx+c111*dlz+c110)*dlx                      &
            +(c012*dlz+c011)*dlz+c010                          &
            -ab010*tmp12 )*fac                                 &
            +dyvaxu-dyvax!+sy*(1.-fac)

       gdzf(ieq,i,j,k) = tmp11*(                               &
             (3.*c003*dlz+2.*c102*dlx+2.*c012*dly+2.*c002)*dlz &
            +(c201*dlx+c111*dly+c101)*dlx                      &
            +(c021*dly+c011)*dly+c001                          &
            -ab001*tmp12 )*fac                                 &
            +dzvaxu-dzvax!+sz*(1.-fac)


       ieq = 4

       uvz = 0.25*(+f(7,i,j,km)+f(7,ip,j,km) &
                   +f(7,i,j,k)+f(7,ip,j,k))+vaz
       uvy = 0.25*(+f(5,i,jm,k)+f(5,ip,jm,k) &
                   +f(5,i,j,k)+f(5,ip,j,k))+vay  

       jsn = -sign(1.D0,uvy)
       juw = j+jsn
       dly = -delt*uvy*flg(ieq,i,j,k)

       ksn = -sign(1.D0,uvz)
       kuw = k+ksn
       dlz = -delt*uvz*flg(ieq,i,j,k)
          
       dx = dble(isn)
       ddx = 1./dx
       dx2 = dx*dx
       ddx2 = ddx*ddx
       dy = dble(jsn)
       ddy = 1./dy
       dy2 = dy*dy
       ddy2 = ddy*ddy
       dz = dble(ksn)
       ddz = 1./dz
       dz2 = dz*dz
       ddz2 = ddz*ddz
          
       sx = (f(ieq,iuw,j,k)-f(ieq,i,j,k))*ddx
       tmp4 = dxf(ieq,iuw,j,k)-sx 
       tmp4 = tmp4 + 1.D-18*sign(1.D0,tmp4)
       b100 = 0.5*(1.+dtanh((dabs(tmp4)-eps)/(0.05*eps))) &
             *(dabs((sx-dxf(ieq,i,j,k))/tmp4)-1.0)*ddx
       ab100 = a100*b100
       tmp7 = 1.0+ab100*dx
       c100 = dxf(ieq,i,j,k)+ab100*f(ieq,i,j,k)
       c300 = ( tmp7*tmp4+dxf(ieq,i,j,k)-sx )*ddx2
       c200 = ( tmp7*f(ieq,iuw,j,k)-f(ieq,i,j,k)-c100*dx )*ddx2-c300*dx
       
       sy = (f(ieq,i,juw,k)-f(ieq,i,j,k))*ddy
       tmp5 = dyf(ieq,i,juw,k)-sy
       tmp5 = tmp5 + 1.D-18*sign(1.D0,tmp5)
       b010 = 0.5*(1.+dtanh((dabs(tmp5)-eps)/(0.05*eps))) &
             *(dabs((sy-dyf(ieq,i,j,k))/tmp5)-1.0)*ddy
       ab010 = a010*b010
       tmp8 = 1.0+ab010*dy
       c010 = dyf(ieq,i,j,k)+ab010*f(ieq,i,j,k)
       c030 = ( tmp8*tmp5+dyf(ieq,i,j,k)-sy )*ddy2
       c020 = ( tmp8*f(ieq,i,juw,k)-f(ieq,i,j,k)-c010*dy )*ddy2-c030*dy

       sz = (f(ieq,i,j,kuw)-f(ieq,i,j,k))*ddz
       tmp6 = dzf(ieq,i,j,kuw)-sz
       tmp6 = tmp6 + 1.D-18*sign(1.D0,tmp6)
       b001 = 0.5*(1.+dtanh((dabs(tmp6)-eps)/(0.05*eps))) &
             *(dabs((sz-dzf(ieq,i,j,k))/tmp6)-1.0)*ddz
       ab001 = a001*b001
       tmp9 = 1.0+ab001*dz
       c001 = dzf(ieq,i,j,k)+ab001*f(ieq,i,j,k)
       c003 = ( tmp9*tmp6+dzf(ieq,i,j,k)-sz )*ddz2
       c002 = ( tmp9*f(ieq,i,j,kuw)-f(ieq,i,j,k)-c001*dz )*ddz2-c003*dz

       sumxy = +(c100+c200*dx+c300*dx2)*dx &
               +(c010+c020*dy+c030*dy2)*dy+f(ieq,i,j,k)
       sumxz = +(c100+c200*dx+c300*dx2)*dx &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)
       sumyz = +(c010+c020*dy+c030*dy2)*dy &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)

       c210 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab010*dy)*dx*dxf(ieq,i,juw,k)       &
               -ab100*dx*f(ieq,i,juw,k)+c100*dx         &
               -sumxy )*ddx2*ddy
       c120 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab100*dx)*dy*dyf(ieq,iuw,j,k)       &
               -ab010*dy*f(ieq,iuw,j,k)+c010*dy         &
               -sumxy )*ddx*ddy2
       c110 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -c210*dx2*dy-c120*dx*dy2                 &
               -sumxy )*ddx*ddy

       c201 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab001*dz)*dx*dxf(ieq,i,j,kuw)       &
               -ab100*dx*f(ieq,i,j,kuw)+c100*dx         &
               -sumxz )*ddx2*ddz
       c102 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab100*dx)*dz*dzf(ieq,iuw,j,k)       &
               -ab001*dz*f(ieq,iuw,j,k)+c001*dz         &
               -sumxz )*ddx*ddz2
       c101 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -c201*dx2*dz-c102*dx*dz2                 &
               -sumxz )*ddx*ddz

       c021 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab001*dz)*dy*dyf(ieq,i,j,kuw)       &
               -ab010*dy*f(ieq,i,j,kuw)+c010*dy         &
               -sumyz )*ddy2*ddz
       c012 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab010*dy)*dz*dzf(ieq,i,juw,k)       &
               -ab001*dz*f(ieq,i,juw,k)+c001*dz         &
               -sumyz )*ddy*ddz2
       c011 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -c021*dy2*dz-c012*dy*dz2                 &
               -sumyz )*ddy*ddz

       c111 = ( (1.+ab100*dx+ab010*dy+ab001*dz)*f(ieq,iuw,juw,kuw) &
               -0.5*(sumxy+sumxz+sumyz-f(ieq,i,j,k))               &
               -c210*dx2*dy-c120*dx*dy2                            &
               -c201*dx2*dz-c102*dx*dz2                            &
               -c021*dy2*dz-c012*dy*dz2                            &
               -c110*dx*dy-c101*dx*dz                              &
               -c011*dy*dz                                         &
              )*ddx*ddy*ddz

       tmp10 = ( (c300*dlx+c210*dly+c201*dlz+c200)*dlx+c110*dly+c100 )*dlx &
              +( (c030*dly+c021*dlz+c120*dlx+c020)*dly+c011*dlz+c010 )*dly &
              +( (c003*dlz+c102*dlx+c012*dly+c002)*dlz+c101*dlx+c001 )*dlz &
              +c111*dlx*dly*dlz                                            &
              +f(ieq,i,j,k)

       tmp11 = 1./(1.0D0+ab100*dlx+ab010*dly+ab001*dlz)
       tmp12 = tmp10*tmp11

       gf(ieq,i,j,k) = tmp12*fac                                    &
                      +(f(ieq,i,j,k)+sx*dlx+sy*dly+sz*dlz)*(1.-fac) &
                      -(vaxu-vax)

       gdxf(ieq,i,j,k) = tmp11*(                               &
             (3.*c300*dlx+2.*c210*dly+2.*c201*dlz+2.*c200)*dlx &
            +(c120*dly+c111*dlz+c110)*dly                      & 
            +(c102*dlz+c101)*dlz+c100                          &
            -ab100*tmp12 )*fac                                 &
            -(dxvaxu-dxvax)!+sx*(1.-fac)

       gdyf(ieq,i,j,k) = tmp11*(                               &
             (3.*c030*dly+2.*c021*dlz+2.*c120*dlx+2.*c020)*dly &
            +(c210*dlx+c111*dlz+c110)*dlx                      &
            +(c012*dlz+c011)*dlz+c010                          &
            -ab010*tmp12 )*fac                                 &
            -(dyvaxu-dyvax)!+sy*(1.-fac)

       gdzf(ieq,i,j,k) = tmp11*(                               &
             (3.*c003*dlz+2.*c102*dlx+2.*c012*dly+2.*c002)*dlz &
            +(c201*dlx+c111*dly+c101)*dlx                      &
            +(c021*dly+c011)*dly+c001                          &
            -ab001*tmp12 )*fac                                 &
            -(dzvaxu-dzvax)!+sz*(1.-fac)


       ieq = 5

       fac = min(min(0.5*(1.0+tanh((0.5*(f(2,i,j,k)+f(2,i,jp,k))-pmin)*dp)), &
                     0.5*(1.0+tanh((0.5*(f(1,i,j,k)+f(1,i,jp,k))-nmin)*dn))), &
                 0.5*(1.0+tanh((0.5*(1.D0/(f(1,i,j,k)*f(1,i,j,k))*f(2,i,j,k)+1.D0/(f(1,i,jp,k)*f(1,i,jp,k))*f(2,i,jp,k))-nmin)*dn)) &
                )
!       fac = 0.5*(1.0+tanh((0.5*(f(2,i,j,k)+f(2,i,jp,k))-pmin)*dp))

       uvx = 0.125*(+f(3,im,j,k)+f(3,i,j,k)    &
                    +f(3,im,jp,k)+f(3,i,jp,k)  &
                    +f(4,im,j,k)+f(4,i,j,k)    &
                    +f(4,im,jp,k)+f(4,i,jp,k)) 
       uvy = 0.5*(+f(5,i,j,k)+f(6,i,j,k))
       uvz = 0.125*(+f(7,i,j,km)+f(7,i,jp,km) &
                    +f(7,i,j,k)+f(7,i,jp,k)   &
                    +f(8,i,j,km)+f(8,i,jp,km) &
                    +f(8,i,j,k)+f(8,i,jp,k))  

       isn = -sign(1.D0,uvx)
       iuw = i+isn
       dlx = -delt*uvx*flg(ieq,i,j,k)
        
       jsn = -sign(1.D0,uvy)
       juw = j+jsn
       dly = -delt*uvy*flg(ieq,i,j,k)

       ksn = -sign(1.D0,uvz)
       kuw = k+ksn
       dlz = -delt*uvz*flg(ieq,i,j,k)

       !dipole field at upwind position
       x0 = (i+dlx-px0)*drs
       y0 = (j+0.5+dly-py0)*drs
       z0 = (k+dlz-pz0)*drs
       r0   = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i+dlx-pxm)*drs
       ym = (j+0.5+dly-pym)*drs
       zm = (k+dlz-pzm)*drs
       rm   = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       vayu  = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       dxvayu = dxbdy(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
               -0.5*vayu*dn0*dxn0(x0,r0)
       dyvayu = dybdy(y0,z0,r0,ym,zm,rm,dr05,drm5)*dsn0 &
               -0.5*vayu*dn0*dyn0(y0,r0)
       dzvayu = dzbdy(y0,z0,r0,ym,zm,rm,dr05,drm5)*dsn0 &
               -0.5*vayu*dn0*dzn0(z0,r0)
       !end dipole 

       !dipole field at i,j+1/2,k
       x0 = (i-px0)*drs
       y0 = (j+0.5-py0)*drs
       z0 = (k-pz0)*drs
       r0   = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i-pxm)*drs
       ym = (j+0.5-pym)*drs
       zm = (k-pzm)*drs
       rm   = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       vax   = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       vay   = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       vaz   = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       dxvay = dxbdy(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dn0*dxn0(x0,r0)
       dyvay = dybdy(y0,z0,r0,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dn0*dyn0(y0,r0)
       dzvay = dzbdy(y0,z0,r0,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vay*dn0*dzn0(z0,r0)
       !end dipole 

       uvz = 0.25*(+f(8,i,j,km)+f(8,i,jp,km) &
                   +f(8,i,j,k)+f(8,i,jp,k))-vaz
       uvx = 0.25*(+f(4,im,j,k)+f(4,i,j,k)    &
                   +f(4,im,jp,k)+f(4,i,jp,k))-vax

       isn = -sign(1.D0,uvx)
       iuw = i+isn
       dlx = -delt*uvx*flg(ieq,i,j,k)

       ksn = -sign(1.D0,uvz)
       kuw = k+ksn
       dlz = -delt*uvz*flg(ieq,i,j,k)
          
       dx = dble(isn)
       ddx = 1./dx
       dx2 = dx*dx
       ddx2 = ddx*ddx
       dy = dble(jsn)
       ddy = 1./dy
       dy2 = dy*dy
       ddy2 = ddy*ddy
       dz = dble(ksn)
       ddz = 1./dz
       dz2 = dz*dz
       ddz2 = ddz*ddz
       
       sx = (f(ieq,iuw,j,k)-f(ieq,i,j,k))*ddx
       tmp4 = dxf(ieq,iuw,j,k)-sx 
       tmp4 = tmp4 + 1.D-18*sign(1.D0,tmp4)
       b100 = 0.5*(1.+dtanh((dabs(tmp4)-eps)/(0.05*eps))) &
             *(dabs((sx-dxf(ieq,i,j,k))/tmp4)-1.0)*ddx
       ab100 = a100*b100
       tmp7 = 1.0+ab100*dx
       c100 = dxf(ieq,i,j,k)+ab100*f(ieq,i,j,k)
       c300 = ( tmp7*tmp4+dxf(ieq,i,j,k)-sx )*ddx2
       c200 = ( tmp7*f(ieq,iuw,j,k)-f(ieq,i,j,k)-c100*dx )*ddx2-c300*dx
       
       sy = (f(ieq,i,juw,k)-f(ieq,i,j,k))*ddy
       tmp5 = dyf(ieq,i,juw,k)-sy
       tmp5 = tmp5 + 1.D-18*sign(1.D0,tmp5)
       b010 = 0.5*(1.+dtanh((dabs(tmp5)-eps)/(0.05*eps))) &
             *(dabs((sy-dyf(ieq,i,j,k))/tmp5)-1.0)*ddy
       ab010 = a010*b010
       tmp8 = 1.0+ab010*dy
       c010 = dyf(ieq,i,j,k)+ab010*f(ieq,i,j,k)
       c030 = ( tmp8*tmp5+dyf(ieq,i,j,k)-sy )*ddy2
       c020 = ( tmp8*f(ieq,i,juw,k)-f(ieq,i,j,k)-c010*dy )*ddy2-c030*dy

       sz = (f(ieq,i,j,kuw)-f(ieq,i,j,k))*ddz
       tmp6 = dzf(ieq,i,j,kuw)-sz
       tmp6 = tmp6 + 1.D-18*sign(1.D0,tmp6)
       b001 = 0.5*(1.+dtanh((dabs(tmp6)-eps)/(0.05*eps))) &
             *(dabs((sz-dzf(ieq,i,j,k))/tmp6)-1.0)*ddz
       ab001 = a001*b001
       tmp9 = 1.0+ab001*dz
       c001 = dzf(ieq,i,j,k)+ab001*f(ieq,i,j,k)
       c003 = ( tmp9*tmp6+dzf(ieq,i,j,k)-sz )*ddz2
       c002 = ( tmp9*f(ieq,i,j,kuw)-f(ieq,i,j,k)-c001*dz )*ddz2-c003*dz

       sumxy = +(c100+c200*dx+c300*dx2)*dx &
               +(c010+c020*dy+c030*dy2)*dy+f(ieq,i,j,k)
       sumxz = +(c100+c200*dx+c300*dx2)*dx &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)
       sumyz = +(c010+c020*dy+c030*dy2)*dy &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)
        
       c210 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab010*dy)*dx*dxf(ieq,i,juw,k)       &
               -ab100*dx*f(ieq,i,juw,k)+c100*dx         &
               -sumxy )*ddx2*ddy
       c120 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab100*dx)*dy*dyf(ieq,iuw,j,k)       &
               -ab010*dy*f(ieq,iuw,j,k)+c010*dy         &
               -sumxy )*ddx*ddy2
       c110 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -c210*dx2*dy-c120*dx*dy2                 &
               -sumxy )*ddx*ddy

       c201 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab001*dz)*dx*dxf(ieq,i,j,kuw)       &
               -ab100*dx*f(ieq,i,j,kuw)+c100*dx         &
               -sumxz )*ddx2*ddz
       c102 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab100*dx)*dz*dzf(ieq,iuw,j,k)       &
               -ab001*dz*f(ieq,iuw,j,k)+c001*dz         &
               -sumxz )*ddx*ddz2
       c101 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -c201*dx2*dz-c102*dx*dz2                 &
               -sumxz )*ddx*ddz

       c021 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab001*dz)*dy*dyf(ieq,i,j,kuw)       &
               -ab010*dy*f(ieq,i,j,kuw)+c010*dy         &
               -sumyz )*ddy2*ddz
       c012 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab010*dy)*dz*dzf(ieq,i,juw,k)       &
               -ab001*dz*f(ieq,i,juw,k)+c001*dz         &
               -sumyz )*ddy*ddz2
       c011 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -c021*dy2*dz-c012*dy*dz2                 &
               -sumyz )*ddy*ddz

       c111 = ( (1.+ab100*dx+ab010*dy+ab001*dz)*f(ieq,iuw,juw,kuw) &
               -0.5*(sumxy+sumxz+sumyz-f(ieq,i,j,k))               &
               -c210*dx2*dy-c120*dx*dy2                            &
               -c201*dx2*dz-c102*dx*dz2                            &
               -c021*dy2*dz-c012*dy*dz2                            &
               -c110*dx*dy-c101*dx*dz                              &
               -c011*dy*dz                                         &
              )*ddx*ddy*ddz

       tmp10 = ( (c300*dlx+c210*dly+c201*dlz+c200)*dlx+c110*dly+c100 )*dlx &
              +( (c030*dly+c021*dlz+c120*dlx+c020)*dly+c011*dlz+c010 )*dly &
              +( (c003*dlz+c102*dlx+c012*dly+c002)*dlz+c101*dlx+c001 )*dlz &
              +c111*dlx*dly*dlz                                            &
              +f(ieq,i,j,k)

       tmp11 = 1./(1.0D0+ab100*dlx+ab010*dly+ab001*dlz)
       tmp12 = tmp10*tmp11

       gf(ieq,i,j,k) = tmp12*fac                                    &
                      +(f(ieq,i,j,k)+sx*dlx+sy*dly+sz*dlz)*(1.-fac) &
                      +vayu-vay

       gdxf(ieq,i,j,k) = tmp11*(                               &
             (3.*c300*dlx+2.*c210*dly+2.*c201*dlz+2.*c200)*dlx &
            +(c120*dly+c111*dlz+c110)*dly                      & 
            +(c102*dlz+c101)*dlz+c100                          &
            -ab100*tmp12 )*fac                                 &
            +dxvayu-dxvay!+sx*(1.-fac)

       gdyf(ieq,i,j,k) = tmp11*(                               &
             (3.*c030*dly+2.*c021*dlz+2.*c120*dlx+2.*c020)*dly &
            +(c210*dlx+c111*dlz+c110)*dlx                      &
            +(c012*dlz+c011)*dlz+c010                          &
            -ab010*tmp12 )*fac                                 &
            +dyvayu-dyvay!+sy*(1.-fac)

       gdzf(ieq,i,j,k) = tmp11*(                               &
             (3.*c003*dlz+2.*c102*dlx+2.*c012*dly+2.*c002)*dlz &
            +(c201*dlx+c111*dly+c101)*dlx                      &
            +(c021*dly+c011)*dly+c001                          &
            -ab001*tmp12 )*fac                                 &
            +dzvayu-dzvay!+sz*(1.-fac)

        
       ieq = 6

       uvz = 0.25*(+f(7,i,j,km)+f(7,i,jp,km) &
                   +f(7,i,j,k)+f(7,i,jp,k))+vaz
       uvx = 0.25*(+f(3,im,j,k)+f(3,i,j,k)    &
                   +f(3,im,jp,k)+f(3,i,jp,k))+vax

       isn = -sign(1.D0,uvx)
       iuw = i+isn
       dlx = -delt*uvx*flg(ieq,i,j,k)

       ksn = -sign(1.D0,uvz)
       kuw = k+ksn
       dlz = -delt*uvz*flg(ieq,i,j,k)

       dx = dble(isn)
       ddx = 1./dx
       dx2 = dx*dx
       ddx2 = ddx*ddx
       dy = dble(jsn)
       ddy = 1./dy
       dy2 = dy*dy
       ddy2 = ddy*ddy
       dz = dble(ksn)
       ddz = 1./dz
       dz2 = dz*dz
       ddz2 = ddz*ddz
        
       sx = (f(ieq,iuw,j,k)-f(ieq,i,j,k))*ddx
       tmp4 = dxf(ieq,iuw,j,k)-sx 
       tmp4 = tmp4 + 1.D-18*sign(1.D0,tmp4)
       b100 = 0.5*(1.+dtanh((dabs(tmp4)-eps)/(0.05*eps))) &
             *(dabs((sx-dxf(ieq,i,j,k))/tmp4)-1.0)*ddx
       ab100 = a100*b100
       tmp7 = 1.0+ab100*dx
       c100 = dxf(ieq,i,j,k)+ab100*f(ieq,i,j,k)
       c300 = ( tmp7*tmp4+dxf(ieq,i,j,k)-sx )*ddx2
       c200 = ( tmp7*f(ieq,iuw,j,k)-f(ieq,i,j,k)-c100*dx )*ddx2-c300*dx
       
       sy = (f(ieq,i,juw,k)-f(ieq,i,j,k))*ddy
       tmp5 = dyf(ieq,i,juw,k)-sy
       tmp5 = tmp5 + 1.D-18*sign(1.D0,tmp5)
       b010 = 0.5*(1.+dtanh((dabs(tmp5)-eps)/(0.05*eps))) &
             *(dabs((sy-dyf(ieq,i,j,k))/tmp5)-1.0)*ddy
       ab010 = a010*b010
       tmp8 = 1.0+ab010*dy
       c010 = dyf(ieq,i,j,k)+ab010*f(ieq,i,j,k)
       c030 = ( tmp8*tmp5+dyf(ieq,i,j,k)-sy )*ddy2
       c020 = ( tmp8*f(ieq,i,juw,k)-f(ieq,i,j,k)-c010*dy )*ddy2-c030*dy

       sz = (f(ieq,i,j,kuw)-f(ieq,i,j,k))*ddz
       tmp6 = dzf(ieq,i,j,kuw)-sz
       tmp6 = tmp6 + 1.D-18*sign(1.D0,tmp6)
       b001 = 0.5*(1.+dtanh((dabs(tmp6)-eps)/(0.05*eps))) &
             *(dabs((sz-dzf(ieq,i,j,k))/tmp6)-1.0)*ddz
       ab001 = a001*b001
       tmp9 = 1.0+ab001*dz
       c001 = dzf(ieq,i,j,k)+ab001*f(ieq,i,j,k)
       c003 = ( tmp9*tmp6+dzf(ieq,i,j,k)-sz )*ddz2
       c002 = ( tmp9*f(ieq,i,j,kuw)-f(ieq,i,j,k)-c001*dz )*ddz2-c003*dz

       sumxy = +(c100+c200*dx+c300*dx2)*dx &
               +(c010+c020*dy+c030*dy2)*dy+f(ieq,i,j,k)
       sumxz = +(c100+c200*dx+c300*dx2)*dx &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)
       sumyz = +(c010+c020*dy+c030*dy2)*dy &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)

       c210 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab010*dy)*dx*dxf(ieq,i,juw,k)       &
               -ab100*dx*f(ieq,i,juw,k)+c100*dx         &
               -sumxy )*ddx2*ddy
       c120 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab100*dx)*dy*dyf(ieq,iuw,j,k)       &
               -ab010*dy*f(ieq,iuw,j,k)+c010*dy         &
               -sumxy )*ddx*ddy2
       c110 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -c210*dx2*dy-c120*dx*dy2                 &
               -sumxy )*ddx*ddy

       c201 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab001*dz)*dx*dxf(ieq,i,j,kuw)       &
               -ab100*dx*f(ieq,i,j,kuw)+c100*dx         &
               -sumxz )*ddx2*ddz
       c102 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab100*dx)*dz*dzf(ieq,iuw,j,k)       &
               -ab001*dz*f(ieq,iuw,j,k)+c001*dz         &
               -sumxz )*ddx*ddz2
       c101 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -c201*dx2*dz-c102*dx*dz2                 &
               -sumxz )*ddx*ddz

       c021 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab001*dz)*dy*dyf(ieq,i,j,kuw)       &
               -ab010*dy*f(ieq,i,j,kuw)+c010*dy         &
               -sumyz )*ddy2*ddz
       c012 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab010*dy)*dz*dzf(ieq,i,juw,k)       &
               -ab001*dz*f(ieq,i,juw,k)+c001*dz         &
               -sumyz )*ddy*ddz2
       c011 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -c021*dy2*dz-c012*dy*dz2                 &
               -sumyz )*ddy*ddz

       c111 = ( (1.+ab100*dx+ab010*dy+ab001*dz)*f(ieq,iuw,juw,kuw) &
               -0.5*(sumxy+sumxz+sumyz-f(ieq,i,j,k))               &
               -c210*dx2*dy-c120*dx*dy2                            &
               -c201*dx2*dz-c102*dx*dz2                            &
               -c021*dy2*dz-c012*dy*dz2                            &
               -c110*dx*dy-c101*dx*dz                              &
               -c011*dy*dz                                         &
              )*ddx*ddy*ddz

       tmp10 = ( (c300*dlx+c210*dly+c201*dlz+c200)*dlx+c110*dly+c100 )*dlx &
              +( (c030*dly+c021*dlz+c120*dlx+c020)*dly+c011*dlz+c010 )*dly &
              +( (c003*dlz+c102*dlx+c012*dly+c002)*dlz+c101*dlx+c001 )*dlz &
              +c111*dlx*dly*dlz                                            &
              +f(ieq,i,j,k)

       tmp11 = 1./(1.0D0+ab100*dlx+ab010*dly+ab001*dlz)
       tmp12 = tmp10*tmp11

       gf(ieq,i,j,k) = tmp12*fac                                    &
                      +(f(ieq,i,j,k)+sx*dlx+sy*dly+sz*dlz)*(1.-fac) &
                      -(vayu-vay)

       gdxf(ieq,i,j,k) = tmp11*(                               &
             (3.*c300*dlx+2.*c210*dly+2.*c201*dlz+2.*c200)*dlx &
            +(c120*dly+c111*dlz+c110)*dly                      & 
            +(c102*dlz+c101)*dlz+c100                          &
            -ab100*tmp12 )*fac                                 &
            -(dxvayu-dxvay)!+sx*(1.-fac)

       gdyf(ieq,i,j,k) = tmp11*(                               &
             (3.*c030*dly+2.*c021*dlz+2.*c120*dlx+2.*c020)*dly &
            +(c210*dlx+c111*dlz+c110)*dlx                      &
            +(c012*dlz+c011)*dlz+c010                          &
            -ab010*tmp12 )*fac                                 &
            -(dyvayu-dyvay)!+sy*(1.-fac)

       gdzf(ieq,i,j,k) = tmp11*(                               &
             (3.*c003*dlz+2.*c102*dlx+2.*c012*dly+2.*c002)*dlz &
            +(c201*dlx+c111*dly+c101)*dlx                      &
            +(c021*dly+c011)*dly+c001                          &
            -ab001*tmp12 )*fac                                 &
            -(dzvayu-dzvay)!+sz*(1.-fac)


       ieq = 7

       fac = min(min(0.5*(1.0+tanh((0.5*(f(2,i,j,k)+f(2,i,j,kp))-pmin)*dp)), &
                     0.5*(1.0+tanh((0.5*(f(1,i,j,k)+f(1,i,j,kp))-nmin)*dn))), &
                 0.5*(1.0+tanh((0.5*(1.D0/(f(1,i,j,k)*f(1,i,j,k))*f(2,i,j,k)+1.D0/(f(1,i,j,kp)*f(1,i,j,kp))*f(2,i,j,kp))-nmin)*dn)) &
                )
!       fac = 0.5*(1.0+tanh((0.5*(f(2,i,j,k)+f(2,i,j,kp))-pmin)*dp))

       uvx = 0.125*(+f(3,im,j,k)+f(3,i,j,k)    &
                    +f(3,im,j,kp)+f(3,i,j,kp)  &
                    +f(4,im,j,k)+f(4,i,j,k)    &
                    +f(4,im,j,kp)+f(4,i,j,kp)) 
       uvy = 0.125*(+f(5,i,jm,k)+f(5,i,j,k)    &
                    +f(5,i,jm,kp)+f(5,i,j,kp)  &
                    +f(6,i,jm,k)+f(6,i,j,k)    &
                    +f(6,i,jm,kp)+f(6,i,j,kp))
       uvz = 0.5*(+f(7,i,j,k)+f(8,i,j,k))

       isn = -sign(1.D0,uvx)
       iuw = i+isn
       dlx = -delt*uvx*flg(ieq,i,j,k)

       jsn = -sign(1.D0,uvy)
       juw = j+jsn
       dly = -delt*uvy*flg(ieq,i,j,k)

       ksn = -sign(1.D0,uvz)
       kuw = k+ksn
       dlz = -delt*uvz*flg(ieq,i,j,k)

       !dipole field at upwind position
       x0 = (i+dlx-px0)*drs
       y0 = (j+dly-py0)*drs
       z0 = (k+0.5+dlz-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i+dlx-pxm)*drs
       ym = (j+dly-pym)*drs
       zm = (k+0.5+dlz-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)
                  
       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       vazu   = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       dxvazu = dxbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
               -0.5*vazu*dn0*dxn0(x0,r0)
       dyvazu = dybdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
               -0.5*vazu*dn0*dyn0(y0,r0)
       dzvazu = dzbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
               -0.5*vazu*dn0*dzn0(z0,r0)
       !end dipole 

       !dipole field at i,j,k+1/2
       x0 = (i-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k+0.5-pz0)*drs
       r0 = dsqrt(+x0*x0+y0*y0+z0*z0)
       dr05 = r0**(-5)

       xm = (i-pxm)*drs
       ym = (j-pym)*drs
       zm = (k+0.5-pzm)*drs
       rm = dsqrt(+xm*xm+ym*ym+zm*zm)
       drm5 = rm**(-5)

       dn0  = 1./n0(r0)
       dsn0 = dsqrt(dn0)
       vax   = bdx(x0,z0,xm,zm,dr05,drm5)*dsn0
       vay   = bdy(y0,z0,ym,zm,dr05,drm5)*dsn0
       vaz   = bdz(z0,r0,zm,rm,dr05,drm5)*dsn0
       dxvaz = dxbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dn0*dxn0(x0,r0)
       dyvaz = dybdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dn0*dyn0(y0,r0)
       dzvaz = dzbdz(x0,y0,z0,r0,xm,ym,zm,rm,dr05,drm5)*dsn0 &
              -0.5*vaz*dn0*dzn0(z0,r0)
       !end dipole 

       uvy = 0.25*(+f(6,i,jm,k)+f(6,i,j,k)    &
                   +f(6,i,jm,kp)+f(6,i,j,kp))-vay
       uvx = 0.25*(+f(4,im,j,k)+f(4,i,j,k)    &
                   +f(4,im,j,kp)+f(4,i,j,kp))-vax

       isn = -sign(1.D0,uvx)
       iuw = i+isn
       dlx = -delt*uvx*flg(ieq,i,j,k)

       jsn = -sign(1.D0,uvy)
       juw = j+jsn
       dly = -delt*uvy*flg(ieq,i,j,k)

       dx = dble(isn)
       ddx = 1./dx
       dx2 = dx*dx
       ddx2 = ddx*ddx
       dy = dble(jsn)
       ddy = 1./dy
       dy2 = dy*dy
       ddy2 = ddy*ddy
       dz = dble(ksn)
       ddz = 1./dz
       dz2 = dz*dz
       ddz2 = ddz*ddz
        
       sx = (f(ieq,iuw,j,k)-f(ieq,i,j,k))*ddx
       tmp4 = dxf(ieq,iuw,j,k)-sx 
       tmp4 = tmp4 + 1.D-18*sign(1.D0,tmp4)
       b100 = 0.5*(1.+dtanh((dabs(tmp4)-eps)/(0.05*eps))) &
             *(dabs((sx-dxf(ieq,i,j,k))/tmp4)-1.0)*ddx
       ab100 = a100*b100
       tmp7 = 1.0+ab100*dx
       c100 = dxf(ieq,i,j,k)+ab100*f(ieq,i,j,k)
       c300 = ( tmp7*tmp4+dxf(ieq,i,j,k)-sx )*ddx2
       c200 = ( tmp7*f(ieq,iuw,j,k)-f(ieq,i,j,k)-c100*dx )*ddx2-c300*dx
       
       sy = (f(ieq,i,juw,k)-f(ieq,i,j,k))*ddy
       tmp5 = dyf(ieq,i,juw,k)-sy
       tmp5 = tmp5 + 1.D-18*sign(1.D0,tmp5)
       b010 = 0.5*(1.+dtanh((dabs(tmp5)-eps)/(0.05*eps))) &
             *(dabs((sy-dyf(ieq,i,j,k))/tmp5)-1.0)*ddy
       ab010 = a010*b010
       tmp8 = 1.0+ab010*dy
       c010 = dyf(ieq,i,j,k)+ab010*f(ieq,i,j,k)
       c030 = ( tmp8*tmp5+dyf(ieq,i,j,k)-sy )*ddy2
       c020 = ( tmp8*f(ieq,i,juw,k)-f(ieq,i,j,k)-c010*dy )*ddy2-c030*dy

       sz = (f(ieq,i,j,kuw)-f(ieq,i,j,k))*ddz
       tmp6 = dzf(ieq,i,j,kuw)-sz
       tmp6 = tmp6 + 1.D-18*sign(1.D0,tmp6)
       b001 = 0.5*(1.+dtanh((dabs(tmp6)-eps)/(0.05*eps))) &
             *(dabs((sz-dzf(ieq,i,j,k))/tmp6)-1.0)*ddz
       ab001 = a001*b001
       tmp9 = 1.0+ab001*dz
       c001 = dzf(ieq,i,j,k)+ab001*f(ieq,i,j,k)
       c003 = ( tmp9*tmp6+dzf(ieq,i,j,k)-sz )*ddz2
       c002 = ( tmp9*f(ieq,i,j,kuw)-f(ieq,i,j,k)-c001*dz )*ddz2-c003*dz

       sumxy = +(c100+c200*dx+c300*dx2)*dx &
               +(c010+c020*dy+c030*dy2)*dy+f(ieq,i,j,k)
       sumxz = +(c100+c200*dx+c300*dx2)*dx &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)
       sumyz = +(c010+c020*dy+c030*dy2)*dy &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)
          
       c210 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab010*dy)*dx*dxf(ieq,i,juw,k)       &
               -ab100*dx*f(ieq,i,juw,k)+c100*dx         &
               -sumxy )*ddx2*ddy
       c120 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab100*dx)*dy*dyf(ieq,iuw,j,k)       &
               -ab010*dy*f(ieq,iuw,j,k)+c010*dy         &
               -sumxy )*ddx*ddy2
       c110 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -c210*dx2*dy-c120*dx*dy2                 &
               -sumxy )*ddx*ddy

       c201 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab001*dz)*dx*dxf(ieq,i,j,kuw)       &
               -ab100*dx*f(ieq,i,j,kuw)+c100*dx         &
               -sumxz )*ddx2*ddz
       c102 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab100*dx)*dz*dzf(ieq,iuw,j,k)       &
               -ab001*dz*f(ieq,iuw,j,k)+c001*dz         &
               -sumxz )*ddx*ddz2
       c101 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -c201*dx2*dz-c102*dx*dz2                 &
               -sumxz )*ddx*ddz

       c021 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab001*dz)*dy*dyf(ieq,i,j,kuw)       &
               -ab010*dy*f(ieq,i,j,kuw)+c010*dy         &
               -sumyz )*ddy2*ddz
       c012 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab010*dy)*dz*dzf(ieq,i,juw,k)       &
               -ab001*dz*f(ieq,i,juw,k)+c001*dz         &
               -sumyz )*ddy*ddz2
       c011 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -c021*dy2*dz-c012*dy*dz2                 &
               -sumyz )*ddy*ddz

       c111 = ( (1.+ab100*dx+ab010*dy+ab001*dz)*f(ieq,iuw,juw,kuw) &
               -0.5*(sumxy+sumxz+sumyz-f(ieq,i,j,k))               &
               -c210*dx2*dy-c120*dx*dy2                            &
               -c201*dx2*dz-c102*dx*dz2                            &
               -c021*dy2*dz-c012*dy*dz2                            &
               -c110*dx*dy-c101*dx*dz                              &
               -c011*dy*dz                                         &
               )*ddx*ddy*ddz

       tmp10 = ( (c300*dlx+c210*dly+c201*dlz+c200)*dlx+c110*dly+c100 )*dlx &
              +( (c030*dly+c021*dlz+c120*dlx+c020)*dly+c011*dlz+c010 )*dly &
              +( (c003*dlz+c102*dlx+c012*dly+c002)*dlz+c101*dlx+c001 )*dlz &
              +c111*dlx*dly*dlz                                            &
              +f(ieq,i,j,k)

       tmp11 = 1./(1.0D0+ab100*dlx+ab010*dly+ab001*dlz)
       tmp12 = tmp10*tmp11

       gf(ieq,i,j,k) = tmp12*fac                                    &
                      +(f(ieq,i,j,k)+sx*dlx+sy*dly+sz*dlz)*(1.-fac) &
                      +vazu-vaz

       gdxf(ieq,i,j,k) = tmp11*(                               &
             (3.*c300*dlx+2.*c210*dly+2.*c201*dlz+2.*c200)*dlx &
            +(c120*dly+c111*dlz+c110)*dly                      & 
            +(c102*dlz+c101)*dlz+c100                          &
            -ab100*tmp12 )*fac                                 &
            +dxvazu-dxvaz!+sx*(1.-fac)

       gdyf(ieq,i,j,k) = tmp11*(                               &
             (3.*c030*dly+2.*c021*dlz+2.*c120*dlx+2.*c020)*dly &
            +(c210*dlx+c111*dlz+c110)*dlx                      &
            +(c012*dlz+c011)*dlz+c010                          &
            -ab010*tmp12 )*fac                                 &
            +dyvazu-dyvaz!+sy*(1.-fac)

       gdzf(ieq,i,j,k) = tmp11*(                               &
             (3.*c003*dlz+2.*c102*dlx+2.*c012*dly+2.*c002)*dlz &
            +(c201*dlx+c111*dly+c101)*dlx                      &
            +(c021*dly+c011)*dly+c001                          &
            -ab001*tmp12 )*fac                                 &
            +dzvazu-dzvaz!+sz*(1.-fac)


       ieq = 8

       uvy = 0.25*(+f(5,i,jm,k)+f(5,i,j,k)    &
                   +f(5,i,jm,kp)+f(5,i,j,kp))+vay
       uvx = 0.25*(+f(3,im,j,k)+f(3,i,j,k)    &
                   +f(3,im,j,kp)+f(3,i,j,kp))+vax

       isn = -sign(1.D0,uvx)
       iuw = i+isn
       dlx = -delt*uvx*flg(ieq,i,j,k)
        
       jsn = -sign(1.D0,uvy)
       juw = j+jsn
       dly = -delt*uvy*flg(ieq,i,j,k)
        
       dx = dble(isn)
       ddx = 1./dx
       dx2 = dx*dx
       ddx2 = ddx*ddx
       dy = dble(jsn)
       ddy = 1./dy
       dy2 = dy*dy
       ddy2 = ddy*ddy
       dz = dble(ksn)
       ddz = 1./dz
       dz2 = dz*dz
       ddz2 = ddz*ddz
          
       sx = (f(ieq,iuw,j,k)-f(ieq,i,j,k))*ddx
       tmp4 = dxf(ieq,iuw,j,k)-sx 
       tmp4 = tmp4 + 1.D-18*sign(1.D0,tmp4)
       b100 = 0.5*(1.+dtanh((dabs(tmp4)-eps)/(0.05*eps))) &
             *(dabs((sx-dxf(ieq,i,j,k))/tmp4)-1.0)*ddx
       ab100 = a100*b100
       tmp7 = 1.0+ab100*dx
       c100 = dxf(ieq,i,j,k)+ab100*f(ieq,i,j,k)
       c300 = ( tmp7*tmp4+dxf(ieq,i,j,k)-sx )*ddx2
       c200 = ( tmp7*f(ieq,iuw,j,k)-f(ieq,i,j,k)-c100*dx )*ddx2-c300*dx
       
       sy = (f(ieq,i,juw,k)-f(ieq,i,j,k))*ddy
       tmp5 = dyf(ieq,i,juw,k)-sy
       tmp5 = tmp5 + 1.D-18*sign(1.D0,tmp5)
       b010 = 0.5*(1.+dtanh((dabs(tmp5)-eps)/(0.05*eps))) &
             *(dabs((sy-dyf(ieq,i,j,k))/tmp5)-1.0)*ddy
       ab010 = a010*b010
       tmp8 = 1.0+ab010*dy
       c010 = dyf(ieq,i,j,k)+ab010*f(ieq,i,j,k)
       c030 = ( tmp8*tmp5+dyf(ieq,i,j,k)-sy )*ddy2
       c020 = ( tmp8*f(ieq,i,juw,k)-f(ieq,i,j,k)-c010*dy )*ddy2-c030*dy

       sz = (f(ieq,i,j,kuw)-f(ieq,i,j,k))*ddz
       tmp6 = dzf(ieq,i,j,kuw)-sz
       tmp6 = tmp6 + 1.D-18*sign(1.D0,tmp6)
       b001 = 0.5*(1.+dtanh((dabs(tmp6)-eps)/(0.05*eps))) &
             *(dabs((sz-dzf(ieq,i,j,k))/tmp6)-1.0)*ddz
       ab001 = a001*b001
       tmp9 = 1.0+ab001*dz
       c001 = dzf(ieq,i,j,k)+ab001*f(ieq,i,j,k)
       c003 = ( tmp9*tmp6+dzf(ieq,i,j,k)-sz )*ddz2
       c002 = ( tmp9*f(ieq,i,j,kuw)-f(ieq,i,j,k)-c001*dz )*ddz2-c003*dz

       sumxy = +(c100+c200*dx+c300*dx2)*dx &
               +(c010+c020*dy+c030*dy2)*dy+f(ieq,i,j,k)
       sumxz = +(c100+c200*dx+c300*dx2)*dx &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)
       sumyz = +(c010+c020*dy+c030*dy2)*dy &
               +(c001+c002*dz+c003*dz2)*dz+f(ieq,i,j,k)

       c210 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab010*dy)*dx*dxf(ieq,i,juw,k)       &
               -ab100*dx*f(ieq,i,juw,k)+c100*dx         &
               -sumxy )*ddx2*ddy
       c120 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -(1.+ab100*dx)*dy*dyf(ieq,iuw,j,k)       &
               -ab010*dy*f(ieq,iuw,j,k)+c010*dy         &
               -sumxy )*ddx*ddy2

       c110 = ( (1.+ab100*dx+ab010*dy)*f(ieq,iuw,juw,k) &
               -c210*dx2*dy-c120*dx*dy2                 &
               -sumxy )*ddx*ddy
       c201 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab001*dz)*dx*dxf(ieq,i,j,kuw)       &
               -ab100*dx*f(ieq,i,j,kuw)+c100*dx         &
               -sumxz )*ddx2*ddz
       c102 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -(1.+ab100*dx)*dz*dzf(ieq,iuw,j,k)       &
               -ab001*dz*f(ieq,iuw,j,k)+c001*dz         &
               -sumxz )*ddx*ddz2
       c101 = ( (1.+ab100*dx+ab001*dz)*f(ieq,iuw,j,kuw) &
               -c201*dx2*dz-c102*dx*dz2                 &
               -sumxz )*ddx*ddz

       c021 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab001*dz)*dy*dyf(ieq,i,j,kuw)       &
               -ab010*dy*f(ieq,i,j,kuw)+c010*dy         &
               -sumyz )*ddy2*ddz
       c012 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -(1.+ab010*dy)*dz*dzf(ieq,i,juw,k)       &
               -ab001*dz*f(ieq,i,juw,k)+c001*dz         &
               -sumyz )*ddy*ddz2
       c011 = ( (1.+ab010*dy+ab001*dz)*f(ieq,i,juw,kuw) &
               -c021*dy2*dz-c012*dy*dz2                 &
               -sumyz )*ddy*ddz

       c111 = ( (1.+ab100*dx+ab010*dy+ab001*dz)*f(ieq,iuw,juw,kuw)       &
               -0.5*(sumxy+sumxz+sumyz-f(ieq,i,j,k))                     &
               -c210*dx2*dy-c120*dx*dy2                                  &
               -c201*dx2*dz-c102*dx*dz2                                  &
               -c021*dy2*dz-c012*dy*dz2                                  &
               -c110*dx*dy-c101*dx*dz                                    &
               -c011*dy*dz                                               &
               )*ddx*ddy*ddz

       tmp10 = ( (c300*dlx+c210*dly+c201*dlz+c200)*dlx+c110*dly+c100 )*dlx &
              +( (c030*dly+c021*dlz+c120*dlx+c020)*dly+c011*dlz+c010 )*dly &
              +( (c003*dlz+c102*dlx+c012*dly+c002)*dlz+c101*dlx+c001 )*dlz &
              +c111*dlx*dly*dlz                                            &
              +f(ieq,i,j,k)

       tmp11 = 1./(1.0D0+ab100*dlx+ab010*dly+ab001*dlz)
       tmp12 = tmp10*tmp11

       gf(ieq,i,j,k) = tmp12*fac                                    &
                      +(f(ieq,i,j,k)+sx*dlx+sy*dly+sz*dlz)*(1.-fac) &
                      -(vazu-vaz)

       gdxf(ieq,i,j,k) = tmp11*(                               &
             (3.*c300*dlx+2.*c210*dly+2.*c201*dlz+2.*c200)*dlx &
            +(c120*dly+c111*dlz+c110)*dly                      & 
            +(c102*dlz+c101)*dlz+c100                          &
            -ab100*tmp12 )*fac                                 &
            -(dxvazu-dxvaz)!+sx*(1.-fac)

       gdyf(ieq,i,j,k) = tmp11*(                               &
             (3.*c030*dly+2.*c021*dlz+2.*c120*dlx+2.*c020)*dly &
            +(c210*dlx+c111*dlz+c110)*dlx                      &
            +(c012*dlz+c011)*dlz+c010                          &
            -ab010*tmp12 )*fac                                 &
            -(dyvazu-dyvaz)!+sy*(1.-fac)

       gdzf(ieq,i,j,k) = tmp11*(                               &
             (3.*c003*dlz+2.*c102*dlx+2.*c012*dly+2.*c002)*dlz &
            +(c201*dlx+c111*dly+c101)*dlx                      &
            +(c021*dly+c011)*dly+c001                          &
            -ab001*tmp12 )*fac                                 &
            -(dzvazu-dzvaz)!+sz*(1.-fac)
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP END PARALLEL

  end subroutine advection__rcip


end module advection

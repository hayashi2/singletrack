# -*- coding: utf-8 -*-
import matplotlib 
matplotlib.use("Agg")
from pylab import *
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches
import scipy.optimize
import matplotlib.colors as col
import  matplotlib.cm as cm
import matplotlib.ticker as tic
from matplotlib.ticker import MaxNLocator


m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 10000
#files ="/work/m_hayashi/RBF6/ptl"
#outputpath= "/work/m_hayashi/output/flux/xy_600keV/"
#outputname= "RBF4dW"
#outputtitle= "RBF4_dW<0.6   "
iniene = 1
enest = iniene-1
#dx = 0.5
#dy = 0.5
#dA = dx*dy

dL = 0.5
dphi = float(1.0/180.0)*np.pi*15.
da = float(1.0/180.0)*np.pi
de = 0.25
dt = 6 
#grx = 2*int(15/dx)
#gry = 2*int(15/dx)
grL = int((10.-5.5)/dL)
grML = 2*int(np.pi/dphi)
gra = int(np.pi/da)
gre = int(10./de)+1
grt =180

la_fontsize=16
ti_fontsize=15

def main():
    
    data = np.load("/work/m_hayashi/Fluxdata/Data_RB005_AE8_ver2.npz")
    flux1= data["flux"]
    flux2 = np.sum(flux1[:,:,:,:,:],axis=1)/24 # integral for mlt
    flux1 = flux1[:,16,:,:,:] #MLT=16
    
    flux1 =flux1[3,:,:,:] #L=7    
    print "load"
    alp = np.empty((gra,gre,grt))
    for i in range(0,gre):
        for j in range(0,grt):
            alp[:,i,j] = 2*np.pi*np.sin(np.radians(np.arange(0,gra)))
    flux1 = np.sum(alp[80:150,:,:]*flux1[80:150,:,:],axis=0) #integral for a
    
    flux1 = flux1/(flux1[1,0]/7.687e6)
    fig = figure(1)    
    xx,yy = [],[]
    ax1 = fig.add_subplot(111)
    
    for i in xrange(0,np.shape(flux1)[0]-20):
        x = np.linspace(0*6,150*6,np.size(flux1[i,0:150]))
        ax1.plot(x,flux1[i,0:150], color=cm.jet(float(i)/(np.shape(flux1)[0]-20)),label=str(i*0.25))
        xx.append(x[np.argmax(flux1[i,0:150])])
        yy.append(np.max(flux1[i,0:150]))
    ax1.scatter(xx,yy,color="k")
    ax1.set_xlabel("time")
    ax1.set_ylabel("flux")
    ax1.set_yscale("log")
    #ax1.set_ylim(5e1,1e4)
    ax1.legend(loc="upper right",fontsize=6.5)
    fig.savefig("f.png")
    
    fig2 = figure(2,figsize=(10,2.5))
    fig2.subplots_adjust(bottom=0.27)
    ax2 = fig2.add_subplot(111)
    ay2 = ax2.imshow(flux2[0:5,90,:,0]/50,origin="lower",interpolation="none",\
                   aspect="auto",extent=[0,10,5.5,8.0],norm=LogNorm())
    CB = colorbar(ay2)
    CB.set_label("flux")
    ax2.set_xlabel("Energy [MeV]")
    ax2.set_ylabel("L")
    ax2.set_title(" t=0 [sec]")
    fig2.savefig("ini_diff_AE8.png")
    

main()

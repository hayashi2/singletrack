module crct

  implicit none

  private

  public :: crct__init, crct__exec

  integer, save      :: neq, nxgs, nxge, nygs, nyge, nzgs, nzge
  integer, save      :: nxs, nxe, nys, nye, nzs, nze
  integer, save      :: nxs1, nxe1, nys1, nye1, nzs1, nze1
  integer, save      :: nproc_i, nproc_j, nproc_k
  integer, save      :: ncomw, mproc_null, mnpr, opsum, opmax
  integer, save      :: iup, idown, jup, jdown, kup, kdown
  real(8), save      :: rin, drs, px0, py0, pz0, pxm, pym, pzm
  real(8), parameter :: divb_min=1d-4

contains


  subroutine crct__init(neqin,nxgsin,nxgein,nygsin,nygein,nzgsin,nzgein, &
                        nxsin,nxein,nysin,nyein,nzsin,nzein,             &
                        nxs1in,nxe1in,nys1in,nye1in,nzs1in,nze1in,       &
                        nproc_iin,nproc_jin,nproc_kin,                   &
                        ncomwin,mproc_nullin,mnprin,opsumin,opmaxin,     &
                        iupin,idownin,jupin,jdownin,kupin,kdownin,       &
                        rinin,drsin,px0in,py0in,pz0in,pxmin,pymin,pzmin)
    integer, intent(in) :: neqin, nxgsin, nxgein, nygsin, nygein, nzgsin, nzgein
    integer, intent(in) :: nxsin, nxein, nysin, nyein, nzsin, nzein
    integer, intent(in) :: nxs1in, nxe1in, nys1in, nye1in, nzs1in, nze1in
    integer, intent(in) :: nproc_iin, nproc_jin, nproc_kin
    integer, intent(in) :: ncomwin, mproc_nullin, mnprin, opsumin, opmaxin
    integer, intent(in) :: iupin, idownin, jupin, jdownin, kupin, kdownin
    real(8), intent(in) :: rinin, drsin, px0in, py0in, pz0in, pxmin, pymin, pzmin

    neq        = neqin
    nxgs       = nxgsin
    nxge       = nxgein
    nygs       = nygsin
    nyge       = nygein
    nzgs       = nzgsin
    nzge       = nzgein
    nxs        = nxsin
    nxe        = nxein
    nys        = nysin
    nye        = nyein
    nzs        = nzsin
    nze        = nzein
    nxs1       = nxs1in
    nxe1       = nxe1in
    nys1       = nys1in
    nye1       = nye1in
    nzs1       = nzs1in
    nze1       = nze1in
    nproc_i    = nproc_iin
    nproc_j    = nproc_jin
    nproc_k    = nproc_kin
    ncomw      = ncomwin
    mproc_null = mproc_nullin
    mnpr       = mnprin
    opsum      = opsumin
    opmax      = opmaxin
    iup        = iupin
    idown      = idownin
    jup        = jupin
    jdown      = jdownin
    kup        = kupin
    kdown      = kdownin
    rin        = rinin
    drs        = drsin
    px0        = px0in
    py0        = py0in
    pz0        = pz0in
    pxm        = pxmin
    pym        = pymin
    pzm        = pzmin

  end subroutine crct__init


  subroutine crct__exec(gf,f,dxf,dyf,dzf, &
                        nerr,nstat,       &
                        Db,bf,flg)

    use functions, only : bdx, bdy, bdz, n0
    use boundary, only : boundary__f, boundary__bf
    real(8), intent(inout) :: gf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: f(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: dxf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: dyf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(inout) :: dzf(neq,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    real(8), intent(out)   :: bf(3,nxs1:nxe1,nys1:nye1,nzs1:nze1)
    integer, intent(inout) :: nerr, nstat(:)
    real(8), intent(in)    :: flg(neq,nxs:nxe,nys:nye,nzs:nze)
    real(8), intent(in)    :: Db(nxs1:nxe1,nys1:nye1,nzs1:nze1)
    logical, save              :: lflag=.true.
    integer                    :: i, j, k, ieq, ip, jp, kp, im, jm, km
    real(8), allocatable, save :: x(:,:,:), r(:,:,:)
    real(8)                    :: x0, y0, z0, r0, dr05, xm, ym, zm, rm, drm5, dn0
    real(8)                    :: dn, dva, divb_max, divb_max_g

    if(lflag)then
       allocate( x(nxs1:nxe1,nys1:nye1,nzs1:nze1) )
       allocate( r(nxs1:nxe1,nys1:nye1,nzs1:nze1) )
       lflag = .false.
!       x = 0.0D0
    endif

!$OMP PARALLEL PRIVATE(i,j,k)

!$OMP DO
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       x(i,j,k) = 0.0D0
    enddo
    enddo
    enddo
!$OMP END DO

!$OMP DO
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       !Cs to N
!       f(1,i,j,k) = dlog(1.D0/(f(1,i,j,k)*f(1,i,j,k))*f(2,i,j,k))
       f(1,i,j,k) = 1.D0/(f(1,i,j,k)*f(1,i,j,k))*f(2,i,j,k)

       !z+- -> v, va
       f(3,i,j,k) = 0.5*(+f(3,i,j,k)+f(4,i,j,k))
       f(4,i,j,k) = +f(3,i,j,k)-f(4,i,j,k)
       f(5,i,j,k) = 0.5*(+f(5,i,j,k)+f(6,i,j,k)) 
       f(6,i,j,k) = +f(5,i,j,k)-f(6,i,j,k)
       f(7,i,j,k) = 0.5*(+f(7,i,j,k)+f(8,i,j,k))
       f(8,i,j,k) = +f(7,i,j,k)-f(8,i,j,k)
    enddo
    enddo
    enddo
!$OMP END DO

!$OMP DO PRIVATE(x0,xm,y0,ym,z0,zm,r0,rm,dr05,drm5,dn,dn0)
    do k=nzs,nze
    do j=nys,nye
    do i=nxs,nxe

       !at (i+1/2,j,k)
       x0 = (i+0.5-px0)*drs
       y0 = (j-py0)*drs
       z0 = (k-pz0)*drs
       r0 = dsqrt(+x0**2+y0**2+z0**2)
       dr05 = r0**(-5)

       xm = (i+0.5-pxm)*drs
       ym = (j-pym)*drs
       zm = (k-pzm)*drs
       rm = dsqrt(+xm**2+ym**2+zm**2)
       drm5 = rm**(-5)

       dn  = dsqrt(0.5*(+f(1,i,j,k)+f(1,i+1,j,k)))
       dn0 = dsqrt(0.5*(+Db(i,j,k)+Db(i+1,j,k)))
       bf(1,i,j,k) = f(4,i,j,k)*dn &
                    +(dn/dn0-1.)*bdx(x0,z0,xm,zm,dr05,drm5)

       !at (i,j+1/2,k)
       x0 = (i-px0)*drs
       y0 = (j+0.5-py0)*drs
       r0 = dsqrt(+x0**2+y0**2+z0**2)
       dr05 = r0**(-5)
       xm = (i-pxm)*drs
       ym = (j+0.5-pym)*drs
       rm = dsqrt(+xm**2+ym**2+zm**2)
       drm5 = rm**(-5)

       dn = dsqrt(0.5*(+f(1,i,j,k)+f(1,i,j+1,k)))
       dn0 = dsqrt(0.5*(+Db(i,j,k)+Db(i,j+1,k)))
       bf(2,i,j,k) = f(6,i,j,k)*dn &
                    +(dn/dn0-1.)*bdy(y0,z0,ym,zm,dr05,drm5)

       !at (i,j,k+1/2)
       y0 = (j-py0)*drs
       z0 = (k+0.5-pz0)*drs
       r0 = dsqrt(+x0**2+y0**2+z0**2)
       dr05 = r0**(-5)

       ym = (j-pym)*drs
       zm = (k+0.5-pzm)*drs
       rm = dsqrt(+xm**2+ym**2+zm**2)
       drm5 = rm**(-5)

       dn = dsqrt(0.5*(+f(1,i,j,k)+f(1,i,j,k+1)))
       dn0 = dsqrt(0.5*(+Db(i,j,k)+Db(i,j,k+1)))
       bf(3,i,j,k) = f(8,i,j,k)*dn &
                    +(dn/dn0-1.)*bdz(z0,r0,zm,rm,dr05,drm5)
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT
!$OMP END PARALLEL

    call boundary__bf(bf,3,nerr,nstat)

    !calc divB
    divb_max = 0.0
!$OMP PARALLEL
!$OMP DO PRIVATE(i,j,k,im,jm,km,ip,jp,kp) &
!$OMP REDUCTION(MAX:divb_max)
    do k=nzs,nze
       km = k-1
       kp = k+1
    do j=nys,nye
       jm = j-1
       jp = j+1
    do i=nxs,nxe
       im = i-1
       ip = i+1

       r(i,j,k) = (-bf(1,i-1,j,k)+bf(1,i,j,k) &
                   -bf(2,i,j-1,k)+bf(2,i,j,k) &
                   -bf(3,i,j,k-1)+bf(3,i,j,k) &
                  )*flg(1,i,j,k)

       !residual = div(B*)
       divb_max = max(divb_max,abs(r(i,j,k)))
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT
!$OMP END PARALLEL

    call MPI_ALLREDUCE(divb_max,divb_max_g,1,mnpr,opmax,ncomw,nerr)
    if(divb_max_g > divb_min) call rbssor(x,                             &
                                          nerr,nstat,                    &
                                          nxs,nxe,nys,nye,nzs,nze,       &
                                          nxs1,nxe1,nys1,nye1,nzs1,nze1, &
                                          r,divb_max_g)

!$OMP PARALLEL PRIVATE(i,j,k)

!$OMP DO 
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       !v, va -> z+-
       f(3,i,j,k) = f(3,i,j,k)+f(4,i,j,k)
       f(4,i,j,k) = f(3,i,j,k)-2.*f(4,i,j,k)
       f(5,i,j,k) = f(5,i,j,k)+f(6,i,j,k)
       f(6,i,j,k) = f(5,i,j,k)-2.*f(6,i,j,k)
       f(7,i,j,k) = f(7,i,j,k)+f(8,i,j,k)
       f(8,i,j,k) = f(7,i,j,k)-2.*f(8,i,j,k)
    enddo
    enddo
    enddo
!$OMP END DO

!$OMP DO PRIVATE(ip,jp,kp,dn,dva)
    do k=nzs,nze
       kp = k+1
    do j=nys,nye
       jp = j+1
    do i=nxs,nxe
       ip = i+1

       dn = 1./dsqrt(0.5*(+f(1,i,j,k)+f(1,ip,j,k)))
       dva = dn*(-x(i,j,k)+x(ip,j,k))*flg(3,i,j,k)
       gf(3,i,j,k) = f(3,i,j,k)-dva
       gf(4,i,j,k) = f(4,i,j,k)+dva

       dn = 1./dsqrt(0.5*(+f(1,i,j,k)+f(1,i,jp,k)))
       dva = dn*(-x(i,j,k)+x(i,jp,k))*flg(5,i,j,k)
       gf(5,i,j,k) = f(5,i,j,k)-dva
       gf(6,i,j,k) = f(6,i,j,k)+dva

       dn = 1./dsqrt(0.5*(+f(1,i,j,k)+f(1,i,j,kp)))
       dva = dn*(-x(i,j,k)+x(i,j,kp))*flg(7,i,j,k)
       gf(7,i,j,k) = f(7,i,j,k)-dva
       gf(8,i,j,k) = f(8,i,j,k)+dva
    enddo
    enddo
    enddo
!$OMP END DO

!$OMP DO 
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       !N => Cs
       f(1,i,j,k) = dsqrt(f(2,i,j,k)/f(1,i,j,k))
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP END PARALLEL

    call boundary__f(gf,neq,nerr,nstat)

!$OMP PARALLEL PRIVATE(i,j,k)

!$OMP DO PRIVATE(im,jm,km,ip,jp,kp)
    do k=nzs,nze
       km = k-1
       kp = k+1
    do j=nys,nye
       jm = j-1
       jp = j+1
    do i=nxs,nxe
       im = i-1
       ip = i+1
       do ieq=3,neq
          dzf(ieq,i,j,k) = dzf(ieq,i,j,k)        &
                          +0.5*(-gf(ieq,i,j,km)) &
                          -0.5*(- f(ieq,i,j,km)) 
          dyf(ieq,i,j,k) = dyf(ieq,i,j,k)        &
                          +0.5*(-gf(ieq,i,jm,k)) &
                          -0.5*(- f(ieq,i,jm,k))   
          dxf(ieq,i,j,k) = dxf(ieq,i,j,k)                       &
                          +0.5*(-gf(ieq,im,j,k)+gf(ieq,ip,j,k)) &
                          -0.5*(- f(ieq,im,j,k)+ f(ieq,ip,j,k)) 
          dyf(ieq,i,j,k) = dyf(ieq,i,j,k)        &
                          +0.5*(+gf(ieq,i,jp,k)) &
                          -0.5*(+ f(ieq,i,jp,k)) 
          dzf(ieq,i,j,k) = dzf(ieq,i,j,k)        &
                          +0.5*(+gf(ieq,i,j,kp)) &
                          -0.5*(+ f(ieq,i,j,kp))
       enddo
    enddo
    enddo
    enddo
!$OMP END DO

!$OMP DO
    do k=nzs1,nze1
    do j=nys1,nye1
    do i=nxs1,nxe1
       do ieq=3,neq
          f(ieq,i,j,k) = gf(ieq,i,j,k)
       enddo
    enddo
    enddo
    enddo
!$OMP END DO NOWAIT

!$OMP END PARALLEL

  end subroutine crct__exec


  subroutine rbssor(xsmth,                                     &
                    nerr,nstat,                                &
                    nxsmg,nxemg,nysmg,nyemg,nzsmg,nzemg,       &
                    nxs1mg,nxe1mg,nys1mg,nye1mg,nzs1mg,nze1mg, &
                    rsmth,divb_max_g0)

    !-----------------------------------------------------------------------
    !  #  Red-Black Symmetric SOR method 
    !-----------------------------------------------------------------------
    use boundary, only : boundary__phi

    real(8), intent(inout) :: xsmth(nxs1mg:nxe1mg,nys1mg:nye1mg,nzs1mg:nze1mg)
    integer, intent(inout) :: nerr, nstat(:)
    integer, intent(in)    :: nxsmg, nxemg, nysmg, nyemg, nzsmg, nzemg
    integer, intent(in)    :: nxs1mg, nxe1mg, nys1mg, nye1mg, nzs1mg, nze1mg
    real(8), intent(in)    :: rsmth(nxs1mg:nxe1mg,nys1mg:nye1mg,nzs1mg:nze1mg)
    real(8), intent(in)    :: divb_max_g0
    integer                :: i, j, k, ip, jp, kp, im, jm, km, ite
    real(8), parameter     :: fac=1.D0/6.D0, alpha=1.0D0
    real(8)                :: dx, divb_max, divb_max_g

    divb_max_g = divb_max_g0

    ite = 0
    do while(divb_max_g > divb_min)

       ite = ite+1

       divb_max = 0.0D0

       !Forward sweep for red
!$OMP PARALLEL DO PRIVATE(i,j,k,im,jm,km,ip,jp,kp,dx) &
!$OMP REDUCTION(MAX:divb_max)
       do k=nzsmg,nzemg
          km = k-1
          kp = k+1
       do j=nysmg,nyemg
          jm = j-1
          jp = j+1
       do i=nxsmg+mod(j+k,2),nxemg,2
          im = i-1
          ip = i+1

          dx = +rsmth(i,j,k)                                 &
               -(+xsmth(i,j,km)+xsmth(i,jm,k)+xsmth(im,j,k)  &
                 -6.*xsmth(i,j,k)                            &
                 +xsmth(ip,j,k)+xsmth(i,jp,k)+xsmth(i,j,kp) )

          xsmth(i,j,k) = +xsmth(i,j,k)-dx*fac*alpha
          divb_max = max(divb_max,abs(dx))
       enddo
       enddo
       enddo
!$OMP END PARALLEL DO

       call boundary__phi(xsmth,nerr,nstat)    

       !Forward sweep for black
!$OMP PARALLEL DO PRIVATE(i,j,k,im,jm,km,ip,jp,kp,dx) &
!$OMP REDUCTION(MAX:divb_max)
       do k=nzsmg,nzemg
          km = k-1
          kp = k+1
       do j=nysmg,nyemg
          jm = j-1
          jp = j+1
       do i=nxsmg+mod(1+j+k,2),nxemg,2
          im = i-1
          ip = i+1

          dx = +rsmth(i,j,k)                                &
               -(+xsmth(i,j,km)+xsmth(i,jm,k)+xsmth(im,j,k) &
                 -6.*xsmth(i,j,k)                           &
                 +xsmth(ip,j,k)+xsmth(i,jp,k)+xsmth(i,j,kp) )

          xsmth(i,j,k) = +xsmth(i,j,k)-dx*fac*alpha
          divb_max = max(divb_max,abs(dx))
       enddo
       enddo
       enddo
!$OMP END PARALLEL DO

       call boundary__phi(xsmth,nerr,nstat)            

       call MPI_ALLREDUCE(divb_max,divb_max_g,1,mnpr,opmax,ncomw,nerr)

       if(divb_max_g > divb_max_g0*10.0)then
          call MPI_ABORT(ncomw, 9, nerr)
          call MPI_FINALIZE(nerr)
       endif
    enddo

  end subroutine rbssor
  

end module crct 

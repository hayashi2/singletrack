# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 14:27:16 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches

if __name__=="__main__":
    n=498
    ion()
    figure(figsize=(9,13))
    wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='k')
    wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='w')    
    fig = gcf()
    fig.gca().add_artist(wedge1)
    fig.gca().add_artist(wedge2)    
    
    plot((-10,10), (0,0 ), color='gray',linestyle='--')
    plot((0,0),(-8,8),'gray',linestyle='--')
    
    xlim(-11,11)
    ylim(-11,11)
    setp(gca().get_xticklabels(), fontsize=20, visible=True)
    setp(gca().get_yticklabels(), fontsize=20, visible=True)
    ylabel("y[Re]",fontsize=25)
    xlabel("x[Re]",fontsize=25)
    title("time="+str(n*5+1)+"s")
    num=0
    while(num <= 127):
        filename= "/work/m_hayashi/RB000/ptl%03i.dat"%num
        
        head = ("head","<i")
        tail = ("tail","<i")
        rtime = ("rtime","<f8")
        nnp = ("np","<i4")
        maxptl =("maxptl","<i4")
        
        dt = np.dtype([head,rtime,nnp,maxptl,
                       ("xp","<128f8"),("yp","<128f8"),("zp","<128f8"),
                       ("lp","<128f8"),("xeq","<128f8"),("yeq","<128f8"),
                       ("zeq","<128f8"),("aeq","<128f8"),("beq","<128f8"),
                       ("ppara","<128f8"),("pperp","<128f8"),("mu","<128f8"),
                       ("bp","<128f8"),("prebp","<128f8"),("db","<128f8"),
                       ("predb","<128f8"),("nump","<128i4"),("ierr","<128i4"),tail])

        fd = open(filename,"r")
        chunk = np.fromfile(fd, dtype=dt,count=n+1)
       # print np.size(chunk)
        
       # print "rtime:",int(chunk[n]["rtime"])
       # print "np:",chunk[n]["np"]
       # print "maxptl",chunk[n]["maxptl"]
        
        xp = chunk[n]["xp"].reshape((1,128),order="F")
        yp = chunk[n]["yp"].reshape((1,128),order="F")
        zp = chunk[n]["zp"].reshape((1,128),order="F")
        lp = chunk[n]["lp"].reshape((1,128),order="F")
        xeq = chunk[n]["xeq"].reshape((1,128),order="F")
        yeq = chunk[n]["yeq"].reshape((1,128),order="F")
        zeq = chunk[n]["zeq"].reshape((1,128),order="F")
        aeq = chunk[n]["aeq"].reshape((1,128),order="F")
        beq = chunk[n]["beq"].reshape((1,128),order="F")
        ppara = chunk[n]["ppara"].reshape((1,128),order="F")
        pperp = chunk[n]["pperp"].reshape((1,128),order="F")
        mu = chunk[n]["mu"].reshape((1,128),order="F")
        bp = chunk[n]["bp"].reshape((1,128),order="F")
        prebp = chunk[n]["prebp"].reshape((1,128),order="F")
        db = chunk[n]["db"].reshape((1,128),order="F")
        predb = chunk[n]["predb"].reshape((1,128),order="F") 
        nump = chunk[n]["nump"].reshape((1,128),order="F")
        ierr = chunk[n]["ierr"].reshape((1,128),order="F")
    
    #print xp
        scatter (xp/6380000,yp/6380000,alpha=0.5,c="green",s=5,lw=0)
        num += 1
        draw()
        fd.close()

# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("Agg")
from pylab import *
import numpy as np
from spacepy import pycdf
import matplotlib.ticker as tic
import matplotlib.colors as col
import matplotlib.cm as cm
import sys
import subprocess
import os
from datetime import datetime,date,timedelta
from matplotlib import gridspec

dL = 1./5.
grL = int(np.round((11./dL)))

def main():
    data = np.loadtxt("/home/m_hayashi/data/Swdata_20170401_1231.txt",skiprows=1,dtype = float)
    Bz = data[:,3]
    Vsw = data[:,4]
    Pdyn = data [:,5]
    Bz[Bz==999.9] = np.nan
    Vsw[Vsw==9999.0] = np.nan
    Pdyn[Pdyn== 99.99] = np.nan
    print np.max(Vsw),np.max(Pdyn)
    ###### Figure Setting 1 ######
    fig = figure(1,figsize=(8,5))

    fig.subplots_adjust(hspace=0.1)
    #gs = gridspec.GridSpec(2,1,height_ratios=[5,2])
    #ax0 = fig.add_subplot(gs[0,0])
    #ax5 = fig.add_subplot(gs[1,0])
    
    ax0 = fig.add_subplot(311)
    ax1 = fig.add_subplot(312)
    ax2 = fig.add_subplot(313)
    ### Draw and Plot ###
    
    ax0.plot(Bz,"k")
    ax1.plot(Vsw,"k")
    ax2.plot(Pdyn,"k")
    ###### Figure setting 2 ######
    
    #ax0.set_title("HEP-H   1.0 - 2.0 MeV  "+s_date.strftime("%Y%m%d")+"-"+e_date.strftime("%Y%m%d"),fontsize=15)
    ax0.grid()
    ax1.grid()
    ax2.grid()
    ax0.set_ylabel("Bz_GSM [nT]")
    ax1.set_ylabel("Vsw [km/s]")
    ax2.set_ylabel("Pdyn [nPa]")
    ax0.set_yticks(np.linspace(-25,25,6))
    ax1.set_yticks(np.linspace(200,1000,5))
    ax0.set_xlim(0,np.size(Bz))
    ax1.set_xlim(0,np.size(Bz))
    ax2.set_xlim(0,np.size(Bz))
    
    setp(ax0.get_xticklabels(),visible=False)
    setp(ax1.get_xticklabels(),visible=False)
    
    ###### Save Figure ######
    fig.savefig("Sw_20170401_2131.png")
    close(fig)



def Read_Dst(s_date,e_date):
    filename = "/home/m_hayashi/ERG/Dst/Dst_"+str(s_date.year)+".txt"
    day_st = int(s_date.strftime("%j")) -1
    day_ed = int(s_date.strftime("%j")) -1
    delD  = int(((e_date - s_date).total_seconds())/60./60./24.)
    Dst = np.loadtxt(filename,skiprows = 2).astype(int)
    #dst = np.arange(delD*24 + 1)
    dst=[]
    dst.append(Dst[day_st-1,-1])
    for num in range(delD):
        #dst[delD*24+1:delD*24 +24] = Dst[day_st + num,2:]
        dst.extend(Dst[day_st + num,2:].tolist())
    print dst
    return dst


def daterange(s_date,e_date):
    return (s_date + timedelta(days=i) for i in range((e_date-s_date).days+1))

main()

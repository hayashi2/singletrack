# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 13:49:29 2015

@author: m_hayashi
"""

import numpy as np
from pylab import *
import matplotlib.patches as mpatches
m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 100
ppath = "/work/m_hayashi/RB018/"
fname = "ptl000"
t = 49
filename = ppath + fname + ".dat"

def main():
  
   x,y = readfile()
   print np.shape(x)
   for n in range(0,100):
      if(y[0,n] >=0 and int(x[0,n])==0):
         print "num=" +str(n),x[0,n]
def readfile():
        
   head = ("head","<i")
   tail = ("tail","<i")
   rtime = ("rtime","<f8")
   nnp = ("np","<i4")
   maxptl =("maxptl","<i4")
   xxp = ("xp","<"+str(ptl)+"f8")
   yyp = ("yp","<"+str(ptl)+"f8")
   zzp = ("zp","<"+str(ptl)+"f8")
   llp = ("lp","<"+str(ptl)+"f8")
   xxeq = ("xeq","<"+str(ptl)+"f8")
   yyeq = ("yeq","<"+str(ptl)+"f8")
   zzeq = ("zeq","<"+str(ptl)+"f8")
   aaeq = ("aeq","<"+str(ptl)+"f8")
   bbeq = ("beq","<"+str(ptl)+"f8")
   p_ppara = ("ppara","<"+str(ptl)+"f8")
   p_pperp = ("pperp","<"+str(ptl)+"f8")
   mmu = ("mu","<"+str(ptl)+"f8")
   bbp = ("bp","<"+str(ptl)+"f8")
   pprebp = ("prebp","<"+str(ptl)+"f8")
   ddb = ("db","<"+str(ptl)+"f8")
   ppredb = ("predb","<"+str(ptl)+"f8")
   nnump = ("nump","<"+str(ptl)+"i4")
   iierr = ("ierr","<"+str(ptl)+"i4")

   dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                  aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                  iierr,tail])
               
   fd = open(filename,"r")
   chunk = np.fromfile(fd, dtype=dt,count = t+1 )
   
   x1 = chunk[t]["xp"].reshape((1,ptl),order="F")
   y1 = chunk[t]["yp"].reshape((1,ptl),order="F")
   
 
   """
   zp = chunk[t]["zp"].reshape((1,ptl),order="F")
   lp = chunk[t]["lp"].reshape((1,ptl),order="F")
   xeq = chunk[t]["xeq"].reshape((1,ptl),order="F")
   yeq = chunk[t]["yeq"].reshape((1,ptl),order="F")
   zeq = chunk[t]["zeq"].reshape((1,ptl),order="F")
   aeq = chunk[t]["aeq"].reshape((1,ptl),order="F")
   beq = chunk[t]["beq"].reshape((1,ptl),order="F")
   ppara = chunk[t]["ppara"].reshape((1,ptl),order="F")
   pperp = chunk[t]["pperp"].reshape((1,ptl),order="F")
   mu = chunk[t]["mu"].reshape((1,ptl),order="F")
   bp = chunk[t]["bp"].reshape((1,ptl),order="F")
   prebp = chunk[t]["prebp"].reshape((1,ptl),order="F")
   db = chunk[t]["db"].reshape((1,ptl),order="F")
   predb = chunk[t]["predb"].reshape((1,ptl),order="F") 
   nump = chunk[t]["nump"].reshape((1,ptl),order="F")
   ierr = chunk[t]["ierr"].reshape((1,ptl),order="F")
   """
   fd.close()   
   return x1/6380000,y1/6380000

main()

# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:08:26 2015

@author: m_hayashi
"""
import numpy as np
from pylab import *
import matplotlib.patches as mpatches

patha = "/work/ymatumot/simulation/GM-RB"
pathb = "/run_Vsw600/"
bname = "MHD_B"
k = 1.38e-23
b0 = 10 
e0 = 2.2
v0 = 220 #(km/s)
p0 = 0.04 #(nPa)
n0 = 1

def main():
    for t in range(0,200):
        bfilename = patha + pathb + bname + str(t).zfill(3) + ".dat"
        print "read... B" +str(t)

        BX,BY,BZ= readB(bfilename)
        Z,X = np.mgrid[120/3.:-120.66666/3.:-1./3.,90./3.:-(297.-90.)/3:-1./3.]
        print BX.shape,X.shape,Z.shape
        c = np.sqrt((BX[120,:,:].T)**2 +(BZ[120,:,:].T)**2)
        clf()
        figure(figsize=(7,5))
        title("time= "+str(t*12)+" sec" )
        streamplot(X[75:165,45:150],Z[75:165,45:150],BX[120,45:150,75:165].T,BZ[120,45:150,75:165].T,linewidth=0.5,color="k",arrowsize=0.7,density=3)
        ax=gca()
        ax.invert_xaxis()
        ax.invert_yaxis()
        xlabel("x [Re]")
        ylabel("z [Re]")
        grid()
        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
        fig = gcf()
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)
        savefig("/work/m_hayashi/output/filine/2Dline"+str(t).zfill(3) +".jpeg")
        
 
def readB(bfilename):
    
    head = ("head","<i")
    tail = ("tail","<i")
    b = ("b","<51750171f4")
    
    dt = np.dtype([head,b,tail])
    fd = open(bfilename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=1)
    fd.close()
    
    bf = chunk[0]["b"].reshape((297,241,241,3),order="F")*b0
    bt = bf.transpose(1,0,2,3)
    bx = -bt[:,:,:,0]
    by = -bt[:,:,:,1]
    bz = bt[:,:,:,2]
        
    return bx,by,bz



def nx(x):
    return int(x*3 + 90)

def ny(y):
    return int(y*3 + 121)

main()

# -*- coding: utf-8 -*-
"""
Created on Sat Jan 23 17:46:25 2016

@author: Miyoshi
"""

import numpy as np
from pylab import *

path = "/work/m_hayashi/Xray/Vsw600/"
name = "MHDXray_"
time = 0
#time=18

# xray=np.empty([297,241,241])

image_view=np.empty([19,19])

data = np.load(path+name+str(time).zfill(3)+".npz")
xray = data["Xray"]

fp = open('/work/m_hayashi/xraytest.dat','w')
for i in range(0,297):
  for j in range(0,241):
    for k in range(0,241):
      xpoint=30.0-(i-1)*1./3.
      ypoint=40.0-(j-1)*1./3.
      zpoint=-40.0+(k-1)*1./3.
      fp.write("%5.2f\t%5.2f\t%5.2f\t%7.4e\n" % (xpoint,ypoint,zpoint,xray[i,j,k]))
     # print "%5.2f %5.2f %5.2f %7.4e" % (xpoint,ypoint,zpoint,xray[i,j,k])
fp.close()

# -*- coding: utf-8 -*-
from pylab import *
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches

m  = 9.1094e-31
e = 1.6e-19
c = 3.0e8 
ptl = 1000
enemin = 5
enest = enemin-1
files ="/work/m_hayashi/RBF2/ptl"
t1=0
dA = 0.2*0.2
da = float(1.0/360.0)*np.pi
de = 0.2e6
dt = 6

def main():
    w = weighting()

    
    count = np.zeros((150,150,80,200)) #x,y,ene,t
    #flux1 = np.zeros((150,150,80,200))
    flux = np.zeros((150,150,200))         # integral of a and ene

    # file loop 
    for filenum in range(0,128):
        filename = files + str(filenum).zfill(3) +".dat"

    #read file
        xp2,yp2,a2,mu2,ene2 ,frag2=  readfile(filename,199)
        x2,y2 = xp2/6370000,yp2/6370000
        L2 = np.sqrt((x2)**2 + (y2)**2)
    
    #grid number
        xg, yg, ag, eg = griding(x2,y2,a2,ene2)
        
        print "readfile..." ,filenum
    #ptl loop
        for t in range(0,200):
            for m in range(0,ptl):
    #count per bins
                if(xg[t,m] <150 and yg[t,m] <150 and frag2[t,m]==0):
                    count[xg[t,m],yg[t,m],eg[t,m],t] = count[xg[t,m],yg[t,m],eg[t,m],t] + w[filenum,m]/(dA*da*de*dt*(np.cos(a2[t,m]))*2*np.pi*(sin(a2[t,m])))
    # end file loop
               
    #integral of ene                       
    for j in range(0,80):
        flux +=  count[:,:,j,:]

    #show images
    for t in range(0,200):
        clf()
        imshow(np.absolute(flux[25:125,25:125,t].T),origin='lower',extent=[10,-10,10,-10],norm=LogNorm(vmin=1e0,vmax=1e2))
        #imshow(np.absolute(flux[25:125,25:125].T),origin='lower',extent=[10,-10,10,-10],norm=LogNorm(vmin=1e2,vmax=1e3))
        grid()
        title("time=" + str((t+1)*6)+"sec")
        xlabel("x(GSM)[Re]",fontsize=13)
        ylabel("y(GSM)[Re]",fontsize=13)
        CB=colorbar()   
        CB.set_label('flux [/Re$^2$ sec str MeV]',fontsize=15)
        CB.ax.tick_params(labelsize=13)
        wedge1 = mpatches.Wedge((0,0), 1, -90, 90,fc='w')
        wedge2 = mpatches.Wedge((0,0), 1, 90, -90,fc='k')    
        fig = gcf()
        fig.gca().add_artist(wedge1)
        fig.gca().add_artist(wedge2)    
        draw()
        savefig("/home/m_hayashi/output/flux/xy/5MeV"+str(t).zfill(3)+".jpeg")
        print("saved..."+str(t))

def readfile(filename,t):
    count=t+1
    head = ("head","<i")
    tail = ("tail","<i")
    rtime = ("rtime","<f8")
    nnp = ("np","<i4")
    maxptl =("maxptl","<i4")
    xxp = ("xp","<"+str(ptl)+"f8")
    yyp = ("yp","<"+str(ptl)+"f8")
    zzp = ("zp","<"+str(ptl)+"f8")
    llp = ("lp","<"+str(ptl)+"f8")
    xxeq = ("xeq","<"+str(ptl)+"f8")
    yyeq = ("yeq","<"+str(ptl)+"f8")
    zzeq = ("zeq","<"+str(ptl)+"f8")
    aaeq = ("aeq","<"+str(ptl)+"f8")
    bbeq = ("beq","<"+str(ptl)+"f8")
    p_ppara = ("ppara","<"+str(ptl)+"f8")
    p_pperp = ("pperp","<"+str(ptl)+"f8")
    mmu = ("mu","<"+str(ptl)+"f8")
    bbp = ("bp","<"+str(ptl)+"f8")
    pprebp = ("prebp","<"+str(ptl)+"f8")
    ddb = ("db","<"+str(ptl)+"f8")
    ppredb = ("predb","<"+str(ptl)+"f8")
    nnump = ("nump","<"+str(ptl)+"i4")
    iierr = ("ierr","<"+str(ptl)+"i4")
    
    dt = np.dtype([head,rtime,nnp,maxptl,xxp,yyp,zzp,llp,xxeq,yyeq,zzeq,
                   aaeq,bbeq,p_ppara,p_pperp,mmu, bbp,pprebp,ddb,ppredb,nnump,
                   iierr,tail])
    
    fd = open(filename,"r")
    chunk = np.fromfile(fd, dtype=dt,count=t+1)
   
    xp = chunk[:]["xp"].reshape((count,ptl),order="F")
    yp = chunk[:]["yp"].reshape((count,ptl),order="F")
    zp = chunk[:]["zp"].reshape((count,ptl),order="F")
    lp = chunk[:]["lp"].reshape((count,ptl),order="F")
    xeq = chunk[:]["xeq"].reshape((count,ptl),order="F")
    yeq = chunk[:]["yeq"].reshape((count,ptl),order="F")
    zeq = chunk[:]["zeq"].reshape((count,ptl),order="F")
    aeq = chunk[:]["aeq"].reshape((count,ptl),order="F")
    beq = chunk[:]["beq"].reshape((count,ptl),order="F")
    ppara = chunk[:]["ppara"].reshape((count,ptl),order="F")
    pperp = chunk[:]["pperp"].reshape((count,ptl),order="F")
    mu = chunk[:]["mu"].reshape((count,ptl),order="F")
    bp = chunk[:]["bp"].reshape((count,ptl),order="F")
    prebp = chunk[:]["prebp"].reshape((count,ptl),order="F")
    db = chunk[:]["db"].reshape((count,ptl),order="F")
    predb = chunk[:]["predb"].reshape((count,ptl),order="F") 
    nump = chunk[:]["nump"].reshape((count,ptl),order="F")
    ierr = chunk[:]["ierr"].reshape((count,ptl),order="F")
   
    fd.close()     
    g = np.sqrt(1 + (ppara**2 + pperp**2) / (m*c)**2)
    ene = (m * c**2 * (g - 1) / e)

    return xeq ,yeq,aeq,mu,ene,ierr

def weighting():
    count = np.zeros((150,150,80)) #x,y,ene,
    weight = np.empty((128,ptl))

    # file loop 
    for filenum in range(0,128):
        filename = files + str(filenum).zfill(3) +".dat"

    #read file
        xp1, yp1, a1, mu1, ene1, frag1 =  readfile(filename,0) 
        x1,y1 = xp1/6370000,yp1/6370000
        L1 = np.sqrt((x1)**2 + (y1)**2)

    #grid number
        xg, yg, ag, eg = griding(x1,y1,a1,ene1)
   
    #ptl loop
        for m in range(0,ptl):
    #count per bins
                count[xg[0,m],yg[0,m],eg[0,m]] +=1 

    
    for filenum2 in range(0,128):
        print "weighting..."
        filename2 = files + str(filenum2).zfill(3) +".dat"
        xp2, yp2, a2, mu2, ene2, frag2 =  readfile(filename2,0) 
        x2,y2 = xp2/6370000,yp2/6370000
        L2 = np.sqrt((x2)**2 + (y2)**2)
        #grid number
        xg2, yg2, ag2, eg2 = griding(x2,y2,a2,ene2)
        
        for m in range(0,ptl):
            if(count[xg2[0,m],yg2[0,m],eg2[0,m]]!=0):
                weight[filenum2,m] = L2[0,m]*np.abs(np.sin(a2[0,m]))*(ene2/1e6)[0,m]/count[xg2[0,m],yg2[0,m],eg2[0,m]]
    return weight

def griding(x,y,a,ene):
    xg = np.around(5*x).astype(int) + 75
    yg = np.around(5*y).astype(int) + 75
    ag = np.around(((180/np.pi)*a)).astype(int)
    eg = np.around(5*(ene/1e6 - enest)).astype(int)
    
    return xg,yg,ag,eg

main()
